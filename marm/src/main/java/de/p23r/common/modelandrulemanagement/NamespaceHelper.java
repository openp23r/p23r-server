package de.p23r.common.modelandrulemanagement;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.Charset;

import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class NamespaceHelper.
 *
 * Helper Class, Provides namespace information for XML Recources.
 *
 * @author sro
 *
 */
public class NamespaceHelper {

	private final Logger log = LoggerFactory.getLogger(NamespaceHelper.class);

	/**
	 * Returns the namespace of given xsd content.
	 *
	 * @param xsdContent
	 *            the xsd content
	 * @return namespace or null if something went wrong
	 */
	public String getNamespaceFromXsdContent(String xsdContent) {
		Document schema = getDocument(xsdContent);
		return schema != null ? schema.getRootElement().getAttributeValue("targetNamespace") : null;
	}

	/**
	 * Returns the namespace from given xml content.
	 *
	 * @param xmlContent
	 *            the xml content
	 * @return the namespace or null if something went wrong
	 */
	public String getNamespaceFromXmlContent(String xmlContent) {
		Document schema = getDocument(xmlContent);
		return schema != null ? schema.getRootElement().getNamespaceURI() : null;
	}

	/**
	 * Gets the namespace from x query content.
	 *
	 * @param xQueryContent the x query content
	 * @return the namespace from x query content
	 */
	public String getNamespaceFromXQueryContent(String xQueryContent) {
		String result = "";

		int begin = xQueryContent.indexOf("declare default element namespace \"");

		if (begin >= 0) {
			result = xQueryContent.substring(begin + 35);
			result = result.substring(0, result.indexOf("\""));
		}
		log.debug("found Namespace: " + result);
		return result;
	}
	
	private Document getDocument(String content) {
		SAXBuilder builder = new SAXBuilder();
		try {
			return builder.build(new ByteArrayInputStream(content.getBytes(Charset.forName("UTF-8"))));
		} catch (JDOMException e) {
			log.error("getting namespace from xml content", e);
		} catch (IOException e) {
			log.error("getting namespace from xml content", e);
		}
		return null;		
	}
}
