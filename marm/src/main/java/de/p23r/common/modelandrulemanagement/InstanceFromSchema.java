package de.p23r.common.modelandrulemanagement;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.xml.namespace.QName;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.stream.StreamResult;

import jlibs.xml.sax.XMLDocument;
import jlibs.xml.xsd.XSInstance;
import jlibs.xml.xsd.XSParser;

import org.apache.xerces.dom.DOMInputImpl;
import org.apache.xerces.impl.xs.XSLoaderImpl;
import org.apache.xerces.impl.xs.util.LSInputListImpl;
import org.apache.xerces.xs.XSModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The class InstanceFromSchema. Provides methods for generating xml files from
 * schemata.
 * 
 * @author bfu
 * 
 */
public class InstanceFromSchema {

	private final Logger log = LoggerFactory.getLogger(getClass());
	
	/**
	 * Reads out a file and returns the content as string.
	 *
	 * @param path            the file's path
	 * @param encoding            the file's encoding
	 * @return content as string
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static String readFile(String path, Charset encoding) throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return encoding.decode(ByteBuffer.wrap(encoded)).toString();
	}

	/**
	 * Parse the xsd as file and returns the associated model.
	 * 
	 * @param uri
	 *            the file's uri
	 * @return model
	 */
	public XSModel loadURI(String uri) {
		return new XSParser().parse(uri);
	}

	/**
	 * Parse the xsd as string and returns the associated model.
	 * 
	 * @param xsd
	 *            the schema as string
	 * @return model
	 */
	public XSModel loadString(String xsd) {
		return new XSLoaderImpl().load(new DOMInputImpl(null, null, null, xsd, "UTF-8"));
	}

	/**
	 * Load strings.
	 *
	 * @param xsds the xsds
	 * @return the XS model
	 */
	public XSModel loadStrings(String ...xsds) {
		DOMInputImpl[] inputs = new DOMInputImpl[xsds.length];
		for (int i = 0; i < xsds.length; ++i) {
			inputs[i] = new DOMInputImpl(null, null, null, xsds[i], "UTF-8");			
		}
		return new XSParser().parse(new LSInputListImpl(inputs, inputs.length));
	}
	
	/**
	 * Load streams.
	 *
	 * @param xsds the xsds
	 * @return the XS model
	 */
	public XSModel loadStreams(InputStream ...xsds) {
		DOMInputImpl[] inputs = new DOMInputImpl[xsds.length];
		for (int i = 0; i < xsds.length; ++i) {
			inputs[i] = new DOMInputImpl(null, null, null, xsds[i], "UTF-8");			
		}
		return new XSParser().parse(new LSInputListImpl(inputs, inputs.length));
	}
	
	/**
	 * Generate xml.
	 *
	 * @param root the root
	 * @param namespace the namespace
	 * @param schemas the schemas
	 * @return the string
	 */
	public String generateXML(String root, String namespace, InputStream ...schemas) {
		XSModel model = loadStreams(schemas);
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		try {
			generateXML(model, output, root, namespace);
		} catch (TransformerConfigurationException e) {
			log.error("generating xml from schema", e);
		}
		return output.toString();
	}
	
	/**
	 * Generates an xml for the given schema model and writes the content to the
	 * output stream.
	 *
	 * @param model            the xsd model
	 * @param out            the output stream
	 * @param root the root
	 * @param namespace the namespace
	 * @throws TransformerConfigurationException the transformer configuration exception
	 */
	public void generateXML(XSModel model, OutputStream out, String root, String namespace) throws TransformerConfigurationException {
		XSInstance instance = new XSInstance();
		// set min and max number of generated elements
		instance.minimumElementsGenerated = 1;
		instance.maximumElementsGenerated = 100;
		// don't generate optional elements and attributes. null means random
		instance.generateOptionalElements = Boolean.FALSE; 
		instance.generateOptionalAttributes = Boolean.FALSE;
		// don't generate model comment
		instance.showContentModel = Boolean.FALSE;
		instance.sampleValueGenerator = new SampleDefaultValueGenerator();

		QName rootElement = new QName(namespace, root);
		XMLDocument xml = new XMLDocument(new StreamResult(out), true, 4, null);
	    // generate xml
		instance.generate(model, rootElement, xml);
	}

}