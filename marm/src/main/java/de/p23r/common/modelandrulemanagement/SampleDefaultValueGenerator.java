package de.p23r.common.modelandrulemanagement;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.DatatypeConverter;

import jlibs.core.io.IOUtil;
import jlibs.core.util.RandomUtil;
import jlibs.xml.Namespaces;
import jlibs.xml.xsd.XSInstance.SampleValueGenerator;

import org.apache.xerces.xs.XSAttributeDeclaration;
import org.apache.xerces.xs.XSElementDeclaration;
import org.apache.xerces.xs.XSFacet;
import org.apache.xerces.xs.XSObjectList;
import org.apache.xerces.xs.XSSimpleTypeDefinition;
import org.apache.xerces.xs.XSValue;

/**
 * The Class SampleDefaultValueGenerator.
 */
public class SampleDefaultValueGenerator implements SampleValueGenerator {

    private static final String XSD_DATE_FORMAT = "yyyy-MM-dd";
    private static final String XSD_TIME_FORMAT = "HH:mm:ss";

    private Map<String, Integer> counters = new HashMap<String, Integer>();

    /*
     * (non-Javadoc)
     * 
     * @see jlibs.xml.xsd.XSInstance.SampleValueGenerator#generateSampleValue(org .apache.xerces.xs.XSElementDeclaration,
     * org.apache.xerces.xs.XSSimpleTypeDefinition)
     */
    @Override
    public String generateSampleValue(XSElementDeclaration element, XSSimpleTypeDefinition simpleType) {
        return generateSampleValue(element.getValueConstraintValue(), simpleType, element.getName());
    }

    /*
     * (non-Javadoc)
     * 
     * @see jlibs.xml.xsd.XSInstance.SampleValueGenerator#generateSampleValue(org .apache.xerces.xs.XSAttributeDeclaration,
     * org.apache.xerces.xs.XSSimpleTypeDefinition)
     */
    @Override
    public String generateSampleValue(XSAttributeDeclaration attribute, XSSimpleTypeDefinition simpleType) {
        return generateSampleValue(attribute.getValueConstraintValue(), simpleType, attribute.getName());
    }

    private String generateSampleValue(XSValue defaultValue, XSSimpleTypeDefinition simpleType, String name) {
        if (defaultValue != null) {
            return defaultValue.getNormalizedValue();
        }
        return generateRandomValue(simpleType, name);
    }

    private String generateRandomValue(XSSimpleTypeDefinition simpleType, String hint) {
        XSSimpleTypeDefinition builtInType = simpleType;
        while (!Namespaces.URI_XSD.equals(builtInType.getNamespace())) {
            builtInType = (XSSimpleTypeDefinition) builtInType.getBaseType();
        }

        String name = builtInType.getName().toLowerCase();
        String value = null;
        if ("boolean".equals(name)) {
            value = RandomUtil.randomBoolean() ? "true" : "false";
        } else if ("double".equals(name) || "decimal".equals(name) || "float".equals(name) || name.endsWith("integer") || name.endsWith("int") || name.endsWith("long")
                || name.endsWith("short") || name.endsWith("byte")) {
            value = new Range(simpleType).randomNumber();
        } else if ("date".equals(name)) {
            value = new SimpleDateFormat(XSD_DATE_FORMAT).format(new Date());
        } else if ("time".equals(name)) {
            value = new SimpleDateFormat(XSD_TIME_FORMAT).format(new Date());
        } else if ("datetime".equals(name)) {
            Date date = new Date();
            value = new SimpleDateFormat(XSD_DATE_FORMAT).format(date) + 'T' + new SimpleDateFormat(XSD_TIME_FORMAT).format(date);
        } else {
            Integer count = counters.get(hint);
            count = count == null ? 1 : ++count;
            counters.put(hint, count);
            String countStr = count.toString();

            XSFacet lengthFacet = getFacet(simpleType, XSSimpleTypeDefinition.FACET_LENGTH);

            XSFacet facet = getFacet(simpleType, XSSimpleTypeDefinition.FACET_MINLENGTH);
            if (facet == null) {
                facet = lengthFacet;
            }
            if (facet != null) {
                int len = Integer.parseInt(facet.getLexicalFacetValue());
                len -= hint.length();
                len -= countStr.length();
                if (len > 0) {
                    char ch[] = new char[len];
                    Arrays.fill(ch, '_');
                    hint += new String(ch);
                }
            }
            facet = getFacet(simpleType, XSSimpleTypeDefinition.FACET_MAXLENGTH);
            if (facet == null) {
                facet = lengthFacet;
            }
            if (facet != null) {
                int maxLen = Integer.parseInt(facet.getLexicalFacetValue());
                int len = maxLen;
                len = hint.length() + countStr.length() - len;
                if (len > 0) {
                    if (hint.length() > len) {
                        hint = hint.substring(0, hint.length() - len);
                    } else {
                        hint = hint.substring(0, maxLen);
                        countStr = "";
                    }
                }
            }
            value = hint + countStr;

            if ("base64binary".equals(name)) {
                value = DatatypeConverter.printBase64Binary(value.getBytes(IOUtil.UTF_8));
            }
        }
        return value;
    }

    private XSFacet getFacet(XSSimpleTypeDefinition simpleType, int kind) {
        XSObjectList facets = simpleType.getFacets();
        for (int i = 0; i < facets.getLength(); i++) {
            XSFacet facet = (XSFacet) facets.item(i);
            if (facet.getFacetKind() == kind) {
                return facet;
            }
        }
        return null;
    }

    private class Range {
        String minInclusive;
        String minExclusive;
        String maxInclusive;
        String maxExclusive;
        int totalDigits = -1;
        int fractionDigits = -1;

        Range(XSSimpleTypeDefinition simpleType) {
            XSFacet facet = getFacet(simpleType, XSSimpleTypeDefinition.FACET_MININCLUSIVE);
            if (facet != null) {
                minInclusive = facet.getLexicalFacetValue();
            }
            facet = getFacet(simpleType, XSSimpleTypeDefinition.FACET_MINEXCLUSIVE);
            if (facet != null) {
                minExclusive = facet.getLexicalFacetValue();
            }
            facet = getFacet(simpleType, XSSimpleTypeDefinition.FACET_MAXINCLUSIVE);
            if (facet != null) {
                maxInclusive = facet.getLexicalFacetValue();
            }
            facet = getFacet(simpleType, XSSimpleTypeDefinition.FACET_MAXEXCLUSIVE);
            if (facet != null) {
                maxExclusive = facet.getLexicalFacetValue();
            }

            facet = getFacet(simpleType, XSSimpleTypeDefinition.FACET_TOTALDIGITS);
            if (facet != null) {
                totalDigits = Integer.parseInt(facet.getLexicalFacetValue());
            }

            facet = getFacet(simpleType, XSSimpleTypeDefinition.FACET_FRACTIONDIGITS);
            if (facet != null) {
                fractionDigits = Integer.parseInt(facet.getLexicalFacetValue());
            }
        }

        private String applyDigits(Object obj) {
            String str = applyExponent(String.valueOf(obj));
            String number, fraction;
            int dot = str.indexOf(".");
            if (dot == -1) {
                number = str;
                fraction = "";
            } else {
                number = str.substring(0, dot);
                fraction = str.substring(dot + 1);
            }
            boolean negative = false;
            if (number.startsWith("-")) {
                negative = true;
                number = number.substring(1);
            }
            if (totalDigits >= 0) {
                if (number.length() > totalDigits) {
                    number = number.substring(0, totalDigits);
                }
            }
            if (fractionDigits >= 0) {
                if (fraction.length() > fractionDigits) {
                    fraction = fraction.substring(0, fractionDigits);
                }
            }

            str = negative ? "-" : "";
            str += number;
            if (fraction.length() > 0) {
                str += '.' + fraction;
            }
            return str;
        }

        private String applyExponent(String str) {
            int index = str.indexOf('E');
            if (index == -1) {
                return str;
            }

            int exponent = Integer.parseInt(str.substring(index + (str.charAt(index + 1) == '+' ? 2 : 1)));
            str = str.substring(0, index);

            boolean negative = false;
            if (str.charAt(0) == '-') {
                negative = true;
                str = str.substring(1);
            }

            if (exponent != 0) {
                int dot = str.indexOf('.');
                String beforeDot, afterDot;
                if (dot == -1) {
                    beforeDot = str;
                    afterDot = "";
                } else {
                    beforeDot = str.substring(0, dot);
                    afterDot = str.substring(dot + 1);
                }

                if (exponent < 0) {
                    while (exponent != 0) {
                        if (beforeDot.length() == 1)
                            beforeDot = "0" + beforeDot;
                        afterDot = beforeDot.substring(beforeDot.length() - 1) + afterDot;
                        beforeDot = beforeDot.substring(0, beforeDot.length() - 1);
                        exponent++;
                    }
                } else {
                    while (exponent != 0) {
                        if (afterDot.isEmpty()) {
                            afterDot = "0";
                        }
                        beforeDot += afterDot.substring(0, 1);
                        afterDot = afterDot.substring(1);
                        exponent--;
                    }
                }
                str = afterDot.isEmpty() ? beforeDot : beforeDot + "." + afterDot;
            }
            if (negative) {
                str = "-" + str;
            }

            return str;
        }

        public String randomNumber() {
            if (fractionDigits == 0) {
                // NOTE: min/max facets can have fractional part
                // even though fractionDigits is zero
                long min = Long.MIN_VALUE;
                if (minInclusive != null) {
                    min = (long) Double.parseDouble(minInclusive);
                }
                if (minExclusive != null) {
                    min = (long) Double.parseDouble(minExclusive) + 1;
                }

                long max = Long.MAX_VALUE;
                if (maxInclusive != null) {
                    max = (long) Double.parseDouble(maxInclusive);
                }
                if (maxExclusive != null) {
                    max = (long) Double.parseDouble(maxExclusive) - 1;
                }

                return applyDigits(RandomUtil.random(min, max));
            } else {
                double min = Double.MIN_VALUE;
                if (minInclusive != null) {
                    min = Double.parseDouble(minInclusive);
                }
                if (minExclusive != null) {
                    min = Double.parseDouble(minExclusive) + 1;
                }
                double max = Double.MAX_VALUE;
                if (maxInclusive != null) {
                    max = Double.parseDouble(maxInclusive);
                }
                if (maxExclusive != null) {
                    max = Double.parseDouble(maxExclusive) - 1;
                }

                return applyDigits(RandomUtil.random(min, max));
            }
        }
    }
}
