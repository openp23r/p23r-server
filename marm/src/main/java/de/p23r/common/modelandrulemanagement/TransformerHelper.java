package de.p23r.common.modelandrulemanagement;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.charset.Charset;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * The Class TransformerHelper.
 *
 * @author sim
 */
public class TransformerHelper {

	private final static Logger log = LoggerFactory.getLogger(TransformerHelper.class);
	
	/**
	 * Document to string.
	 *
	 * @param document the document
	 * @return the string
	 */
	public final static String documentToString(Document document) {
		return nodeToString(document, true);
	}
	
	/**
	 * Element to string.
	 *
	 * @param element the element
	 * @return the string
	 */
	public final static String elementToString(Element element) {
		return nodeToString(element, true);
	}
	
	/**
	 * Element to string.
	 *
	 * @param node the node
	 * @param format the format
	 * @return the string
	 */
	public final static String nodeToString(Node node, boolean format) {
		ByteArrayOutputStream writer = new ByteArrayOutputStream();
		
		TransformerFactory factory = TransformerFactory.newInstance();
		try {
			Transformer transformer = factory.newTransformer();
			if (format) {
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");				
			}
	        transformer.transform(new DOMSource(node), new StreamResult(writer));
		} catch (TransformerConfigurationException e) {
			log.error("transforming element to string", e);
		} catch (TransformerException e) {
			log.error("transforming element to string", e);
		}
		return writer.toString();		
	}
	
	/**
	 * Document from string.
	 *
	 * @param xml the xml
	 * @return the document
	 */
	public final static Document documentFromString(String xml) {
		return (Document)nodeFromSource(new StreamSource(new ByteArrayInputStream(xml.getBytes(Charset.forName("UTF-8")))));
	}	

	/**
	 * Element from string.
	 *
	 * @param xml the xml string
	 * @return the element
	 */
	public final static Element elementFromString(String xml) {
		Document doc = (Document)nodeFromSource(new StreamSource(new ByteArrayInputStream(xml.getBytes(Charset.forName("UTF-8")))));
		return doc.getDocumentElement();
	}	

	/**
	 * Element from stream.
	 *
	 * @param stream the stream
	 * @return the element
	 */
	public final static Element elementFromStream(InputStream stream) {
		Document doc = (Document)nodeFromSource(new StreamSource(stream));
		return doc.getDocumentElement();
	}	
	
	/**
	 * Element from source.
	 *
	 * @param source the source
	 * @return the element
	 */
	public final static Node nodeFromSource(Source source) {
		TransformerFactory factory = TransformerFactory.newInstance();
		DOMResult result = new DOMResult();
		try {
			Transformer transformer = factory.newTransformer();
	        transformer.transform(source, result);
		} catch (TransformerConfigurationException e) {
			log.error("transforming source to elment", e);
			return null;
		} catch (TransformerException e) {
			log.error("transforming source to elment", e);
			return null;
		}
		return result.getNode();		
	}
	
	/**
	 * String from source.
	 *
	 * @param source the source
	 * @return the string
	 */
	public final static String stringFromSource(Source source) {
		TransformerFactory factory = TransformerFactory.newInstance();
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		StreamResult result = new StreamResult(stream);
		try {
			Transformer transformer = factory.newTransformer();
	        transformer.transform(source, result);
		} catch (TransformerConfigurationException e) {
			log.error("transforming source to elment", e);
			return null;
		} catch (TransformerException e) {
			log.error("transforming source to elment", e);
			return null;
		}
		return stream.toString();		
	}
	
	/**
	 * Clean namespaces.
	 *
	 * @param xml the xml
	 * @return the node
	 */
	public final static Node cleanNamespaces(String xml) {
		InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream("cleannamespaces.xslt");
		TransformerFactory factory = TransformerFactory.newInstance();
		DOMResult result = new DOMResult();
		try {
			Transformer transformer = factory.newTransformer(new StreamSource(stream));
	        transformer.transform(new StreamSource(new ByteArrayInputStream(xml.getBytes(Charset.forName("UTF-8")))), result);
		} catch (TransformerConfigurationException e) {
			log.error("transforming source to elment", e);
			return null;
		} catch (TransformerException e) {
			log.error("transforming source to elment", e);
			return null;
		}
		return result.getNode();		
	}
		
}
