package de.p23r.common.modelandrulemanagement.archive;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.p23r.common.modelandrulemanagement.NamespaceHelper;
import de.p23r.common.modelandrulemanagement.SchemaHelperFactory;
import de.p23r.leitstelle.ns.p23r.nrl.modelmanifest1_1.ModelManifest;
import de.p23r.leitstelle.ns.p23r.nrl.modelpackagemanifest1_1.ModelPackageManifest;

/**
 * The Class ModelPackageArchive provides access to internal ModelPackage Resources and Artefacts.
 * It also provides Helpers for converting the Archive Structure in a appropriate entity model for
 * persisting purposes.
 *
 * @author sro
 */
public class ModelPackageArchive extends DefaultArchive {


	/**
	 * Instantiates a new model package archive.
	 */
	public ModelPackageArchive() {
	}

	/**
	 * Instantiates a new model package archive.
	 *
	 * @param zipContent
	 *            the zip content
	 */
	public ModelPackageArchive(InputStream zipContent) {
		super(zipContent);
	}

	/**
	 * Gets the internal archive path to the model package.
	 *
	 * @return the model package path
	 */
	public String getModelPackagePath(){
		return getPackageRootPath();
	}


	/**
	 * Gets the model package manifest.
	 *
	 * @return the model package manifest
	 */
	public String getModelPackageManifest() {
		return getPackageManifest();
	}

	/**
	 * Gets the model package manifest helper (JAXB Class).
	 *
	 * @return the model package manifest helper
	 */
	public ModelPackageManifest getModelPackageManifestHelper() {
		return SchemaHelperFactory.getModelPackageManifestSchemaHelper().create(getModelPackageManifest());
	}

	/**
	 * Gets a List of model pathes.
	 *
	 * @return the model pathes
	 */
	public List<String> getModelPathes(){
		List<String> result = new ArrayList<String>();

		for (String modelPath : getContent(getModelPackagePath() + "[^/]+/").keySet()){
			if (!modelPath.endsWith("/sources/")) {
				result.add(modelPath);
			}
		}

		return result;
	}

	/**
	 * Gets the pivot data model schemas.
	 *
	 * @return the data model schemas in Map:
	 * String Key = namespace, String value = schema content
	 */
	public Map<String, String> getDataModelSchemas(){

		return getDataModelSchemas(null);
	}

	/**
	 * Gets the pivot data model schemas.
	 *
	 * @param modelPath
	 *            the model path
	 * @return the data model schemas in Map:
	 * String Key = namespace, String value = schema content
	 */
	public Map<String, String> getDataModelSchemas(String modelPath){

		String rootPath = (modelPath == null) ? getModelPackagePath() : modelPath;
		NamespaceHelper helper = new NamespaceHelper();

		Map<String, String> result = new HashMap<String, String>();

		Map<String,String> zipResults = getContent(rootPath + ".+" + SCHEMA_SUFFIX);
		for (String xsdContent : zipResults.values()){
			result.put(helper.getNamespaceFromXsdContent(xsdContent), xsdContent);
		}

		return result;
	}

	/**
	 * Gets the model manifest.
	 *
	 * @param modelPath
	 *            the model path
	 * @return the model manifest
	 */
	public String getModelManifest(String modelPath) {

		return getValueFromContentSearchResult(getContent(modelPath + MANIFEST_NAME + XML_SUFFIX));
	}

	/**
	 * Gets the model manifest helper (JAXB Class).
	 *
	 * @param modelPath
	 *            the model path
	 * @return the model manifest helper
	 */
	public ModelManifest getModelManifestHelper(String modelPath) {
		return SchemaHelperFactory.getModelManifestSchemaHelper().create(getModelManifest(modelPath));
	}

}
