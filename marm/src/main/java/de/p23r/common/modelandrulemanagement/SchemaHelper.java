package de.p23r.common.modelandrulemanagement;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
//import java.util.HashMap;
import java.util.List;
//import java.util.Map;


import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.input.BOMInputStream;
//import org.apache.xmlbeans.SchemaType;
//import org.apache.xmlbeans.SchemaTypeSystem;
//import org.apache.xmlbeans.XmlBeans;
//import org.apache.xmlbeans.XmlException;
//import org.apache.xmlbeans.XmlObject;
//import org.apache.xmlbeans.XmlOptions;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

/**
 * The Class AbstractSchemaHelper.
 *
 * @author sro
 * @author sim
 * @param <SchemaObject>            the generic type
 */
public class SchemaHelper<SchemaObject> {

	private static final Logger log = LoggerFactory.getLogger(SchemaHelper.class);

	protected JAXBContext jc;

//	private final String SCHEMA_COMMON_1_0_LOCATION = "xsd/Common1-0.xsd";

	/**
 * The SCHEM a_ commo n_1_0_ namespace.
 */
private final String SCHEMA_COMMON_1_0_NAMESPACE = "de.p23r.leitstelle.ns.p23r.nrl.common1_0";

	private final String SCHEMA_COMMON_1_1_LOCATION = "xsd/CommonNrl1-1.xsd";

	private final String SCHEMA_COMMON_1_1_NAMESPACE = "de.p23r.leitstelle.ns.p23r.nrl.commonnrl1_1";

	private final String SCHEMA_XMLDSIG_LOCATION = "xsd/xmldsig-core-schema.xsd";

	private final String SCHEMA_XMLDSIG_NAMESPACE = "org.w3._2000._09.xmldsig_";

	private String namespace;

	private String schemaLocation;

	private String schema;

	private Class<SchemaObject> schemaHelperClass;

	/**
	 * Instantiates a new abstract schema helper.
	 * 
	 * @param namespace
	 *            the namespace
	 * @param schemaLocation
	 *            the schema location
	 * @param schemaHelperClass
	 *            the schema helper class
	 */
	public SchemaHelper(String namespace, String schemaLocation, Class<SchemaObject> schemaHelperClass) {
		this.namespace = namespace;
		this.schemaLocation = schemaLocation;
		this.schemaHelperClass = schemaHelperClass;
		try {
			jc = JAXBContext.newInstance(new Class[] { schemaHelperClass });
		} catch (JAXBException e) {
			log.error("instantiating jaxb context", e);
		}
	}

	/**
	 * Instantiates a new abstract schema helper.
	 *
	 * @param namespace            the namespace
	 * @param dependencies the dependencies
	 * @param schemaLocation            the schema location
	 * @param schemaHelperClass            the schema helper class
	 */
	public SchemaHelper(String namespace, List<String> dependencies, String schemaLocation, Class<SchemaObject> schemaHelperClass) {
		this.namespace = namespace;
		this.schemaLocation = schemaLocation;
		this.schemaHelperClass = schemaHelperClass;
		if (namespace != null) {
			StringBuffer namespaces = new StringBuffer(SCHEMA_XMLDSIG_NAMESPACE + ":" + SCHEMA_COMMON_1_0_NAMESPACE + ":" + SCHEMA_COMMON_1_1_NAMESPACE + ":"  + namespace);
			for (String dependency : dependencies) {
				namespaces.append(":");
				namespaces.append(dependency);
			}
			try {
				jc = JAXBContext.newInstance(namespaces.toString());
			} catch (JAXBException e) {
				log.error("instantiating jaxb context", e);
			}
		}
	}

	/**
	 * Instantiates a new schema helper.
	 * 
	 * @param schema
	 *            the schema
	 */
	public SchemaHelper(String schema) {
		this.schema = schema;
		this.namespace = new NamespaceHelper().getNamespaceFromXsdContent(schema);
	}

	/**
	 * Creates the.
	 * 
	 * @return the schema object
	 */
	@SuppressWarnings("unchecked")
	public SchemaObject create() {
		SchemaObject result = null;
		if (schemaHelperClass != null) {
			try {
				Object objectFactory = getObjectFactory();

				Method method = objectFactory.getClass().getMethod("create" + schemaHelperClass.getSimpleName(), (Class[]) null);

				result = (SchemaObject) method.invoke(objectFactory, (Object[]) null);
			} catch (IllegalArgumentException e) {
				log.warn("problem creating jaxb classes: ", e);
			} catch (IllegalAccessException e) {
				log.warn("problem creating jaxb classes: ", e);
			} catch (InvocationTargetException e) {
				log.warn("problem creating jaxb classes: ", e);
			} catch (ClassNotFoundException e) {
				log.warn("problem creating jaxb classes: ", e);
			} catch (SecurityException e) {
				log.warn("problem creating jaxb classes: ", e);
			} catch (NoSuchMethodException e) {
				log.warn("problem creating jaxb classes: ", e);
			} catch (InstantiationException e) {
				log.warn("problem creating jaxb classes: ", e);
			}
		}
		return result;
	}

	/**
	 * Marshall.
	 * 
	 * @param schemaObject
	 *            the schema object
	 * @return the string
	 */
	@SuppressWarnings("unchecked")
	public String marshall(SchemaObject schemaObject) {

		String result = null;
		if (schemaHelperClass != null) {
			try {
				Object objectFactory = getObjectFactory();
				Method method = objectFactory.getClass().getMethod("create" + schemaHelperClass.getSimpleName(), schemaHelperClass);
				result = marshallObject((JAXBElement<SchemaObject>) method.invoke(objectFactory, schemaObject));
			} catch (IllegalArgumentException e) {
				log.warn("problem creating jaxb classes: ", e);
			} catch (IllegalAccessException e) {
				log.warn("problem creating jaxb classes: ", e);
			} catch (InvocationTargetException e) {
				log.warn("problem creating jaxb classes: ", e);
			} catch (ClassNotFoundException e) {
				log.warn("problem creating jaxb classes: ", e);
			} catch (SecurityException e) {
				log.warn("problem creating jaxb classes: ", e);
			} catch (NoSuchMethodException e) {
				log.warn("problem creating jaxb classes: ", e);
			} catch (InstantiationException e) {
				log.warn("problem creating jaxb classes: ", e);
			}
		}
		return result;
	}

	/**
	 * Marshall to jdom.
	 *
	 * @param schemaObject the schema object
	 * @return the org.jdom2. document
	 */
	@SuppressWarnings("unchecked")
	public org.jdom2.Document marshallToJdom(SchemaObject schemaObject) {

		org.jdom2.Document result = null;
		if (schemaHelperClass != null) {
			try {
				Object objectFactory = getObjectFactory();
				Method method = objectFactory.getClass().getMethod("create" + schemaHelperClass.getSimpleName(), schemaHelperClass);
				result = marshallObjectToJDOM((JAXBElement<SchemaObject>) method.invoke(objectFactory, schemaObject));
			} catch (IllegalArgumentException e) {
				log.warn("problem creating jaxb classes: ", e);
			} catch (IllegalAccessException e) {
				log.warn("problem creating jaxb classes: ", e);
			} catch (InvocationTargetException e) {
				log.warn("problem creating jaxb classes: ", e);
			} catch (ClassNotFoundException e) {
				log.warn("problem creating jaxb classes: ", e);
			} catch (SecurityException e) {
				log.warn("problem creating jaxb classes: ", e);
			} catch (NoSuchMethodException e) {
				log.warn("problem creating jaxb classes: ", e);
			} catch (InstantiationException e) {
				log.warn("problem creating jaxb classes: ", e);
			}
		}
		return result;
	}

	/**
	 * Marshall fragment.
	 *
	 * @param elem the elem
	 * @return the string
	 */
	public String marshallFragment(JAXBElement<?> elem) {
		ByteArrayOutputStream writer = new ByteArrayOutputStream();
		try {
			XMLOutputFactory factory = XMLOutputFactory.newInstance();
			factory.setProperty(XMLOutputFactory.IS_REPAIRING_NAMESPACES, new Boolean(true));

			XMLStreamWriter xmlStreamWriter = factory.createXMLStreamWriter(writer);
			xmlStreamWriter.setDefaultNamespace(namespace);

			Marshaller m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			m.marshal(elem, xmlStreamWriter);
		} catch (JAXBException e) {
			log.error("marshalling object", e);
		} catch (XMLStreamException e) {
			log.error("marshalling object", e);
		} catch (FactoryConfigurationError e) {
			log.error("marshalling object", e);
		}
		return writer.toString();
	}

	/**
	 * Gets the object factory.
	 * 
	 * @return the object factory
	 * @throws ClassNotFoundException
	 *             the class not found exception
	 * @throws InstantiationException
	 *             the instantiation exception
	 * @throws IllegalAccessException
	 *             the illegal access exception
	 */
	private Object getObjectFactory() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		Class<?> objectFactoryClass = Class.forName(namespace + ".ObjectFactory");
		return objectFactoryClass.newInstance();
	}

	/**
	 * Gets the schema object class.
	 * 
	 * @return the schema object class
	 */
	@SuppressWarnings({ "unchecked", "unused" })
	private Class<SchemaObject> getSchemaObjectClass() {
		ParameterizedType pt = (ParameterizedType) getClass().getGenericSuperclass();
		Type actualTypeArgument = pt.getActualTypeArguments()[0];
		Class<SchemaObject> schemaObjectClass = null;
		if (actualTypeArgument instanceof ParameterizedType) {
			schemaObjectClass = (Class<SchemaObject>) ((ParameterizedType) actualTypeArgument).getRawType();
		} else {
			schemaObjectClass = (Class<SchemaObject>) actualTypeArgument;
		}
		return schemaObjectClass;
	}

	/**
	 * Creates the.
	 * 
	 * @param xml
	 *            the xml
	 * @return the schema object
	 */
	public SchemaObject create(String xml) {
		if (schemaHelperClass != null) {
			InputStream stream = new ByteArrayInputStream(xml.getBytes(Charset.forName("UTF-8")));
			BOMInputStream bomStream = new BOMInputStream(stream);
			JAXBElement<SchemaObject> elem = createObject(new StreamSource(bomStream));
			SchemaObject obj = elem.getValue();
			return obj;
		} else {
			return null;
		}
	}

	/**
	 * Creates the.
	 * 
	 * @param xml
	 *            the xml
	 * @return the schema object
	 */
	public SchemaObject create(InputStream xml) {
		if (schemaHelperClass != null) {
			BOMInputStream bomStream = new BOMInputStream(xml);
			return createObject(new StreamSource(bomStream)).getValue();
		} else {
			return null;
		}
	}

	/**
	 * Creates the.
	 * 
	 * @param xml
	 *            the xml
	 * @return the schema object
	 */
	public SchemaObject create(Element xml) {
		if (schemaHelperClass != null) {
			return createObject(new DOMSource(xml)).getValue();
		} else {
			return null;
		}
	}

	/**
	 * Creates a Schema object from XML-Instanz (String).
	 * 
	 * @param source
	 *            the source
	 * @return the notification profile
	 */
	protected JAXBElement<SchemaObject> createObject(Source source) {
		JAXBElement<SchemaObject> result = null;
		try {
			Unmarshaller um = jc.createUnmarshaller();
			result = um.unmarshal(source, schemaHelperClass);
		} catch (JAXBException e) {
			log.error("unmarshalling xml instance", e);
		}

		return result;
	}

	/**
	 * Marshall object.
	 * 
	 * @param object
	 *            the object
	 * @return the string
	 */
	protected String marshallObject(JAXBElement<SchemaObject> object) {
		ByteArrayOutputStream writer = new ByteArrayOutputStream();
		try {
			XMLOutputFactory factory = XMLOutputFactory.newInstance();
			factory.setProperty(XMLOutputFactory.IS_REPAIRING_NAMESPACES, new Boolean(true));

			XMLStreamWriter xmlStreamWriter = factory.createXMLStreamWriter(writer);
			xmlStreamWriter.setDefaultNamespace(namespace);

			Marshaller m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			m.marshal(object, xmlStreamWriter);
		} catch (JAXBException e) {
			log.error("marshalling object", e);
		} catch (XMLStreamException e) {
			log.error("marshalling object", e);
		} catch (FactoryConfigurationError e) {
			log.error("marshalling object", e);
		}
		return writer.toString();
	}

	/**
	 * Marshall object to jdom.
	 *
	 * @param object the object
	 * @return the org.jdom2. document
	 */
	protected org.jdom2.Document marshallObjectToJDOM(JAXBElement<SchemaObject> object) {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		try {
			XMLOutputFactory factory = XMLOutputFactory.newInstance();
			factory.setProperty(XMLOutputFactory.IS_REPAIRING_NAMESPACES, Boolean.TRUE);

			XMLStreamWriter xmlStreamWriter = factory.createXMLStreamWriter(output);
			xmlStreamWriter.setDefaultNamespace(namespace);

			Marshaller m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");

			m.marshal(object, xmlStreamWriter);

			String out = output.toString();
			
			return new SAXBuilder().build(new ByteArrayInputStream(output.toByteArray()));
		} catch (JAXBException e) {
			log.error("marshalling object", e);
		} catch (XMLStreamException e) {
			log.error("marshalling object", e);
		} catch (FactoryConfigurationError e) {
			log.error("marshalling object", e);
		} catch (JDOMException e) {
			log.error("building jdom object", e);
		} catch (IOException e) {
			log.error("building jdom object", e);
		}
		return null;
	}

	/**
	 * Creates the default instance.
	 *
	 * @param root the root
	 * @return the string
	 */
	public String createDefaultInstance(String root) {
		InstanceFromSchema ins = new InstanceFromSchema();
		
		InputStream commonSchema = Thread.currentThread().getContextClassLoader().getResourceAsStream(SCHEMA_COMMON_1_1_LOCATION);
		InputStream xmldsigSchema = Thread.currentThread().getContextClassLoader().getResourceAsStream(SCHEMA_XMLDSIG_LOCATION);
		return ins.generateXML(root, namespace, getSchemaAsStream(), commonSchema, xmldsigSchema);
	}

	/**
	 * Gets the xsd schema as stream.
	 * 
	 * @return the schema as stream
	 */
	private InputStream getSchemaAsStream() {
		InputStream result = null;
		if (schema != null) {
			try {
				result = new ByteArrayInputStream(schema.getBytes("UTF-8"));
			} catch (UnsupportedEncodingException e) {
				log.error("problem creating InputStream from schema string.");
			}
		} else {
			result = Thread.currentThread().getContextClassLoader().getResourceAsStream(schemaLocation);
		}
		return result;
	}

}
