/**
 * Provides wrapper classes which provides access to the internal structure and resources of p23r model and rule archives.
 */
package de.p23r.common.modelandrulemanagement.archive;