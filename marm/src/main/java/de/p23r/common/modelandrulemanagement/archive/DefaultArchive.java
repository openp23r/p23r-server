package de.p23r.common.modelandrulemanagement.archive;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.p23r.common.modelandrulemanagement.NamespaceHelper;

/**
 * Basic Archive Class, manages Zip structures and contents.
 *
 * @author sro
 *
 */
public class DefaultArchive {

	private final Logger log = LoggerFactory.getLogger(getClass());

	/** The Constant SELECTION_SUFFIX. */
	public static final String SELECTION_SUFFIX = ".*";

	/** The Constant SELECTION_DATSEL_SUFFIX. */
	public static final String SELECTION_DATSEL_SUFFIX = "\\.dats";

	/** The Constant SELECTION_XQUERY_SUFFIX. */
	public static final String SELECTION_XQUERY_SUFFIX = "\\.xquery";

	/** The Constant TRANSFORMATION_SUFFIX. */
	public static final String TRANSFORMATION_SUFFIX = "\\.xsl[t]?";

	/** The Constant SCHEMA_SUFFIX. */
	public static final String SCHEMA_SUFFIX = "\\.xsd";

	/** The Constant XML_SUFFIX. */
	public static final String XML_SUFFIX = "\\.xml";

	/** The Constant SELECTION_NAME. */
	public static final String SELECTION_NAME = "Selection";

	/** The Constant CONFIGURATION_NAME. */
	public static final String CONFIGURATION_NAME = "Configuration";

	/** The Constant MESSAGE_NAME. */
	public static final String MESSAGE_NAME = "Message";

	/** The Constant TESTMESSAGE_NAME. */
	public static final String TESTNOTIFICATION_NAME = "Notification-[0-9]+";

	/** The Constant KEYSTORE_NAME. */
	public static final String KEYSTORE_NAME = "Signature";

	/** The Constant KEYSTORE_SUFFIX. */
	public static final String KEYSTORE_SUFFIX = "\\.pfx";

	/** The Constant MULTI_PROFILE_GENERATION_NAME. */
	protected static final String MULTI_PROFILE_GENERATION_NAME = "MultiNotificationProfile";

	/** The Constant NOTIFICATION_CORE_NAME. */
	public static final String NOTIFICATION_CORE_NAME = "NotificationCore";

	/** The Constant MANIFEST_NAME. */
	public static final String MANIFEST_NAME = "Manifest";

	/** The Constant RECOMMENDATION_NAME. */
	public static final String RECOMMENDATION_NAME = "Recommendation";

	private static final int BUFFER_SIZE = 1024;

	private final Map<String, Map<String, String>> contentCache = new HashMap<String, Map<String, String>>();

	private byte[] zipContent;

	/**
	 * The Enum PackageTypes.
	 */
	public enum PackageTypes {
		/** The RULEPACKAGE Type. */
		RULEPACKAGE,
		/** The MODELPACKAGE Type. */
		MODELPACKAGE,
		/** The TESTPACKAGE Type. */
		TESTPACKAGE,
		/** The PACKAGELIST Type. */
		PACKAGELIST,
		/** The UNKNOWN Type. */
		UNKNOWN
	}

	/**
	 * Instantiates a new default archive.
	 */
	public DefaultArchive() {
	}
	
	/**
	 * Instantiates a new default archive.
	 *
	 * @param zipContent
	 *            the zip content
	 */
	public DefaultArchive(InputStream zipContent) {
		if (this.zipContent == null) {
			try {
				this.zipContent = IOUtils.toByteArray(zipContent);
			} catch (IOException e) {
				log.error("reading zip content from input stream", e);
			}
		}
	}

	/**
	 * Gets the archive (zip) content.
	 *
	 * @return the zip content
	 */
	public byte[] getZipContent() {
		checkValidity();
		return zipContent;
	}

	/**
	 * Check validity.
	 *
	 * @throws IllegalStateException the illegal state exception
	 */
	protected void checkValidity() throws IllegalStateException {
		if (zipContent == null) {
			throw new IllegalStateException("archive content not set");
		}
	}
	
	/**
	 * Sets the archive (zip) content.
	 *
	 * @param zipContent
	 *            the new zip content
	 */
	public void setZipContent(byte[] zipContent) {
		this.zipContent = zipContent.clone();
	}

	/**
	 * Gets the root path of within archive (of package or package list).
	 *
	 * @return the package root path
	 */
	public String getPackageRootPath(){
		return getKeyFromContentSearchResult(getContent("[^/]+/[^/]+/"));
	}

	/**
	 * Gets the archive (package ...) manifest.
	 *
	 * @return the package manifest
	 */
	public String getPackageManifest() {
		return getValueFromContentSearchResult(getContent(getPackageRootPath() + MANIFEST_NAME + XML_SUFFIX));
	}

	/**
	 * Gets the key (resource path within archive) from content search result.
	 * Helper Method.
	 *
	 * @param contentSearchResult
	 *            the content search result
	 * @return the key from content search result
	 */
	protected String getKeyFromContentSearchResult(Map<String, String> contentSearchResult){
		String result = null;
		if (!contentSearchResult.keySet().isEmpty()){
			result = contentSearchResult.keySet().iterator().next();
		}
		return result;
	}

	/**
	 * Gets the value from content search result.
	 * Helper Method
	 *
	 * @param contentSearchResult
	 *            the content search result
	 * @return the value from content search result
	 */
	protected String getValueFromContentSearchResult(Map<String, String> contentSearchResult){
		String result = null;
		if (!contentSearchResult.values().isEmpty()){
			result = contentSearchResult.values().iterator().next();
		}
		return result;
	}

	/**
	 * Returns internal Archive Resources (by given searchstring) within Result Map.
	 *
	 * @param searchString
	 *            the search string
	 * @return a Result Map with String key = internal Resource Path; value = Resource Content
	 */
	public Map<String, String> getContent(String searchString) {
		checkValidity();
		log.trace("extracting {} from package archive content", searchString);
		Map<String, String> result = new HashMap<String, String>();

		ZipInputStream in = new ZipInputStream(new ByteArrayInputStream(zipContent));

		if (contentCache.containsKey(searchString)) {
			log.trace("found cached entry for {}", searchString);
			result = contentCache.get(searchString);
		} else {

			ZipEntry file = null;
			try {
				while ((file = in.getNextEntry()) != null) {
					String name = file.getName();
					// System.out.println("entry found: " + name);
					// log.debug("entry {} found", name);
					if (name.matches(searchString)) {
						log.trace("found entry, extracting {}", name);
						String content = "";
						if (file.isDirectory()) {
							content = "directory";
						} else {
							// BOMInputStream bomExtract = new
							// BOMInputStream(in);
							ByteArrayOutputStream obuf = new ByteArrayOutputStream();
							byte[] buf = new byte[1024];
							int n;
							while ((n = in.read(buf, 0, BUFFER_SIZE)) > -1) {
								// workaround for skipping BOM
								if (buf[0] == (byte)0xEF && buf[1] == (byte)0xBB
										&& buf[2] == (byte)0xBF) {
									log.debug("skipping BOM");
									obuf.write(buf, 3, n - 3);
								} else {
									obuf.write(buf, 0, n);
								}
							}
							content = obuf.toString();
							// content = new
							// String(obuf.toString().getBytes("utf8"));
							obuf.close();
						}
						result.put(name, content);
					}
				}
				in.close();
			} catch (IOException e) {
				log.error("processing zip file", e);
			}

			contentCache.put(searchString, result);

		}
		return result;
	}

	/**
	 * Creates the from content.
	 *
	 * @param content the content
	 * @return the default archive
	 */
	public static DefaultArchive createFromContent(InputStream content) {
		DefaultArchive da = new DefaultArchive(content);

		String manifest = null;
		// try for package list within root
		Iterator<String> iterator = da.getContent(MANIFEST_NAME + XML_SUFFIX).values().iterator();
		if(!iterator.hasNext()){
			// try for other packages in <package name>/<release>/
			iterator = da.getContent("[^/]+/[^/]+/" + MANIFEST_NAME + XML_SUFFIX).values().iterator();
		}

		DefaultArchive result = null;
		if (iterator.hasNext()){
			manifest = iterator.next();
			String namespace = new NamespaceHelper().getNamespaceFromXmlContent(manifest);

			if (namespace != null) {
				if (namespace.contains("RulePackageManifest")) {
					result = new NotificationRulePackageArchive();
				} else if (namespace.contains("ModelPackageManifest")) {
					result = new ModelPackageArchive();
				} else if (namespace.contains("PackageList")) {
					result = new NotificationRulePackageListArchive();
				} else if (namespace.contains("TestPackageManifest")) {
					result = new TestPackageArchive();
				}
				if (result != null) {
					result.setZipContent(da.getZipContent()); 								
				}
			}
		}

		return result;
	}

}
