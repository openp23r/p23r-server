package de.p23r.common.modelandrulemanagement.archive;

import de.p23r.leitstelle.ns.p23r.nrl.testcasemanifest1_0.TestCaseManifest;
import de.p23r.leitstelle.ns.p23r.nrl.testpackagemanifest1_0.TestPackageManifest;
import de.p23r.leitstelle.ns.p23r.nrl.testsetmanifest1_1.TestSetManifest;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.p23r.common.modelandrulemanagement.NamespaceHelper;
import de.p23r.common.modelandrulemanagement.SchemaHelperFactory;

/**
 * The Class TestpackageArchive provides access to internal Testpackage Resources and Artifacts.
 * It also provides Helpers for converting the Archive Structure in a appropriate entity model for
 * persisting purposes.
 *
 * @author sim
 */
public class TestPackageArchive extends DefaultArchive {


	/**
	 * Instantiates a new test package archive.
	 */
	public TestPackageArchive() {
	}

	/**
	 * Instantiates a new test package archive.
	 *
	 * @param zipContent the zip content
	 */
	public TestPackageArchive(InputStream zipContent) {
		super(zipContent);
	}

	/**
	 * Gets the test package path.
	 *
	 * @return the test package path
	 */
	public String getTestPackagePath(){
		return getPackageRootPath();
	}

	/**
	 * Gets the test package manifest helper.
	 *
	 * @return the test package manifest helper
	 */
	public TestPackageManifest getTestPackageManifestHelper() {
		return SchemaHelperFactory.getTestPackageManifestSchemaHelper().create(getPackageManifest());
	}

	/**
	 * Gets the test set manifest.
	 *
	 * @param testsetPath the testset path
	 * @return the test set manifest
	 */
	public String getTestSetManifest(String testsetPath) {
		return getValueFromContentSearchResult(getContent(testsetPath + MANIFEST_NAME + XML_SUFFIX));
	}

	/**
	 * Gets the test set manifest helper.
	 *
	 * @param testsetPath the testset path
	 * @return the test set manifest helper
	 */
	public TestSetManifest getTestSetManifestHelper(String testsetPath) {
		String manifest = getValueFromContentSearchResult(getContent(testsetPath + MANIFEST_NAME + XML_SUFFIX));
		return SchemaHelperFactory.getTestSetManifestSchemaHelper().create(manifest);
	}

	/**
	 * Gets the test case manifest.
	 *
	 * @param testcasePath the testcase path
	 * @return the test case manifest
	 */
	public String getTestCaseManifest(String testcasePath) {
		return getValueFromContentSearchResult(getContent(testcasePath + MANIFEST_NAME + XML_SUFFIX));
	}

	/**
	 * Gets the test case manifest helper.
	 *
	 * @param testcasePath the testcase path
	 * @return the test case manifest helper
	 */
	public TestCaseManifest getTestCaseManifestHelper(String testcasePath) {
		String manifest = getValueFromContentSearchResult(getContent(testcasePath + MANIFEST_NAME + XML_SUFFIX));
		return SchemaHelperFactory.getTestCaseManifestSchemaHelper().create(manifest);
	}

	/**
	 * Gets the test case manifest helper.
	 *
	 * @param testsetName the testset name
	 * @param testcaseName the testcase name
	 * @return the test case manifest helper
	 */
	public TestCaseManifest getTestCaseManifestHelper(String testsetName, String testcaseName) {
		return getTestCaseManifestHelper(getTestPackagePath() + testsetName + "/" + testcaseName + "/");
	}

	/**
	 * Gets a List of model pathes.
	 *
	 * @return the model pathes
	 */
	public List<String> getTestSetPathes(){
		List<String> result = new ArrayList<String>();

		for (String testsetPath : getContent(getTestPackagePath() + "[^/]+/").keySet()){
			if (!testsetPath.endsWith("/data/") && !testsetPath.endsWith("/sources/")) {
				result.add(testsetPath);
			}
		}

		return result;
	}

	/**
	 * Gets the test case pathes.
	 *
	 * @param testsetPath the testset path
	 * @return the test case pathes
	 */
	public List<String> getTestCasePathes(String testsetPath){
		List<String> result = new ArrayList<String>();

		for (String testcasePath : getContent(testsetPath + "[^/]+/").keySet()) {
			if (!testcasePath.endsWith("/data/") && !testcasePath.endsWith("/sources/")) {
				result.add(testcasePath);				
			}
		}

		return result;
	}

	/**
	 * Gets the pivot data model schemas.
	 *
	 * @param testset the testset
	 * @param testcase the testcase
	 * @return the data model schemas in Map:
	 * String Key = namespace, String value = schema content
	 */
	public Map<String, String> getDataForTestCase(String testset, String testcase) {
		
		NamespaceHelper helper = new NamespaceHelper();

		Map<String, String> result = new HashMap<String, String>();

		Map<String,String> zipResults = getContent(getTestPackagePath() + testset + "/data/.+" + XML_SUFFIX);
		for (String xmlContent : zipResults.values()){
			result.put(helper.getNamespaceFromXmlContent(xmlContent), xmlContent);
		}

		zipResults = getContent(getTestPackagePath() + testset + "/" + testcase + "/data/.+" + XML_SUFFIX);
		for (String xmlContent : zipResults.values()) {
			result.put(helper.getNamespaceFromXmlContent(xmlContent), xmlContent);
		}

		return result;
	}

	/**
	 * Gets the matching filter.
	 *
	 * @param testsetName the testset name
	 * @param testcaseName the testcase name
	 * @param notification the notification
	 * @return the matching filter
	 */
	public String getMatchingFilter(String testsetName, String testcaseName, String notification) {
		Pattern pattern = Pattern.compile("[0-9]+");
		Matcher matcher = pattern.matcher(notification);
		String number = null;
		while (matcher.find()) {
			number = matcher.group();
		}
		return number == null ? null : getValueFromContentSearchResult(getContent(getTestPackagePath() + testsetName + "/" + testcaseName + "/Filter-" + number + TRANSFORMATION_SUFFIX));			
	}

	/**
	 * Gets the model manifest.
	 *
	 * @param testcasePath the testcase path
	 * @return the model manifest
	 */
	public String getTestCaseMessage(String testcasePath) {
		return getValueFromContentSearchResult(getContent(testcasePath + MESSAGE_NAME + XML_SUFFIX));
	}

	/**
	 * Gets the expected notifications.
	 *
	 * @param testsetName the testset name
	 * @param testcaseName the testcase name
	 * @return the expected notifications
	 */
	public Map<String, String> getExpectedNotifications(String testsetName, String testcaseName) {
		return getContent(getTestPackagePath() + testsetName + "/" + testcaseName + "/" + TESTNOTIFICATION_NAME + XML_SUFFIX);
	}

	/**
	 * Gets the key store.
	 *
	 * @param testsetName the testset name
	 * @param testcaseName the testcase name
	 * @return the key store
	 */
	public String getKeyStore(String testsetName, String testcaseName) {
		return getValueFromContentSearchResult(getContent(getTestPackagePath() + testsetName + "/" + testcaseName + "/" + KEYSTORE_NAME + XML_SUFFIX));
	}

}
