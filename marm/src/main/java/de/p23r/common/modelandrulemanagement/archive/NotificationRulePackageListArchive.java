package de.p23r.common.modelandrulemanagement.archive;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import de.p23r.common.modelandrulemanagement.SchemaHelperFactory;
import de.p23r.leitstelle.ns.p23r.nrl.packagelist1_1.PackageList;

/**
 * The Class NotificationRulePackageListArchive.
 * 
 * @author sro
 */
public class NotificationRulePackageListArchive extends DefaultArchive {

	/**
	 * Instantiates a new notification rule package list archive.
	 */
	public NotificationRulePackageListArchive() {
	}

	/**
	 * Instantiates a new notification rule package list archive.
	 * 
	 * @param zipContent
	 *            the zip content
	 */
	public NotificationRulePackageListArchive(InputStream zipContent) {
		super(zipContent);
	}

	/**
	 * Return Manifest Content of NotificationRulePackageList.
	 * 
	 * @return String Manifest
	 */
	public String getNotificationRulePackageListManifest() {
		return getPackageManifest();
	}

	/**
	 * Gets the notification rule package list manifest helper (JAXB Class).
	 * 
	 * @return the notification rule package list manifest helper
	 */
	public PackageList getNotificationRulePackageListManifestHelper() {
		return SchemaHelperFactory.getPackageListManifestSchemaHelper().create(getNotificationRulePackageListManifest());
	}

	/**
	 * Gets the notification rule package pathes.
	 * 
	 * @return the notification rule package pathes
	 */
	public List<String> getNotificationRulePackagePathes() {
		List<String> result = new ArrayList<String>();

		for (String rulePackage : getContent(getPackageRootPath() + "[^/]+/").keySet()) {
			result.add(rulePackage);
		}

		return result;
	}

	/**
	 * Gets the notification rule package recommendation selection.
	 * 
	 * @param packagePath
	 *            the package path
	 * @return the notification rule package recommendation selection
	 */
	public String getNotificationRulePackageRecommendationSelection(String packagePath) {
//		return getValueFromContentSearchResult(getContent(packagePath + RECOMMENDATION_NAME + SELECTION_XQUERY_SUFFIX));
		Map<String, String> selections = getContent(packagePath + RECOMMENDATION_NAME + SELECTION_SUFFIX);
		return filterSelections(selections);
	}

	/**
	 * Gets the notification rule package recommendation transformation.
	 * 
	 * @param packagePath
	 *            the package path
	 * @return the notification rule package recommendation transformation
	 */
	public String getNotificationRulePackageRecommendationTransformation(String packagePath) {
		return getValueFromContentSearchResult(getContent(packagePath + RECOMMENDATION_NAME + TRANSFORMATION_SUFFIX));
	}

	private String filterSelections(Map<String, String> selections) {
		for (Entry<String, String> entry : selections.entrySet()) {
			String key = entry.getKey();
			if (key.endsWith(SELECTION_DATSEL_SUFFIX)) {
//				String result = compileDatSel(key, entry.getValue());
				String result = entry.getValue();
				if (result != null) {
					return result;
				}
			}
		}
		for (Entry<String,String> entry : selections.entrySet()) {
			String key = entry.getKey();
			if (key.endsWith(SELECTION_XQUERY_SUFFIX)) {
				return entry.getValue();
			}
		}
		return selections.isEmpty() ? null : selections.values().iterator().next();
	}
}
