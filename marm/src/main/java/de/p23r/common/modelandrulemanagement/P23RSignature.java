package de.p23r.common.modelandrulemanagement;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.security.InvalidAlgorithmParameterException;
import java.security.Key;
import java.security.KeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.crypto.AlgorithmMethod;
import javax.xml.crypto.Data;
import javax.xml.crypto.KeySelector;
import javax.xml.crypto.KeySelectorException;
import javax.xml.crypto.KeySelectorResult;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.OctetStreamData;
import javax.xml.crypto.URIDereferencer;
import javax.xml.crypto.URIReference;
import javax.xml.crypto.URIReferenceException;
import javax.xml.crypto.XMLCryptoContext;
import javax.xml.crypto.XMLStructure;
import javax.xml.crypto.dom.DOMStructure;
import javax.xml.crypto.dsig.CanonicalizationMethod;
import javax.xml.crypto.dsig.DigestMethod;
import javax.xml.crypto.dsig.Reference;
import javax.xml.crypto.dsig.SignatureMethod;
import javax.xml.crypto.dsig.SignedInfo;
import javax.xml.crypto.dsig.Transform;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.dom.DOMValidateContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.dsig.keyinfo.KeyValue;
import javax.xml.crypto.dsig.keyinfo.X509Data;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3._2000._09.xmldsig.ObjectFactory;
import org.w3._2000._09.xmldsig.SignatureType;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import de.p23r.common.modelandrulemanagement.TransformerHelper;

/**
 * The Class P23RSignature.
 */
public class P23RSignature {

	private final static Logger log = LoggerFactory.getLogger(P23RSignature.class);

	private final static String LOCAL_REF_URI = "local:element";

	private final static String HASH_ALGORITHM = "SHA-256";

	/**
	 * Sign with transformation.
	 * 
	 * @param element
	 *            the element
	 * @param cert
	 *            the cert
	 * @param privateKey
	 *            the private key
	 * @param transformation
	 *            the transformation
	 * @return the signature type
	 */
	public Element signWithTransformation(final Element element, X509Certificate cert, PrivateKey privateKey, String transformation) {
		return signElement(element, cert, privateKey, transformation);
	}

	/**
	 * Sign.
	 * 
	 * @param element
	 *            the element
	 * @param cert
	 *            the cert
	 * @param privateKey
	 *            the private key
	 * @return the signature type
	 */
	public Element sign(final Element element, X509Certificate cert, PrivateKey privateKey) {
		return signWithTransformation(element, cert, privateKey, null);
	}

	/**
	 * Sign element.
	 * 
	 * @param element
	 *            the element
	 * @param cert
	 *            the cert
	 * @param privateKey
	 *            the private key
	 * @param transformation
	 *            the transformation
	 * @return the element
	 */
	public Element signElement(final Element element, X509Certificate cert, PrivateKey privateKey, String transformation) {
		// First, create a DOM XMLSignatureFactory that will be used to
		// generate the XMLSignature and marshal it to DOM.
		// String providerName = System.getProperty("jsr105Provider",
		// "org.jcp.xml.dsig.internal.dom.XMLDSigRI");
		XMLSignatureFactory fac = XMLSignatureFactory.getInstance("DOM");

		// Create a Reference to an external URI that will be digested
		// using the SHA1 digest algorithm

		SignedInfo si = null;
		Reference ref = null;
		try {
			List<Transform> transforms = new ArrayList<Transform>();
			if (transformation != null) {
				Document trans = TransformerHelper.documentFromString(transformation);
				DOMStructure stylesheet = new DOMStructure(trans);
				Transform t = fac.newTransform(Transform.XSLT, stylesheet);
				transforms.add(t);
			}
			// transforms.add(fac.newCanonicalizationMethod(CanonicalizationMethod.EXCLUSIVE,
			// (C14NMethodParameterSpec) null));

			ref = fac.newReference(LOCAL_REF_URI, fac.newDigestMethod(DigestMethod.SHA1, null));

			si = fac.newSignedInfo(fac.newCanonicalizationMethod(CanonicalizationMethod.EXCLUSIVE, (C14NMethodParameterSpec) null),
					fac.newSignatureMethod(SignatureMethod.RSA_SHA1, null), Collections.singletonList(ref));
		} catch (NoSuchAlgorithmException e) {
			log.error("generating signed info", e);
			return null;
		} catch (InvalidAlgorithmParameterException e) {
			log.error("generating signed info", e);
			return null;
		}

		// Create a KeyValue containing the given PublicKey
		KeyInfoFactory kif = fac.getKeyInfoFactory();
		KeyValue kv = null;
		try {
			kv = kif.newKeyValue(cert.getPublicKey());
		} catch (KeyException e) {
			log.error("generating key value", e);
			return null;
		}
		KeyInfo ki = kif.newKeyInfo(Collections.singletonList(kv));

		// Create the XMLSignature (but don't sign it yet)
		XMLSignature signature = fac.newXMLSignature(si, ki);

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		Document doc = null;
		try {
			doc = factory.newDocumentBuilder().newDocument();
		} catch (ParserConfigurationException e) {
			log.error("generating result document", e);
			return null;
		}

		// Create a DOMSignContext and set the signing Key to the DSA
		// PrivateKey and specify where the XMLSignature should be inserted
		// in the target document (in this case, the document root)
		DOMSignContext signContext = new DOMSignContext(privateKey, doc);

		if (log.isTraceEnabled()) {
			signContext.setProperty("javax.xml.crypto.dsig.cacheReference", Boolean.TRUE);
		}

		signContext.setURIDereferencer(new URIDereferencer() {

			@Override
			public Data dereference(URIReference uriReference, XMLCryptoContext context) throws URIReferenceException {
				if (LOCAL_REF_URI.equals(uriReference.getURI())) {
					ByteArrayOutputStream output = new ByteArrayOutputStream();
					StreamResult result = new StreamResult(output);
					DOMSource source = new DOMSource(element);
					try {
						TransformerFactory.newInstance().newTransformer().transform(source, result);
					} catch (Exception e) {
						throw new URIReferenceException("Konnte nicht referenzieren", e);
					}
					InputStream is = new ByteArrayInputStream(output.toByteArray());
					return new OctetStreamData(is);
				}
				return null;
			}
		});

		// Marshal, generate (and sign) the detached XMLSignature. The DOM
		// Document will contain the XML Signature if this method returns
		// successfully.
		try {
			signature.sign(signContext);
		} catch (MarshalException e) {
			log.error("signing document", e);
			return null;
		} catch (XMLSignatureException e) {
			log.error("signing document", e);
			return null;
		}

		if (log.isTraceEnabled()) {
			log.trace("canonicalized sign data: {}", streamToString(ref.getDigestInputStream()));
			log.trace("signature: {}", TransformerHelper.elementToString(doc.getDocumentElement()));
		}

		return doc.getDocumentElement();
	}

	/**
	 * Validate.
	 * 
	 * @param signatureType
	 *            the signature type
	 * @param element
	 *            the element
	 * @return true, if successful
	 */
	public boolean validate(SignatureType signatureType, final Element element) {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(SignatureType.class);
			Marshaller marshaller = jaxbContext.createMarshaller();

			JAXBElement<SignatureType> jaxbSignature = new ObjectFactory().createSignature(signatureType);

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true);

			Document doc = factory.newDocumentBuilder().newDocument();

			marshaller.marshal(jaxbSignature, doc);

			DOMValidateContext validateContext = new DOMValidateContext(new KeyValueKeySelector(), doc.getDocumentElement());

			if (log.isTraceEnabled()) {
				validateContext.setProperty("javax.xml.crypto.dsig.cacheReference", Boolean.TRUE);
			}

			validateContext.setURIDereferencer(new URIDereferencer() {

				@Override
				public Data dereference(URIReference uriReference, XMLCryptoContext context) throws URIReferenceException {
					if (LOCAL_REF_URI.equals(uriReference.getURI())) {
						ByteArrayOutputStream output = new ByteArrayOutputStream();
						StreamResult result = new StreamResult(output);
						DOMSource source = new DOMSource(element);
						try {
							TransformerFactory.newInstance().newTransformer().transform(source, result);
						} catch (Exception e) {
							throw new URIReferenceException("Konnte nicht referenzieren", e);
						}
						InputStream is = new ByteArrayInputStream(output.toByteArray());
						return new OctetStreamData(is);
					}
					return null;
				}
			});

			XMLSignature signature = XMLSignatureFactory.getInstance("DOM").unmarshalXMLSignature(validateContext);

			boolean result = signature.validate(validateContext);
			if (!result) {
				log.error("core validation failed");
				boolean sv = signature.getSignatureValue().validate(validateContext);
				log.error("signature validation status: {}", sv);
				if (!sv) {
					Iterator<?> i = signature.getSignedInfo().getReferences().iterator();
					for (int j = 0; i.hasNext(); j++) {
						boolean refValid = ((Reference) i.next()).validate(validateContext);
						log.error("ref[{}] validity status: {}", j, refValid);
					}
				}
			}

			if (log.isTraceEnabled()) {
				Reference ref = (Reference) signature.getSignedInfo().getReferences().get(0);
				String canonicalized = streamToString(ref.getDigestInputStream());
				log.trace("canonicalized validate data: {}", canonicalized);
			}

			return result;

		} catch (JAXBException e) {
			log.error("could not validate signature", e);
		} catch (ParserConfigurationException e) {
			log.error("could not validate signature", e);
		} catch (MarshalException e) {
			log.error("could not validate signature", e);
		} catch (XMLSignatureException e) {
			log.error("could not validate signature", e);
		}
		return false;
	}

	private static class KeyValueKeySelector extends KeySelector {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * javax.xml.crypto.KeySelector#select(javax.xml.crypto.dsig.keyinfo
		 * .KeyInfo, javax.xml.crypto.KeySelector.Purpose,
		 * javax.xml.crypto.AlgorithmMethod, javax.xml.crypto.XMLCryptoContext)
		 */
		public KeySelectorResult select(KeyInfo keyInfo, KeySelector.Purpose purpose, AlgorithmMethod method, XMLCryptoContext context)
				throws KeySelectorException {

			if (keyInfo == null) {
				throw new KeySelectorException("Null KeyInfo object!");
			}
			SignatureMethod sm = (SignatureMethod) method;
			List<?> list = keyInfo.getContent();

			for (int i = 0; i < list.size(); i++) {
				XMLStructure xmlStructure = (XMLStructure) list.get(i);
				if (xmlStructure instanceof KeyValue) {
					PublicKey pk = null;
					try {
						pk = ((KeyValue) xmlStructure).getPublicKey();
					} catch (KeyException ke) {
						throw new KeySelectorException(ke);
					}
					// make sure algorithm is compatible with method
					if (algEquals(sm.getAlgorithm(), pk.getAlgorithm())) {
						return new SimpleKeySelectorResult(pk);
					}
				}
				if (xmlStructure instanceof X509Data) {
					X509Data x509Date = (X509Data) xmlStructure;

					for (Object o : x509Date.getContent()) {
						if (o instanceof java.security.cert.X509Certificate) {
							java.security.cert.X509Certificate cert = (java.security.cert.X509Certificate) o;
							PublicKey pk = cert.getPublicKey();
							// make sure algorithm is compatible with method
							if (algEquals(sm.getAlgorithm(), pk.getAlgorithm())) {
								return new SimpleKeySelectorResult(pk);
							}
						}
					}
				}
			}
			throw new KeySelectorException("No KeyValue element found!");
		}

		/**
		 * Alg equals.
		 * 
		 * @param algURI
		 *            the alg uri
		 * @param algName
		 *            the alg name
		 * @return true, if successful
		 */
		static boolean algEquals(String algURI, String algName) {
			if (algName.equalsIgnoreCase("DSA") && algURI.equalsIgnoreCase(SignatureMethod.DSA_SHA1)) {
				return true;
			} else if (algName.equalsIgnoreCase("RSA") && algURI.equalsIgnoreCase(SignatureMethod.RSA_SHA1)) {
				return true;
			} else {
				return false;
			}
		}

		/**
		 * The Class SimpleKeySelectorResult.
		 */
		private static class SimpleKeySelectorResult implements KeySelectorResult {

			/**
			 * The public key.
			 */
			private PublicKey pk;

			/**
			 * Instantiates a new simple key selector result.
			 * 
			 * @param pk
			 *            the pk
			 */
			SimpleKeySelectorResult(PublicKey pk) {
				this.pk = pk;
			}

			/*
			 * (non-Javadoc)
			 * 
			 * @see javax.xml.crypto.KeySelectorResult#getKey()
			 */
			public Key getKey() {
				return pk;
			}
		}

	}

	private String streamToString(InputStream is) {
		Writer writer = new StringWriter();
		char[] buffer = new char[1024];
		try {
			Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			int n;
			try {
				while ((n = reader.read(buffer)) != -1) {
					writer.write(buffer, 0, n);
				}
			} catch (IOException e) {
				log.error("reading stream failed", e);
			}
		} catch (UnsupportedEncodingException e) {
			log.error("reading stream failed", e);
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				log.error("closing stream failed", e);
			}
		}
		return writer.toString();
	}

	/**
	 * Sign embedded.
	 *
	 * @param element the element
	 * @param cert the cert
	 * @param privateKey the private key
	 * @return the element
	 */
	public Element signEmbedded(final Element element, X509Certificate cert, PrivateKey privateKey) {
		XMLSignatureFactory fac = XMLSignatureFactory.getInstance("DOM");

		// Create a Reference to an external URI that will be digested
		// using the SHA1 digest algorithm

		SignedInfo si = null;
		Reference ref = null;
		try {
			ref = fac.newReference("", fac.newDigestMethod(DigestMethod.SHA1, null),
					Collections.singletonList(fac.newTransform(Transform.ENVELOPED, (TransformParameterSpec) null)), null, null);
			si = fac.newSignedInfo(fac.newCanonicalizationMethod(CanonicalizationMethod.EXCLUSIVE, (C14NMethodParameterSpec) null),
					fac.newSignatureMethod(SignatureMethod.RSA_SHA1, null), Collections.singletonList(ref));
		} catch (NoSuchAlgorithmException e) {
			log.error("generating signed info", e);
			return null;
		} catch (InvalidAlgorithmParameterException e) {
			log.error("generating signed info", e);
			return null;
		}

		// Create a KeyValue containing the given PublicKey
		KeyInfoFactory kif = fac.getKeyInfoFactory();
		KeyValue kv = null;
		try {
			kv = kif.newKeyValue(cert.getPublicKey());
		} catch (KeyException e) {
			log.error("generating new key value with given public key", e);
			return null;
		}
		KeyInfo ki = kif.newKeyInfo(Collections.singletonList(kv));

		// Create the XMLSignature (but don't sign it yet)
		XMLSignature signature = fac.newXMLSignature(si, ki);

		// Create a DOMSignContext and set the signing Key to the DSA
		// PrivateKey and specify where the XMLSignature should be inserted
		// in the target document (in this case, the document root)
		DOMSignContext signContext = new DOMSignContext(privateKey, element);

		if (log.isTraceEnabled()) {
			signContext.setProperty("javax.xml.crypto.dsig.cacheReference", Boolean.TRUE);
		}

		// Marshal, generate (and sign) the detached XMLSignature. The DOM
		// Document will contain the XML Signature if this method returns
		// successfully.
		try {
			signature.sign(signContext);
		} catch (MarshalException e) {
			log.error("sign element", e);
			return null;
		} catch (XMLSignatureException e) {
			log.error("sign element", e);
			return null;
		}

		if (log.isTraceEnabled()) {
			log.trace("sign data: {}", streamToString(ref.getDigestInputStream()));
			log.trace("signature: {}", TransformerHelper.elementToString(element));
		}

		return element;
	}

	/**
	 * Validate embedded.
	 *
	 * @param element the element
	 * @return true, if successful
	 */
	public boolean validateEmbedded(final Element element) {
		NodeList nl = element.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");
		if (nl.getLength() == 0) {
			return false;
		}
		DOMValidateContext valContext = new DOMValidateContext(new KeyValueKeySelector(), nl.item(0));

		XMLSignatureFactory factory = XMLSignatureFactory.getInstance("DOM");
		try {
			XMLSignature signature = factory.unmarshalXMLSignature(valContext);
			return signature.validate(valContext);
		} catch (MarshalException e) {
			log.error("validating enveloped signature", e);
		} catch (XMLSignatureException e) {
			log.error("validating enveloped signature", e);
		}

		return false;
	}

	/**
	 * Hash.
	 *
	 * @param data the data
	 * @return the byte[]
	 */
	public static byte[] hash(byte[] data) {
		byte[] hash = null;
		try {
			MessageDigest digest = MessageDigest.getInstance(HASH_ALGORITHM);
			hash = digest.digest(data);
		} catch (NoSuchAlgorithmException e) {
			log.error("hash generation failure!", e);
			hash = new byte[] {};
		}
		return hash;
	}

	/**
	 * Checks if is hash valid.
	 *
	 * @param digesta the digesta
	 * @param data the data
	 * @return true, if is hash valid
	 */
	public static boolean isHashValid(byte[] digesta, byte[] data) {
		return MessageDigest.isEqual(digesta, hash(data));
	}

}
