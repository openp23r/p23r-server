/**
 * Provides several helper classes to deal with files from p23r model and package archives
 */
package de.p23r.common.modelandrulemanagement;