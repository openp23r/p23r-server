package de.p23r.common.modelandrulemanagement.archive;

//import java.io.IOException;
import java.io.InputStream;
//import java.io.InputStreamReader;
//import java.io.OutputStream;
//import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
//import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.p23r.common.modelandrulemanagement.SchemaHelperFactory;
import de.p23r.leitstelle.ns.p23r.nrl.rulegroupmanifest1_1.RuleGroupManifest;
import de.p23r.leitstelle.ns.p23r.nrl.rulemanifest1_1.RuleManifest;
import de.p23r.leitstelle.ns.p23r.nrl.rulepackagemanifest1_1.RulePackageManifest;

/**
 * NotificationRulePackage Archive Class, manages NotificationRulePackage
 * Archive structure and provides access to internal Artifacts of
 * NotificationRulePackage.
 * 
 * Provides Functionality of converting Archive to appropriate Persistence
 * Entity.
 * 
 * @author sro
 * 
 */
public class NotificationRulePackageArchive extends DefaultArchive {

	private static final Logger log = LoggerFactory.getLogger(NotificationRulePackageArchive.class);

	/**
	 * Instantiates a new notification rule package archive.
	 */
	public NotificationRulePackageArchive() {
	}

	/**
	 * Instantiates a new notification rule package archive.
	 * 
	 * @param zipContent
	 *            the zip content
	 */
	public NotificationRulePackageArchive(InputStream zipContent) {
		super(zipContent);
	}

	/**
	 * Gets the notification rule package path.
	 * 
	 * @return the notification rule package path
	 */
	public String getNotificationRulePackagePath() {
		return getPackageRootPath();
	}

	/**
	 * Return NotificationRulePackage Manifest Content.
	 * 
	 * @return String Manifest
	 */
	public String getNotificationRulePackageManifest() {
		return getPackageManifest();
	}

	/**
	 * Gets the notification rule package manifest helper (JAXB Class).
	 * 
	 * @return the notification rule package manifest helper
	 */
	public RulePackageManifest getNotificationRulePackageManifestHelper() {
		return SchemaHelperFactory.getNotificationRulePackageManifestSchemaHelper().create(getNotificationRulePackageManifest());
	}

	/**
	 * Gets the notification rule package recommendation selection.
	 * 
	 * @return the notification rule package recommendation selection
	 */
	public String getNotificationRulePackageRecommendationSelection() {
		Map<String, String> selections = getContent(getNotificationRulePackagePath() + RECOMMENDATION_NAME + SELECTION_SUFFIX);
		return filterSelections(selections);
	}

	/**
	 * Gets the notification rule package recommendation transformation.
	 * 
	 * @return the notification rule package recommendation transformation
	 */
	public String getNotificationRulePackageRecommendationTransformation() {
		return getValueFromContentSearchResult(getContent(getNotificationRulePackagePath() + RECOMMENDATION_NAME + TRANSFORMATION_SUFFIX));
	}

	/**
	 * Gets the pathes to the notification rule groups.
	 * 
	 * @return the notification rule groups
	 */
	public List<String> getNotificationRuleGroupPathes() {
		List<String> result = new ArrayList<String>();

		for (String ruleGroup : getContent(getNotificationRulePackagePath() + "[^/]+/").keySet()) {
			if (!ruleGroup.endsWith("/sources/")&& !ruleGroup.endsWith("/shared/")) {
				result.add(ruleGroup);
			}
		}

		return result;
	}

	/**
	 * Return NotificationRuleGroup Manifest Content for given
	 * NotificationRuleGroup.
	 * 
	 * @param groupPath
	 *            the group name
	 * @return String Manifest
	 */
	public String getNotificationRuleGroupManifest(String groupPath) {
		return getValueFromContentSearchResult(getContent(groupPath + MANIFEST_NAME + XML_SUFFIX));
	}

	/**
	 * Gets the notification rule group manifest helper.
	 * 
	 * @param groupPath
	 *            the group path
	 * @return the notification rule group manifest helper
	 */
	public RuleGroupManifest getNotificationRuleGroupManifestHelper(String groupPath) {
		return SchemaHelperFactory.getNotificationRuleGroupManifestSchemaHelper().create(getNotificationRuleGroupManifest(groupPath));
	}

	/**
	 * Gets the notification rule group configuration selection.
	 * 
	 * @param groupPath
	 *            the group path
	 * @return the notification rule group configuration selection
	 */
	public String getNotificationRuleGroupConfigurationSelection(String groupPath) {
		Map<String, String> selections = getContent(groupPath + CONFIGURATION_NAME + SELECTION_SUFFIX);
		return filterSelections(selections);
	}

	/**
	 * Gets the notification rule group configuration transformation.
	 * 
	 * @param groupPath
	 *            the group path
	 * @return the notification rule group configuration transformation
	 */
	public String getNotificationRuleGroupConfigurationTransformation(String groupPath) {
		return getValueFromContentSearchResult(getContent(groupPath + CONFIGURATION_NAME + TRANSFORMATION_SUFFIX));
	}

	/**
	 * Gets the notification rule group recommendation selection.
	 * 
	 * @param groupPath
	 *            the group path
	 * @return the notification rule group recommendation selection
	 */
	public String getNotificationRuleGroupRecommendationSelection(String groupPath) {
		Map<String, String> selections = getContent(groupPath + RECOMMENDATION_NAME + SELECTION_SUFFIX);
		return filterSelections(selections);
	}

	/**
	 * Gets the notification rule group recommendation transformation.
	 * 
	 * @param groupPath
	 *            the group path
	 * @return the notification rule group recommendation transformation
	 */
	public String getNotificationRuleGroupRecommendationTransformation(String groupPath) {
		return getValueFromContentSearchResult(getContent(groupPath + RECOMMENDATION_NAME + TRANSFORMATION_SUFFIX));
	}

	/**
	 * Gets the notification rules.
	 * 
	 * @param groupPath
	 *            the group name
	 * @return the notification rules
	 */
	public List<String> getNotificationRulePathes(String groupPath) {
		List<String> result = new ArrayList<String>();

		for (String rule : getContent(groupPath + "[^/]+/").keySet()) {
			if (!rule.endsWith("/sources/") && !rule.endsWith("/shared/")) {
				result.add(rule);
			}
		}

		return result;
	}

	/**
	 * Return NotificationRule Manifest Content for given NotificationRule.
	 * 
	 * @param rulePath
	 *            the rule name
	 * @return String Manifest
	 */
	public String getNotificationRuleManifest(String rulePath) {
		return getValueFromContentSearchResult(getContent(rulePath + MANIFEST_NAME + XML_SUFFIX));
	}

	/**
	 * Gets the notification rule manifest helper (JAXB Class).
	 * 
	 * @param rulePath
	 *            the rule path
	 * @return the notification rule manifest helper
	 */
	public RuleManifest getNotificationRuleManifestHelper(String rulePath) {
		return SchemaHelperFactory.getNotificationRuleManifestSchemaHelper().create(getNotificationRuleManifest(rulePath));
	}

	/**
	 * Gets the notification rule selection.
	 * 
	 * @param rulePath
	 *            the rule path
	 * @return the notification rule selection
	 */
	public String getNotificationRuleSelection(String rulePath) {
		Map<String, String> selections = getContent(rulePath + SELECTION_NAME + SELECTION_SUFFIX);
		return filterSelections(selections);
	}

	/**
	 * Gets the notification rule message selection.
	 * 
	 * @param rulePath
	 *            the rule path
	 * @return the notification rule message selection
	 */
	public String getNotificationRuleMultiProfileGenerationSelection(String rulePath) {
		Map<String, String> selections = getContent(rulePath + MULTI_PROFILE_GENERATION_NAME + SELECTION_SUFFIX);
		return filterSelections(selections);
	}

	/**
	 * Gets the notification rule message selection transformation.
	 * 
	 * @param rulePath
	 *            the rule path
	 * @return the notification rule message selection transformation
	 */
	public String getNotificationRuleMultiProfileGenerationSelectionTransformation(String rulePath) {
		return getValueFromContentSearchResult(getContent(rulePath + MULTI_PROFILE_GENERATION_NAME + TRANSFORMATION_SUFFIX));
	}

	/**
	 * Gets the notification rule notification core transformation.
	 * 
	 * @param rulePath
	 *            the rule path
	 * @return the notification rule notification core transformation
	 */
	public String getNotificationRuleNotificationCoreTransformation(String rulePath) {
		return getValueFromContentSearchResult(getContent(rulePath + NOTIFICATION_CORE_NAME + TRANSFORMATION_SUFFIX));
	}

	/**
	 * Gets the notification rule message schema.
	 * 
	 * @param rulePath
	 *            the rule path
	 * @return the notification rule message schema
	 */
	public String getNotificationRuleMessageSchema(String rulePath) {
		return getValueFromContentSearchResult(getContent(rulePath + MESSAGE_NAME + SCHEMA_SUFFIX));
	}

	/**
	 * Gets all notification rule schemas.
	 * 
	 * @param rulePath
	 *            the rule path
	 * @return the notification rule schemas as map: String key = namespace;
	 *         String value = schema content
	 */
	public Map<String, String> getNotificationRuleSchemas(String rulePath) {
		Map<String, String> result = new HashMap<String, String>();

		for (Entry<String, String> xsdContent : getContent(rulePath + "[^/]+" + SCHEMA_SUFFIX).entrySet()) {
			result.put(xsdContent.getKey(), xsdContent.getValue());
		}

		return result;
	}

	/**
	 * Gets all notification presentation scripts.
	 * 
	 * @param rulePath
	 *            the rule path
	 * @return the notification presentation scripts as map: String key = script
	 *         name; String value = script content
	 */
	public Map<String, String> getNotificationRulePresentationScripts(String rulePath) {

		Map<String, String> result = new HashMap<String, String>();

		Map<String, String> zipResults = getContent(rulePath + "representation[^/]+" + TRANSFORMATION_SUFFIX);
		for (Entry<String, String> entry : zipResults.entrySet()) {
			result.put(entry.getKey().replace(rulePath, ""), entry.getValue());
		}

		return result;

	}

	private String filterSelections(Map<String, String> selections) {
		for (Entry<String, String> entry : selections.entrySet()) {
			String key = entry.getKey();
			if (key.endsWith(SELECTION_DATSEL_SUFFIX)) {
//				String result = compileDatSel(key, entry.getValue());
				String result = entry.getValue();
				log.debug("found p23r selection script {}", key);
				if (result != null) {
					return result;
				}
			}
		}
		for (Entry<String,String> entry : selections.entrySet()) {
			String key = entry.getKey();
			if (key.endsWith(SELECTION_XQUERY_SUFFIX)) {
				return entry.getValue();
			}
		}
		return selections.isEmpty() ? null : selections.values().iterator().next();
	}

//	private String compileDatSel(String name, String script) {
//		Process cmd = null;
//		try {
//			cmd = Runtime.getRuntime().exec("cmd.exe");
//		} catch (IOException e) {
//			log.error("process cmd", e);
//			return null;
//		}
//
//		final InputStream inStream = cmd.getInputStream();
//		new Thread(new Runnable() {
//			public void run() {
//				InputStreamReader reader = new InputStreamReader(inStream);
//				Scanner scan = new Scanner(reader);
//				while (scan.hasNextLine()) {
//					log.debug(scan.nextLine());
//				}
//				scan.close();
//			}
//		}).start();
//
//		OutputStream outStream = cmd.getOutputStream();
//		PrintWriter pWriter = new PrintWriter(outStream);
//		pWriter.println("echo Hello World: " + name);
//		pWriter.flush();
//		pWriter.close();
//		
//		return null;
//	}

}
