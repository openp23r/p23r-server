package de.p23r.common.modelandrulemanagement;

import java.util.HashMap;
import java.util.Map;

import de.p23r.leitstelle.ns.p23r.nrl.configurationresult1_0.ConfigurationResult;
import de.p23r.leitstelle.ns.p23r.nrl.modelmanifest1_1.ModelManifest;
import de.p23r.leitstelle.ns.p23r.nrl.modelpackagemanifest1_1.ModelPackageManifest;
import de.p23r.leitstelle.ns.p23r.nrl.multinotificationprofileoutput1_1.MultiNotificationProfileOutput;
import de.p23r.leitstelle.ns.p23r.nrl.notificationprofile1_1.NotificationProfile;
import de.p23r.leitstelle.ns.p23r.nrl.packagelist1_1.PackageList;
import de.p23r.leitstelle.ns.p23r.nrl.recommendationresult1_1.RecommendationResult;
import de.p23r.leitstelle.ns.p23r.nrl.rulegroupmanifest1_1.RuleGroupManifest;
import de.p23r.leitstelle.ns.p23r.nrl.rulemanifest1_1.RuleManifest;
import de.p23r.leitstelle.ns.p23r.nrl.rulepackagemanifest1_1.RulePackageManifest;
import de.p23r.leitstelle.ns.p23r.nrl.testcasemanifest1_0.TestCaseManifest;
import de.p23r.leitstelle.ns.p23r.nrl.testpackagemanifest1_0.TestPackageManifest;
import de.p23r.leitstelle.ns.p23r.nrl.testsetmanifest1_1.TestSetManifest;


/**
 * A factory for creating SchemaHelper objects.
 *
 * @author sro
 * @author sim
 */
public class SchemaHelperFactory {

	/** The schema helpers. */
	private static Map<String, SchemaHelper<?>> schemaHelpers = new HashMap<String, SchemaHelper<?>>();

	/**
	 * Gets the notification profile schema helper.
	 *
	 * @return the notification profile schema helper
	 */
	@SuppressWarnings("unchecked")
	public static SchemaHelper<NotificationProfile> getNotificationProfileSchemaHelper() {
		String namespace = "de.p23r.leitstelle.ns.p23r.nrl.notificationprofile1_1";
		SchemaHelper<NotificationProfile> result = (SchemaHelper<NotificationProfile>) schemaHelpers.get(namespace);

		if (result == null) {
			result = new SchemaHelper<NotificationProfile>(namespace, "xsd/NotificationProfile1-1.xsd", NotificationProfile.class);
			schemaHelpers.put(namespace, result);
		}
		return result;
	}

	/**
	 * Gets the recommendation result schema helper.
	 *
	 * @return the recommendation result schema helper
	 */
	@SuppressWarnings("unchecked")
	public static SchemaHelper<RecommendationResult> getRecommendationResultSchemaHelper() {
		String namespace = "de.p23r.leitstelle.ns.p23r.nrl.recommendationresult1_1";
		SchemaHelper<RecommendationResult> result = (SchemaHelper<RecommendationResult>) schemaHelpers.get(namespace);

		if (result == null) {
			result = new SchemaHelper<RecommendationResult>(namespace, "xsd/RecommendationResult1-1.xsd", RecommendationResult.class);
			schemaHelpers.put(namespace, result);
		}
		return result;
	}

	/**
	 * Gets the multi notification profile output schema helper.
	 *
	 * @return the multi notification profile output schema helper
	 */
	@SuppressWarnings("unchecked")
	public static SchemaHelper<MultiNotificationProfileOutput> getMultiNotificationProfileOutputSchemaHelper() {
		String namespace = "de.p23r.leitstelle.ns.p23r.nrl.multinotificationprofileoutput1_1";
		SchemaHelper<MultiNotificationProfileOutput> result = (SchemaHelper<MultiNotificationProfileOutput>) schemaHelpers.get(namespace);

		if (result == null) {
			result = new SchemaHelper<MultiNotificationProfileOutput>(namespace, "xsd/MultiNotificationProfileOutput1-1.xsd", MultiNotificationProfileOutput.class);
			schemaHelpers.put(namespace, result);
		}
		return result;
	}

	/**
	 * Gets the notification rule package manifest schema helper.
	 *
	 * @return the notification rule package manifest schema helper
	 */
	@SuppressWarnings("unchecked")
	public static SchemaHelper<RulePackageManifest> getNotificationRulePackageManifestSchemaHelper() {
		String namespace = "de.p23r.leitstelle.ns.p23r.nrl.rulepackagemanifest1_1";
		SchemaHelper<RulePackageManifest> result = (SchemaHelper<RulePackageManifest>) schemaHelpers.get(namespace);

		if (result == null) {
			result = new SchemaHelper<RulePackageManifest>(namespace, "xsd/RulePackageManifest1-1.xsd", RulePackageManifest.class);
			schemaHelpers.put(namespace, result);
		}
		return result;
	}

	/**
	 * Gets the notification rule group manifest schema helper.
	 *
	 * @return the notification rule group manifest schema helper
	 */
	@SuppressWarnings("unchecked")
	public static SchemaHelper<RuleGroupManifest> getNotificationRuleGroupManifestSchemaHelper() {
		String namespace = "de.p23r.leitstelle.ns.p23r.nrl.rulegroupmanifest1_1";
		SchemaHelper<RuleGroupManifest> result = (SchemaHelper<RuleGroupManifest>) schemaHelpers.get(namespace);

		if (result == null) {
			result = new SchemaHelper<RuleGroupManifest>(namespace, "xsd/RuleGroupManifest1-1.xsd", RuleGroupManifest.class);
			schemaHelpers.put(namespace, result);
		}
		return result;
	}

	/**
	 * Gets the notification rule manifest schema helper.
	 *
	 * @return the notification rule manifest schema helper
	 */
	@SuppressWarnings("unchecked")
	public static SchemaHelper<RuleManifest> getNotificationRuleManifestSchemaHelper() {
		String namespace = "de.p23r.leitstelle.ns.p23r.nrl.rulemanifest1_1";
		SchemaHelper<RuleManifest> result = (SchemaHelper<RuleManifest>) schemaHelpers.get(namespace);

		if (result == null) {
			result = new SchemaHelper<RuleManifest>(namespace, "xsd/RuleManifest1-1.xsd", RuleManifest.class);
			schemaHelpers.put(namespace, result);
		}
		return result;
	}

	/**
	 * Gets the model package manifest schema helper.
	 *
	 * @return the model package manifest schema helper
	 */
	@SuppressWarnings("unchecked")
	public static SchemaHelper<ModelPackageManifest> getModelPackageManifestSchemaHelper() {
		String namespace = "de.p23r.leitstelle.ns.p23r.nrl.modelpackagemanifest1_1";
		SchemaHelper<ModelPackageManifest> result = (SchemaHelper<ModelPackageManifest>) schemaHelpers.get(namespace);

		if (result == null) {
			result = new SchemaHelper<ModelPackageManifest>(namespace, "xsd/ModelPackageManifest1-1.xsd", ModelPackageManifest.class);
			schemaHelpers.put(namespace, result);
		}
		return result;
	}

	/**
	 * Gets the model manifest schema helper.
	 *
	 * @return the model manifest schema helper
	 */
	@SuppressWarnings("unchecked")
	public static SchemaHelper<ModelManifest> getModelManifestSchemaHelper() {
		String namespace = "de.p23r.leitstelle.ns.p23r.nrl.modelmanifest1_1";
		SchemaHelper<ModelManifest> result = (SchemaHelper<ModelManifest>) schemaHelpers.get(namespace);

		if (result == null) {
			result = new SchemaHelper<ModelManifest>(namespace, "xsd/ModelManifest1-1.xsd", ModelManifest.class);
			schemaHelpers.put(namespace, result);
		}
		return result;
	}

	/**
	 * Gets the package list manifest schema helper.
	 *
	 * @return the package list manifest schema helper
	 */
	@SuppressWarnings("unchecked")
	public static SchemaHelper<PackageList> getPackageListManifestSchemaHelper() {
		String namespace = "de.p23r.leitstelle.ns.p23r.nrl.packagelist1_1";
		SchemaHelper<PackageList> result = (SchemaHelper<PackageList>) schemaHelpers.get(namespace);

		if (result == null) {
			result = new SchemaHelper<PackageList>(namespace, "xsd/PackageList1-1.xsd", PackageList.class);
			schemaHelpers.put(namespace, result);
		}
		return result;
	}

	/**
	 * Gets the configuration result schema helper.
	 *
	 * @return the configuration result schema helper
	 */
	@SuppressWarnings("unchecked")
	public static SchemaHelper<ConfigurationResult> getConfigurationResultSchemaHelper() {
		String namespace = "de.p23r.leitstelle.ns.p23r.nrl.configurationresult1_0";
		SchemaHelper<ConfigurationResult> result = (SchemaHelper<ConfigurationResult>) schemaHelpers.get(namespace);
		
		if (result == null) {
			result = new SchemaHelper<ConfigurationResult>(namespace, "xsd/ConfigurationResult1-0.xsd", ConfigurationResult.class);
			schemaHelpers.put(namespace, result);
		}
		return result;
	}

	/**
	 * Gets the test package manifest schema helper.
	 *
	 * @return the test package manifest schema helper
	 */
	@SuppressWarnings("unchecked")
	public static SchemaHelper<TestPackageManifest> getTestPackageManifestSchemaHelper() {
		String namespace = "de.p23r.leitstelle.ns.p23r.nrl.testpackagemanifest1_0";
		SchemaHelper<TestPackageManifest> result = (SchemaHelper<TestPackageManifest>) schemaHelpers.get(namespace);

		if (result == null) {
			result = new SchemaHelper<TestPackageManifest>(namespace, "schema/TestPackageManifest1-0.xsd", TestPackageManifest.class);
			schemaHelpers.put(namespace, result);
		}
		return result;
	}

	/**
	 * Gets the test set manifest schema helper.
	 *
	 * @return the test set manifest schema helper
	 */
	@SuppressWarnings("unchecked")
	public static SchemaHelper<TestSetManifest> getTestSetManifestSchemaHelper() {
		String namespace = "de.p23r.leitstelle.ns.p23r.nrl.testsetmanifest1_1";
		SchemaHelper<TestSetManifest> result = (SchemaHelper<TestSetManifest>) schemaHelpers.get(namespace);

		if (result == null) {
			result = new SchemaHelper<TestSetManifest>(namespace, "schema/TestSetManifest1-0.xsd", TestSetManifest.class);
			schemaHelpers.put(namespace, result);
		}
		return result;
	}

	/**
	 * Gets the test case manifest schema helper.
	 *
	 * @return the test case manifest schema helper
	 */
	@SuppressWarnings("unchecked")
	public static SchemaHelper<TestCaseManifest> getTestCaseManifestSchemaHelper() {
		String namespace = "de.p23r.leitstelle.ns.p23r.nrl.testcasemanifest1_0";
		SchemaHelper<TestCaseManifest> result = (SchemaHelper<TestCaseManifest>) schemaHelpers.get(namespace);

		if (result == null) {
			result = new SchemaHelper<TestCaseManifest>(namespace, "schema/TestCaseManifest1-0.xsd", TestCaseManifest.class);
			schemaHelpers.put(namespace, result);
		}
		return result;
	}

	/**
	 * Gets the dynamic schema helper.
	 *
	 * @param schema the schema
	 * @return the dynamic schema helper
	 */
	public static SchemaHelper<Object> getDynamicSchemaHelper(String schema) {
		return new SchemaHelper<Object>(schema);
	}

}
