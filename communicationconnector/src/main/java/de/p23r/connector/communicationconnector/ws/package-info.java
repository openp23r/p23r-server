/**
 * Provides the web service implementation classes for the generic communication connector.
 */
package de.p23r.connector.communicationconnector.ws;