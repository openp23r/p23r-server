package de.p23r.server.testmanager.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.JsonNode;

import de.p23r.server.messagereceiver.JsonMessage;

public interface RestClientInterface {

	@POST
	@Path("/")
	@Consumes("application/json")
	@Produces("application/xml")
	public Response test(JsonNode body);

	@POST
	@Path("/processor/{tenant}/deliver/{context}")
	@Consumes("application/json")
	@Produces("text/plain")
	public String deliverJsonMessage(JsonMessage message, @PathParam("tenant") String tenant, @PathParam("context") String context);
	
}
