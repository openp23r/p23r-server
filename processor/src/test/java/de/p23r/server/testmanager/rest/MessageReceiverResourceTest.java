package de.p23r.server.testmanager.rest;

import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.jboss.resteasy.client.jaxrs.engines.ApacheHttpClient4Engine;
import org.testng.annotations.Test;

import de.p23r.server.messagereceiver.JsonMessage;

import org.testng.annotations.BeforeTest;

public class MessageReceiverResourceTest {

	
	private RestClientInterface clientInterface;

	@BeforeTest
	public void beforeTest() {
		ClientConnectionManager cm = new PoolingClientConnectionManager();
		DefaultHttpClient httpClient = new DefaultHttpClient(cm);
		ApacheHttpClient4Engine engine = new ApacheHttpClient4Engine(httpClient);

		ResteasyClient client = new ResteasyClientBuilder().httpEngine(engine).build();
		ResteasyWebTarget target = client.target("http://localhost:8080/messagereceiver/rest");
		clientInterface = target.proxy(RestClientInterface.class);
	}

	@Test
	public void deliverJsonMessage() {
		JsonMessage message = new JsonMessage();
		message.setMessage("a message");

		String messageId = clientInterface.deliverJsonMessage(message, "default", "local");
		System.out.println("message id: " + messageId);
	}
	
}
