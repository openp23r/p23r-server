package de.p23r.server.testmanager.rest;

import javax.ws.rs.core.Response.Status;

import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.jboss.resteasy.client.jaxrs.engines.ApacheHttpClient4Engine;
import org.jboss.resteasy.client.jaxrs.internal.ClientResponse;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

public class TestPackageResourceTest {

	
	private RestClientInterface clientInterface;

	@BeforeTest
	public void beforeTest() {
		ClientConnectionManager cm = new PoolingClientConnectionManager();
		DefaultHttpClient httpClient = new DefaultHttpClient(cm);
		ApacheHttpClient4Engine engine = new ApacheHttpClient4Engine(httpClient);

		ResteasyClient client = new ResteasyClientBuilder().httpEngine(engine).build();
		ResteasyWebTarget target = client.target("http://localhost:8080/processor/test");
		clientInterface = target.proxy(RestClientInterface.class);
	}

	@Test
	public void test() {
		ObjectNode body = new ObjectMapper().createObjectNode();
		body.put("testpackage", "https://projekt-leitstelle-adpsw-projektleitstelle.p23r.de/TP-ADPSW-Projektleitstelle.zip");
		body.put("rulepackage", "https://projekt-leitstelle-adpsw-projektleitstelle.p23r.de/NRP-ADPSW-Projektleitstelle.zip");

		body.put("username", "adpsw-projektleitstelle");
		body.put("password", "0hczkwre");
		
//		ArrayNode testsets = new ObjectMapper().createArrayNode();
//		testsets.add("f1fcaa82-c0fb-41f7-91fc-b4e6aaf80157");
//		testsets.add("5d70c9c8-7b44-464a-a9ba-0b0a83eb986a");

//		body.put("selectedTestSets", testsets);
		
		long duration = System.currentTimeMillis();
		ClientResponse response = (ClientResponse)clientInterface.test(body);
		duration = System.currentTimeMillis() - duration;
		System.out.println("test time duration " + duration + "ms");
		if (response.getStatusInfo() == Status.OK) {
			String result = response.readEntity(String.class);
			System.out.println(result);
		} else {
			System.out.println(response.getStatusInfo().getReasonPhrase());
		}
		
		response.close();
	}

	@Test
	public void testStarter() {
		ObjectNode body = new ObjectMapper().createObjectNode();
		body.put("testpackage", "https://projekt-leitstelle-starter-projektleitstelle.p23r.de/TP-starter-projektleitstelle.zip");
		body.put("rulepackage", "https://projekt-leitstelle-starter-projektleitstelle.p23r.de/NRP-starter-projektleitstelle.zip");

		body.put("username", "starter-projektleitstelle");
		body.put("password", "hudt0t4p");
		
//		ArrayNode testsets = new ObjectMapper().createArrayNode();
//		testsets.add("f1fcaa82-c0fb-41f7-91fc-b4e6aaf80157");
//		testsets.add("5d70c9c8-7b44-464a-a9ba-0b0a83eb986a");

//		body.put("selectedTestSets", testsets);
		
		long duration = System.currentTimeMillis();
		ClientResponse response = (ClientResponse)clientInterface.test(body);
		duration = System.currentTimeMillis() - duration;
		System.out.println("test time duration " + duration + "ms");
		if (response.getStatusInfo() == Status.OK) {
			String result = response.readEntity(String.class);
			System.out.println(result);
		} else {
			System.out.println(response.getStatusInfo().getReasonPhrase());
		}
		
		response.close();
	}
	
	@Test
	public void testLocal() {
		ObjectNode body = new ObjectMapper().createObjectNode();
		body.put("testpackage", "http://localhost:8080/TP-PackagePingTest.zip");
		body.put("rulepackage", "http://localhost:8080/NRP-PackagePingTest.zip");
		
		ArrayNode testsets = new ObjectMapper().createArrayNode();
		testsets.add("582d74e3-d5f2-4e1d-86c3-91cd44225da4");
		body.put("selectedTestSets", testsets);
		
		long duration = System.currentTimeMillis();
		ClientResponse response = (ClientResponse)clientInterface.test(body);
		duration = System.currentTimeMillis() - duration;
		System.out.println("test time duration " + duration + "ms");
		if (response.getStatusInfo() == Status.OK) {
			String result = response.readEntity(String.class);
			System.out.println(result);
		} else {
			System.out.println(response.getStatusInfo().getReasonPhrase());
		}
		
		response.close();
	}
	
	@Test
	public void testLocal2() {
		ObjectNode body = new ObjectMapper().createObjectNode();
		body.put("testpackage", "http://localhost:8080/TP-PackagePingPingTest.zip");
		body.put("rulepackage", "http://localhost:8080/NRP-PackagePingPingTest.zip");
		
		ArrayNode testsets = new ObjectMapper().createArrayNode();
		testsets.add("582d74e3-d5f2-4e1d-86c3-91cd44225da4");
		body.put("selectedTestSets", testsets);
		
		long duration = System.currentTimeMillis();
		ClientResponse response = (ClientResponse)clientInterface.test(body);
		duration = System.currentTimeMillis() - duration;
		System.out.println("test time duration " + duration + "ms");
		if (response.getStatusInfo() == Status.OK) {
			String result = response.readEntity(String.class);
			System.out.println(result);
		} else {
			System.out.println(response.getStatusInfo().getReasonPhrase());
		}
		
		response.close();
	}
	
}
