package de.p23r.server.common.compiler;


import java.io.InputStream;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class P23RSelectionCompilerTest {

	private static final String CLI_COMPILER = "/program files/nodejs/node /users/sim/git/p23rselectioncompiler/cli/js/dats2xquery.js";
	
	@DataProvider
	public Object[][] resources1() {
		return new Object[][] {
			{ "test0.dats" },
			{ "test1.dats" },
			{ "test2.dats" }
		};
	}
	
	@Test(dataProvider="resources1")
	public void compileSelectionScripts(String script) {
		InputStream input = Thread.currentThread().getContextClassLoader().getResourceAsStream(script);
		
		P23RSelectionCompiler compiler = P23RSelectionCompiler.newCompiler(CLI_COMPILER);
		String result = compiler.compileSelection(input, "processor");
		if (!compiler.isFailure()) {
			System.out.println(result);			
		}
		assert !compiler.isFailure() : compiler.getErrors();
	}
	
	@DataProvider
	public Object[][] resources2() {
		return new Object[][] {
			{ "test3.dats" },
			{ "test4.dats" }
		};
	}
	
	@Test(dataProvider="resources2")
	public void compileSourceDataSelectionScripts(String script) {
		InputStream input = Thread.currentThread().getContextClassLoader().getResourceAsStream(script);
		
		P23RSelectionCompiler compiler = P23RSelectionCompiler.newCompiler(CLI_COMPILER);
		String result = compiler.compileSelection(input, "connector");
		if (!compiler.isFailure()) {
			System.out.println(result);			
		}
		assert !compiler.isFailure() : compiler.getErrors();
	}
	
	@DataProvider
	public Object[][] fragments() {
		return new Object[][] {
			{ "this is a comment" },
			{ "\tmodel X=http://blub.de\n\ttag{X}" },
			{ "\tsimpletag { \"hello\" \" world\" }" },
			{ "\tsimpletag { \"hello\" + \" world\" }" },
			{ "\touter { inner {} }" },
			{ "\touter { inner {} inner {} }" },
			{ "\touter { inner { @attr { \"hello world\" } } }" }
		};
	}
	
	@Test(dataProvider="fragments")
	public void compileSelectionFragments(String fragment) {
		P23RSelectionCompiler compiler = P23RSelectionCompiler.newCompiler(CLI_COMPILER);
		String result = compiler.compileSelection(fragment, "processor");
		if (!compiler.isFailure()) {
			System.out.println(result);			
		}
		assert !compiler.isFailure() : compiler.getErrors();		
	}
	
}
