	notificationProfile {
		@xmlns { "http://leitstelle.p23r.de/NS/p23r/nrl/NotificationProfile1-1" }
		@tenant { [ for profile return profile.notificationProfile@tenant ] }
		[
			for profile
			return 
				profile.notificationProfile.*
		]
	}