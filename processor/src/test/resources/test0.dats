# Selection.dats

Zunächst werden die Antragstellerdaten dem Modell Applicant zugeordnet.

	model Applicant = http://leitstelle.p23r.de/NS/P23R/ruleGroups/Starter/enterpriseCore1-0

Die Benachrichtigung mit dem Namensraum _Selection_ wird erzeugt.

	selection {
		@xmlns { "http://leitstelle.p23r.de/NS/P23R/ruleGroups/Starter/AddressChange/Selection1-0" }
		[

Die beiden letzten Adressen werden aus dem Unternehmsprofil gefiltert und kopiert.

			for each applicant in Applicant.enterpriseCore.profile
			take 1
			return profile {
				@xmlns { "http://leitstelle.p23r.de/NS/P23R/ruleGroups/Starter/AddressChange/Selection1-0" }
				company {
					@name {applicant.company.name}
				}
				[
					for each location in applicant.headquarters.addresses
					orderby location.validfrom
					take -2
					return addresses {
						@validFrom { location.validfrom }
						@lastModified { location.lastmodified }
						@houseNumber { location.housenumber }
						@street { location.street }
						@city { location.city }
						@zipCode { location.zipCode }
						@country { location.country }
					}
				]
			}
		]
	}