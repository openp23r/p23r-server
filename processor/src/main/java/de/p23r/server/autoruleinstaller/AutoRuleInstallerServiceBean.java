package de.p23r.server.autoruleinstaller;

import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.switchyard.component.bean.Reference;
import org.switchyard.component.bean.Service;
import org.xmldb.api.base.Collection;

import de.p23r.common.existdb.P23RXmlDbConfig;
import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault_Exception;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.GetNotificationRulePackageStates;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.GetNotificationRulePackageStatesResponse;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.UpdateNotificationRulePackages;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.UpdateNotificationRulePackagesResponse;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.types.RuleGroupStateType;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.types.RulePackageStateType;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.types.RuleStateType;
import de.p23r.server.modelandrulemanagement.ModelAndRuleManagementService;
import de.p23r.server.modelandrulemanagement.persistence.PackageListsRepository;
import de.p23r.server.modelandrulemanagement.persistence.RulePackageRepository;

@Service(AutoRuleInstallerService.class)
public class AutoRuleInstallerServiceBean implements AutoRuleInstallerService {

	private final Logger log = LoggerFactory.getLogger(getClass());
	
	@Inject
	@Reference
	private ModelAndRuleManagementService marm;

	@Inject
	private P23RXmlDbConfig config;

	@Inject
	private PackageListsRepository packageListsRepository;
	
	@Inject
	private RulePackageRepository rulePackageRepository;
	
	@Override
	public void run() {
		marm.internalUpdate();

		String autoRuleActivate = config.get("/configuration/controlcentres/controlcentre[@id = 'default']/autoRuleActivate/text()", null);
		if (autoRuleActivate == null) {
			log.debug("no rules configured for auto activation.");
		} else {
			log.debug("auto activation for {}.", autoRuleActivate);
			String[] rules = autoRuleActivate.split(" ");
			for (String rule : rules) {
				String[] path = rule.split(":");
				if (path.length != 2) {
					continue;
				}
				
				String ruleGroupName = path[0];
				String ruleName = path[1];
				
				RuleStateType ruleState = packageListsRepository.getRuleStateByName("default", ruleGroupName, ruleName);
				if (true) {
				}
			}
		}
	}

}
