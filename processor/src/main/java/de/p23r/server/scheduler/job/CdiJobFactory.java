package de.p23r.server.scheduler.job;

import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;

import org.apache.deltaspike.core.api.literal.DefaultLiteral;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.spi.JobFactory;
import org.quartz.spi.TriggerFiredBundle;

/**
 * A factory for creating CdiJob objects.
 *
 * @author sim
 */
public class CdiJobFactory implements JobFactory {

    private BeanManager beanManager;

    /**
     * Instantiates a new cdi job factory.
     *
     * @param beanManager the bean manager
     */
    public CdiJobFactory(BeanManager beanManager) {
        this.beanManager = beanManager;
    }

	/* (non-Javadoc)
	 * @see org.quartz.spi.JobFactory#newJob(org.quartz.spi.TriggerFiredBundle, org.quartz.Scheduler)
	 */
	@Override
	public Job newJob(TriggerFiredBundle bundle, Scheduler scheduler) throws SchedulerException {
        JobDetail jobDetail = bundle.getJobDetail();
        Class<?> jobClass = jobDetail.getJobClass();

        try {
            if (jobClass.getInterfaces().length > 0) {
//			for (Class<?> interfaceClass : jobClass.getInterfaces()) {
                Bean<?> bean = beanManager.getBeans(jobClass, new DefaultLiteral()).iterator().next();
                CreationalContext<?> ctx = beanManager.createCreationalContext(bean);
                return (Job) beanManager.getReference(bean, bean.getBeanClass(), ctx);
            }

            return (Job) jobClass.newInstance();
        } catch (Exception e) {
            SchedulerException se = new SchedulerException("Problem instantiating class '"
                    + jobDetail.getJobClass().getName() + "'", e);
            throw se;
        }
	}
}
