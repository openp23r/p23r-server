package de.p23r.server.scheduler;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * The Class JobDescriptionParameter.
 *
 * @author sim
 */
public class JobDescriptionParameter implements Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = -1386537025087061015L;

    private String type = "schedule";

    private String manifest;

    private String schema;

    private String serviceName;

    private String actionName;

    private Map<String, String> parameters;

    private Date executionDate;

    /**
     * Gets the type.
     *
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the type.
     *
     * @param type the new type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Gets the manifest.
     *
     * @return the manifest
     */
    public String getManifest() {
        return manifest;
    }

    /**
     * Sets the manifest.
     *
     * @param manifest the new manifest
     */
    public void setManifest(String manifest) {
        this.manifest = manifest;
    }

    /**
     * Gets the schema.
     *
     * @return the schema
     */
    public String getSchema() {
        return schema;
    }

    /**
     * Sets the schema.
     *
     * @param schema the new schema
     */
    public void setSchema(String schema) {
        this.schema = schema;
    }

    /**
     * Gets the service name.
     *
     * @return the service name
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     * Sets the service name.
     *
     * @param serviceName the new service name
     */
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    /**
     * Gets the action name.
     *
     * @return the action name
     */
    public String getActionName() {
        return actionName;
    }

    /**
     * Sets the action name.
     *
     * @param actionName the new action name
     */
    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    /**
     * Gets the execution date.
     *
     * @return the execution date
     */
    public Date getExecutionDate() {
        return executionDate != null ? (Date) executionDate.clone() : null;
    }

    /**
     * Sets the execution date.
     *
     * @param executionDate the new execution date
     */
    public void setExecutionDate(Date executionDate) {
        this.executionDate = (Date) executionDate.clone();
    }

    /**
     * Gets the parameters.
     *
     * @return the parameters
     */
    public Map<String, String> getParameters() {
        return parameters;
    }

    /**
     * Sets the parameters.
     *
     * @param parameters the parameters
     */
    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }

}
