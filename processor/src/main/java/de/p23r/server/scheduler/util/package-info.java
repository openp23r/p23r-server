/**
 * Provides utility helper classes for the scheduler.
 */
package de.p23r.server.scheduler.util;