/**
 * Provides classes for the scheduler service. An interface, an implementation bean class and parameter classes for the interface.
 */
package de.p23r.server.scheduler;