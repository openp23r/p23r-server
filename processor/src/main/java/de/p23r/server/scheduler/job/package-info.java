/**
 * Provides implementations for the different scheduling jobs.
 */
package de.p23r.server.scheduler.job;