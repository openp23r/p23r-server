package de.p23r.server.scheduler;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.event.Event;
import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
//import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.switchyard.component.bean.Reference;
import org.switchyard.component.bean.Service;

import de.p23r.common.modelandrulemanagement.SchemaHelperFactory;
import de.p23r.leitstelle.ns.p23r.nrl.rulemanifest1_1.RuleManifest;
import de.p23r.server.common.P23RError;
import de.p23r.server.common.annotations.P23RUnit;
import de.p23r.server.exceptions.P23RServiceException;
import de.p23r.server.messagereceiver.MessageReceiverService;
import de.p23r.server.protocolpool.Protocol;
import de.p23r.server.scheduler.job.CdiJobFactory;
import de.p23r.server.scheduler.job.ScheduleJob;
import de.p23r.server.scheduler.job.TimerJob;
import de.p23r.server.scheduler.util.TriggerCalculation;
import static de.p23r.server.common.Messages.*;

/**
 * The Class SchedulerServiceBean.
 *
 * @author sim
 */
@Service(SchedulerService.class)
public class SchedulerServiceBean implements SchedulerService {

	@Inject
	private Logger log;

	private static final String COMPONENT_NAME = message(SCHEDULER);

//	@Inject
//	@Reference
//	private MessageReceiverService receiver;

	@Inject @P23RUnit
	private Protocol protocol;

	@Inject
	private BeanManager beanManager;

	@Inject @P23RUnit
	private Event<String> initialized;
	
	private Scheduler scheduler;

	/**
	 * Inits the scheduler service.
	 */
	@PostConstruct
	public void init() {
		try {
			scheduler = StdSchedulerFactory.getDefaultScheduler();
			scheduler.setJobFactory(new CdiJobFactory(beanManager));

			scheduler.start();
		} catch (Exception e) {
			scheduler = null;
			log.error("error initialization of Quartz", e);
		}

		initialized.fire(COMPONENT_NAME);
	}

	/**
	 * Release.
	 */
	@PreDestroy
	public void release() {
		if (scheduler != null) {
			try {
				scheduler.shutdown(true);
			} catch (SchedulerException e) {
				log.error("Quartz shutdown failed!", e);
			}
		}
	}

	/* (non-Javadoc)
	 * @see de.p23r.server.scheduler.SchedulerService#scheduleJob(de.p23r.server.scheduler.JobDescriptionParameter)
	 */
	@Override
	public String scheduleJob(JobDescriptionParameter jobDescription) throws P23RServiceException {
		if (scheduler == null) {
			String msg = message(SCHEDULER_SCHEDULER_NOT_AVAILABLE);
			log.error(msg);
			protocol.logError(P23RError.GENERAL, msg);
			throw new P23RServiceException(COMPONENT_NAME, msg);
		}

		String type = jobDescription.getType();
		if ("schedule".equals(type)) {
			return addScheduler(jobDescription);
		} else if ("timer".equals(type)) {
			return addTimer(jobDescription);
		} else {
			throw new P23RServiceException(COMPONENT_NAME, "unknown job type: " + type);
		}
	}

	private String addTimer(JobDescriptionParameter jobDescription) throws P23RServiceException {

		String serviceName = jobDescription.getServiceName();
		if (serviceName == null) {
			String msg = message(PARAMETER_MISSING, new String[] { "serviceName" });
			log.error(msg);
			protocol.logError(P23RError.PARAMETER_MISSING, msg);
			throw new P23RServiceException(COMPONENT_NAME, msg);
		}

		String actionName = jobDescription.getActionName();
		if (actionName == null) {
			String msg = message(PARAMETER_MISSING, new String[] { "actionName" });
			log.error(msg);
			protocol.logError(P23RError.PARAMETER_MISSING, msg);
			throw new P23RServiceException(COMPONENT_NAME, msg);
		}

		Map<String, String> parameters = jobDescription.getParameters();
		if (parameters == null) {
			String msg = message(PARAMETER_MISSING, new String[] { "parameters" });
			log.error(msg);
			protocol.logError(P23RError.PARAMETER_MISSING, msg);
			throw new P23RServiceException(COMPONENT_NAME, msg);
		}

		Date executionDate = jobDescription.getExecutionDate();
		if (executionDate == null) {
			String msg = message(PARAMETER_MISSING, new String[] { "executionDate" });
			log.error(msg);
			protocol.logError(P23RError.PARAMETER_MISSING, msg);
			throw new P23RServiceException(COMPONENT_NAME, msg);
		}

		Date date = Calendar.getInstance().getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd.hhmmss");

//		JobDetail jd = new JobDetail("JOB-" + sdf.format(date), null, TimerJob.class);
//		jd.getJobDataMap().put("servicename", serviceName);
//		jd.getJobDataMap().put("actionname", actionName);
//		jd.getJobDataMap().putAll(parameters);

		 JobDetail jd = JobBuilder.newJob(TimerJob.class).withIdentity("JOB-"
		 + sdf.format(date)).usingJobData("servicename", serviceName)
		 .usingJobData("actionname", actionName)
		 // .usingJobData("sender", sender)
		 // .usingJobData("from",
		 // message.getHeader().getCall().getFrom())
		 .build();
		 jd.getJobDataMap().putAll(parameters);

//		Trigger tr = new SimpleTrigger("TRIGGER-" + sdf.format(date), executionDate);
		
		Trigger tr = TriggerBuilder.newTrigger().withIdentity("TRIGGER-" + sdf.format(date)).startAt(executionDate).build();

		scheduleJob(jd, tr);

		protocol.logInfo(message(SCHEDULER_SUCCESSFULLY_SCHEDULED_JOB, new String[] { jd.getKey().getName(), tr.getNextFireTime().toString() }));
		return jd.getKey().getName();
	}

	/**
	 * Schedules the given job with the given trigger
	 * 
	 * @param jd
	 *            the job
	 * @param tr
	 *            the trigger
	 * @throws P23RServiceException
	 */
	private void scheduleJob(JobDetail jd, Trigger tr) throws P23RServiceException {
		if (scheduler != null) {
			try {
				scheduler.scheduleJob(jd, tr);
			} catch (SchedulerException e) {
				String msg = message(SCHEDULER_ERROR_SCHEDULING_JOB, new String[] { jd.getKey().getName() });
				log.error(msg, e);
				protocol.logError(P23RError.GENERAL, msg);
				throw new P23RServiceException(COMPONENT_NAME, msg + ": " + e.getMessage());
			}
		} else {
			log.error(message(SCHEDULER_SCHEDULER_NOT_AVAILABLE));
			protocol.logError(P23RError.GENERAL, message(SCHEDULER_SCHEDULER_NOT_AVAILABLE));
			throw new P23RServiceException(COMPONENT_NAME, message(SCHEDULER_SCHEDULER_NOT_AVAILABLE));
		}
	}

	private String addScheduler(JobDescriptionParameter jobDescription) throws P23RServiceException {

		String ruleManifest = jobDescription.getManifest();
		RuleManifest manifest = SchemaHelperFactory.getNotificationRuleManifestSchemaHelper().create(ruleManifest);

		String schema = jobDescription.getSchema();
//		JobDetail jd = new JobDetail(manifest.getId(), null, ScheduleJob.class);
//		jd.getJobDataMap().put("ruleManifest", ruleManifest);
//		jd.getJobDataMap().put("messageSchema", schema);

		 JobDetail jd =
		 JobBuilder.newJob(ScheduleJob.class).withIdentity(manifest.getId() +
		 "-JOB")
		 // .usingJobData("rule", ruleManifest)
		 .usingJobData("schema", schema)
		 // .usingJobData("sender", sender)
		 // .usingJobData("from",
		 // message.getHeader().getCall().getFrom())
		 .storeDurably().build();
		 jd.getJobDataMap().put("rule", manifest);

		try {
			scheduler.addJob(jd, true);
		} catch (SchedulerException e) {
			String msg = message(SCHEDULER_ERROR_SCHEDULING_JOB, new String[] { jd.getKey().getName() });
			log.error(msg, e);
			protocol.logError(P23RError.GENERAL, msg);
			throw new P23RServiceException(COMPONENT_NAME, msg + ": " + e.getMessage());
		}

		List<Trigger> triggers = TriggerCalculation.calculateTriggers(manifest, jd.getKey().getName());
		if (triggers.isEmpty()) {
			try {
				scheduler.deleteJob(jd.getKey());
			} catch (SchedulerException e) {
				log.warn("deleting job due to empty trigger list", e);
			}
			String msg = message(SCHEDULER_NO_TRIGGERS_FOUND, new String[] { jd.getKey().getName() });
			protocol.logInfo(manifest.getName(), manifest.getId(), manifest.getRelease(), msg);
			return null;
			// throw new P23RServiceException(msg, P23RFault.SD003);
		}

		Date first = getEarliestTriggerDate(triggers, jd);

		log.debug("calculated next trigger at {}", first.toString());
		jd.getJobDataMap().put("applyat", first);

		protocol.logInfo(manifest.getName(), manifest.getId(), manifest.getRelease(),
				message(SCHEDULER_SUCCESSFULLY_SCHEDULED_JOB, new String[] { jd.getKey().getName(), first.toString() }));
		return jd.getKey().getName();
	}

	/**
	 * Returns the earliest trigger date of the given triggers for the given
	 * job.
	 * 
	 * @param triggers
	 *            the triggers
	 * @param jd
	 *            the job
	 * @return earliest trigger date
	 * @throws P23RServiceException
	 */
	private Date getEarliestTriggerDate(List<Trigger> triggers, JobDetail jd) throws P23RServiceException {
		Date first = null;
		for (Trigger trigger : triggers) {
			try {
				Date next = scheduler.scheduleJob(trigger);
				if (first == null || next.before(first)) {
					first = next;
				}
			} catch (SchedulerException e) {
				String msg = message(SCHEDULER_ERROR_SCHEDULING_JOB, new String[] { jd.getKey().getName() });
				log.error(msg, e);
				protocol.logError(P23RError.GENERAL, msg);
				throw new P23RServiceException(COMPONENT_NAME, msg + ": " + e.getMessage());
			}
		}
		return first;
	}

	/* (non-Javadoc)
	 * @see de.p23r.server.scheduler.SchedulerService#cancelJob(java.lang.String)
	 */
	@Override
	public void cancelJob(String identifier) throws P23RServiceException {
		if (scheduler == null) {
			String msg = message(SCHEDULER_SCHEDULER_NOT_AVAILABLE);
			log.error(msg);
			protocol.logError(P23RError.GENERAL, msg);
			throw new P23RServiceException(COMPONENT_NAME, msg);
		}

		try {
			scheduler.deleteJob(JobKey.jobKey(identifier));
		} catch (SchedulerException e) {
			String msg = message(SCHEDULER_ERROR_CANCELING_JOB, new String[] { identifier });
			log.error(msg, e);
			protocol.logError(P23RError.GENERAL, msg);
			throw new P23RServiceException(COMPONENT_NAME, msg + ": " + e.getMessage());
		}
	}

}
