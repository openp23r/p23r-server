package de.p23r.server.scheduler.job;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The ScheduleJob class describes a timer job. It is used for the Quartz.
 * 
 * @author mma
 * 
 */
public class TimerJob implements Job {

    private final Logger log = LoggerFactory.getLogger(getClass());

//	private ProtocolPoolService protocolPoolService;

    /*
     * (non-Javadoc)
     * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
     */
    public final void execute(JobExecutionContext context) throws JobExecutionException {
        log.debug("timer {} triggered", context.getJobDetail().getKey().getName());

//		String servicename = (String) context.getJobDetail().getJobDataMap().get("servicename");
//		String actionname = (String) context.getJobDetail().getJobDataMap().get("actionname");
//		String parametername = (String) context.getJobDetail().getJobDataMap().get("parametername");
//		String parameter = (String) context.getJobDetail().getJobDataMap().get("parameter");
//		IMessageSender sender = (IMessageSender) context.getJobDetail().getJobDataMap().get("sender");
//
//		try {
//			if (sender == null) {
//				sender = new MessageSender(servicename);
//			}
//			sender.callService(actionname, parametername, parameter);
//			log.debug("service {} succesfully called back at ", servicename, context.getFireTime().toString());
//			protocol.logInfo(Messages.getString(Messages.SCHEDULER_SUCCESSFULLY_TRIGGERED_JOB), new String[] {context.getFireTime().toString(), servicename});
//		} catch (MessageDeliverException e) {
//			String err = "error calling service";
//			log.error(err, e);
//			protocol.logError(1, err);
//		}
    }
}
