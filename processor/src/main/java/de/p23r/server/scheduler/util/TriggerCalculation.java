package de.p23r.server.scheduler.util;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;

import org.quartz.CronScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.p23r.leitstelle.ns.p23r.nrl.rulemanifest1_1.RuleManifest;

/**
 * The class provides a method for calculating the next fire date.
 * 
 * @author ayb
 * 
 */
public final class TriggerCalculation {

	private static final Logger LOG = LoggerFactory.getLogger(TriggerCalculation.class);

	/**
	 * Prevents this class from being instantiated.
	 */
	private TriggerCalculation() {
	}

	/**
	 * Calculate triggers.
	 * 
	 * @param manifest
	 *            the manifest
	 * @param jobName
	 *            the job name
	 * @return the list
	 */
	public static final List<Trigger> calculateTriggers(RuleManifest manifest, String jobName) {
		Date minFireDate = manifest.getValidFrom().toGregorianCalendar().getTime();
		LOG.debug("min fire date {}", minFireDate.toString());
		Date maxFireDate = null;
		if (manifest.getValidUntil() != null) {
			maxFireDate = manifest.getValidUntil().toGregorianCalendar().getTime();			
			LOG.debug("max fire date {}", maxFireDate.toString());
		}


		int daysBefore = 0;
		Duration before = manifest.getCreateDaysBefore();
		if (before != null) {
			daysBefore = before.getDays();
			LOG.debug("fire {} days before", daysBefore);
		}

		List<Trigger> triggers = new ArrayList<Trigger>();

		for (XMLGregorianCalendar xgc : manifest.getSubmitAts()) {
			Date submitAt = xgc.toGregorianCalendar().getTime();
			if (submitAt.after(new Date()) && submitAt.after(minFireDate) && (maxFireDate != null && submitAt.before(maxFireDate))) {
//				Trigger trigger = new SimpleTrigger(jobName + triggers.size(), submitAt);
//				trigger.setJobName(jobName);
				 Trigger trigger =
				 TriggerBuilder.newTrigger().withIdentity(jobName +
				 triggers.size())
				 .forJob(jobName).startAt(submitAt).build();
				triggers.add(trigger);
				LOG.debug("added concrete trigger at {}", submitAt.toString());
			}
		}

		for (XMLGregorianCalendar xgc : manifest.getSubmitAtDailyPeriods()) {
			String expression = "0 " + xgc.getMinute() + " " + xgc.getHour() + " * * ?";
			addCronTrigger(triggers, expression, jobName, minFireDate, maxFireDate);
		}

		for (int dayOfWeek : manifest.getSubmitAtWeeklyPeriods()) {
			dayOfWeek -= daysBefore % 7;
			dayOfWeek = (dayOfWeek + 7) % 7;

			String expression = "0 0 0 ? * " + dayOfWeek;
			addCronTrigger(triggers, expression, jobName, minFireDate, maxFireDate);
		}

		for (XMLGregorianCalendar xgc : manifest.getSubmitAtMonthlyPeriods()) {
			Calendar calendar = xgc.toGregorianCalendar();
			calendar.roll(Calendar.DAY_OF_MONTH, -daysBefore);

			String expression = "0 0 0 " + calendar.get(Calendar.DAY_OF_MONTH) + " * ?";
			addCronTrigger(triggers, expression, jobName, minFireDate, maxFireDate);
		}

		for (XMLGregorianCalendar xgc : manifest.getSubmitAtYearlyPeriods()) {
			Calendar calendar = xgc.toGregorianCalendar();
			calendar.roll(Calendar.DAY_OF_YEAR, -daysBefore);

			String expression = "0 0 0 " + calendar.get(Calendar.DAY_OF_MONTH) + " " + (calendar.get(Calendar.MONTH) + 1) + " * ?";
			addCronTrigger(triggers, expression, jobName, minFireDate, maxFireDate);
		}

		return triggers;
	}

	private static final void addCronTrigger(List<Trigger> triggers, String expression, String jobName, Date minFireDate, Date maxFireDate) {

//		Trigger trigger = new CronTrigger(jobName + triggers.size(), expression);
//		trigger.setStartTime(minFireDate.after(new Date()) ? minFireDate : new Date());
//		trigger.setEndTime(maxFireDate);
//		trigger.setJobName(jobName);

			Trigger trigger = TriggerBuilder.newTrigger().withIdentity(jobName +
			 triggers.size()).forJob(jobName)
			 .withSchedule(CronScheduleBuilder.cronSchedule(expression))
			 .startAt(minFireDate.after(new Date()) ? minFireDate : new
			 Date()).endAt(maxFireDate).build();

			triggers.add(trigger);

			LOG.debug("added cron trigger {} with expression {}", trigger.getKey().getName(), expression);
	}

}
