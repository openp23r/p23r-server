package de.p23r.server.scheduler.job;

import java.io.IOException;
import java.io.StringReader;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.switchyard.component.bean.Reference;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import static de.p23r.server.common.Messages.*;
import de.p23r.common.modelandrulemanagement.SchemaHelper;
import de.p23r.common.modelandrulemanagement.SchemaHelperFactory;
import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault_Exception;
import de.p23r.leitstelle.ns.p23r.imessagedeliverlocal1_0.DeliverMessage;
import de.p23r.leitstelle.ns.p23r.nrl.rulemanifest1_1.RuleManifest;
import de.p23r.server.common.annotations.P23RUnit;
import de.p23r.server.messagereceiver.MessageReceiverService;
import de.p23r.server.protocolpool.Protocol;

/**
 * The ScheduleJob class describes a schedule job. It is used for the Quartz.
 * 
 * @author ayb
 * 
 */
public class ScheduleJob implements Job {

	@Inject
	private Logger log;

	@Inject @P23RUnit
	private Protocol protocol;

	@Inject
	@Reference
	private MessageReceiverService messageReceiver;

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
	 */
	public final void execute(JobExecutionContext context) throws JobExecutionException {
		log.debug("job {} triggered", context.getJobDetail().getKey().getName());

		RuleManifest rule = (RuleManifest) context.getJobDetail().getJobDataMap().get("rule");
		String schema = (String) context.getJobDetail().getJobDataMap().get("schema");
		Date applyAt = (Date) context.getJobDetail().getJobDataMap().get("applyat");
		String tenant = (String) context.getJobDetail().getJobDataMap().get("tenant");

		Map<String, Object> defValues = new HashMap<String, Object>();
		defValues.put("applyat", applyAt);

		SchemaHelper<Object> helper = SchemaHelperFactory.getDynamicSchemaHelper(schema);
		String data = helper.createDefaultInstance("message");

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			log.error("document builder", e);
			return;
		}

		Document doc;
		try {
			doc = builder.parse(new InputSource(new StringReader(data)));
		} catch (SAXException e) {
			log.error("parsing message error", e);
			return;
		} catch (IOException e) {
			log.error("parsing message error", e);
			return;
		}

		// MessageDescriptionParameter messageDescription = new
		// MessageDescriptionParameter();
		// messageDescription.setMessage(doc);
		// messageDescription.setOriginatorId("p23r");
		// messageDescription.setSourceInterface("scheduler");
		// messageDescription.setSourceUri("p23r:local");

		DeliverMessage request = new DeliverMessage();
		request.setTenant(tenant);
		request.setMessage(doc.getDocumentElement());
		try {
			messageReceiver.deliverMessage(request);
		} catch (P23RAppFault_Exception e) {
			log.error("deliver message", e);
			return;
		}

		Date next = context.getNextFireTime();
		context.getJobDetail().getJobDataMap().put("applyat", next);

		if (next != null) {
			protocol.logInfo(rule.getName(), rule.getId(), rule.getRelease(),
					message(SCHEDULER_TRIGGER_NEXT, new String[] { context.getJobDetail().getKey().getName(), next.toString() }));
		} else {
			protocol.logInfo(rule.getName(), rule.getId(), rule.getRelease(),
					message(SCHEDULER_TRIGGER_FINAL, new String[] { context.getJobDetail().getKey().getName() }));
		}
	}
}
