package de.p23r.server.scheduler;

import de.p23r.server.exceptions.P23RServiceException;

/**
 * The Interface SchedulerService.
 *
 * @author sim
 */
public interface SchedulerService {

    /**
     * Schedule job.
     *
     * @param jobDescription the job description
     * @return the string
     * @throws P23RServiceException the p23 r service exception
     */
    String scheduleJob(JobDescriptionParameter jobDescription) throws P23RServiceException;
    
	/**
	 * Cancel job.
	 *
	 * @param identifier the identifier
	 * @throws P23RServiceException the p23 r service exception
	 */
	void cancelJob(String identifier) throws P23RServiceException;

}
