package de.p23r.server.datapool;

import java.io.Serializable;

/**
 * The Class SelectionTransformationParameter.
 *
 * @author sim
 */
public class SelectionTransformationParameter implements Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = -5269328915979936267L;

    private String selection;

    private String transformation;

    private String schema;

    /**
     * Gets the selection.
     *
     * @return the selection
     */
    public String getSelection() {
        return selection;
    }

    /**
     * Sets the selection.
     *
     * @param selection the new selection
     */
    public void setSelection(String selection) {
        this.selection = selection;
    }

    /**
     * Gets the transformation.
     *
     * @return the transformation
     */
    public String getTransformation() {
        return transformation;
    }

    /**
     * Sets the transformation.
     *
     * @param transformation the new transformation
     */
    public void setTransformation(String transformation) {
        this.transformation = transformation;
    }

    /**
     * Gets the schema.
     *
     * @return the schema
     */
    public String getSchema() {
        return schema;
    }

    /**
     * Sets the schema.
     *
     * @param schema the new schema
     */
    public void setSchema(String schema) {
        this.schema = schema;
    }

}
