package de.p23r.server.datapool;

public class SourceDataQueryRequest {

	private String selection;
	
	private String parameters;
	
	private String namespace;

	private String parametersFormat;
	
	public String getSelection() {
		return selection;
	}

	public void setSelection(String selection) {
		this.selection = selection;
	}

	public String getParameters() {
		return parameters;
	}

	public void setParameters(String parameters) {
		this.parameters = parameters;
	}

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public String getParametersFormat() {
		return parametersFormat;
	}

	public void setParametersFormat(String parametersFormat) {
		this.parametersFormat = parametersFormat;
	}
	
}
