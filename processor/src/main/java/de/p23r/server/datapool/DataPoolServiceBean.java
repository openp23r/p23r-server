package de.p23r.server.datapool;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.slf4j.Logger;
import org.switchyard.common.type.Classes;
import org.switchyard.component.bean.Service;

import de.p23r.common.existdb.P23RXmlDbConfig;
import de.p23r.common.existdb.P23RXmlDbResource;
import de.p23r.common.existdb.P23RXmlDbResourceContentAccess;
import de.p23r.common.existdb.SystemConfiguration;
import de.p23r.server.common.GenerationContext;
import de.p23r.server.common.NotificationProfileHelper;
import de.p23r.server.common.P23RError;
import de.p23r.server.common.TBRS;
import de.p23r.server.common.annotations.P23RUnit;
import de.p23r.server.common.compiler.P23RSelectionCompiler;
import de.p23r.server.exceptions.P23RServiceException;
import de.p23r.server.protocolpool.Protocol;
import de.p23r.server.xml.P23RProcessor;
import static de.p23r.server.common.Messages.*;

/**
 * The Class DataPoolServiceBean.
 *
 * @author sim
 */
@Service(DataPoolService.class)
public class DataPoolServiceBean implements DataPoolService {

	private static final String COMPONENT_NAME = message(DATAPOOL);

	@Inject
	private Logger log;

	@Inject @P23RUnit
	private Protocol protocol;

	@Inject
	private SystemConfiguration systemConfiguration;

	@Inject
	private P23RXmlDbConfig p23rXmlDbConfig;

	@Inject
	private P23RProcessor p23rProcessor;

	@Inject
	private P23RSelectionCompiler selectionCompiler;
	
//	@Inject
//	private Context context;
	
	@Inject @P23RUnit
	private Event<String> initialized;
	
	private static final MessageFormat XUPDATE_DELETE_GROUP_CONFIG = new MessageFormat("declare namespace cns=\"" + TBRS.NAMESPACE_CONFIGURATION + "\"; "
			+ "for $group in /cns:configuration/cns:ruleGroups[@id=\"{0}\"] return update delete $group");

	private static final MessageFormat XUPDATE_INSERT_GROUP_CONFIGS = new MessageFormat("declare namespace cns=\"" + TBRS.NAMESPACE_CONFIGURATION + "\"; "
			+ "if (exists(/cns:configuration/cns:ruleGroups[@id=\"{0}\"]))" + "then update replace /cns:configuration/cns:ruleGroups[@id=\"{0}\"] with {1} "
			+ "else update insert {1} into /cns:configuration");

	private P23RXmlDbResource configurationResource;

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
		
		configurationResource = p23rXmlDbConfig.getResource("/server/configurations.xml", false);

		//update system configuration and recover modified values
		try {
			InputStream newconfig = Classes.getResourceAsStream("configurations.xml");
			SAXBuilder builder = new SAXBuilder();
			Document conf = builder.build(newconfig);
			Element system = conf.getRootElement().getChild("system", conf.getRootElement().getNamespace());
			
			// 1. keep old values
			String id = systemConfiguration.getP23RId();
			
			// 2. write new config
			systemConfiguration.update(new XMLOutputter(Format.getPrettyFormat()).outputString(system));

			// 3. recover old values
			systemConfiguration.setP23RId(id);
			
		} catch (IOException e) {
			log.error("loading resource configuration.xml", e);
		} catch (JDOMException e) {
			log.error("parsing configuration", e);
		}

		initialized.fire(COMPONENT_NAME);
	}

	/**
	 * Destroy.
	 */
	@PreDestroy
	public void destroy() {
		if (configurationResource != null) {
			configurationResource.free();
		}
	}

	/* (non-Javadoc)
	 * @see de.p23r.server.datapool.DataPoolService#addConfiguration(de.p23r.server.datapool.SelectionTransformationParameter)
	 */
	@Override
	public void addConfiguration(SelectionTransformationParameter selectionTransformation) throws P23RServiceException {

		String selection = selectionTransformation.getSelection();
		if (selection == null) {
			String msg = message(PARAMETER_MISSING, new String[] { "selection" });
			protocol.logError(P23RError.PARAMETER_MISSING, msg);
			throw new P23RServiceException(COMPONENT_NAME, msg);
		}
		String transformation = selectionTransformation.getTransformation();
		if (transformation == null) {
			String msg = message(PARAMETER_MISSING, new String[] { "transformation" });
			protocol.logError(P23RError.PARAMETER_MISSING, msg);
			throw new P23RServiceException(COMPONENT_NAME, msg);
		}

		String selectionMode = p23rXmlDbConfig.get(P23RXmlDbConfig.XQUERY_GENERAL_SELECTIONMODE, "auto");
		if (selectionMode.equalsIgnoreCase("strict") || (selectionMode.equalsIgnoreCase("auto") && P23RSelectionCompiler.isP23RSelection(selection))) {
			selection = selectionCompiler.compileSelection(selection, "processor");
		}
		
		Map<String, String> docs = new HashMap<String, String>();
		docs.put(TBRS.NAMESPACE_CONFIGURATION, P23RXmlDbResourceContentAccess.getContentAsString(configurationResource));

		GenerationContext context = new GenerationContext();
		context.setClientConfig("default");
		
		String selectionResult = p23rProcessor.query(selection, docs, context);
		if (selectionResult == null || selectionResult.isEmpty()) {
			log.error("rule group configuration selection result is empty");
		} else {
			String transformationResult = p23rProcessor.transform(transformation, selectionResult, docs, context);

			if (!p23rProcessor.validate(transformationResult, null)) {
				String msg = message(DATAPOOL_INVALID_CONFIGURATION_RESULT);
				protocol.logError(P23RError.VALIDATION_FAILURE, msg);
				throw new P23RServiceException(COMPONENT_NAME, msg);
			}

			// got the transformation result -> update the configuration
			addConfiguration(transformationResult);
		}
	}

	/**
	 * Adds a new configuration for the given transformation result.
	 * 
	 * @param transformationResult
	 *            the transformation result
	 * @throws P23RServiceException
	 */
	private void addConfiguration(String transformationResult) throws P23RServiceException {
		Document configurationResult = null;
		SAXBuilder builder = new SAXBuilder();
		try {
			configurationResult = builder.build(new ByteArrayInputStream(transformationResult.getBytes(TBRS.CHARSET)));
		} catch (JDOMException e) {
			log.error("parsing transformation result", e);
			protocol.logError(P23RError.GENERAL, "configuration build of transformation result " + transformationResult + " failed");
			throw new P23RServiceException(COMPONENT_NAME, message(DATAPOOL_INVALID_CONFIGURATION_RESULT));
		} catch (IOException e) {
			log.error("reading transformation result", e);
			protocol.logError(P23RError.GENERAL, "configuration build of transformation result " + transformationResult + " failed");
			throw new P23RServiceException(COMPONENT_NAME, message(DATAPOOL_INVALID_CONFIGURATION_RESULT));
		}

		if (configurationResult != null) {
			Element ruleGroup = configurationResult.getRootElement().getChild("ruleGroups", Namespace.getNamespace(TBRS.NAMESPACE_CONFIGURATIONRESULT));
			ruleGroup.detach();
			ruleGroup.setNamespace(Namespace.getNamespace(TBRS.NAMESPACE_CONFIGURATION));

			String groupConfiguration = new XMLOutputter().outputString(ruleGroup);
			String xupdate = XUPDATE_INSERT_GROUP_CONFIGS.format(new Object[] { ruleGroup.getAttributeValue("id"), groupConfiguration });

			if (configurationResource.query(xupdate) != null) {
				log.debug("insertion of configuration {} was successfull", groupConfiguration);
			} else {
				protocol.logError(P23RError.GENERAL, "insertion of configuration " + groupConfiguration + " failed");
				throw new P23RServiceException(COMPONENT_NAME, message(DATAPOOL_INVALID_CONFIGURATION_RESULT));
			}
		}
	}

	/* (non-Javadoc)
	 * @see de.p23r.server.datapool.DataPoolService#deleteConfiguration(java.lang.String)
	 */
	@Override
	public void deleteConfiguration(String groupId) throws P23RServiceException {

		if (groupId == null) {
			String msg = message(PARAMETER_MISSING, new String[] { "groupId" });
			protocol.logError(P23RError.PARAMETER_MISSING, msg);
			throw new P23RServiceException(COMPONENT_NAME, msg);
		}

		String xupdate = XUPDATE_DELETE_GROUP_CONFIG.format(new Object[] { groupId });

		if (configurationResource.query(xupdate) != null) {
			log.debug("deletion of configuration was successfull");
		} else {
			protocol.logError(P23RError.GENERAL, "deletion of configuration failed");
			throw new P23RServiceException(COMPONENT_NAME, message(DATAPOOL_INVALID_CONFIGURATION_RESULT));
		}
	}

	/* (non-Javadoc)
	 * @see de.p23r.server.datapool.DataPoolService#getNamespaceList(java.lang.String)
	 */
	@Override
	public List<String> getNamespaceList(String tenant) {
		List<String> namespaces = new ArrayList<String>();
		namespaces.add(TBRS.NAMESPACE_CONFIGURATION);
		namespaces.addAll(p23rXmlDbConfig.getAll(String.format(P23RXmlDbConfig.XQUERY_NAMESPACES, tenant)));
		return namespaces;
	}

	/* (non-Javadoc)
	 * @see de.p23r.server.datapool.DataPoolService#selectData(de.p23r.server.common.GenerationContext)
	 */
	@Override
	public String selectData(GenerationContext context) throws P23RServiceException {
		
		// check selection script
		String selection = context.getProperties().get(GenerationContext.SELECTION_SCRIPT);
		if (selection == null) {
			String msg = message(PARAMETER_MISSING, new String[] { "selection" });
			protocol.logError(P23RError.PARAMETER_MISSING, msg);
			throw new P23RServiceException(COMPONENT_NAME, msg);
		}

		String selectionMode = p23rXmlDbConfig.get(P23RXmlDbConfig.XQUERY_GENERAL_SELECTIONMODE, "auto");
		if (selectionMode.equalsIgnoreCase("strict") || (selectionMode.equalsIgnoreCase("auto") && P23RSelectionCompiler.isP23RSelection(selection))) {
			selection = selectionCompiler.compileSelection(selection, "processor");
		}
		
		// building a namespace->resource map for resolving documents
		Map<String, String> docs = new HashMap<String, String>();

		// add profile if available
		NotificationProfileHelper profileHelper = context.getProfile();
		if (profileHelper != null) {
			log.debug("received selection for {}: {}", profileHelper.getNotificationId(), selection);
			docs.put(TBRS.NAMESPACE_NOTIFICATIONPROFILE, profileHelper.toString());
			protocol.logInfo(profileHelper, message(DATAPOOL_DATASELECTION_RECEIVED));
		} else {
			log.debug("received selection: {}", selection);
			protocol.logInfo(message(DATAPOOL_DATASELECTION_RECEIVED));
		}

		// add configuration
		docs.put(TBRS.NAMESPACE_CONFIGURATION, P23RXmlDbResourceContentAccess.getContentAsString(configurationResource));

		String notification = p23rProcessor.query(selection, docs, context);
		log.debug("selection result: {}", notification);

		if (profileHelper != null) {
			protocol.logInfo(profileHelper, message(DATAPOOL_DATASELECTION_SUCCESSFULL));
		} else {
			protocol.logInfo(message(DATAPOOL_DATASELECTION_SUCCESSFULL));
		}
		return notification;
	}

}
