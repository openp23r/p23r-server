package de.p23r.server.datapool;

import java.util.List;

import de.p23r.server.common.GenerationContext;
import de.p23r.server.exceptions.P23RServiceException;

/**
 * The Interface DataPoolService.
 *
 * @author sim
 */
public interface DataPoolService {

	/**
	 * Gets the namespace list.
	 *
	 * @param tenant the tenant
	 * @return the namespace list
	 */
	List<String> getNamespaceList(String tenant);

	/**
	 * Adds a configuration.
	 *
	 * @param parameter the parameter
	 * @throws P23RServiceException the p23 r service exception
	 */
	void addConfiguration(SelectionTransformationParameter parameter) throws P23RServiceException;

	/**
	 * Delete a configuration.
	 *
	 * @param groupId the group id
	 * @throws P23RServiceException the p23 r service exception
	 */
	void deleteConfiguration(String groupId) throws P23RServiceException;
	
	/**
	 * Select data.
	 *
	 * @param context the context
	 * @return the string
	 * @throws P23RServiceException the p23 r service exception
	 */
	String selectData(GenerationContext context) throws P23RServiceException;
	
}
