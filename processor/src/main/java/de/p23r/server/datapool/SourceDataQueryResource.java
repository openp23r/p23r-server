package de.p23r.server.datapool;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

@Path("/source")
public interface SourceDataQueryResource {

	@POST
	@Consumes("application/json")
	@Produces("application/json")
    @Path("/{id}")
	SourceDataQueryResponse selectData(SourceDataQueryRequest request, @PathParam("id") String id);
	
}
