package de.p23r.server.modelandrulemanagement;

import java.io.Serializable;

/**
 * The Class SelectionInformation.
 *
 * @author sim
 */
public class SelectionInformation implements Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = 4005015974572503732L;

    private String schema;

    private String script;

    /**
     * Gets the schema.
     *
     * @return the schema
     */
    public String getSchema() {
        return schema;
    }

    /**
     * Sets the schema.
     *
     * @param schema the new schema
     */
    public void setSchema(String schema) {
        this.schema = schema;
    }

    /**
     * Gets the script.
     *
     * @return the script
     */
    public String getScript() {
        return script;
    }

    /**
     * Sets the script.
     *
     * @param script the new script
     */
    public void setScript(String script) {
        this.script = script;
    }

}
