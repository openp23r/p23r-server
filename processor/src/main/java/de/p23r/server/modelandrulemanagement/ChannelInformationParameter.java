package de.p23r.server.modelandrulemanagement;

import java.io.Serializable;

/**
 * The Class ChannelInformationParameter.
 *
 * @author sim
 */
public class ChannelInformationParameter implements Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = -7321289934376611983L;

    private String ruleId;

    private String receiverGroup;

    private String channelName;

    /**
     * Gets the rule id.
     *
     * @return the rule id
     */
    public String getRuleId() {
        return ruleId;
    }

    /**
     * Sets the rule id.
     *
     * @param ruleId the new rule id
     */
    public void setRuleId(String ruleId) {
        this.ruleId = ruleId;
    }

    /**
     * Gets the receiver group.
     *
     * @return the receiver group
     */
    public String getReceiverGroup() {
        return receiverGroup;
    }

    /**
     * Sets the receiver group.
     *
     * @param receiverGroup the new receiver group
     */
    public void setReceiverGroup(String receiverGroup) {
        this.receiverGroup = receiverGroup;
    }

    /**
     * Gets the channel name.
     *
     * @return the channel name
     */
    public String getChannelName() {
        return channelName;
    }

    /**
     * Sets the channel name.
     *
     * @param channelName the new channel name
     */
    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

}
