package de.p23r.server.modelandrulemanagement;

import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
//import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.impl.client.DefaultHttpClient;
//import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.jboss.resteasy.client.ClientExecutor;
import org.jboss.resteasy.client.ProxyFactory;
import org.jboss.resteasy.client.core.executors.ApacheHttpClient4Executor;
//import org.jboss.resteasy.client.jaxrs.ResteasyClient;
//import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
//import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
//import org.jboss.resteasy.client.jaxrs.engines.ApacheHttpClient4Engine;
import org.jboss.resteasy.plugins.providers.RegisterBuiltin;
import org.jboss.resteasy.spi.ResteasyProviderFactory;

import de.p23r.common.existdb.P23RXmlDbConfig;
import de.p23r.common.modelandrulemanagement.archive.DefaultArchive;
import de.p23r.common.modelandrulemanagement.archive.NotificationRulePackageListArchive;
import de.p23r.server.common.P23RTsl;

/**
 * The Class ModelAndRuleDepot.
 */
@ApplicationScoped
@SuppressWarnings("deprecation")
public class ModelAndRuleDepot implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3093489154943183661L;

	private Map<String, ModelAndRuleDepotRest> depots = new HashMap<String, ModelAndRuleDepotRest>();

	@Inject
	private P23RXmlDbConfig p23rConfig;
	
	@Inject
	private P23RTsl p23rTsl;
	
	private ModelAndRuleDepotRest initFromClientConfig(String clientConfig) {
		
		// get address and credentials first 
		String username = p23rConfig.get(String.format(P23RXmlDbConfig.XQUERY_USERNAME, clientConfig), "");
		String password = p23rConfig.get(String.format(P23RXmlDbConfig.XQUERY_PASSWORD, clientConfig), "");
		
		String endpointAddress = p23rTsl.discoverModelAndRuleDepotAddress(clientConfig);

		ModelAndRuleDepotRest restClient = init(endpointAddress, username, password);
        depots.put(clientConfig, restClient);
        
        return restClient;
	}

	private ModelAndRuleDepotRest init(String address, String username, String password) {

		DefaultHttpClient httpClient = new DefaultHttpClient();
		
		if (username != null) {
			Credentials credentials = new UsernamePasswordCredentials(username, password);
	        httpClient.getCredentialsProvider().setCredentials(AuthScope.ANY, credentials);			
		}

        ClientExecutor clientExecutor = new ApacheHttpClient4Executor(httpClient);

        RegisterBuiltin.register(ResteasyProviderFactory.getInstance());
        
        ModelAndRuleDepotRest restClient = ProxyFactory.create(ModelAndRuleDepotRest.class, address, clientExecutor);
        
        return restClient;
        
//		ClientConnectionManager cm = new PoolingClientConnectionManager();
//		DefaultHttpClient httpClient = new DefaultHttpClient(cm);
//		Credentials credentials = new UsernamePasswordCredentials(username, password);
//        httpClient.getCredentialsProvider().setCredentials(AuthScope.ANY, credentials);
//        
//		ApacheHttpClient4Engine engine = new ApacheHttpClient4Engine(httpClient);
//
//		ResteasyClient client = new ResteasyClientBuilder().httpEngine(engine).build();
//		ResteasyWebTarget target = client.target(address);
//		restClient = target.proxy(ModelAndRuleDepotRest.class);
				
	}
	
	/**
	 * Load packages list.
	 *
	 * @param clientConfig the client config
	 * @return the notification rule package list archive
	 */
	public NotificationRulePackageListArchive loadPackagesList(String clientConfig) {
		ModelAndRuleDepotRest restClient = depots.get(clientConfig);
		if (restClient == null) {
			restClient = initFromClientConfig(clientConfig);
		}

		if (restClient != null) {
			InputStream stream = restClient.getActualPackageList();
			if (stream != null) {
				return (NotificationRulePackageListArchive)DefaultArchive.createFromContent(stream);
			}			
		}
		return null;
	}

	/**
	 * Load package.
	 *
	 * @param path the path
	 * @param clientConfig the client config
	 * @return the default archive
	 */
	public DefaultArchive loadPackage(String path, String clientConfig) {
		ModelAndRuleDepotRest restClient = depots.get(clientConfig);
		if (restClient == null) {
			restClient = initFromClientConfig(clientConfig);
		}

		if (restClient != null) {
			InputStream stream = restClient.getPackage(path);
			if (stream != null) {
				return DefaultArchive.createFromContent(stream);
			}			
		}
		return null;
	}

	/**
	 * Load package from uri.
	 *
	 * @param rulepackage the rulepackage
	 * @param username the username
	 * @param password the password
	 * @return the default archive
	 */
	public DefaultArchive loadPackageFromUri(String rulepackage, String username, String password) {
		
		// splitt name from rest...
		String name = rulepackage.substring(rulepackage.lastIndexOf("/"));
		String address = rulepackage.substring(0, rulepackage.lastIndexOf("/"));
		
		if (name != null && !name.isEmpty()) {
			ModelAndRuleDepotRest restClient = init(address, username, password);
			if (restClient != null) {
				InputStream stream = restClient.getPackage(name);
				if (stream != null) {
					return DefaultArchive.createFromContent(stream);
				}			
			}
		}
		
		return null;
	}

}
