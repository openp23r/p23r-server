/**
 * Provides classes that stores runtime information about p23r packages inside an xml db.
 */
package de.p23r.server.modelandrulemanagement.persistence;