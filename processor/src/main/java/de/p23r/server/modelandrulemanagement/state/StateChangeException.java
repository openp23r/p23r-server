package de.p23r.server.modelandrulemanagement.state;

/**
 * The Class StateChangeException.
 * 
 * @author sro
 */
public class StateChangeException extends Exception {

    /**
	 *
	 */
    private static final long serialVersionUID = -3029371765916404713L;

    /**
     * Instantiates a new state change exception.
     * 
     * @param message the message
     */
    public StateChangeException(String message) {
        super(message);
    }
}
