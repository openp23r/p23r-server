package de.p23r.server.modelandrulemanagement.util;

import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.p23r.common.existdb.P23RXmlDbConfig;
import de.p23r.common.modelandrulemanagement.NamespaceHelper;
import de.p23r.common.modelandrulemanagement.archive.ModelPackageArchive;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.types.RulePackageStateType;
import de.p23r.leitstelle.ns.p23r.nrl.modelmanifest1_1.ModelManifest;
import de.p23r.leitstelle.ns.p23r.nrl.modelpackagemanifest1_1.ModelPackageManifest;
import de.p23r.server.xml.P23RProcessor;

/**
 * The Class ModelPackageVerifier.
 */
@ApplicationScoped
public class ModelPackageVerifier {

	private final Logger log = LoggerFactory.getLogger(getClass());
	
	@Inject
	private P23RXmlDbConfig p23rConfig;

	@Inject
	private P23RProcessor p23rProcessor;
	
	/**
	 * Verify package.
	 *
	 * @param modelpackageArchive the modelpackage archive
	 * @param state the state
	 * @return true, if successful
	 */
	public boolean verifyPackage(ModelPackageArchive modelpackageArchive, RulePackageStateType state) {
		boolean verificationDisabled = Boolean.parseBoolean(p23rConfig.get(P23RXmlDbConfig.XQUERY_GENERAL_VERIFICATIONDISABLED, "false"));
		if (verificationDisabled) {
			return true;
		}
		
		String root = null; // one root allowed
		Map<String, String> tmp = modelpackageArchive.getContent("[^/]+/");
		if (tmp.size() != 1) {
			log.error("Model package contains more then one root folder");
			state.getErrors().add("Modellpaket enthält nicht genau einen Paket-Ordner.");
			return false;
		}
		root = tmp.keySet().iterator().next();			

		String release = null; // one release allowed
		tmp = modelpackageArchive.getContent(root + "[^/]+/");
		if (tmp.size() != 1) {
			log.error("Model package contains more then one release folder");
			state.getErrors().add("Modellpaket enthält nicht genau einen Release-Ordner.");
			return false;
		}

		release = tmp.keySet().iterator().next();			

		// package manifest schema conform?
		String m = modelpackageArchive.getModelPackageManifest();
		if (!p23rProcessor.validate(m, null, null)) {
			log.error("Model package contains an invalid package manifest");
			state.getErrors().add("Modellpaket enthält ein nicht schemakonformes Manifest.");
			return false;
		}
		
		ModelPackageManifest manifest = modelpackageArchive.getModelPackageManifestHelper();
		if (!manifest.getName().equals(root.substring(0, root.length() - 1))) {
			log.error("Model package manifest contains an invalid package name (not equal to the root folder)");
			state.getErrors().add("Der Name des Modellpakets im Manifest entspricht nicht dem Paket-Ordner.");
			return false;
		}
		
		if (!manifest.getRelease().equals(release.substring(root.length(), release.length() - 1))) {
			log.error("Model package manifest contains an invalid package release (not equal to the release folder)");
			state.getErrors().add("Das Release des Modellpakets im Manifest etspricht nicht dem Release-Ordner.");
			return false;
		}
		
		List<String> models = modelpackageArchive.getModelPathes();
		boolean valid = false;
		for (String model : models) {
			String modelmanifest = modelpackageArchive.getModelManifest(model);
			String[] fragments = model.split("/");
			String modelName = fragments[fragments.length - 1];
			
			if (!p23rProcessor.validate(modelmanifest, null, null)) {
				log.warn("Model '" + modelName + "' contains an invalid manifest");
				state.getWarnings().add("Modell '" + modelName + "' enthält ein nicht schemakonformes Manifest.");
			} else {
				ModelManifest modelManifest = modelpackageArchive.getModelManifestHelper(model);
				if (!modelManifest.getName().equals(modelName)) {
					log.warn("Model manifest contains an invalid name (not equal to the folder name + '" + modelName + "')");
					state.getWarnings().add("Der Name des Modells im Manifest entspricht nicht dem Ordnernamen '" + modelName + "'.");
				} else {
					String schema = modelManifest.getSchema();
					String namespace = modelManifest.getNamespace();
					
					Map<String, String> content = modelpackageArchive.getContent(model + schema);
					if (content.isEmpty()) {
						// does not exist
					} else if (!namespace.equals(new NamespaceHelper().getNamespaceFromXsdContent(content.values().iterator().next()))) {
						// namespace not equal
					} else {
						valid = true;						
					}					
				}
			}
		}
		
		return valid;
	}
	
}
