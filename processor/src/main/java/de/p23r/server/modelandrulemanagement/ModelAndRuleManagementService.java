package de.p23r.server.modelandrulemanagement;

import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault_Exception;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.GetNotificationRulePackageStates;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.GetNotificationRulePackageStatesResponse;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.IsUpToDate;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.IsUpToDateResponse;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.SetNotificationRulePackageStates;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.SetNotificationRulePackageStatesResponse;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.UpdateNotificationRulePackages;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.UpdateNotificationRulePackagesResponse;
import de.p23r.server.common.GenerationContext;
import de.p23r.server.exceptions.P23RRuleNotFoundException;

/**
 * The Interface ModelAndRuleManagementService.
 *
 * @author sim
 */
public interface ModelAndRuleManagementService {

	/**
	 * Gets the notification rule package states.
	 *
	 * @param request the request
	 * @return the notification rule package states
	 * @throws P23RAppFault_Exception the p23 r app fault_ exception
	 */
	GetNotificationRulePackageStatesResponse getNotificationRulePackageStates(GetNotificationRulePackageStates request) throws P23RAppFault_Exception;
    
    /**
     * Checks if is up to date.
     *
     * @param request the request
     * @return the checks if is up to date response
     * @throws P23RAppFault_Exception the p23 r app fault_ exception
     */
    IsUpToDateResponse isUpToDate(IsUpToDate request) throws P23RAppFault_Exception;

    /**
     * Update notification rule packages.
     *
     * @param request the request
     * @return the update notification rule packages response
     * @throws P23RAppFault_Exception the p23 r app fault_ exception
     */
    UpdateNotificationRulePackagesResponse updateNotificationRulePackages(UpdateNotificationRulePackages request) throws P23RAppFault_Exception;
    
    /**
     * Sets the notification rule package states.
     *
     * @param request the request
     * @return the sets the notification rule package states response
     * @throws P23RAppFault_Exception the p23 r app fault_ exception
     */
    SetNotificationRulePackageStatesResponse setNotificationRulePackageStates(SetNotificationRulePackageStates request) throws P23RAppFault_Exception;

    /**
     * Install rulepackage.
     *
     * @param context the context
     * @return the string
     */
    String installRulepackage(GenerationContext context);

    /**
     * Uninstall rulepackage.
     *
     * @param rulepackageId the rulepackage id
     */
    void uninstallRulepackage(String rulepackageId);
    
    /**
     * Resolve resource.
     *
     * @param location the location
     * @return the string
     */
    String resolveResource(String location);

    /**
     * Gets the rule information by message type.
     *
     * @param namespace the namespace
     * @return the rule information by message type
     * @throws P23RRuleNotFoundException the p23 r rule not found exception
     */
    NotificationRuleInformation getRuleInformationByMessageType(String namespace) throws P23RRuleNotFoundException;

	/**
	 * Gets the core transformation.
	 *
	 * @param notificationRuleId the notification rule id
	 * @return the core transformation
	 */
	String getCoreTransformation(String notificationRuleId);

	/**
	 * Gets the presentation transformation.
	 *
	 * @param channelInformation the channel information
	 * @return the presentation transformation
	 */
	String getPresentationTransformation(ChannelInformationParameter channelInformation);

	/**
	 * Gets the next generation step.
	 *
	 * @param parameter the parameter
	 * @return the next generation step
	 */
	GenerationStep getNextGenerationStep(NextGenerationStepParameter parameter);

	/**
	 * Gets the first generation step.
	 *
	 * @param ruleId the rule id
	 * @return the first generation step
	 */
	GenerationStep getFirstGenerationStep(String ruleId);
    
	/**
	 * Gets the schema from namespace.
	 *
	 * @param parameter the parameter
	 * @return the schema from namespace
	 */
	String getSchemaFromNamespace(NextGenerationStepParameter parameter);

	/**
	 * Internal update.
	 */
	void internalUpdate();
	
}
