package de.p23r.server.modelandrulemanagement;

import java.io.Serializable;

/**
 * The Class NotificationRuleInformation.
 *
 * @author sim
 */
public class NotificationRuleInformation implements Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = 9032538354702820884L;

    private String manifest;

    private String groupManifest;

    private String messageSchema;

    private String multiProfileSelection;

    private String multiProfileTransformation;

    private boolean activated;

    private String specification;
    
    /**
     * Gets the manifest.
     *
     * @return the manifest
     */
    public String getManifest() {
        return manifest;
    }

    /**
     * Sets the manifest.
     *
     * @param manifest the new manifest
     */
    public void setManifest(String manifest) {
        this.manifest = manifest;
    }

    /**
     * Gets the group manifest.
     *
     * @return the group manifest
     */
    public String getGroupManifest() {
        return groupManifest;
    }

    /**
     * Sets the group manifest.
     *
     * @param groupManifest the new group manifest
     */
    public void setGroupManifest(String groupManifest) {
        this.groupManifest = groupManifest;
    }

    /**
     * Gets the message schema.
     *
     * @return the message schema
     */
    public String getMessageSchema() {
        return messageSchema;
    }

    /**
     * Sets the message schema.
     *
     * @param messageSchema the new message schema
     */
    public void setMessageSchema(String messageSchema) {
        this.messageSchema = messageSchema;
    }

    /**
     * Gets the multi profile selection.
     *
     * @return the multi profile selection
     */
    public String getMultiProfileSelection() {
        return multiProfileSelection;
    }

    /**
     * Sets the multi profile selection.
     *
     * @param multiProfileSelection the new multi profile selection
     */
    public void setMultiProfileSelection(String multiProfileSelection) {
        this.multiProfileSelection = multiProfileSelection;
    }

    /**
     * Gets the multi profile transformation.
     *
     * @return the multi profile transformation
     */
    public String getMultiProfileTransformation() {
        return multiProfileTransformation;
    }

    /**
     * Sets the multi profile transformation.
     *
     * @param multiProfileTransformation the new multi profile transformation
     */
    public void setMultiProfileTransformation(String multiProfileTransformation) {
        this.multiProfileTransformation = multiProfileTransformation;
    }

	/**
	 * Checks if is activated.
	 *
	 * @return true, if is activated
	 */
	public boolean isActivated() {
		return activated;
	}

	/**
	 * Sets the activated.
	 *
	 * @param activated the new activated
	 */
	public void setActivated(boolean activated) {
		this.activated = activated;
	}

	/**
	 * Gets the specification.
	 *
	 * @return the specification
	 */
	public String getSpecification() {
		return specification;
	}

	/**
	 * Sets the specification.
	 *
	 * @param specification the new specification
	 */
	public void setSpecification(String specification) {
		this.specification = specification;
	}

}
