package de.p23r.server.modelandrulemanagement.persistence;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.inject.Inject;

import org.apache.commons.io.IOUtils;
import org.exist.xmldb.EXistResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.Resource;
import org.xmldb.api.base.ResourceSet;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.CollectionManagementService;
import org.xmldb.api.modules.XQueryService;

import de.p23r.common.existdb.P23RXmlDbConfig;
import de.p23r.common.existdb.P23RXmlDbResource;
import de.p23r.common.modelandrulemanagement.archive.DefaultArchive;
import de.p23r.server.common.TBRS;
import static de.p23r.server.common.TBRS.*;

/**
 * The Class XmlDbPackageRepository.
 */
public abstract class XmlDbPackageRepository implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -534564251653031757L;

	private final Logger log = LoggerFactory.getLogger(getClass());
	
	/**
	 * The p23r xml db config.
	 */
	@Inject
	protected P23RXmlDbConfig p23rXmlDbConfig;

	private Collection packages;
	
	/**
	 * The basecollection.
	 */
	protected Collection basecollection;

	/**
	 * Inits the.
	 *
	 * @param collectionName the collection name
	 */
	protected void init(String collectionName) {
		try {
			packages = p23rXmlDbConfig.getBaseAsCollection().getChildCollection("packages");
			if (packages == null) {
				CollectionManagementService service = (CollectionManagementService) p23rXmlDbConfig.getBaseAsCollection().getService("CollectionManagementService", "1.0");
				packages = service.createCollection("packages");
			}
			basecollection = packages.getChildCollection(collectionName);
			if (basecollection == null) {
				CollectionManagementService service = (CollectionManagementService) packages.getService("CollectionManagementService", "1.0");
				basecollection = service.createCollection(collectionName);
			}
		} catch (XMLDBException e) {
			log.error("initializing packages repository", e);
		}		
	}
	
	/**
	 * Destroy.
	 */
	protected void destroy() {
		try {
			if (basecollection != null && basecollection.isOpen()) {
				basecollection.close();
			}
			if (packages != null && packages.isOpen()) {
				packages.close();
			}
		} catch (XMLDBException e) {
			log.warn("closing packages collections", e);
		}
	}
	
	/**
	 * Persist.
	 *
	 * @param packageArchive the package archive
	 */
	protected void persist(DefaultArchive packageArchive) {
		if (basecollection == null) {
			throw new IllegalStateException("base collection not initialized");
		}
		
		ZipInputStream zipFile = new ZipInputStream(new ByteArrayInputStream(packageArchive.getZipContent()));
		try {
			ZipEntry entry = zipFile.getNextEntry();
			while (entry != null) {
				P23RXmlDbResource resource = new P23RXmlDbResource(entry.getName(), basecollection);
				if (!entry.isDirectory() && entry.getSize() > 0) {
					if (entry.getName().endsWith(TRANSFORMATION_SUFFIX) || entry.getName().endsWith(XML_SUFFIX) || entry.getName().endsWith(SCHEMA_SUFFIX)) {
						resource.setContent(IOUtils.toString(zipFile, TBRS.CHARSET));
					} else {
						resource.setBinaryContent(IOUtils.toByteArray(zipFile));
					}						
				}
				
				resource.free();					
				zipFile.closeEntry();
				entry = zipFile.getNextEntry();
			}
		} catch (IOException e) {
			log.error("reading zip archive", e);
		}
	}

	/**
	 * Removes the.
	 *
	 * @param name the name
	 */
	protected void remove(String name) {
		try {
			CollectionManagementService service = (CollectionManagementService) basecollection.getService("CollectionManagementService", "1.0");
			service.removeCollection(name);
		} catch (XMLDBException e) {
			log.error("removing collection", e);
		}		
	}

	/**
	 * Query.
	 *
	 * @param query the query
	 * @param namespace the namespace
	 * @return the resource set
	 */
	protected ResourceSet query(String query, String namespace) {
		if (basecollection == null) {
			throw new IllegalStateException("base collection not initialized");			
		}

		try {
			XQueryService service = (XQueryService) basecollection.getService("XQueryService", "1.0");
			if (namespace != null) {
				service.setNamespace("", namespace);
			}
			return service.query(query);
		} catch (XMLDBException e) {
			log.error("querying package pool", e);
		}
		return null;
	}
	
	/**
	 * Query single resource.
	 *
	 * @param query the query
	 * @param namespace the namespace
	 * @return the resource
	 */
	protected Resource querySingleResource(String query, String namespace) {
		ResourceSet result = query(query, namespace);
		try {
			if (result != null && result.getSize() > 0) {
				return result.getResource(0);
			}
		} catch (XMLDBException e) {
			log.error("extracting single resource", e);
		}			
		return null;
	}
	
	/**
	 * Gets the resource.
	 *
	 * @param rest the rest
	 * @return the resource
	 */
	public P23RXmlDbResource getResource(String rest) {
		return new P23RXmlDbResource(rest, basecollection);
	}

	/**
	 * Find collection.
	 *
	 * @param query the query
	 * @param namespace the namespace
	 * @return the collection
	 */
	protected Collection findCollection(String query, String namespace) {
		Resource resource = querySingleResource(query, namespace);
		try {
			return resource != null ? p23rXmlDbConfig.getCollection(resource.getParentCollection().getName()) : null;
		} catch (XMLDBException e) {
			log.error("getting parent collection", e);
		} finally {
			if (resource != null) {
				try {
					((EXistResource)resource).freeResources();
				} catch (XMLDBException e) {
					log.error("freeing resource", e);
				}
			}
		}			
		return null;				
	}
	
}	