package de.p23r.server.modelandrulemanagement.persistence;

import static de.p23r.server.common.TBRS.NAMESPACE_MODELMANIFEST;
import static de.p23r.server.common.TBRS.NAMESPACE_MODELPACKAGEMANIFEST;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.Resource;
import org.xmldb.api.base.XMLDBException;

import de.p23r.common.modelandrulemanagement.archive.ModelPackageArchive;

/**
 * The Class ModelPackageRepository.
 */
@ApplicationScoped
public class ModelPackageRepository extends XmlDbPackageRepository {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3293555881029726846L;

	private final static String COLLECTIONNAME = "modelpackages";
		
	private static final String QUERY_MODELPACKAGENAME = "/modelPackageManifest[@id = '%s']/@name/string()";
	private static final String QUERY_MODELPACKAGEMANIFEST = "/modelPackageManifest[@id = '%s']";
	private static final String QUERY_MODELPACKAGEMANIFESTBYNAMEANDRELEASE = "/modelPackageManifest[@name = '%s'][@release = '%s']";
	private static final String QUERY_MODELMANIFEST = "/modelManifest[@id = '%s']";

	private final Logger log = LoggerFactory.getLogger(getClass());
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
		super.init(COLLECTIONNAME);
	}

	/* (non-Javadoc)
	 * @see de.p23r.server.modelandrulemanagement.persistence.XmlDbPackageRepository#destroy()
	 */
	@PreDestroy
	public void destroy() {
		super.destroy();
	}
	
	/**
	 * Persist package.
	 *
	 * @param archive the archive
	 */
	public void persistPackage(ModelPackageArchive archive) {
		persist(archive);
	}

	/**
	 * Removes the package.
	 *
	 * @param id the id
	 */
	public void removePackage(String id) {
		String packageName = getPackageName(id);
		if (packageName != null) {
			remove(packageName);
		}
	}

	private String getPackageName(String id) {
		Resource resource = querySingleResource(String.format(QUERY_MODELPACKAGENAME, id), NAMESPACE_MODELPACKAGEMANIFEST);
		try {
			return resource != null ? (String)resource.getContent() : null;
		} catch (XMLDBException e) {
			log.error("get content from resource", e);
		}
		return null;
	}
	
	/**
	 * Find model package by id.
	 *
	 * @param id the id
	 * @return the collection
	 */
	public Collection findModelPackageById(String id) {
		return findCollection(String.format(QUERY_MODELPACKAGEMANIFEST, id), NAMESPACE_MODELPACKAGEMANIFEST);
	}

	/**
	 * Find model by id.
	 *
	 * @param id the id
	 * @return the collection
	 */
	public Collection findModelById(String id) {
		return findCollection(String.format(QUERY_MODELMANIFEST, id), NAMESPACE_MODELMANIFEST);
	}

	/**
	 * Find model package by name and release.
	 *
	 * @param name the name
	 * @param release the release
	 * @return the collection
	 */
	public Collection findModelPackageByNameAndRelease(String name, String release) {
		return findCollection(String.format(QUERY_MODELPACKAGEMANIFESTBYNAMEANDRELEASE, name, release), NAMESPACE_MODELMANIFEST);
	}
		
}
