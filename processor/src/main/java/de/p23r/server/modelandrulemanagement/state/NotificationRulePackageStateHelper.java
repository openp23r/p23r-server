package de.p23r.server.modelandrulemanagement.state;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.types.RuleGroupStateType;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.types.RulePackageStateType;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.types.RuleStateType;
import de.p23r.server.modelandrulemanagement.state.StateChangeRequests.NotificationRuleChangeType;
import de.p23r.server.modelandrulemanagement.state.StateChangeRequests.NotificationRuleGroupChangeType;
import de.p23r.server.modelandrulemanagement.state.StateChangeRequests.NotificationRulePackageChangeType;

/**
 * The Class NotificationRulePackageStateHelper is a Helper Class for State
 * Information. Provides Functionality to create State Differences and validate
 * State Change Requests.
 * 
 * @author sro
 */
public class NotificationRulePackageStateHelper {

	private static final Logger LOG = LoggerFactory.getLogger(NotificationRulePackageStateHelper.class);

	/** The new (requested) states. */
	List<RulePackageStateType> newStates = null;

	/** The current (actual persisted) states. */
	List<RulePackageStateType> currentStates = null;

	/**
	 * Instantiates a new notification rule package state helper.
	 *
	 * @param newStates the new states
	 * @param currentStates the current states
	 */
	public NotificationRulePackageStateHelper(List<RulePackageStateType> newStates, List<RulePackageStateType> currentStates) {
		this.newStates = newStates;
		this.currentStates = currentStates;
	}

	/**
	 * Generate the StateChangeRequest Object.
	 * 
	 * @return the state change requests
	 */
	public StateChangeRequests generateStateChangeRequests() {
		StateChangeRequests result = new StateChangeRequests();

		// generate state change requests

		for (RulePackageStateType newPackageState : newStates) {

			// search corresponding currentState
			RulePackageStateType currentPackageState = getCurrentState(newPackageState);

			if (currentPackageState == null) {
				newPackageState.getErrors().add("the package with id " + newPackageState.getId() + "is not available in MARM Depot!");
				// skip this package state because the current package state is
				// null and that leads
				// to many nullpointer exceptions
				continue;
			}

			// check state differences
			checkStateDifferences(newPackageState, currentPackageState, result);

			for (RuleGroupStateType newGroupState : newPackageState.getRuleGroups()) {
				checkGroupStateDifferences(currentPackageState, newGroupState, result);
			}
		}

		return result;
	}

	/**
	 * Checks the differences between a package state and a group state.
	 * 
	 * @param currentPackageState
	 *            the package state
	 * @param newGroupState
	 *            the group state
	 * @param result
	 *            the state change requests
	 */
	private void checkGroupStateDifferences(RulePackageStateType currentPackageState, RuleGroupStateType newGroupState, StateChangeRequests result) {
		// search corresponding currentState
		RuleGroupStateType currentGroupState = getCurrentState(currentPackageState, newGroupState);

		if (currentGroupState == null) {
			newGroupState.getErrors().add("the group with id " + newGroupState.getId() + "is not available in MARM Depot!");
			return;
		}

		// check state differences
		checkStateDifferences(newGroupState, currentGroupState, result);

		for (RuleStateType newRuleState : newGroupState.getRules()) {

			// search corresponding currentState
			RuleStateType currentRuleState = getCurrentState(currentGroupState, newRuleState);

			if (currentRuleState == null) {
				newRuleState.getErrors().add("the rule with id " + newRuleState.getId() + "is not available in MARM Depot!");
			} else {
				// check state differences
				checkStateDifferences(newRuleState, currentRuleState, result);
			}
		}
	}

	/**
	 * Checks the state differences for rule states.
	 * 
	 * @param newRuleState
	 *            the new rule state
	 * @param currentRuleState
	 *            the current rule state
	 * @param result
	 *            the request
	 */
	private void checkStateDifferences(RuleStateType newRuleState, RuleStateType currentRuleState, StateChangeRequests result) {
		if (newRuleState.isIsDeactivated().booleanValue() != currentRuleState.isIsDeactivated().booleanValue()) {
			try {
				NotificationRuleChangeType changeType = NotificationRuleChangeType.DEACTIVATION;
				changeType.setStateType(newRuleState);
				result.addNotificationRuleChange(newRuleState.getId(), changeType);
			} catch (StateChangeException e) {
				LOG.error("Failed to add notification rule change. ", e);
				newRuleState.getErrors().add(e.getMessage());
			}
		}
	}

	/**
	 * Checks the state differences for rule package states.
	 * 
	 * @param newPackageState
	 *            the new package state
	 * @param currentPackageState
	 *            the current package state
	 * @param result
	 *            the request
	 */
	private void checkStateDifferences(RulePackageStateType newPackageState, RulePackageStateType currentPackageState, StateChangeRequests result) {
		if (newPackageState.isIsSelected().booleanValue() != currentPackageState.isIsSelected().booleanValue()) {
			try {
				NotificationRulePackageChangeType changeType = newPackageState.isIsSelected() ? NotificationRulePackageChangeType.SELECTION : NotificationRulePackageChangeType.DESELECTION;
				changeType.setStateType(newPackageState);
				result.addNotificationRulePackageChange(newPackageState.getId(), changeType);
			} catch (StateChangeException e) {
				LOG.error("Failed to add notification rule package change.", e);
				newPackageState.getErrors().add(e.getMessage());
			}
		}
	}

	/**
	 * Checks the state differences for rule group states.
	 * 
	 * @param newGroupState
	 *            the new group state
	 * @param currentGroupState
	 *            the current group state
	 * @param result
	 *            the request
	 */
	private void checkStateDifferences(RuleGroupStateType newGroupState, RuleGroupStateType currentGroupState, StateChangeRequests result) {
		if (newGroupState.isIsActivated().booleanValue() != currentGroupState.isIsActivated().booleanValue()) {
			try {
				NotificationRuleGroupChangeType changeType = newGroupState.isIsActivated() ? NotificationRuleGroupChangeType.ACTIVATION : NotificationRuleGroupChangeType.DEACTIVATION;
				changeType.setStateType(newGroupState);
				result.addNotificationRuleGroupChange(newGroupState.getId(), changeType);
			} catch (StateChangeException e) {
				LOG.error("Failed to add notification rule group change.", e);
				newGroupState.getErrors().add(e.getMessage());
			}
		}
	}

	/**
	 * Returns the current rule state
	 * 
	 * @param currentGroupState
	 *            the current group state
	 * @param newRuleState
	 *            the new rule state
	 * @return current rule state
	 */
	private RuleStateType getCurrentState(RuleGroupStateType currentGroupState, RuleStateType newRuleState) {
		for (RuleStateType state : currentGroupState.getRules()) {
			if (state.getId().equals(newRuleState.getId())) {
				return state;
			}
		}
		return null;
	}

	/**
	 * Returns the current rule group state
	 * 
	 * @param currentPackageState
	 *            the current package state
	 * @param newGroupState
	 *            the new group state
	 * @return current rule group state
	 */
	private RuleGroupStateType getCurrentState(RulePackageStateType currentPackageState, RuleGroupStateType newGroupState) {
		for (RuleGroupStateType state : currentPackageState.getRuleGroups()) {
			if (state.getId().equals(newGroupState.getId())) {
				return state;
			}
		}
		return null;
	}

	/**
	 * Returns the current rule package state
	 * 
	 * @param newPackageState
	 *            the new package state
	 * @return current rule package state
	 */
	private RulePackageStateType getCurrentState(RulePackageStateType newState) {
		for (RulePackageStateType state : currentStates) {
			if (state.getId().equals(newState.getId())) {
				return state;
			}
		}
		return null;
	}
}
