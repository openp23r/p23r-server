package de.p23r.server.modelandrulemanagement;

import java.io.Serializable;

/**
 * The Class NextGenerationStepParameter.
 */
public class NextGenerationStepParameter implements Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1731171464535277496L;

    private String ruleId;

    private String namespace;

    /**
     * Gets the rule id.
     *
     * @return the rule id
     */
    public String getRuleId() {
        return ruleId;
    }

    /**
     * Sets the rule id.
     *
     * @param ruleId the new rule id
     */
    public void setRuleId(String ruleId) {
        this.ruleId = ruleId;
    }

    /**
     * Gets the namespace.
     *
     * @return the namespace
     */
    public String getNamespace() {
        return namespace;
    }

    /**
     * Sets the namespace.
     *
     * @param namespace the new namespace
     */
    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

}
