package de.p23r.server.modelandrulemanagement.state;

import java.util.HashMap;
import java.util.Map;

import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.types.RuleGroupStateType;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.types.RulePackageStateType;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.types.RuleStateType;

/**
 * The Class StateChangeRequests provides Information about the requested NotificationRulePackage
 * State Changes.
 * 
 * @author sro
 * 
 */
public class StateChangeRequests {

    private Map<String, NotificationRulePackageChangeType> notificationRulePackageStateChanges = new HashMap<String, StateChangeRequests.NotificationRulePackageChangeType>();
    private Map<String, NotificationRuleGroupChangeType> notificationRuleGroupStateChanges = new HashMap<String, StateChangeRequests.NotificationRuleGroupChangeType>();
    private Map<String, NotificationRuleChangeType> notificationRuleStateChanges = new HashMap<String, StateChangeRequests.NotificationRuleChangeType>();

    /**
     * The Enum NotificationRulePackageChangeType.
     */
    public enum NotificationRulePackageChangeType {

        /** The SELECTION. */
        SELECTION,

        /** The DESELECTION. */
        DESELECTION;

        private RulePackageStateType stateType;

        /**
         * Sets the state type.
         * 
         * @param stateType the new state type
         */
        public void setStateType(RulePackageStateType stateType) {
            this.stateType = stateType;
        }

        /**
         * Gets the state type.
         * 
         * @return the state type
         */
        public RulePackageStateType getStateType() {
            return this.stateType;
        }
    }

    /**
     * The Enum NotificationRuleGroupChangeType.
     */
    public enum NotificationRuleGroupChangeType {

        /** The ACTIVATION. */
        ACTIVATION,

        /** The DEACTIVATION. */
        DEACTIVATION;

        private RuleGroupStateType stateType;

        /**
         * Sets the state type.
         * 
         * @param stateType the new state type
         */
        public void setStateType(RuleGroupStateType stateType) {
            this.stateType = stateType;
        }

        /**
         * Gets the state type.
         * 
         * @return the state type
         */
        public RuleGroupStateType getStateType() {
            return this.stateType;
        }
    }

    /**
     * The Enum NotificationRuleChangeType.
     */
    public enum NotificationRuleChangeType {

        /** The DEACTIVATION. */
        DEACTIVATION;

        private RuleStateType stateType;

        /**
         * Sets the state type.
         * 
         * @param stateType the new state type
         */
        public void setStateType(RuleStateType stateType) {
            this.stateType = stateType;
        }

        /**
         * Gets the state type.
         * 
         * @return the state type
         */
        public RuleStateType getStateType() {
            return this.stateType;
        }
    }

    /**
     * Adds a notification rule package change.
     * 
     * @param packageId the package id
     * @param changeType the change type
     * @throws StateChangeException the state change exception
     */
    public void addNotificationRulePackageChange(String packageId,
            NotificationRulePackageChangeType changeType) throws StateChangeException {
        if (notificationRulePackageStateChanges.containsKey(packageId)) {
            throw new StateChangeException("for the package id " + packageId
                    + "there is allready an entry in the change map!");
        }
        notificationRulePackageStateChanges.put(packageId, changeType);
    }

    /**
     * Adds a notification rule group change.
     * 
     * @param groupId the group id
     * @param changeType the change type
     * @throws StateChangeException the state change exception
     */
    public void addNotificationRuleGroupChange(String groupId, NotificationRuleGroupChangeType changeType)
            throws StateChangeException {
        if (notificationRuleGroupStateChanges.containsKey(groupId)) {
            throw new StateChangeException("for the group id " + groupId
                    + "there is allready an entry in the change map!");
        }
        notificationRuleGroupStateChanges.put(groupId, changeType);
    }

    /**
     * Adds a notification rule change.
     * 
     * @param ruleId the rule id
     * @param changeType the change type
     * @throws StateChangeException the state change exception
     */
    public void addNotificationRuleChange(String ruleId, NotificationRuleChangeType changeType)
            throws StateChangeException {
        if (notificationRuleStateChanges.containsKey(ruleId)) {
            throw new StateChangeException("for the rule id " + ruleId
                    + "there is allready an entry in the change map!");
        }
        notificationRuleStateChanges.put(ruleId, changeType);
    }

    /**
     * Checks if constructed StateChangeRequest Object is valid.
     * 
     * @return true, if is valid
     */
    public boolean isValid() {
    	return isGroupChangeRequested() || isRuleChangeRequested() || isPackageChangeRequested();
    }

    /**
     * Gets the notification rule package state changes.
     * 
     * @return the notification rule package state changes
     */
    public Map<String, NotificationRulePackageChangeType> getNotificationRulePackageStateChanges() {
        return notificationRulePackageStateChanges;
    }

    /**
     * Gets the notification rule group state changes.
     * 
     * @return the notification rule group state changes
     */
    public Map<String, NotificationRuleGroupChangeType> getNotificationRuleGroupStateChanges() {
        return notificationRuleGroupStateChanges;
    }

    /**
     * Gets the notification rule state changes.
     * 
     * @return the notification rule state changes
     */
    public Map<String, NotificationRuleChangeType> getNotificationRuleStateChanges() {
        return notificationRuleStateChanges;
    }

    /**
     * Checks if package changes are requested. Convenient Method.
     * 
     * @return true, if is package change requested
     */
    public boolean isPackageChangeRequested() {
        return !notificationRulePackageStateChanges.isEmpty();
    }

    /**
     * Checks if group change are requested. Convenient Method.
     * 
     * @return true, if is group change requested
     */
    public boolean isGroupChangeRequested() {
        return !notificationRuleGroupStateChanges.isEmpty();
    }

    /**
     * Checks if rule changes are requested. Convenient Method.
     * 
     * @return true, if is rule change requested
     */
    public boolean isRuleChangeRequested() {
        return !notificationRuleStateChanges.isEmpty();
    }

}
