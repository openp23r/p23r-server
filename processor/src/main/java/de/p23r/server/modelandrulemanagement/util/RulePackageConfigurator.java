package de.p23r.server.modelandrulemanagement.util;

import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.switchyard.component.bean.Reference;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.XMLDBException;

import de.p23r.common.existdb.P23RXmlDbResource;
import de.p23r.common.existdb.P23RXmlDbResourceContentAccess;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.types.RulePackageStateType;
import de.p23r.server.common.TBRS;
import de.p23r.server.datapool.DataPoolService;
import de.p23r.server.datapool.SelectionTransformationParameter;
import de.p23r.server.exceptions.P23RServiceException;

/**
 * The Class RulePackageConfigurator.
 */
public class RulePackageConfigurator {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Inject
	@Reference
	private DataPoolService datapool;

	/**
	 * Configure package.
	 *
	 * @param rulepackage the rulepackage
	 * @param state the state
	 */
	public void configurePackage(Collection rulepackage, RulePackageStateType state) {
		// 1. set and save the state

		// 2. iterate all groups and configure them...
		try {
			for (String group : rulepackage.listChildCollections()) {
				Collection groupCollection = rulepackage.getChildCollection(group);
				P23RXmlDbResource configurationSelection = new P23RXmlDbResource(TBRS.CONFIGURATIONSELECTION, groupCollection, false);
				P23RXmlDbResource configurationTransformation = new P23RXmlDbResource(TBRS.CONFIGURATIONTRANSFORMATION, groupCollection, false);
				if (configurationSelection.exists() && configurationTransformation.exists()) {
					SelectionTransformationParameter selectionTransformation = new SelectionTransformationParameter();
					selectionTransformation.setSelection(P23RXmlDbResourceContentAccess.getContentAsString(configurationSelection));
					selectionTransformation.setTransformation(P23RXmlDbResourceContentAccess.getContentAsString(configurationTransformation));

					try {
						datapool.addConfiguration(selectionTransformation);
					} catch (P23RServiceException e) {
						log.error("adding group config", e);
					}
				}
			}
		} catch (XMLDBException e) {
			log.error("configuring package groups", e);
		}		
	}

	/**
	 * Removes the configurations.
	 *
	 * @param groupIds the group ids
	 */
	public void removeConfigurations(List<String> groupIds) {
		for (String groupId : groupIds) {
			try {
				datapool.deleteConfiguration(groupId);
			} catch (P23RServiceException e) {
				log.warn("removing group configuration", e);
			}
		}		
	}
	
	/**
	 * Configure rule group.
	 *
	 * @param group the group
	 */
	public void configureRuleGroup(Collection group) {
		P23RXmlDbResource selection = new P23RXmlDbResource(TBRS.CONFIGURATIONSELECTION, group, false);
		
		// save configuration
		if (selection.exists()) {
			String selectionScript = P23RXmlDbResourceContentAccess.getContentAsString(selection);

			P23RXmlDbResource transformation = new P23RXmlDbResource(TBRS.CONFIGURATIONTRANSFORMATION, group, false);
			String transformationScript = P23RXmlDbResourceContentAccess.getContentAsString(transformation);

			SelectionTransformationParameter selectionTransformation = new SelectionTransformationParameter();
			selectionTransformation.setSelection(selectionScript);
			selectionTransformation.setTransformation(transformationScript);

			try {
				datapool.addConfiguration(selectionTransformation);
			} catch (P23RServiceException e) {
				log.error("adding group config", e);
			}
		}		
	}
	
}
