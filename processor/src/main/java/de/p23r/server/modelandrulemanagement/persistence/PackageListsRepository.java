package de.p23r.server.modelandrulemanagement.persistence;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.transform.stream.StreamSource;

import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmldb.api.base.Resource;
import org.xmldb.api.base.ResourceIterator;
import org.xmldb.api.base.ResourceSet;
import org.xmldb.api.base.XMLDBException;

import de.p23r.common.existdb.P23RXmlDbResource;
import de.p23r.common.existdb.P23RXmlDbResourceContentAccess;
import de.p23r.common.modelandrulemanagement.P23RSignature;
import de.p23r.common.modelandrulemanagement.archive.NotificationRulePackageListArchive;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.types.RulePackageStateType;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.types.RuleStateType;
import de.p23r.server.common.TBRS;

/**
 * The Class PackageListsRepository.
 */
@ApplicationScoped
public class PackageListsRepository extends XmlDbPackageRepository {

	private static final long serialVersionUID = 3293555881029726846L;

	private final static String COLLECTIONNAME = "packagelists";

	private static final String QUERY_RULEPACKAGEFILE = "//rulePackages[@id = '%s']/rulePackageFile";

	private static final String QUERY_FILES = "//files";

	private static final String QUERY_RECOMMENDATIONSCRIPT = "//rulePackages[@id = '%s']/files[fn:ends-with(@location, '%s')]";

	private static final String QUERY_P23RSPECIFICATIONS = "//rulePackages[@id = '%s']/dependencies/*:p23rSpecifications/text()";

	private static final String QUERY_LATESTP23RSPECIFICATION = "fn:max(//rulePackages[@id = '%s']/dependencies/*:p23rSpecifications/text())";

	private static final String QUERY_DEPENDENTNAMESPACES = "//modelPackages[@name = //rulePackages[@id = '%s']/dependencies/*:modelPackages/@name][release = //rulePackages[@id = '%s']/dependencies/*:modelPackages/*:releases]/namespaces/text()";

	private static final String QUERY_RULESTATE = "//rules[@id = '%s']";

	private static final String QUERY_STATES = "/packageStates";

	private static final String QUERY_PACKAGESTATES = "/packageStates[@id = '%s']";

	private static final String NAMESPACE_IRULEACTIVATE_TYPES = "http://leitstelle.p23r.de/NS/P23R/IRuleActivate1-0/types";

	private static final String UPDATE_GROUPACTIVATION = "update value //ruleGroups[@id = '%s']/@isActivated with '%s'";

	private static final String UPDATE_GROUPRULESDEACTIVATION = "update value //ruleGroups[@id = '%s']/rules/@isDeactivated with '%s'";

	private static final String UPDATE_RULEDEACTIVATION = "update value //rules[@id = '%s']/@isDeactivated with '%s'";

	private final Logger log = LoggerFactory.getLogger(getClass());

	private JAXBContext ctx;

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
		super.init(COLLECTIONNAME);
		try {
			ctx = JAXBContext.newInstance("de.p23r.leitstelle.ns.p23r.iruleactivate1_0.types");
		} catch (JAXBException e) {
			log.error("initializing jaxb context", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.p23r.server.modelandrulemanagement.persistence.XmlDbPackageRepository
	 * #destroy()
	 */
	@PreDestroy
	public void destroy() {
		super.destroy();
	}

	/**
	 * Persist package list.
	 * 
	 * @param archive
	 *            the archive
	 * @param clientConfig
	 *            the client config
	 */
	public void persistPackageList(NotificationRulePackageListArchive archive, String clientConfig) {
		persist(archive);

		// check here all hash values
		ResourceSet resources = query(QUERY_FILES, TBRS.NAMESPACE_PACKAGELISTMANIFEST);
		try {
			ResourceIterator it = resources.getIterator();
			while (it.hasMoreResources()) {
				Resource resource = it.nextResource();
				String fileEntry = (String) resource.getContent();
				SAXBuilder builder = new SAXBuilder();
				Document file = builder.build(new ByteArrayInputStream(fileEntry.getBytes(TBRS.CHARSET)));
				String location = file.getRootElement().getAttributeValue("location");
				String hash = file.getRootElement().getChildText("hash", Namespace.getNamespace("http://leitstelle.p23r.de/NS/p23r/nrl/CommonNrl1-1"));

				// get location from archive
				Map<String, String> found = archive.getContent(archive.getPackageRootPath() + location);
				if (!found.isEmpty()) {
					if (!P23RSignature.isHashValid(DatatypeConverter.parseBase64Binary(hash), found.values().iterator().next().getBytes(TBRS.CHARSET))) {
						log.error("package list contains invalid artifact (hash value): {}", location);
					}
				}
			}
		} catch (XMLDBException e) {
			log.error("iterating over file references in packages list manifest", e);
		} catch (JDOMException e) {
			log.error("parsing file reference", e);
		} catch (IOException e) {
			log.error("reading file reference from xml resource", e);
		}

	}

	/**
	 * Gets the state.
	 * 
	 * @param id
	 *            the id
	 * @return the state
	 */
	public RulePackageStateType getState(String id) {
		Resource resource = querySingleResource(String.format(QUERY_PACKAGESTATES, id), NAMESPACE_IRULEACTIVATE_TYPES);
		try {
			String state = resource == null ? null : (String) resource.getContent();
			if (state != null) {
				Unmarshaller um = ctx.createUnmarshaller();
				JAXBElement<RulePackageStateType> elem = um.unmarshal(new StreamSource(new ByteArrayInputStream(state.getBytes(TBRS.CHARSET))), RulePackageStateType.class);
				return elem.getValue();
			}
		} catch (XMLDBException e) {
			log.error("querying package state", e);
		} catch (JAXBException e) {
			log.error("unmarshallig rule package state", e);
		}
		return null;
	}

	/**
	 * Persist state.
	 * 
	 * @param state
	 *            the state
	 */
	public void persistState(RulePackageStateType state) {
		StringWriter writer = new StringWriter();
		try {
			Marshaller m = ctx.createMarshaller();
			m.marshal(new JAXBElement<RulePackageStateType>(new QName(NAMESPACE_IRULEACTIVATE_TYPES, "packageStates"), RulePackageStateType.class, state),
					writer);

			P23RXmlDbResource resource = new P23RXmlDbResource(state.getId() + TBRS.XML_SUFFIX, basecollection, false);
			resource.setContent(writer.toString());
		} catch (JAXBException e) {
			log.error("marshalling rule package state", e);
		}
	}

	/**
	 * Gets the dependent model namespaces.
	 * 
	 * @param id
	 *            the id
	 * @return the dependent model namespaces
	 */
	public Set<String> getDependentModelNamespaces(String id) {
		ResourceSet namespaces = query(String.format(QUERY_DEPENDENTNAMESPACES, id, id), TBRS.NAMESPACE_PACKAGELISTMANIFEST);
		Set<String> result = new HashSet<String>();
		try {
			ResourceIterator it = namespaces.getIterator();
			while (it.hasMoreResources()) {
				Resource resource = it.nextResource();
				result.add((String) resource.getContent());
			}
		} catch (XMLDBException e) {
			log.error("iterate over dependent namespaces resource set", e);
		}
		return result;
	}

	/**
	 * Gets the recommendation selection script.
	 * 
	 * @param rulepackageId
	 *            the rulepackage id
	 * @return the recommendation selection script
	 */
	public String getRecommendationSelectionScript(String rulepackageId) {
		return getScript(String.format(QUERY_RECOMMENDATIONSCRIPT, rulepackageId, TBRS.RECOMMENDATIONSELECTION));
	}

	/**
	 * Gets the recommendation transformation script.
	 * 
	 * @param rulepackageId
	 *            the rulepackage id
	 * @return the recommendation transformation script
	 */
	public String getRecommendationTransformationScript(String rulepackageId) {
		return getScript(String.format(QUERY_RECOMMENDATIONSCRIPT, rulepackageId, TBRS.RECOMMENDATIONTRANSFORMATION));
	}

	private String getScript(String query) {
		Resource resource = querySingleResource(query, TBRS.NAMESPACE_PACKAGELISTMANIFEST);
		if (resource != null) {
			try {
				String fileEntry = (String) resource.getContent();
				SAXBuilder builder = new SAXBuilder();
				Document file = builder.build(new ByteArrayInputStream(fileEntry.getBytes(TBRS.CHARSET)));
				String location = file.getRootElement().getAttributeValue("location");
				String hash = file.getRootElement().getChildText("hash", Namespace.getNamespace("http://leitstelle.p23r.de/NS/p23r/nrl/CommonNrl1-1"));

				P23RXmlDbResource script = new P23RXmlDbResource(location, p23rXmlDbConfig.getCollection(resource.getParentCollection().getName()));
				if (script.exists()) {
					String content = P23RXmlDbResourceContentAccess.getContentAsString(script);
					if (P23RSignature.isHashValid(DatatypeConverter.parseBase64Binary(hash), content.getBytes(TBRS.CHARSET))) {
						return content;
					} else {
						log.error("invalid hash for {} in package list", location);
					}
				}
			} catch (XMLDBException e) {
				log.error("query single resource", e);
			} catch (JDOMException e) {
				log.error("parsing script location", e);
			} catch (IOException e) {
				log.error("reading script location information", e);
			}
		}
		return "";
	}

	/**
	 * Gets the rule package specifications.
	 *
	 * @param packageId the package id
	 * @return the rule package specifications
	 */
	public Set<String> getRulePackageSpecifications(String packageId) {
		ResourceSet specifications = query(String.format(QUERY_P23RSPECIFICATIONS, packageId), TBRS.NAMESPACE_PACKAGELISTMANIFEST);
		Set<String> result = new HashSet<String>();
		try {
			ResourceIterator it = specifications.getIterator();
			while (it.hasMoreResources()) {
				Resource resource = it.nextResource();
				result.add((String) resource.getContent());
			}
		} catch (XMLDBException e) {
			log.error("iterate over dependent namespaces resource set", e);
		}
		return result;
	}

	/**
	 * Gets the latest package specification.
	 *
	 * @param packageId the package id
	 * @return the latest package specification
	 */
	public String getLatestPackageSpecification(String packageId) {
		Resource resource = querySingleResource(String.format(QUERY_LATESTP23RSPECIFICATION, packageId), TBRS.NAMESPACE_PACKAGELISTMANIFEST);
		if (resource != null) {
			try {
				return (String) resource.getContent();
			} catch (XMLDBException e) {
				log.error("querying latest specification", e);
			}
		}
		return null;
	}
	
	/**
	 * Gets the newer releases.
	 * 
	 * @param ruleId
	 *            the rule id
	 * @return the newer releases
	 */
	public List<RuleStateType> getNewerReleases(String ruleId) {
		return Collections.emptyList();
	}

	/**
	 * Gets the rule state.
	 * 
	 * @param ruleId
	 *            the rule id
	 * @return the rule state
	 */
	public RuleStateType getRuleState(String ruleId) {
		Resource resource = querySingleResource(String.format(QUERY_RULESTATE, ruleId), NAMESPACE_IRULEACTIVATE_TYPES);
		if (resource != null) {
			try {
				String state = (String) resource.getContent();

				Unmarshaller um = ctx.createUnmarshaller();
				JAXBElement<RuleStateType> elem = um.unmarshal(new StreamSource(new ByteArrayInputStream(state.getBytes(TBRS.CHARSET))), RuleStateType.class);
				return elem.getValue();
			} catch (XMLDBException e) {
				log.error("querying package state", e);
			} catch (JAXBException e) {
				log.error("unmarshallig rule package state", e);
			}
		}
		return null;
	}

	/**
	 * Gets the states.
	 * 
	 * @param clientConfig
	 *            the client config
	 * @return the states
	 */
	public List<RulePackageStateType> getStates(String clientConfig) {
		ResourceSet resources = query(QUERY_STATES, NAMESPACE_IRULEACTIVATE_TYPES);
		List<RulePackageStateType> result = new ArrayList<RulePackageStateType>();
		try {
			ResourceIterator it = resources.getIterator();
			while (it.hasMoreResources()) {
				Resource resource = it.nextResource();
				String content = (String) resource.getContent();

				try {
					Unmarshaller um = ctx.createUnmarshaller();
					JAXBElement<RulePackageStateType> elem = um.unmarshal(new StreamSource(new ByteArrayInputStream(content.getBytes(TBRS.CHARSET))),
							RulePackageStateType.class);
					result.add(elem.getValue());
				} catch (JAXBException e) {
					log.error("unmarshalling package state content", e);
				}
			}
		} catch (XMLDBException e) {
			log.error("iterate over package state resource set", e);
		}
		return result;
	}

	/**
	 * Gets the "rulePackages" element from package list manifest.
	 * 
	 * @param packageId
	 *            the package id for the requested element
	 * @return the element "rulePackages" with the given id
	 */
	public String getRulePackageFileInfo(String packageId) {
		Resource resource = querySingleResource(String.format(QUERY_RULEPACKAGEFILE, packageId), TBRS.NAMESPACE_PACKAGELISTMANIFEST);
		try {
			return (String) resource.getContent();
		} catch (XMLDBException e) {
			log.error("get package info content", e);
		}
		return null;
	}

	/**
	 * Activate group.
	 *
	 * @param groupId the group id
	 */
	public void activateGroup(String groupId) {
		query(String.format(UPDATE_GROUPACTIVATION, groupId, "true"), NAMESPACE_IRULEACTIVATE_TYPES);
		query(String.format(UPDATE_GROUPRULESDEACTIVATION, groupId, "false"), NAMESPACE_IRULEACTIVATE_TYPES);
	}

	/**
	 * Deactivate group.
	 *
	 * @param groupId the group id
	 */
	public void deactivateGroup(String groupId) {
		query(String.format(UPDATE_GROUPACTIVATION, groupId, "false"), NAMESPACE_IRULEACTIVATE_TYPES);
	}

	/**
	 * Deactivate rule.
	 *
	 * @param ruleId the rule id
	 */
	public void deactivateRule(String ruleId) {
		query(String.format(UPDATE_RULEDEACTIVATION, ruleId, "true"), NAMESPACE_IRULEACTIVATE_TYPES);
	}

	public RuleStateType getRuleStateByName(String clientConfig, String ruleGroupName, String ruleName) {
		return null;
	}
	
}
