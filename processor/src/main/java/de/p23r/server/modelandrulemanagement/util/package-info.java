/**
 * Provides classes that validate packages on the base of the TBRS specification.
 */
package de.p23r.server.modelandrulemanagement.util;