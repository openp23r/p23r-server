package de.p23r.server.modelandrulemanagement;

import java.io.Serializable;

/**
 * The Class GenerationStep.
 */
public class GenerationStep implements Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = -3592096540101035165L;

	/**
	 * The Enum ScriptType.
	 */
	public enum ScriptType {
		
		/**
		 * The transformation.
		 */
		TRANSFORMATION, 
 /**
  * The selection.
  */
 SELECTION, 
 /**
  * The dats.
  */
 DATS, 
 /**
  * The unknown.
  */
 UNKNOWN
	}

    private String namespace;

    private String name;

    private String title;

    private String notificationScript;

    private String profileScript;

    private ScriptType scriptType;

    /**
     * Gets the namespace.
     *
     * @return the namespace
     */
    public String getNamespace() {
        return namespace;
    }

    /**
     * Sets the namespace.
     *
     * @param namespace the new namespace
     */
    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the title.
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the title.
     *
     * @param title the new title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Gets the notification script.
     *
     * @return the notification script
     */
    public String getNotificationScript() {
        return notificationScript;
    }

    /**
     * Sets the notification script.
     *
     * @param notificationScript the new notification script
     */
    public void setNotificationScript(String notificationScript) {
        this.notificationScript = notificationScript;
    }

    /**
     * Gets the profile script.
     *
     * @return the profile script
     */
    public String getProfileScript() {
        return profileScript;
    }

    /**
     * Sets the profile script.
     *
     * @param profileScript the new profile script
     */
    public void setProfileScript(String profileScript) {
        this.profileScript = profileScript;
    }

	/**
	 * Gets the script type.
	 *
	 * @return the script type
	 */
	public ScriptType getScriptType() {
		return scriptType;
	}

	/**
	 * Sets the script type.
	 *
	 * @param scriptType the new script type
	 */
	public void setScriptType(ScriptType scriptType) {
		this.scriptType = scriptType;
	}

}
