package de.p23r.server.modelandrulemanagement.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.xml.XMLConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.Resource;
import org.xmldb.api.base.ResourceIterator;
import org.xmldb.api.base.ResourceSet;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.XQueryService;

import de.p23r.common.modelandrulemanagement.archive.NotificationRulePackageArchive;

/**
 * The Class RulePackageRepository.
 */
@ApplicationScoped
public class RulePackageRepository extends XmlDbPackageRepository {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3293555881029726846L;

	private final static String COLLECTIONNAME = "rulepackages";
	
	private static final String QUERY_RULEPACKAGEID = "/*:rulePackageManifest/@id/string()";
	private static final String QUERY_RULEPACKAGENAME = "/*:rulePackageManifest[@id = '%s']/@name/string()";
	private static final String QUERY_RULEPACKAGEMANIFEST = "/*:rulePackageManifest[@id = '%s']";
	private static final String QUERY_RULEGROUPIDS = "/*:ruleGroupManifest/@id/string()";
	private static final String QUERY_RULEGROUPMANIFEST = "/*:ruleGroupManifest[@id = '%s']";
	private static final String QUERY_RULEIDS = "/*:ruleManifest/@id/string()";
	private static final String QUERY_RULEMANIFEST = "/*:ruleManifest[@id = '%s']";
	private static final String QUERY_SCHEMATARGETNAMESPACE = "/*:schema[@targetNamespace = '%s']";
	private static final String QUERY_FIRSTGENERATIONSTEP = "/*:ruleManifest/*:generation/*:steps[not(exists(@notificationType)) or @notificationType = '']";
	private static final String QUERY_GENERATIONSTEP = "/*:ruleManifest/*:generation/*:steps[@notificationType = '%s']";
	
	private final Logger log = LoggerFactory.getLogger(getClass());
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
		super.init(COLLECTIONNAME);
	}

	/* (non-Javadoc)
	 * @see de.p23r.server.modelandrulemanagement.persistence.XmlDbPackageRepository#destroy()
	 */
	@PreDestroy
	public void destroy() {
		super.destroy();
	}
	
	/**
	 * Persist package.
	 *
	 * @param archive the archive
	 */
	public void persistPackage(NotificationRulePackageArchive archive) {
		persist(archive);
	}

	/**
	 * Removes the package.
	 *
	 * @param id the id
	 */
	public void removePackage(String id) {
		String packageName = getPackageName(id);
		if (packageName != null) {
			remove(packageName);
		}
	}

	private String getPackageName(String id) {
		Resource resource = querySingleResource(String.format(QUERY_RULEPACKAGENAME, id), null);
		try {
			return resource != null ? (String)resource.getContent() : null;
		} catch (XMLDBException e) {
			log.error("get content from resource", e);
		}
		return null;
	}
	
	/**
	 * Find rule package by id.
	 *
	 * @param id the id
	 * @return the collection
	 */
	public Collection findRulePackageById(String id) {
		return findCollection(String.format(QUERY_RULEPACKAGEMANIFEST, id), null);
	}

	/**
	 * Find rule group by id.
	 *
	 * @param id the id
	 * @return the collection
	 */
	public Collection findRuleGroupById(String id) {
		return findCollection(String.format(QUERY_RULEGROUPMANIFEST, id), null);
	}

	/**
	 * Find rule by id.
	 *
	 * @param id the id
	 * @return the collection
	 */
	public Collection findRuleById(String id) {
		return findCollection(String.format(QUERY_RULEMANIFEST, id), null);
	}
	
	/**
	 * Find rule by message type.
	 *
	 * @param namespace the namespace
	 * @return the collection
	 */
	public Collection findRuleByMessageType(String namespace) {
		return findCollection(String.format(QUERY_SCHEMATARGETNAMESPACE, namespace), XMLConstants.W3C_XML_SCHEMA_NS_URI);
	}
	
	/**
	 * Find rule package group ids.
	 *
	 * @param rulepackageId the rulepackage id
	 * @return the list
	 */
	public List<String> findRulePackageGroupIds(String rulepackageId) {
		List<String> groupIds = new ArrayList<String>();
		
		Collection collection = findRulePackageById(rulepackageId);
		if (collection != null) {
			try {
				XQueryService service = (XQueryService) collection.getService("XQueryService", "1.0");

				ResourceSet result = service.query(QUERY_RULEGROUPIDS);
				ResourceIterator it = result.getIterator();
				while (it.hasMoreResources()) {
					Resource res = it.nextResource();
					groupIds.add((String)res.getContent());
				}
			} catch (XMLDBException e) {
				log.error("querying package pool", e);
			}			
		}
		return groupIds;
	}
	
	/**
	 * Gets the schema resource.
	 *
	 * @param namespace the namespace
	 * @param collection the collection
	 * @return the schema resource
	 */
	public Resource getSchemaResource(String namespace, Collection collection) {
		try {
			XQueryService service = (XQueryService) collection.getService("XQueryService", "1.0");
			if (namespace != null) {
				service.setNamespace("", namespace);
			}
			ResourceSet result = service.query(String.format(QUERY_SCHEMATARGETNAMESPACE, namespace));
			if (result != null && result.getSize() > 0) {
				return result.getResource(0);
			}
		} catch (XMLDBException e) {
			log.error("querying schema resource from collection", e);
		}
		return null;
	}

	/**
	 * Gets the first step.
	 *
	 * @param collection the collection
	 * @return the first step
	 */
	public String getFirstStep(Collection collection) {
		String step = null;
		try {
			XQueryService service = (XQueryService) collection.getService("XQueryService", "1.0");
			ResourceSet result = service.query(QUERY_FIRSTGENERATIONSTEP);
			if (result != null && result.getSize() > 0) {
				step = (String)result.getResource(0).getContent();
			}
		} catch (XMLDBException e) {
			log.error("querying first step from collection", e);
		}
		return step;
	}
	
	/**
	 * Gets the next step.
	 *
	 * @param namespace the namespace
	 * @param collection the collection
	 * @return the next step
	 */
	public String getNextStep(String namespace, Collection collection) {
		String step = null;
		try {
			XQueryService service = (XQueryService) collection.getService("XQueryService", "1.0");
			if (namespace != null) {
				service.setNamespace("", namespace);
			}
			ResourceSet result = service.query(String.format(QUERY_GENERATIONSTEP, namespace));
			if (result != null && result.getSize() > 0) {
				step = (String)result.getResource(0).getContent();
			}
		} catch (XMLDBException e) {
			log.error("querying schema resource from collection", e);
		}
		return step;
	}

	/**
	 * Find rule ids.
	 *
	 * @param groupId the group id
	 * @return the list
	 */
	public List<String> findRuleIds(String groupId) {
		List<String> ruleIds = new ArrayList<String>();
		Collection group = findRuleGroupById(groupId);
		if (group != null) {
			try {
				XQueryService service = (XQueryService) group.getService("XQueryService", "1.0");
				ResourceSet resources = service.query(QUERY_RULEIDS);
				ResourceIterator it = resources.getIterator();
				while (it.hasMoreResources()) {
					Resource resource = it.nextResource();
					ruleIds.add((String)resource.getContent());
				}
			} catch (XMLDBException e) {
				log.error("querying rule ids", e);
			}
		}
		
		return ruleIds;
	}

	/**
	 * Gets the package id by rule id.
	 *
	 * @param ruleId the rule id
	 * @return the package id by rule id
	 */
	public String getPackageIdByRuleId(String ruleId) {
		Collection col = findRuleById(ruleId);
		if (col != null) {
			try {
				XQueryService service = (XQueryService) col.getParentCollection().getParentCollection().getService("XQueryService", "1.0");
				ResourceSet resources = service.query(QUERY_RULEPACKAGEID);			
				if (resources.getSize() > 0) {
					return (String)resources.getResource(0).getContent();
				}
			} catch (XMLDBException e) {
				log.error("querying rule ids", e);
			}
		}
		return null;
	}

}
