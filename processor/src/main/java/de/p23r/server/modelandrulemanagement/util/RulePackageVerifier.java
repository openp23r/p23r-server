package de.p23r.server.modelandrulemanagement.util;

import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.p23r.common.existdb.P23RXmlDbConfig;
import de.p23r.common.modelandrulemanagement.archive.NotificationRulePackageArchive;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.types.RulePackageStateType;
import de.p23r.server.xml.P23RProcessor;

/**
 * The Class RulePackageVerifier.
 */
@ApplicationScoped
public class RulePackageVerifier {

	private final Logger log = LoggerFactory.getLogger(getClass());
	
	@Inject
	private P23RProcessor p23rProcessor;
	
	@Inject
	private P23RXmlDbConfig p23rConfig;
	
	/**
	 * Verify package.
	 *
	 * @param rulepackageArchive the rulepackage archive
	 * @param state the state
	 * @return true, if successful
	 */
	public boolean verifyPackage(NotificationRulePackageArchive rulepackageArchive, RulePackageStateType state) {
		boolean verificationDisabled = Boolean.parseBoolean(p23rConfig.get(P23RXmlDbConfig.XQUERY_GENERAL_VERIFICATIONDISABLED, "false"));
		if (verificationDisabled) {
			return true;
		}
		String root = null; // one root allowed
		Map<String, String> tmp = rulepackageArchive.getContent("[^/]+/");
		if (tmp.size() != 1) {
			log.error("Rule package contains more then one root folder");
			if (state != null) {
				state.getErrors().add("Regelpaket enthält nicht genau einen Paket-Ordner.");				
			}
			return false;
		}
		root = tmp.keySet().iterator().next();			

		tmp = rulepackageArchive.getContent(root + "[^/]+/");
		if (tmp.size() != 1) {
			log.error("Rule package contains more then one release folder");
			if (state != null) {
				state.getErrors().add("Regelpaket enthält nicht genau einen Release-Ordner.");				
			}
			return false;
		}

//		String release = tmp.keySet().iterator().next();			

		// package manifest schema conform?
		String m = rulepackageArchive.getNotificationRulePackageManifest();
		if (!p23rProcessor.validate(m, null)) {
			log.error("Rule package contains an invalid package manifest");
			if (state != null) {
				state.getErrors().add("Regelpaket enthält ein nicht schemakonformes Manifest.");				
			}
			return false;
		}

		// here we can test if the release and package name equals the hierarchy of the archive.
		
		for (String group : rulepackageArchive.getNotificationRuleGroupPathes()) {
			String groupManifest = rulepackageArchive.getNotificationRuleGroupManifest(group);
			if (!p23rProcessor.validate(groupManifest, null)) {
				log.error("Rule package contains an invalid group manifest");
				if (state != null) {
					state.getErrors().add("Regelpaket enthält ein nicht schemakonformes Gruppenmanifest.");					
				}
				return false;				
			} else {
				for (String rule : rulepackageArchive.getNotificationRulePathes(group)) {
					String ruleManifest = rulepackageArchive.getNotificationRuleManifest(rule);
					if (!p23rProcessor.validate(ruleManifest, null)) {
						log.error("Rule package contains an invalid rule manifest");
						if (state != null) {
							state.getErrors().add("Regelpaket enthält ein nicht schemakonformes Regelmanifest.");							
						}
						return false;						
					}
				}
			}
		}
		return true;
	}
	
}
