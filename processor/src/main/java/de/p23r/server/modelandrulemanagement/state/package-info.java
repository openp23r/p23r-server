/**
 * Provides State calculation and investigation functionality.
 */
package de.p23r.server.modelandrulemanagement.state;