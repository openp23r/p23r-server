package de.p23r.server.modelandrulemanagement;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.xml.bind.DatatypeConverter;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import net.sf.ehcache.config.CacheConfiguration;
import net.sf.ehcache.store.MemoryStoreEvictionPolicy;

import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;
import org.slf4j.Logger;
import org.switchyard.component.bean.Reference;
import org.switchyard.component.bean.Service;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.Resource;
import org.xmldb.api.base.XMLDBException;

import de.p23r.common.existdb.P23RXmlDbConfig;
import de.p23r.common.existdb.P23RXmlDbResource;
import de.p23r.common.existdb.P23RXmlDbResourceContentAccess;
import de.p23r.common.modelandrulemanagement.P23RSignature;
import de.p23r.common.modelandrulemanagement.SchemaHelper;
import de.p23r.common.modelandrulemanagement.SchemaHelperFactory;
import de.p23r.common.modelandrulemanagement.TransformerHelper;
import de.p23r.common.modelandrulemanagement.archive.ModelPackageArchive;
import de.p23r.common.modelandrulemanagement.archive.NotificationRulePackageArchive;
import de.p23r.common.modelandrulemanagement.archive.NotificationRulePackageListArchive;
import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault_Exception;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.GetNotificationRulePackageStates;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.GetNotificationRulePackageStatesResponse;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.IsUpToDate;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.IsUpToDateResponse;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.SetNotificationRulePackageStates;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.SetNotificationRulePackageStatesResponse;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.UpdateNotificationRulePackages;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.UpdateNotificationRulePackagesResponse;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.types.RuleGroupStateType;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.types.RulePackageStateResultType;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.types.RulePackageStateType;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.types.RuleStateType;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.types.UpToDateResultType;
import de.p23r.leitstelle.ns.p23r.nrl.commonnrl1_1.PackageSet;
import de.p23r.leitstelle.ns.p23r.nrl.packagelist1_1.ModelPackage;
import de.p23r.leitstelle.ns.p23r.nrl.packagelist1_1.PackageList;
import de.p23r.leitstelle.ns.p23r.nrl.packagelist1_1.RulePackage;
import de.p23r.leitstelle.ns.p23r.nrl.recommendationresult1_1.RecommendationResult;
import de.p23r.leitstelle.ns.p23r.nrl.rulemanifest1_1.RuleManifest;
import de.p23r.server.common.GenerationContext;
import de.p23r.server.common.P23RError;
import de.p23r.server.common.P23RTsl;
import de.p23r.server.common.TBRS;
import de.p23r.server.common.annotations.P23RUnit;
import de.p23r.server.datapool.DataPoolService;
import de.p23r.server.exceptions.P23RRuleNotFoundException;
import de.p23r.server.exceptions.P23RServiceException;
import de.p23r.server.modelandrulemanagement.persistence.ModelPackageRepository;
import de.p23r.server.modelandrulemanagement.persistence.PackageListsRepository;
import de.p23r.server.modelandrulemanagement.persistence.RulePackageRepository;
import de.p23r.server.modelandrulemanagement.state.NotificationRulePackageStateHelper;
import de.p23r.server.modelandrulemanagement.state.StateChangeRequests;
import de.p23r.server.modelandrulemanagement.state.StateChangeRequests.NotificationRuleChangeType;
import de.p23r.server.modelandrulemanagement.state.StateChangeRequests.NotificationRuleGroupChangeType;
import de.p23r.server.modelandrulemanagement.state.StateChangeRequests.NotificationRulePackageChangeType;
import de.p23r.server.modelandrulemanagement.util.ModelPackageVerifier;
import de.p23r.server.modelandrulemanagement.util.RulePackageConfigurator;
import de.p23r.server.modelandrulemanagement.util.RulePackageVerifier;
import de.p23r.server.protocolpool.Protocol;
import de.p23r.server.scheduler.JobDescriptionParameter;
import de.p23r.server.scheduler.SchedulerService;
import de.p23r.server.xml.P23RProcessor;
import static de.p23r.server.common.Messages.*;

/**
 * The Class ModelAndRuleManagementServiceBean.
 *
 * @author sim
 */
@Service(ModelAndRuleManagementService.class)
public class ModelAndRuleManagementServiceBean implements ModelAndRuleManagementService {

	private static final String COMPONENT_NAME = message(MODELANDRULEMANAGEMENT);

	@Inject
	private Logger log;

	private CacheManager manager;

	@Inject
	@Reference
	private SchedulerService scheduler;

	@Inject
	@Reference
	private DataPoolService datapool;

	@Inject
	@P23RUnit
	private Protocol protocol;

	@Inject
	private P23RXmlDbConfig p23rConfig;

	@Inject
	private P23RProcessor p23rProcessor;

	@Inject
	private PackageListsRepository packagelistsRepository;

	@Inject
	private ModelPackageRepository modelpackageRepository;

	@Inject
	private RulePackageRepository rulepackageRepository;

	@Inject
	private ModelAndRuleDepot depot;

	@Inject
	private P23RTsl p23rTsl;

	@Inject
	private RulePackageConfigurator rulePackageConfigurator;

	@Inject
	private RulePackageVerifier rulePackageVerifier;

	@Inject
	private ModelPackageVerifier modelPackageVerifier;

	@Inject
	@P23RUnit
	private Event<String> initialized;

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
		manager = CacheManager.newInstance();
		Cache testCache = new Cache(new CacheConfiguration("p23rcache", 10000).memoryStoreEvictionPolicy(MemoryStoreEvictionPolicy.LFU).eternal(true));
		manager.addCache(testCache);

		initialized.fire(COMPONENT_NAME);
	}

	/**
	 * Destroy.
	 */
	@PreDestroy
	public void destroy() {
		manager.shutdown();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.p23r.server.modelandrulemanagement.ModelAndRuleManagementService#
	 * getNotificationRulePackageStates
	 * (de.p23r.leitstelle.ns.p23r.iruleactivate1_0
	 * .GetNotificationRulePackageStates)
	 */
	@Override
	public GetNotificationRulePackageStatesResponse getNotificationRulePackageStates(GetNotificationRulePackageStates request) {
		String clientConfig = p23rConfig.getClientConfig(request.getTenant());

		List<RulePackageStateType> states = packagelistsRepository.getStates(clientConfig);

		log.debug("successfully provided notification rule package state information");

		GetNotificationRulePackageStatesResponse response = new GetNotificationRulePackageStatesResponse();
		response.setResult(new RulePackageStateResultType());
		response.getResult().getPackageStates().addAll(states);
		response.getResult().getControlCentres().add(p23rTsl.discoverModelAndRuleDepotAddress(clientConfig));
		return response;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.p23r.server.modelandrulemanagement.ModelAndRuleManagementService#
	 * isUpToDate(de.p23r.leitstelle.ns.p23r.iruleactivate1_0.IsUpToDate)
	 */
	@Override
	public IsUpToDateResponse isUpToDate(IsUpToDate request) throws P23RAppFault_Exception {
		String ruleId = request.getRuleId();

		String tenant = request.getTenant();
		if (tenant == null) {
			String msg = message(PARAMETER_MISSING, new String[] { "tenant" });
			protocol.logError(P23RError.PARAMETER_MISSING, msg);
			throw new P23RAppFault_Exception(msg);
		}

		RuleStateType ruleState = packagelistsRepository.getRuleState(ruleId);
		if (ruleState == null) {
			String msg = message(NO_RULE_FOUND, new String[] { ruleId });
			protocol.logError(P23RError.PARAMETER_INVALID, msg);
			throw new P23RAppFault_Exception(msg);
		}

		IsUpToDateResponse response = new IsUpToDateResponse();
		response.setResult(new UpToDateResultType());
		response.getResult().setNewRuleIsKnown(false);
		response.getResult().setNewRuleIsActivated(false);
		response.getResult().setNewRuleIsActive(false);

		List<RuleStateType> states = packagelistsRepository.getNewerReleases(ruleId);
		RuleStateType newest = getNewestRuleState(states);
		if (newest != null) {
			response.getResult().setNewRuleIsKnown(true);
			response.getResult().setNewRuleIsActive(newest.isIsActive());
			response.getResult().setNewRuleIsActivated(!newest.isIsDeactivated());
		}

		protocol.logInfo(null, ruleState.getId(), null, message(MODELANDRULEMANAGEMENT_RULE_UPTODATE_SUCCESSFULL));

		return response;
	}

	private RuleStateType getNewestRuleState(List<RuleStateType> states) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.p23r.server.modelandrulemanagement.ModelAndRuleManagementService#
	 * updateNotificationRulePackages
	 * (de.p23r.leitstelle.ns.p23r.iruleactivate1_0
	 * .UpdateNotificationRulePackages)
	 */
	@Override
	public UpdateNotificationRulePackagesResponse updateNotificationRulePackages(UpdateNotificationRulePackages request) throws P23RAppFault_Exception {
		update(p23rConfig.getClientConfig(request.getTenant()));
		// log info here
		return new UpdateNotificationRulePackagesResponse();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.p23r.server.modelandrulemanagement.ModelAndRuleManagementService#
	 * setNotificationRulePackageStates
	 * (de.p23r.leitstelle.ns.p23r.iruleactivate1_0
	 * .SetNotificationRulePackageStates)
	 */
	@Override
	public SetNotificationRulePackageStatesResponse setNotificationRulePackageStates(SetNotificationRulePackageStates request) throws P23RAppFault_Exception {
		String clientConfig = p23rConfig.getClientConfig(request.getTenant());

		NotificationRulePackageStateHelper stateHelper = new NotificationRulePackageStateHelper(request.getPackageStates(),
				packagelistsRepository.getStates(clientConfig));

		// generate StateChangeRequests
		StateChangeRequests stateChangeRequests = stateHelper.generateStateChangeRequests();

		if (stateChangeRequests.isValid()) {
			log.debug("requested changes are valid");

			// the sequence is important. which strategy is to investigate
			// e.g. first all activations, then deactivations, ...
			// apply changes
			if (stateChangeRequests.isPackageChangeRequested()) {
				applyStateChangeRequests(stateChangeRequests, request.getTenant());
			}
			// apply group changes
			if (stateChangeRequests.isGroupChangeRequested()) {
				applyGroupChangeRequest(stateChangeRequests);
			}
			// apply rule changes
			if (stateChangeRequests.isRuleChangeRequested()) {
				applyRuleChangeRequest(stateChangeRequests);
			}
		}

		SetNotificationRulePackageStatesResponse response = new SetNotificationRulePackageStatesResponse();
		response.setResult(new RulePackageStateResultType());
		response.getResult().getPackageStates().addAll(packagelistsRepository.getStates(clientConfig));
		return response;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.p23r.server.modelandrulemanagement.ModelAndRuleManagementService#
	 * resolveResource(java.lang.String)
	 */
	@Override
	public String resolveResource(String location) {
		log.debug("resolving the following resource: {}", location);

		Cache cache = manager.getCache("p23rcache");
		Element cached = cache.get(location);
		if (cached != null) {
			return (String) cached.getObjectValue();
		}

		int schemeIndex = location.indexOf(':');
		if (schemeIndex < 1) {
			return null;
		}

		String scheme = location.substring(0, schemeIndex);
		String rest = location.substring(schemeIndex + 3);

		P23RXmlDbResource resource = null;
		if ("model".equals(scheme)) {
			resource = modelpackageRepository.getResource(rest);
		} else {
			resource = rulepackageRepository.getResource(rest);
		}

		String content = null;

		if (resource != null && resource.exists()) {
			content = P23RXmlDbResourceContentAccess.getContentAsString(resource);
			if (content != null) {
				cache.put(new Element(location, content));
			}
		}

		return content;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.p23r.server.modelandrulemanagement.ModelAndRuleManagementService#
	 * getRuleInformationByMessageType(java.lang.String)
	 */
	@Override
	public NotificationRuleInformation getRuleInformationByMessageType(String messageType) throws P23RRuleNotFoundException {
		log.debug("providing NotificationRule Information for messagetype {}", messageType);

		NotificationRuleInformation ruleInformation = new NotificationRuleInformation();

		Collection collection = rulepackageRepository.findRuleByMessageType(messageType);
		if (collection == null) {
			String msg = message(NO_RULE_FOUND, new String[] { messageType });
			protocol.logError(P23RError.PARAMETER_INVALID, msg);
			throw new P23RRuleNotFoundException(COMPONENT_NAME, msg);
		}

		P23RXmlDbResource ruleManifestResource = new P23RXmlDbResource(TBRS.MANIFEST, collection, false);
		ruleInformation.setManifest(P23RXmlDbResourceContentAccess.getContentAsString(ruleManifestResource));
		P23RXmlDbResource messageSchema = new P23RXmlDbResource(TBRS.MESSAGE_SCHEMA, collection, false);
		ruleInformation.setMessageSchema(P23RXmlDbResourceContentAccess.getContentAsString(messageSchema));
		try {
			P23RXmlDbResource ruleGroupManifestResource = new P23RXmlDbResource(TBRS.MANIFEST, collection.getParentCollection(), false);
			ruleInformation.setGroupManifest(P23RXmlDbResourceContentAccess.getContentAsString(ruleGroupManifestResource));
		} catch (XMLDBException e) {
			String msg = "group manifest not available";
			protocol.logError(P23RError.GENERAL, msg);
			throw new P23RRuleNotFoundException(COMPONENT_NAME, msg);
		}

		P23RXmlDbResource multiProfileSelectionResource = new P23RXmlDbResource(TBRS.MULTINOTIFICATIONPROFILESELECTION, collection, false);
		if (multiProfileSelectionResource.exists()) {
			ruleInformation.setMultiProfileSelection(P23RXmlDbResourceContentAccess.getContentAsString(multiProfileSelectionResource));
		}
		P23RXmlDbResource multiProfileTransformationResource = new P23RXmlDbResource(TBRS.MULTINOTIFICATIONPROFILETRANSFORMATION, collection, false);
		if (multiProfileTransformationResource.exists()) {
			ruleInformation.setMultiProfileTransformation(P23RXmlDbResourceContentAccess.getContentAsString(multiProfileTransformationResource));
		}

		RuleManifest manifest = SchemaHelperFactory.getNotificationRuleManifestSchemaHelper().create(ruleInformation.getManifest());

		// check state (activated?)
		RuleStateType state = packagelistsRepository.getRuleState(manifest.getId());
		ruleInformation.setActivated(state != null ? !state.isIsDeactivated() : false);

		String packageId = rulepackageRepository.getPackageIdByRuleId(manifest.getId());
		ruleInformation.setSpecification(packagelistsRepository.getLatestPackageSpecification(packageId));

//		String msg = message(MODELANDRULEMANAGEMENT_RULE_INFORMATION_SUCCESSFULL, new String[] { messageType });
//		protocol.logInfo(manifest.getName(), manifest.getId(), manifest.getRelease(), msg);

		return ruleInformation;
	}

	
	/* (non-Javadoc)
	 * @see de.p23r.server.modelandrulemanagement.ModelAndRuleManagementService#getSchemaFromNamespace(de.p23r.server.modelandrulemanagement.NextGenerationStepParameter)
	 */
	@Override
	public String getSchemaFromNamespace(NextGenerationStepParameter parameter) {
		Collection collection = rulepackageRepository.findRuleById(parameter.getRuleId());
		if (collection == null) {
			log.error("no rule for rule id {} found", parameter.getRuleId());
			return null;
		}
		
		String namespace = parameter.getNamespace();
		if (namespace != null && !namespace.isEmpty()) {
			Resource resource = rulepackageRepository.getSchemaResource(namespace, collection);
			if (resource != null) {
				try {
					return (String)resource.getContent();
				} catch (XMLDBException e) {
					log.error("getting content from schema", e);
				}
			}
		}
		return null;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.p23r.server.modelandrulemanagement.ModelAndRuleManagementService#
	 * installRulepackage(java.lang.String)
	 */
	@Override
	public String installRulepackage(GenerationContext context) {

		String username = context.getProperties().get("username");
		String password = context.getProperties().get("password");
		String transactionId = context.getProperties().get("transactionId");

		String rulepackage = context.getProperties().get("rulepackage");
		
		NotificationRulePackageArchive notificationRulePackageArchive = (NotificationRulePackageArchive) depot.loadPackageFromUri(rulepackage, username, password);

		if (notificationRulePackageArchive != null) {
			if (!rulePackageVerifier.verifyPackage(notificationRulePackageArchive, null)) {
				return null;
			}
			String id = notificationRulePackageArchive.getNotificationRulePackageManifestHelper().getId();
			if (rulepackageRepository.findRulePackageById(id) == null) {
				rulepackageRepository.persistPackage(notificationRulePackageArchive);
				Collection collection = rulepackageRepository.findRulePackageById(id);
				if (collection != null) {
					rulePackageConfigurator.configurePackage(collection, null);
				}
			} else {
				id = "[]";
			}

			return id;
		} else {
			protocol.logError(P23RError.GENERAL, transactionId, null, "Regelpaket konnte nicht vom Depot heruntergeladen werden.");
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.p23r.server.modelandrulemanagement.ModelAndRuleManagementService#
	 * uninstallRulepackage(java.lang.String)
	 */
	@Override
	public void uninstallRulepackage(String rulepackageId) {
		List<String> groupIds = rulepackageRepository.findRulePackageGroupIds(rulepackageId);
		rulePackageConfigurator.removeConfigurations(groupIds);
		rulepackageRepository.removePackage(rulepackageId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.p23r.server.modelandrulemanagement.ModelAndRuleManagementService#
	 * getCoreTransformation(java.lang.String)
	 */
	@Override
	public String getCoreTransformation(String notificationRuleId) {
		Collection collection = rulepackageRepository.findRuleById(notificationRuleId);
		P23RXmlDbResource resource = new P23RXmlDbResource(TBRS.CORETRANSFORMATION, collection, false);
		return resource.exists() ? P23RXmlDbResourceContentAccess.getContentAsString(resource) : null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.p23r.server.modelandrulemanagement.ModelAndRuleManagementService#
	 * getPresentationTransformation
	 * (de.p23r.server.modelandrulemanagement.ChannelInformationParameter)
	 */
	@Override
	public String getPresentationTransformation(ChannelInformationParameter channelInformation) {
		String ruleId = channelInformation.getRuleId();
		String receiverGroup = channelInformation.getReceiverGroup();
		String channelName = channelInformation.getChannelName();

		Collection collection = rulepackageRepository.findRuleById(ruleId);

		List<String> representations = new ArrayList<String>();

		try {
			for (String resource : collection.listResources()) {
				if (resource.matches(TBRS.REGEX_REPRESENTATION)) {
					representations.add(resource);
				}
			}
		} catch (XMLDBException e) {
			log.error("getting list of representations", e);
		}

		// 1. perfect match
		String presentationScript = matchPresentationScript(receiverGroup, channelName, representations);
		if (presentationScript == null) {
			presentationScript = matchPresentationScript("", channelName, representations);
		}
		if (presentationScript == null) {
			presentationScript = matchPresentationScript(receiverGroup, "", representations);
		}
		if (presentationScript == null) {
			presentationScript = matchPresentationScript("", "", representations);
		}

		if (presentationScript != null) {
			P23RXmlDbResource resource = new P23RXmlDbResource(presentationScript, collection, false);
			return P23RXmlDbResourceContentAccess.getContentAsString(resource);
		} else {
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.p23r.server.modelandrulemanagement.ModelAndRuleManagementService#
	 * getFirstGenerationStep(java.lang.String)
	 */
	@Override
	public GenerationStep getFirstGenerationStep(String ruleId) {
		GenerationStep firstStep = new GenerationStep();

		Collection collection = rulepackageRepository.findRuleById(ruleId);
		if (collection == null) {
			log.error("no rule for rule id {} found", ruleId);
		} else {
			String step = rulepackageRepository.getFirstStep(collection);
			if (step != null) {
				firstStep = createGenerationStep(step, collection);				
			}
		}

		return firstStep;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.p23r.server.modelandrulemanagement.ModelAndRuleManagementService#
	 * getNextGenerationStep
	 * (de.p23r.server.modelandrulemanagement.NextGenerationStepParameter)
	 */
	@Override
	public GenerationStep getNextGenerationStep(NextGenerationStepParameter parameter) {
		String namespace = parameter.getNamespace();

		GenerationStep nextStep = new GenerationStep();
		nextStep.setNamespace(namespace);

		Collection collection = rulepackageRepository.findRuleById(parameter.getRuleId());
		if (collection == null) {
			log.error("no rule for rule id {} found", parameter.getRuleId());
			return nextStep;
		}

		if (namespace != null && !namespace.isEmpty()) {
			String step = rulepackageRepository.getNextStep(namespace, collection);
			if (step != null) {
				nextStep = createGenerationStep(step, collection);
				nextStep.setNamespace(namespace);
			}
		}

		return nextStep;
	}

	private GenerationStep createGenerationStep(String step, Collection collection) {
		GenerationStep generationStep = new GenerationStep();
		
		SAXBuilder builder = new SAXBuilder();
		Document doc = null;
		try {
			doc = builder.build(new ByteArrayInputStream(step.getBytes(TBRS.CHARSET)));
			generationStep.setName(doc.getRootElement().getAttributeValue("name"));
			generationStep.setTitle(doc.getRootElement().getAttributeValue("title"));

			String notificationScriptName = doc.getRootElement().getAttributeValue("notificationTransformation");
			if (notificationScriptName != null) {
				int idx = notificationScriptName.lastIndexOf('.');
				if (idx != -1) {
					String extension = notificationScriptName.substring(idx);
					if (".xslt".equalsIgnoreCase(extension)) {
						generationStep.setScriptType(GenerationStep.ScriptType.TRANSFORMATION);
					} else if (".xquery".equalsIgnoreCase(extension)) {
						generationStep.setScriptType(GenerationStep.ScriptType.SELECTION);
					} else if (".dats".equalsIgnoreCase(extension)) {
						generationStep.setScriptType(GenerationStep.ScriptType.DATS);
					} else {
						generationStep.setScriptType(GenerationStep.ScriptType.UNKNOWN);
					}
				}
				P23RXmlDbResource notificationScript = new P23RXmlDbResource(notificationScriptName, collection, false);
				if (notificationScript.exists()) {
					generationStep.setNotificationScript(P23RXmlDbResourceContentAccess.getContentAsString(notificationScript));					
				}
			}

			String profileScriptName = doc.getRootElement().getAttributeValue("profileTransformation");
			if (profileScriptName != null) {
				P23RXmlDbResource profileScript = new P23RXmlDbResource(profileScriptName, collection, false);
				if (profileScript.exists()) {
					generationStep.setProfileScript(P23RXmlDbResourceContentAccess.getContentAsString(profileScript));					
				}
			}
		} catch (JDOMException e) {
			log.error("step conversion", e);
		} catch (IOException e) {
			log.error("reading step", e);
		}

		return generationStep;
	}
	
	private String matchPresentationScript(String receiverGroup, String channelName, List<String> representations) {
		for (String representation : representations) {
			String[] nameParts = representation.replace("\\.*$", "").split("-");
			String name = "";
			if (nameParts.length >= 2) {
				name = nameParts[1];
			}
			String group = "";
			if (nameParts.length == 3) {
				group = nameParts[2];
			}
			if (group.equals(receiverGroup) && name.equals(channelName)) {
				return representation;
			}
		}
		return null;
	}

	/**
	 * Applies the state change requests for the given request and the tenant.
	 * 
	 * @param stateChangeRequests
	 *            the request
	 * @param tenant
	 *            the tenant
	 * @throws P23RServiceException
	 */
	private void applyStateChangeRequests(StateChangeRequests stateChangeRequests, String tenant) {
		log.debug("package changes requested.");
		for (String packageId : stateChangeRequests.getNotificationRulePackageStateChanges().keySet()) {
			NotificationRulePackageChangeType changeType = stateChangeRequests.getNotificationRulePackageStateChanges().get(packageId);
			switch (changeType) {
			case SELECTION:
				selectNotificationRulePackage(packageId, tenant);
				break;
			case DESELECTION:
				deselectNotificationRulePackage(packageId);
				break;
			default:
				break;
			}
		}
	}

	/**
	 * Applies the group change request for the given request.
	 * 
	 * @param stateChangeRequests
	 *            the request
	 */
	private void applyGroupChangeRequest(StateChangeRequests stateChangeRequests) {
		log.debug("group changes requested.");
		for (String groupId : stateChangeRequests.getNotificationRuleGroupStateChanges().keySet()) {
			NotificationRuleGroupChangeType changeType = stateChangeRequests.getNotificationRuleGroupStateChanges().get(groupId);
			switch (changeType) {
			case ACTIVATION:
				activateNotificationRuleGroup(groupId);
				break;
			case DEACTIVATION:
				deactivateNotificationRuleGroup(groupId);
				break;
			default:
				break;
			}
		}
	}

	/**
	 * Applies the rule change request for the given request.
	 * 
	 * @param stateChangeRequests
	 *            the request
	 */
	private void applyRuleChangeRequest(StateChangeRequests stateChangeRequests) {
		log.debug("rule changes requested.");
		for (String ruleId : stateChangeRequests.getNotificationRuleStateChanges().keySet()) {
			NotificationRuleChangeType changeType = stateChangeRequests.getNotificationRuleStateChanges().get(ruleId);
			if (changeType == NotificationRuleChangeType.DEACTIVATION) {
				deactivateNotificationRule(ruleId);
			}
		}
	}

	/**
	 * Selects the notification rule package for the given package id and the
	 * tenant and loads it if necessary.
	 * 
	 * @param packageId
	 *            the package id
	 * @param tenant
	 *            the tenant
	 * @throws P23RServiceException
	 */
	private void selectNotificationRulePackage(String packageId, String tenant) {

		RulePackageStateType state = packagelistsRepository.getState(packageId);
		if (state == null) {
			log.error("unable to retrieve NotificationRulePackage with id {}", packageId);
			return;
		}

		String clientConfig = p23rConfig.getClientConfig(tenant);

		// retrieve package only if package wasn't selected before
		// check this by manifest availability
		// Collection rulepackage =
		// rulepackageRepository.findRulePackageById(packageId);
		if (rulepackageRepository.findRulePackageById(packageId) == null) {
			String info = packagelistsRepository.getRulePackageFileInfo(packageId);
			String hash = null;
			String location = null;

			SAXBuilder builder = new SAXBuilder();
			try {
				Document file = builder.build(new ByteArrayInputStream(info.getBytes(TBRS.CHARSET)));
				location = file.getRootElement().getAttributeValue("location");
				hash = file.getRootElement().getChildText("hash", Namespace.getNamespace("http://leitstelle.p23r.de/NS/p23r/nrl/CommonNrl1-1"));
			} catch (JDOMException e) {
				log.error("parsing file info", e);
				return;
			} catch (IOException e) {
				log.error("reading file info bytes", e);
				return;
			}

			String username = p23rConfig.get(String.format(P23RXmlDbConfig.XQUERY_USERNAME, clientConfig), "");
			String password = p23rConfig.get(String.format(P23RXmlDbConfig.XQUERY_PASSWORD, clientConfig), "");

			if (location != null) {
				if (location.startsWith("file://")) {
					location = location.substring(7);
				}
				NotificationRulePackageArchive rulePackageArchive = (NotificationRulePackageArchive) depot.loadPackageFromUri(location, username, password);
				if (P23RSignature.isHashValid(DatatypeConverter.parseBase64Binary(hash), rulePackageArchive.getZipContent())) {
					if (rulePackageVerifier.verifyPackage(rulePackageArchive, state)) {
						rulepackageRepository.persistPackage(rulePackageArchive);
						state.setIsSelected(true);
					}
				} else {
					protocol.logError(P23RError.VALIDATION_FAILURE, "Paketarchiv des Paketes mit der Id " + packageId + " hat ungültigen hash-Wert!");
					state.getErrors().add("Ungültiger hash-Wert des Paketarchives!");
				}
			}
		} else {
			state.setIsSelected(true);
		}

		if (state.isIsSelected()) {
			// rulepackage =
			// rulepackageRepository.findRulePackageById(packageId);

			List<String> groups = rulepackageRepository.findRulePackageGroupIds(packageId);
			for (String groupId : groups) {
				RuleGroupStateType groupState = findRuleGroupState(groupId, state);
				groupState.setIsActivated(false);
				groupState.setIsActive(false);
				groupState.setIsRecommended(isNotificationRuleGroupRecommended(groupId, clientConfig));

				List<String> rules = rulepackageRepository.findRuleIds(groupId);
				for (String ruleId : rules) {
					RuleStateType ruleState = findRuleState(ruleId, groupState);
					ruleState.setIsActive(false);
					ruleState.setIsDeactivated(true);
				}
			}
			log.debug("successfully selected rule package {}", packageId);
			protocol.logInfo(message(MODELANDRULEMANAGEMENT_RULEPACKAGE_SELECTION_SUCCESSFULL, new String[] { packageId }));
		} else {
			log.error("rule package selection for {} failed", packageId);
			protocol.logError(P23RError.VALIDATION_FAILURE, "Selektion des Regelpaketes " + packageId + " schlug fehl");
		}
		packagelistsRepository.persistState(state);

		// lets try to configure only when group activated
		// rulePackageConfigurator.configurePackage(rulepackage, state);

		// should we deal with model package dependencies here?
	}

	private RuleGroupStateType findRuleGroupState(String groupId, RulePackageStateType state) {
		for (RuleGroupStateType groupState : state.getRuleGroups()) {
			if (groupState.getId().equals(groupId)) {
				return groupState;
			}
		}
		RuleGroupStateType groupState = new RuleGroupStateType();
		groupState.setId(groupId);
		state.getRuleGroups().add(groupState);
		return groupState;
	}

	private RuleStateType findRuleState(String ruleId, RuleGroupStateType groupState) {
		for (RuleStateType ruleState : groupState.getRules()) {
			if (ruleState.getId().equals(ruleId)) {
				return ruleState;
			}
		}
		RuleStateType ruleState = new RuleStateType();
		ruleState.setId(ruleId);
		groupState.getRules().add(ruleState);
		return ruleState;
	}

	private boolean isNotificationRulePackageSupported(String rulepackageId, String clientConfig) throws P23RAppFault_Exception {
		
		List<String> configuredNamespaces = datapool.getNamespaceList(clientConfig);
		configuredNamespaces.addAll(configuredNamespaces);

		// we should also check for dependent rule packages...

		Set<String> dependentNamespaces = packagelistsRepository.getDependentModelNamespaces(rulepackageId);
		for (String namespace : configuredNamespaces) {
			dependentNamespaces.remove(namespace);
		}

		boolean result = dependentNamespaces.isEmpty() ? true : false;

		log.debug("package support for package {} is: {}", rulepackageId, result);

		if (!result) {
			log.warn("not all required namespaces for package {} are configured!", rulepackageId);
			for (String namespace : dependentNamespaces) {
				log.debug("{} is not configured!", namespace);
			}
		}

		return result;
	}

	/**
	 * Checks if is notification rule package recommended.
	 *
	 * @param rulepackageId
	 *            the rulepackage id
	 * @param clientConfig
	 *            the client config
	 * @return true, if is notification rule package recommended
	 * @throws P23RAppFault_Exception
	 *             the p23 r app fault_ exception
	 */
	private boolean isNotificationRulePackageRecommended(String rulepackageId, String clientConfig) throws P23RAppFault_Exception {
		// get scripts
		String selectionScript = packagelistsRepository.getRecommendationSelectionScript(rulepackageId);
		String transformationScript = packagelistsRepository.getRecommendationTransformationScript(rulepackageId);

		return calculateRecommendation(selectionScript, transformationScript, clientConfig);
	}

	private boolean isNotificationRuleGroupRecommended(String groupId, String clientConfig) {
		Collection group = rulepackageRepository.findRuleGroupById(groupId);

		P23RXmlDbResource selection = new P23RXmlDbResource(TBRS.RECOMMENDATIONSELECTION, group, false);
		String selectionScript = selection.exists() ? P23RXmlDbResourceContentAccess.getContentAsString(selection) : "";

		P23RXmlDbResource transformation = new P23RXmlDbResource(TBRS.RECOMMENDATIONTRANSFORMATION, group, false);
		String transformationScript = transformation.exists() ? P23RXmlDbResourceContentAccess.getContentAsString(transformation) : "";

		return calculateRecommendation(selectionScript, transformationScript, clientConfig);
	}

	private boolean calculateRecommendation(String selectionScript, String transformationScript, String clientConfig) {
		if (selectionScript.isEmpty()) {
			return false;
		}

		GenerationContext context = new GenerationContext();
		context.getProperties().put(GenerationContext.SELECTION_SCRIPT, selectionScript);
		context.setClientConfig(clientConfig);

		try {
			String data = datapool.selectData(context);
			if (!transformationScript.isEmpty()) {
				data = p23rProcessor.transform(transformationScript, data, null, context);
			}
			SchemaHelper<RecommendationResult> schemaHelper = SchemaHelperFactory.getRecommendationResultSchemaHelper();
			RecommendationResult recommendationResult = schemaHelper.create(data);

			return recommendationResult.getRecommendation().equals("recommended") || recommendationResult.getRecommendation().equals("mandatory");
		} catch (P23RServiceException e) {
			log.error("selecting and transforming recommendation data", e);
		}
		
		return false;
	}

	private void deselectNotificationRulePackage(String notificationRulePackageId) {

		RulePackageStateType state = packagelistsRepository.getState(notificationRulePackageId);
		state.setIsSelected(false);
		state.getErrors().clear();
		state.getWarnings().clear();
		for (RuleGroupStateType groupState : state.getRuleGroups()) {
			groupState.setIsActive(false);
			groupState.setIsActivated(false);
			groupState.getErrors().clear();
			groupState.getWarnings().clear();
			for (RuleStateType ruleState : groupState.getRules()) {
				ruleState.setIsActive(false);
				ruleState.setIsDeactivated(true);
				ruleState.getErrors().clear();
				ruleState.getWarnings().clear();
			}
		}
		packagelistsRepository.persistState(state);
		rulepackageRepository.removePackage(notificationRulePackageId);
		log.debug("successfully deselected rule package {}", notificationRulePackageId);
		protocol.logInfo(message(MODELANDRULEMANAGEMENT_RULEPACKAGE_DESELECTION_SUCCESSFULL, new String[] { notificationRulePackageId }));
	}

	private void activateNotificationRuleGroup(String groupId) {
		Collection group = rulepackageRepository.findRuleGroupById(groupId);
		packagelistsRepository.activateGroup(groupId);
		rulePackageConfigurator.configureRuleGroup(group);

		List<String> rules = rulepackageRepository.findRuleIds(groupId);
		for (String ruleId : rules) {
			Collection rule = rulepackageRepository.findRuleById(ruleId);

			JobDescriptionParameter jobDescription = new JobDescriptionParameter();

			P23RXmlDbResource messageSchema = new P23RXmlDbResource(TBRS.MESSAGE_SCHEMA, rule, false);
			jobDescription.setSchema(P23RXmlDbResourceContentAccess.getContentAsString(messageSchema));
			P23RXmlDbResource manifest = new P23RXmlDbResource(TBRS.MANIFEST, rule, false);
			jobDescription.setManifest(P23RXmlDbResourceContentAccess.getContentAsString(manifest));
			try {
				scheduler.scheduleJob(jobDescription);
			} catch (P23RServiceException e) {
				log.error("scheduling job", e);
			}
		}

	}

	private void deactivateNotificationRuleGroup(String groupId) {

		// should we remove or keep the configuration !?

		packagelistsRepository.deactivateGroup(groupId);

		List<String> rules = rulepackageRepository.findRuleIds(groupId);

		// remove schedule information to scheduler
		for (String ruleId : rules) {
			deactivateNotificationRule(ruleId);
		}
	}

	private void deactivateNotificationRule(String ruleId) {

		packagelistsRepository.deactivateRule(ruleId);

		try {
			scheduler.cancelJob(ruleId);
		} catch (P23RServiceException e) {
			log.error("canceling job", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.p23r.server.modelandrulemanagement.ModelAndRuleManagementService#
	 * internalUpdate()
	 */
	@Override
	public void internalUpdate() {
		if (Boolean.parseBoolean(p23rConfig.get(P23RXmlDbConfig.XQUERY_GENERAL_AUTOUPDATECCSDISABLED, "true"))) {
			return;
		}
		List<String> ccs = p23rConfig.getAll(P23RXmlDbConfig.XQUERY_CCS);
		for (String cc : ccs) {
			try {
				update(cc);
			} catch (P23RAppFault_Exception e) {
				log.error("internally updating CC " + cc, e);
			}
		}
	}

	private void update(String clientConfig) throws P23RAppFault_Exception {

		NotificationRulePackageListArchive packageListArchive = depot.loadPackagesList(clientConfig);
		if (packageListArchive == null) {
			String msg = message(MODELANDRULEMANAGEMENT_UPDATING_PACKAGELIST_FAILED, new String[] { "Paketliste nicht gefunden!" });
			protocol.logError(P23RError.COMMUNICATION_FAILED, msg);
			throw new P23RAppFault_Exception(msg);
		}

		packagelistsRepository.persistPackageList(packageListArchive, clientConfig);

		String packageListManifest = packageListArchive.getPackageManifest();
		if (!new P23RSignature().validateEmbedded(TransformerHelper.elementFromString(packageListManifest))) {
			// String msg =
			// message(MODELANDRULEMANAGEMENT_UPDATING_PACKAGELIST_FAILED, new
			// String[] { "Signatur der Paketliste ungültig!" });
			String msg = "Signatur der Paketliste ungültig!";
			protocol.logWarning(msg);
			// protocolPool.logError(P23RError.VALIDATION_FAILURE, msg);
			// throw new P23RAppFault(msg);
		}

		PackageList packageList = packageListArchive.getNotificationRulePackageListManifestHelper();

		for (RulePackage rulepackage : packageList.getContent().getRulePackages()) {
			RulePackageStateType state = packagelistsRepository.getState(rulepackage.getId());
			if (state == null) {
				state = new RulePackageStateType();
				state.setId(rulepackage.getId());
				state.setIsSelected(false);
				state.setIsRecommended(false);

				// check dependent model packages here
				for (PackageSet set : rulepackage.getDependencies().getModelPackages()) {
					for (String release : set.getReleases()) {
						validateDependentModelPackage(packageList, set.getName(), release, state, clientConfig);
					}
				}

				// supported?
				if (isNotificationRulePackageSupported(rulepackage.getId(), clientConfig)) {
					state.setIsSupported(true);
					// if yes, recommended?
					if (isNotificationRulePackageRecommended(rulepackage.getId(), clientConfig)) {
						state.setIsRecommended(true);
					}
				} else {
					state.setIsSupported(false);
				}

				// we have to calculate the real value...
				state.setLatestRelease(rulepackage.getRelease());

				packagelistsRepository.persistState(state);
			}
		}
		protocol.logInfo(message(MODELANDRULEMANAGEMENT_UPDATING_PACKAGELIST_SUCCESSFULL, new String[] { packageList.getContent().getRelease(),
				packageList.getContent().getReleasedAt().toString() }));
	}

	private void validateDependentModelPackage(PackageList packageList, String name, String release, RulePackageStateType state, String clientConfig) {
		if (modelpackageRepository.findModelPackageByNameAndRelease(name, release) == null) {
			for (ModelPackage modelpackage : packageList.getContent().getModelPackages()) {
				if (name.equals(modelpackage.getName()) && release.equals(modelpackage.getRelease())) {
					String username = p23rConfig.get(String.format(P23RXmlDbConfig.XQUERY_USERNAME, clientConfig), "");
					String password = p23rConfig.get(String.format(P23RXmlDbConfig.XQUERY_PASSWORD, clientConfig), "");

					String location = modelpackage.getModelPackageFile().getLocation();
					if (location.startsWith("file://")) {
						location = location.substring(7);
					}
					ModelPackageArchive modelPackageArchive = (ModelPackageArchive) depot.loadPackageFromUri(location, username, password);

					// check hash value...
					if (P23RSignature.isHashValid(modelpackage.getModelPackageFile().getHash(), modelPackageArchive.getZipContent())) {
						if (modelPackageVerifier.verifyPackage(modelPackageArchive, state)) {
							modelpackageRepository.persistPackage(modelPackageArchive);
						} else {
							protocol.logError(P23RError.VALIDATION_FAILURE, "Modellpaket " + modelpackage.getName() + " " + modelpackage.getRelease()
									+ " hat eine ungültige Struktur!");
						}
					} else {
						protocol.logError(P23RError.VALIDATION_FAILURE, "Modellpaket " + modelpackage.getName() + " " + modelpackage.getRelease()
								+ " hat einen ungültigen hash-Wert!");
					}
				}
			}
		}
	}
}
