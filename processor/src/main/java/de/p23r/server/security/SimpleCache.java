package de.p23r.server.security;

import java.util.HashMap;
import java.util.Map;

import de.p23r.server.security.tsl.TrustServiceListCache;
import de.p23r.server.security.tsl.core.TrustServiceList;

/**
 * A <code>TrustServiceListCache</code> which caches tsl in a Java-Map. This cache is not
 * persistent.
 * 
 * @author bkrauf
 * 
 */
public class SimpleCache implements TrustServiceListCache {
    private final Map<String, TrustServiceList> cache;

    /**
     * Instantiates a new simple cache.
     */
    public SimpleCache() {
        cache = new HashMap<String, TrustServiceList>();
    }

    /*
     * (non-Javadoc)
     * @see de.p23r.common.tsl.TrustServiceListCache#has(java.lang.String)
     */
    @Override
    public boolean has(final String tslName) {
        return cache.containsKey(tslName);
    }

    /*
     * (non-Javadoc)
     * @see de.p23r.common.tsl.TrustServiceListCache#get(java.lang.String)
     */
    @Override
    public TrustServiceList get(final String tslName) {
        return cache.get(tslName);
    }

    /*
     * (non-Javadoc)
     * @see de.p23r.common.tsl.TrustServiceListCache#put(java.lang.String,
     * de.p23r.common.tsl.core.TrustServiceList)
     */
    @Override
    public void put(final String tslName, final TrustServiceList tsl) {
        if (tslName == null || tslName.isEmpty() || tsl == null) {
            return;
        }
        cache.put(tslName, tsl);
    }

}
