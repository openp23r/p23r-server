package de.p23r.server.security.tsl.core;

import java.util.List;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.etsi.uri._02231.v2_.InternationalNamesType;
import org.etsi.uri._02231.v2_.MultiLangNormStringType;
import org.etsi.uri._02231.v2_.ObjectFactory;

/**
 * Utility class for trust service lists.
 * 
 * @author fcorneli
 * 
 */
class TrustServiceListUtils {

    private static final Log LOG = LogFactory.getLog(TrustServiceListUtils.class);

    private TrustServiceListUtils() {
        super();
    }

    /**
     * Gives back the value according to the given locale.
     * 
     * @param i18nName the i18n name
     * @param locale the locale
     * @return the value
     */
    static String getValue(InternationalNamesType i18nName, Locale locale) {
        if (null == i18nName) {
            return null;
        }
        List<MultiLangNormStringType> names = i18nName.getName();
        String enValue = null;
        for (MultiLangNormStringType name : names) {
            String lang = name.getLang().toLowerCase();
            if ("en".equals(lang)) {
                enValue = name.getValue();
            }
            if (locale.getLanguage().equals(lang)) {
                return name.getValue();
            }
        }
        if (null != enValue) {
            return enValue;
        }
        return names.get(0).getValue();
    }

    /**
     * Sets the value according to the given locale.
     * 
     * @param value the value
     * @param locale the locale
     * @param i18nName the i18n name
     */
    public static void setValue(String value, Locale locale, InternationalNamesType i18nName) {
        LOG.debug("set value for locale: " + locale.getLanguage());
        List<MultiLangNormStringType> names = i18nName.getName();
        /*
         * First try to locate an existing entry for the given locale.
         */
        MultiLangNormStringType localeName = null;
        for (MultiLangNormStringType name : names) {
            String lang = name.getLang().toLowerCase();
            if (locale.getLanguage().equals(lang)) {
                localeName = name;
                break;
            }
        }
        if (null == localeName) {
            /*
             * If none was found, create a new one.
             */
            ObjectFactory objectFactory = new ObjectFactory();
            localeName = objectFactory.createMultiLangNormStringType();
            localeName.setLang(locale.getLanguage());
            names.add(localeName);
        }
        localeName.setValue(value);
    }
}
