package de.p23r.server.security.tsl;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.etsi.uri._02231.v2_.PostalAddressType;

import de.p23r.server.security.tsl.core.TrustServiceList;
import de.p23r.server.security.tsl.core.TrustServiceProvider;

/**
 * This class is a delegate for <code>TrustServiceList</code> of the eid-tsl project. It aligns TSL
 * to P23R-TSL-specification.
 * 
 * @author bkrauf
 * 
 */
public class P23RTrustServiceList {

    /**
     * The Enum StatusDeterminationApproach.
     */
    public enum StatusDeterminationApproach {

        /**
         * The ACTIVE.
         */
        ACTIVE("http://uri.etsi.org/TrstSvc/TSLType/StatusDetn/active"),

        /**
         * The PASSIVE.
         */
        PASSIVE("http://uri.etsi.org/TrstSvc/TSLType/StatusDetn/passive");

        private final String uri;

        private StatusDeterminationApproach(String uri) {
            this.uri = uri;
        }

        /**
         * Gets the.
         * 
         * @param uri the uri
         * @return the status determination approach
         */
        public static StatusDeterminationApproach get(String uri) {
            if (ACTIVE.uri.equals(uri)) {
                return ACTIVE;
            }
            return PASSIVE;
        }
    }

    private TrustServiceList tsl = null;

    /**
     * Instantiates a new p23 r trust service list.
     * 
     * @param tsl the tsl
     */
    P23RTrustServiceList(TrustServiceList tsl) {
        this.tsl = tsl;
    }

    /**
     * Gets the scheme operator name.
     * 
     * @return the scheme operator name
     */
    public String getSchemeOperatorName() {
        return tsl.getSchemeOperatorName();
    }

    /**
     * Gets the scheme operator postal address.
     * 
     * @return the scheme operator postal address
     */
    public String getSchemeOperatorPostalAddress() {
        final PostalAddressType postalAddress = tsl.getSchemeOperatorPostalAddress(Locale.GERMAN);
        final StringBuilder sb = new StringBuilder();
        sb.append(postalAddress.getStreetAddress()).append(postalAddress.getPostalCode())
                .append(postalAddress.getLocality()).append(postalAddress.getCountryName());
        return sb.toString();
    }

    /**
     * Gets the scheme operator electronic address.
     * 
     * @return the scheme operator electronic address
     */
    public List<String> getSchemeOperatorElectronicAddress() {
        return tsl.getSchemeOperatorElectronicAddresses();
    }

    /**
     * Gets the scheme name.
     * 
     * @return the scheme name
     */
    public String getSchemeName() {
        return tsl.getSchemeName();
    }

    /**
     * Gets the status determination approach.
     * 
     * @return the status determination approach
     */
    public StatusDeterminationApproach getStatusDeterminationApproach() {
        return StatusDeterminationApproach.get(tsl.getStatusDeterminationApproach());
    }

    /**
     * Gets the issue date and time.
     * 
     * @return the issue date and time
     */
    public Date getIssueDateAndTime() {
        return tsl.getIssueDate();
    }

    /**
     * Gets the next update.
     * 
     * @return the next update
     */
    public Date getNextUpdate() {
        return tsl.getNextUpdate();
    }

    /**
     * Gets the distribution points.
     * 
     * @return the distribution points
     */
    public List<String> getDistributionPoints() {
        return tsl.getDistributionPoints();
    }

    /**
     * Gets the trust service providers.
     * 
     * @return the trust service providers
     */
    public List<TrustServiceProvider> getTrustServiceProviders() {
        return tsl.getTrustServiceProviders();
    }

}
