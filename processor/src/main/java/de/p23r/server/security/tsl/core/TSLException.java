package de.p23r.server.security.tsl.core;

/**
 * The Class TSLException.
 */
public class TSLException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 2636975195665804771L;

    /**
     * Instantiates a new TSL exception.
     */
    public TSLException() {
        super();
    }

    /**
     * Instantiates a new TSL exception.
     *
     * @param msg the msg
     */
    public TSLException(String msg) {
        super(msg);
    }

    /**
     * Instantiates a new TSL exception.
     *
     * @param t the t
     */
    public TSLException(Throwable t) {
        super(t);
    }

    /**
     * Instantiates a new TSL exception.
     *
     * @param msg the msg
     * @param t the t
     */
    public TSLException(String msg, Throwable t) {
        super(msg, t);
    }
}
