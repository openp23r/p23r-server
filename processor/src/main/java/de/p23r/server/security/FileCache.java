package de.p23r.server.security;

import java.io.File;
import java.io.IOException;

import de.p23r.server.security.tsl.TrustServiceListCache;
import de.p23r.server.security.tsl.TrustServiceListException;
import de.p23r.server.security.tsl.core.TrustServiceList;
import de.p23r.server.security.tsl.core.TrustServiceListFactory;

/**
 * A <code>TrustServiceListCache</code> which caches tsl as files in a specified directory.
 * 
 * @author bkrauf
 * 
 */
class FileCache implements TrustServiceListCache {
    private final File cacheLocation;

    /**
     * Creates a new file-based TSL-Cache with the specified directory as cache-base.
     * 
     * @param cacheLocation the cache location
     */
    public FileCache(final File cacheLocation) {
        this.cacheLocation = cacheLocation;
    }

    /*
     * (non-Javadoc)
     * @see de.p23r.common.tsl.TrustServiceListCache#has(java.lang.String)
     */
    @Override
    public boolean has(final String tslName) {
        final File file = new File(cacheLocation + File.separator + tslName);
        return file.exists() && file.canRead();
    }

    /*
     * (non-Javadoc)
     * @see de.p23r.common.tsl.TrustServiceListCache#get(java.lang.String)
     */
    @Override
    public TrustServiceList get(final String tslName) throws TrustServiceListException {
        final File file = new File(cacheLocation + File.separator + tslName);
        if (!file.exists()) {
            return null;
        }
        try {
            return TrustServiceListFactory.newInstance(file.toURI());
        } catch (final IOException e) {
            throw new TrustServiceListException("Failed to read trust service list from file "
                    + file.toString() + ".", e);
        }
    }

    /*
     * (non-Javadoc)
     * @see de.p23r.common.tsl.TrustServiceListCache#put(java.lang.String,
     * de.p23r.common.tsl.core.TrustServiceList)
     */
    @Override
    public void put(final String tslName, final TrustServiceList tsl) throws TrustServiceListException {
        final File file = new File(cacheLocation + File.separator + tslName);
        try {
            tsl.saveAs(file);
        } catch (final IOException e) {
            throw new TrustServiceListException("Failed to write trust service list to file "
                    + file.toString() + ".", e);
        }
    }

}
