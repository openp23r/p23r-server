/**
 * Provides classes representing core data elements of a tsl document.
 */
package de.p23r.server.security.tsl.core;