package de.p23r.server.security.tsl.core;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.math.BigInteger;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.lang.SystemUtils;
import org.etsi.uri._01903.v1_3.QualifyingPropertiesType;
import org.etsi.uri._02231.v2_.AddressType;
import org.etsi.uri._02231.v2_.ObjectFactory;
import org.etsi.uri._02231.v2_.PostalAddressListType;
import org.etsi.uri._02231.v2_.PostalAddressType;
import org.etsi.uri._02231.v2_.TSLSchemeInformationType;
import org.etsi.uri._02231.v2_.TrustStatusListType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

/**
 * The Class TrustServiceListHelper.
 */
public abstract class TrustServiceListHelper {

    private static final Logger LOG = LoggerFactory.getLogger(TrustServiceListHelper.class);

    /**
     * Hide public defualt constructor
     */
    private TrustServiceListHelper() {
    }

    /**
     * Marshall.
     *
     * @param trustStatusList the trust status list
     * @param objectFactory the object factory
     * @return the document
     * @throws JAXBException the JAXB exception
     * @throws ParserConfigurationException the parser configuration exception
     */
    public static Document marshall(TrustStatusListType trustStatusList, ObjectFactory objectFactory)
            throws JAXBException, ParserConfigurationException {
        /*
         * Assign a unique XML Id to the TSL for signing purposes.
         */
        String tslId = "tsl-" + UUID.randomUUID().toString();
        TrustStatusListType trustStatusListType = getTrustStatusList(trustStatusList, objectFactory);
        trustStatusListType.setId(tslId);

        /*
         * TSLTag
         */
        trustStatusListType.setTSLTag(TrustServiceList.TSL_TAG);

        /*
         * Scheme Information - TSL version identifier.
         */
        TSLSchemeInformationType schemeInformation = trustStatusListType.getSchemeInformation();
        if (null == schemeInformation) {
            schemeInformation = objectFactory.createTSLSchemeInformationType();
            trustStatusListType.setSchemeInformation(schemeInformation);
        }
        schemeInformation.setTSLVersionIdentifier(BigInteger.valueOf(3));

        /*
         * Scheme Information - TSL sequence number
         */
        if (null == schemeInformation.getTSLSequenceNumber()) {
            schemeInformation.setTSLSequenceNumber(BigInteger.valueOf(1));
        }

        /*
         * Scheme Information - TSL Type
         */
        schemeInformation.setTSLType(TrustServiceList.TSL_TYPE);

        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setNamespaceAware(true);
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.newDocument();

        JAXBContext jaxbContext = JAXBContext.newInstance(ObjectFactory.class,
            org.etsi.uri.trstsvc.svcinfoext.esigdir_1999_93_ec_trustedlist.__.ObjectFactory.class,
            org.etsi.uri._02231.v2.additionaltypes_.ObjectFactory.class);
        Marshaller marshaller = jaxbContext.createMarshaller();

        ObjectFactory objFactory = new ObjectFactory();
        JAXBElement<TrustStatusListType> trustStatusListElement = objFactory
            .createTrustServiceStatusList(trustStatusListType);
        DOMResult result = new DOMResult(document);
        XMLStreamWriter xmlStreamWriter = null;
        try {
            xmlStreamWriter = XMLOutputFactory.newInstance().createXMLStreamWriter(result);
            xmlStreamWriter.setPrefix("tsl", "http://uri.etsi.org/02231/v2#");
            xmlStreamWriter.setPrefix("ds", "http://www.w3.org/2000/09/xmldsig#");
            xmlStreamWriter.setPrefix("ecc",
                "http://uri.etsi.org/TrstSvc/SvcInfoExt/eSigDir-1999-93-EC-TrustedList/#");
            xmlStreamWriter.setPrefix("xades", "http://uri.etsi.org/01903/v1.3.2#");
            xmlStreamWriter.setPrefix("tslx", "http://uri.etsi.org/02231/v2/additionaltypes#");

        } catch (XMLStreamException e) {
            LOG.error("Error occured during marshalling.", e);
        } catch (FactoryConfigurationError e) {
            LOG.error("Error occured during marshalling.", e);
        }

        marshaller.marshal(trustStatusListElement, xmlStreamWriter);

        return document;
    }

    /**
     * Gets the trust status list.
     *
     * @param trustStatusList the trust status list
     * @param objectFactory the object factory
     * @return the trust status list
     */
    public static TrustStatusListType getTrustStatusList(TrustStatusListType trustStatusList,
            ObjectFactory objectFactory) {
        if (null == trustStatusList) {
            trustStatusList = objectFactory.createTrustStatusListType();
        }
        return trustStatusList;
    }

    /**
     * To file.
     *
     * @param tslFile the tsl file
     * @param tslDocument the tsl document
     * @throws TransformerFactoryConfigurationError the transformer factory configuration error
     * @throws TransformerException the transformer exception
     * @throws FileNotFoundException the file not found exception
     */
    public static void toFile(File tslFile, Document tslDocument)
            throws TransformerFactoryConfigurationError, TransformerException, FileNotFoundException {
        Source source = new DOMSource(tslDocument);
        /*
         * new StreamResult(tslFile) doesn't work on Windows when there are spaces in the filename.
         */
        FileOutputStream outputStream = new FileOutputStream(tslFile);
        Result result = new StreamResult(outputStream);
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        /*
         * We have to omit the ?xml declaration if we want to embed the document.
         */
        // transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        transformer.transform(source, result);
    }

    /**
     * Marshall qualifying properties.
     *
     * @param document the document
     * @param qualifyingProperties the qualifying properties
     * @param xadesObjectFactory the xades object factory
     * @return the node
     */
    public static Node marshallQualifyingProperties(Document document,
            QualifyingPropertiesType qualifyingProperties,
            org.etsi.uri._01903.v1_3.ObjectFactory xadesObjectFactory) {
        Node marshallNode = document.createElement("marshall-node");
        try {
            JAXBContext jaxbContext = JAXBContext
                .newInstance(org.etsi.uri._01903.v1_3.ObjectFactory.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            DOMResult result = new DOMResult(marshallNode);

            XMLStreamWriter xmlStreamWriter = null;
            try {
                xmlStreamWriter = XMLOutputFactory.newInstance().createXMLStreamWriter(result);
                xmlStreamWriter.setPrefix("tsl", "http://uri.etsi.org/02231/v2#");
                xmlStreamWriter.setPrefix("ds", "http://www.w3.org/2000/09/xmldsig#");
                xmlStreamWriter.setPrefix("ecc",
                    "http://uri.etsi.org/TrstSvc/SvcInfoExt/eSigDir-1999-93-EC-TrustedList/#");
                xmlStreamWriter.setPrefix("xades", "http://uri.etsi.org/01903/v1.3.2#");
                xmlStreamWriter.setPrefix("tslx", "http://uri.etsi.org/02231/v2/additionaltypes#");

            } catch (XMLStreamException e) {
                LOG.error("Error occured during marshalling qualified properties.", e);
            } catch (FactoryConfigurationError e) {
                LOG.error("Error occured during marshalling qualified properties.", e);
            }

            marshaller.marshal(xadesObjectFactory.createQualifyingProperties(qualifyingProperties),
                xmlStreamWriter);
        } catch (JAXBException e) {
            throw new TSLException("JAXB error: " + e.getMessage(), e);
        }
        return marshallNode.getFirstChild();
    }

    /**
     * Checks if is java6u18 or above.
     *
     * @return true, if is java6u18 or above
     */
    public static boolean isJava6u18OrAbove() {
        String javaVersion = System.getProperty("java.version");
        String javaVmVersion = System.getProperty("java.vm.version");
        LOG.debug("java version: " + javaVersion);
        LOG.debug("java vm version: " + javaVmVersion);
        if (!SystemUtils.isJavaVersionAtLeast(160)) {
            // 1.5- here
            return false;
        }
        // 1.6+ here
        if (!javaVersion.startsWith("1.6.0")) {
            // 1.7+ here
            return true;
        }
        // 1.6 here
        String updateVersion = javaVersion.substring("1.6.0_".length());
        LOG.debug("update version: " + updateVersion);
        if (updateVersion.indexOf('-') != -1) {
            updateVersion = updateVersion.substring(0, updateVersion.indexOf('-'));
        }
        try {
            Integer updateVersionNumber = Integer.parseInt(updateVersion);
            LOG.debug("update version number: " + updateVersionNumber);
            if (updateVersionNumber < 18) {
                /*
                 * Only from Java 6u18 we have the RSA-SHA256 XML signature algo available.
                 */
                return false;
            }
        } catch (NumberFormatException e) {
            // let's give it a try in this case -> return true
            LOG.warn("java version parsing error: " + updateVersion, e);
        }
        return true;
    }

    /**
     * Returns the signature node.
     *
     * @param tslDocument the tsl document
     * @return signature node
     */
    public static Node getSignatureNode(Document tslDocument) {
        XPathFactory factory = XPathFactory.newInstance();
        XPath xpath = factory.newXPath();
        xpath.setNamespaceContext(new TSLNamespaceContext());
        XPathExpression expr;
        try {
            expr = xpath.compile("tsl:TrustServiceStatusList/ds:Signature");
            return (Node) expr.evaluate(tslDocument, XPathConstants.NODE);
        } catch (XPathExpressionException e) {
            throw new TSLException("XPath error: " + e.getMessage(), e);
        }
    }

    /**
     * Gets the scheme operator address.
     *
     * @param trustStatusList the trust status list
     * @param objectFactory the object factory
     * @return the scheme operator address
     */
    public static AddressType getSchemeOperatorAddress(TrustStatusListType trustStatusList,
            ObjectFactory objectFactory) {
        TSLSchemeInformationType schemeInformation = getSchemeInformation(trustStatusList, objectFactory);
        AddressType schemeOperatorAddress = schemeInformation.getSchemeOperatorAddress();
        if (null == schemeOperatorAddress) {
            schemeOperatorAddress = objectFactory.createAddressType();
            schemeInformation.setSchemeOperatorAddress(schemeOperatorAddress);
        }
        return schemeOperatorAddress;
    }

    /**
     * Gets the scheme information.
     *
     * @param trustStatusList the trust status list
     * @param objectFactory the object factory
     * @return the scheme information
     */
    public static TSLSchemeInformationType getSchemeInformation(TrustStatusListType trustStatusList,
            ObjectFactory objectFactory) {
        TrustStatusListType tslType = getTrustStatusList(trustStatusList, objectFactory);
        TSLSchemeInformationType tslSchemeInformation = tslType.getSchemeInformation();
        if (null == tslSchemeInformation) {
            tslSchemeInformation = objectFactory.createTSLSchemeInformationType();
            tslType.setSchemeInformation(tslSchemeInformation);
        }
        return tslSchemeInformation;
    }

    /**
     * Gets the postal addresses.
     *
     * @param trustStatusList the trust status list
     * @return the postal addresses
     */
    public static List<PostalAddressType> getPostalAddresses(TrustStatusListType trustStatusList) {
        if (trustStatusList != null) {
            TSLSchemeInformationType schemeInformation = trustStatusList.getSchemeInformation();
            if (schemeInformation != null) {
                AddressType schemeOperatorAddress = schemeInformation.getSchemeOperatorAddress();
                if (schemeOperatorAddress != null) {
                    PostalAddressListType postalAddresses = schemeOperatorAddress.getPostalAddresses();
                    if (postalAddresses != null) {
                        return postalAddresses.getPostalAddress();
                    }
                }
            }
        }
        return Collections.emptyList();
    }

    private static class TSLNamespaceContext implements NamespaceContext {

        public String getNamespaceURI(String prefix) {
            String uri;
            if (prefix == null) {
                throw new IllegalArgumentException("Null prefix");
            } else if ("ds".equals(prefix)) {
                uri = XMLSignature.XMLNS;
            } else if ("tsl".equals(prefix)) {
                uri = "http://uri.etsi.org/02231/v2#";
            } else if ("xml".equals(prefix)) {
                uri = XMLConstants.XML_NS_URI;
            } else {
                uri = "";
            }
            return uri;
        }

        // This method isn't necessary for XPath processing.
        public String getPrefix(String uri) {
            throw new UnsupportedOperationException();
        }

        // This method isn't necessary for XPath processing either.
        public Iterator<?> getPrefixes(String uri) {
            throw new UnsupportedOperationException();
        }
    }
}
