package de.p23r.server.security.tsl;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.p23r.server.security.P23RKeyStoreException;
import de.p23r.server.security.SimpleCache;
import de.p23r.server.security.tsl.core.TrustServiceList;
import de.p23r.server.security.tsl.core.TrustServiceListFactory;

/**
 * Provides a P23R-TSL. The TSL is loaded from cache or retrieved from the specified TSL-depot. The
 * class verifies the validity and integrity of the loaded TSL.
 * 
 * @author bkrauf
 * 
 */
public class TrustServiceListDepot {

    /**
     * The Class TslResource.
     */
    public static class TslResource {
        private final String name;
        private final URI bootstrapDistributionPoint;

        private final String username;
        private final String password;

        /**
         * Instantiates a new tsl resource.
         *
         * @param tslName the tsl name
         * @param bootstrapDistributionPoint the bootstrap distribution point
         * @param username the username
         * @param password the password
         */
        public TslResource(final String tslName, final URI bootstrapDistributionPoint, String username,
                String password) {
            this.name = tslName;
            this.bootstrapDistributionPoint = bootstrapDistributionPoint;

            this.username = username;
            this.password = password;
        }
    }

    private static final Logger LOG = LoggerFactory.getLogger(TrustServiceListDepot.class);

    private final TrustServiceListCache cache;

    /**
     * Creates a depot with a simple in-memory caching and with the default P23R-Pilot-keystore for
     * TSL verification.
     * 
     * @throws P23RKeyStoreException the key store exception
     */
    public TrustServiceListDepot() throws P23RKeyStoreException {
        this(new SimpleCache());
    }

    /**
     * Creates a depot with a specific caching mechanism and with a specific keystore for TSL
     * verification.
     *
     * @param cache the cache
     */
    public TrustServiceListDepot(final TrustServiceListCache cache) {
        this.cache = cache;
    }

    /**
     * Gets the.
     * 
     * @return the p23r.tsl from http://leitstelle.p23r.de/p23r.tsl
     * @throws TrustServiceListException the trust service list exception
     */
    public P23RTrustServiceList get() throws TrustServiceListException {
        try {
            return get(new TslResource("p23r.tsl", new URI("http://leitstelle.p23r.de/p23r.tsl"), null, null));
        } catch (final URISyntaxException e) {
            // this should not happen
            LOG.error("invalid tsl url.", e);
            throw new TrustServiceListException(e);
        }
    }

    /**
     * Gets the.
     * 
     * @param tslResource the tsl resource
     * @return the TSL specified by <code>tslResource</code>
     * @throws TrustServiceListException the trust service list exception
     */
    public P23RTrustServiceList get(final TslResource tslResource) throws TrustServiceListException {
        TrustServiceList tsl = get(tslResource.name, tslResource.bootstrapDistributionPoint,
            tslResource.username, tslResource.password, false);

        boolean trustworthy;
        try {
            checkTrustworthyness(tsl);
            trustworthy = true;
        } catch (final CertificateException e) {
            LOG.warn("Certificate is not trustworthy. The tsl will be updated.", e);
            trustworthy = false;
        }

        boolean updated = false;
        if (tsl.isObsolete() || !trustworthy) {
            tsl = update(tslResource.name, tsl, tslResource.username, tslResource.password);
            updated = true;
        }

        if (updated) {
            if (tsl.isObsolete()) {
                throw new TrustServiceListException("The TSL provided by the depot is obsolete.");
            }
            try {
                checkTrustworthyness(tsl);
            } catch (final CertificateException e) {
                throw new TrustServiceListException("The TSL provided by the depot is not trustworthy.", e);
            }
        }

        return new P23RTrustServiceList(tsl);
    }

    private TrustServiceList get(final String tslName, final URI bootstrapDistributionPoint, String username,
            String password, final boolean updateFromDepot) throws TrustServiceListException {
        if (updateFromDepot || !cache.has(tslName)) {
            InputStream tslStream = null;
            try {
                tslStream = fetchTsl(bootstrapDistributionPoint, username, password);
                final TrustServiceList tsl = TrustServiceListFactory.newInstance(tslStream);
                cache.put(tslName, tsl);
                return tsl;
            } catch (final IOException e) {
                throw new TrustServiceListException(
                    "Failed to fetch the TSL " + tslName + " from the depot.", e);
            } finally {
                if (tslStream != null) {
                    IOUtils.closeQuietly(tslStream);
                }
            }
        }
        return cache.get(tslName);
    }

    private TrustServiceList update(final String tslName, final TrustServiceList tsl, String username,
            String password) throws TrustServiceListException {
        final List<String> distributionPointList = tsl.getDistributionPoints();

        if (distributionPointList == null || distributionPointList.isEmpty()) {
            throw new TrustServiceListException(
                "Failed to update Trust Service List: No distribution point was provided.");
        }

        Exception exception = null;
        for (final String dp : distributionPointList) {
            try {
                return get(tslName, new URI(dp), username, password, true);
            } catch (Exception e) {
                // only remember the last exception and retry with next
                // distribution point
                LOG.debug("update tsl", e);
                exception = e;
            }
        }

        throw new TrustServiceListException("Failed to update Trust Service List:", exception);
    }

    private static InputStream fetchTsl(final URI uri, String username, String password) throws IOException {
        DefaultHttpClient httpClient = new DefaultHttpClient();
        httpClient.getCredentialsProvider().setCredentials(AuthScope.ANY,
            new UsernamePasswordCredentials(username, password));

        HttpGet httpGet = new HttpGet(uri.toString());

        HttpResponse response = httpClient.execute(httpGet);

        return response.getEntity().getContent();
    }

    private void checkTrustworthyness(final TrustServiceList tsl) throws TrustServiceListException,
            CertificateExpiredException, CertificateNotYetValidException {
        tsl.verifySignature();
    }

    /**
     * The main method.
     * 
     * @param args the arguments
     */
    public static void main(String[] args) {
        try {
            final TrustServiceListDepot depot = new TrustServiceListDepot();
            final P23RTrustServiceList tsl = depot.get();
            LOG.debug("tsl scheme name: " + tsl.getSchemeName());
        } catch (TrustServiceListException | P23RKeyStoreException e) {
            LOG.error("Could not intantiate the TrustServiceListDepot.", e);
        }
    }

}
