package de.p23r.server.security.tsl.builder;

import java.security.cert.X509Certificate;
import java.util.Date;

/**
 * Implements TSL TSPService-fragment as specified in D 2.1.3 Sicherheitsinfrastruktur Technische
 * Spezifikation V 1.0 subsection 5.4
 * 
 * @author bkrauf
 * 
 */
final class EnterpriseService extends P23RTrustService {

    /**
     * The Enum ServiceTypeIdentifier.
     */
    enum ServiceTypeIdentifier {

        /**
         * The PIP.
         */
        PIP("http://www.p23r.de/Svc/Svctype/PIP"),

        /**
         * The PAP.
         */
        PAP("http://www.p23r.de/Svc/Svctype/PAP");

        private String uri;

        private ServiceTypeIdentifier(String uri) {
            this.uri = uri;
        }
    }

    /**
     * Instantiates a new enterprise service.
     * 
     * @param certificate the certificate
     * @param serviceType the service type
     */
    EnterpriseService(X509Certificate certificate, ServiceTypeIdentifier serviceType) {
        super(certificate, serviceType.uri);
    }

    /**
     * Instantiates a new enterprise service.
     * 
     * @param certificate the certificate
     * @param serviceName the service name
     * @param statusStartingTime the status starting time
     * @param serviceType the service type
     */
    EnterpriseService(X509Certificate certificate, String serviceName, Date statusStartingTime,
            ServiceTypeIdentifier serviceType) {
        super(certificate, serviceName, statusStartingTime, serviceType.uri);
    }
}
