package de.p23r.server.security.tsl.core;

import java.io.ByteArrayInputStream;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.etsi.uri._02231.v2_.DigitalIdentityListType;
import org.etsi.uri._02231.v2_.DigitalIdentityType;
import org.etsi.uri._02231.v2_.InternationalNamesType;
import org.etsi.uri._02231.v2_.MultiLangNormStringType;
import org.etsi.uri._02231.v2_.ObjectFactory;
import org.etsi.uri._02231.v2_.TSPServiceInformationType;
import org.etsi.uri._02231.v2_.TSPServiceType;

/**
 * The Class TrustService.
 */
public class TrustService {
    private static final String DEFAULT_SERVICE_TYPE = "http://uri.etsi.org/TrstSvc/Svctype/CA/QC";

    /**
     * The tsp service.
     */
    protected final TSPServiceType tspService;

    /**
     * The object factory.
     */
    protected final ObjectFactory objectFactory;

    private final DatatypeFactory datatypeFactory;

    /**
     * The constant SERVICE_STATUS
     */
    private static final String SERVICE_STATUS = "http://uri.etsi.org/TrstSvc/eSigDir-1999-93-EC-TrustedList/Svcstatus/undersupervision";

    /**
     * Instantiates a new trust service.
     * 
     * @param tspService the tsp service
     */
    TrustService(TSPServiceType tspService) {
        this.tspService = tspService;
        this.objectFactory = new ObjectFactory();
        try {
            this.datatypeFactory = DatatypeFactory.newInstance();
        } catch (DatatypeConfigurationException e) {
            throw new TSLException("datatype config error: " + e.getMessage(), e);
        }
    }

    /**
     * Instantiates a new trust service.
     * 
     * @param certificate the certificate
     */
    public TrustService(X509Certificate certificate) {
        this(certificate, certificate.getSubjectX500Principal().toString(), certificate.getNotBefore(),
            DEFAULT_SERVICE_TYPE);
    }

    /**
     * Instantiates a new trust service.
     * 
     * @param certificate the certificate
     * @param serviceTypeIdentifier the service type identifier
     */
    public TrustService(X509Certificate certificate, String serviceTypeIdentifier) {
        this(certificate, certificate.getSubjectX500Principal().toString(), certificate.getNotBefore(),
            serviceTypeIdentifier);
    }

    /**
     * Instantiates a new trust service.
     * 
     * @param certificate the certificate
     * @param serviceName the service name
     * @param statusStartingTime the status starting time
     * @param serviceTypeIdentifier the service type identifier
     */
    public TrustService(X509Certificate certificate, String serviceName, Date statusStartingTime,
            String serviceTypeIdentifier) {
        this.objectFactory = new ObjectFactory();
        try {
            this.datatypeFactory = DatatypeFactory.newInstance();
        } catch (DatatypeConfigurationException e) {
            throw new TSLException("datatype config error: " + e.getMessage(), e);
        }

        this.tspService = this.objectFactory.createTSPServiceType();
        TSPServiceInformationType tspServiceInformation = this.objectFactory
            .createTSPServiceInformationType();
        this.tspService.setServiceInformation(tspServiceInformation);
        tspServiceInformation.setServiceTypeIdentifier(serviceTypeIdentifier);
        // TODO convert status starting time to xml gregorian calendar
//        tspServiceInformation.setStatusStartingTime(statusStartingTime);
        InternationalNamesType i18nServiceName = this.objectFactory.createInternationalNamesType();
        List<MultiLangNormStringType> serviceNames = i18nServiceName.getName();
        MultiLangNormStringType serviceNameML = this.objectFactory.createMultiLangNormStringType();
        serviceNames.add(serviceNameML);
        serviceNameML.setLang(Locale.GERMAN.getLanguage());
        serviceNameML.setValue(serviceName);
        tspServiceInformation.setServiceName(i18nServiceName);

        if (certificate != null) {
            DigitalIdentityListType digitalIdentityList = createDigitalIdentity(certificate);
            tspServiceInformation.setServiceDigitalIdentity(digitalIdentityList);
        } else {
            tspServiceInformation.setServiceDigitalIdentity(null);
        }

        tspServiceInformation.setServiceStatus(getServiceStatus());

        XMLGregorianCalendar xmlStatusStartingCalendar = datatypeFactory
            .newXMLGregorianCalendar(new GregorianCalendar(TimeZone.getTimeZone("Z")));
        tspServiceInformation.setStatusStartingTime(xmlStatusStartingCalendar);
    }

    /**
     * Gets the service status.
     * 
     * @return the service status
     */
    protected String getServiceStatus() {
        return SERVICE_STATUS;
    }

    private DigitalIdentityListType createDigitalIdentity(X509Certificate certificate) {
        DigitalIdentityListType digitalIdentityList = this.objectFactory.createDigitalIdentityListType();
        List<DigitalIdentityType> digitalIdentities = digitalIdentityList.getDigitalId();
        DigitalIdentityType digitalIdentity = this.objectFactory.createDigitalIdentityType();
        try {
            digitalIdentity.setX509Certificate(certificate.getEncoded());
        } catch (CertificateEncodingException e) {
            throw new TSLException("X509 encoding error: " + e.getMessage(), e);
        }
        digitalIdentities.add(digitalIdentity);

        digitalIdentity = this.objectFactory.createDigitalIdentityType();
        digitalIdentity.setX509SubjectName(certificate.getSubjectX500Principal().getName());
        digitalIdentities.add(digitalIdentity);

        // digitalIdentity = this.objectFactory.createDigitalIdentityType();
        // byte[] skiValue =
        // certificate.getExtensionValue(X509Extensions.SubjectKeyIdentifier.getId());
        // SubjectKeyIdentifierStructure subjectKeyIdentifierStructure;
        // try
        // {
        // subjectKeyIdentifierStructure = new
        // SubjectKeyIdentifierStructure(skiValue);
        // }
        // catch (IOException e)
        // {
        // throw new TSLException("X509 SKI decoding error: " +
        // e.getMessage(), e);
        // }
        // digitalIdentity.setX509SKI(subjectKeyIdentifierStructure.getKeyIdentifier());
        // digitalIdentities.add(digitalIdentity);

        return digitalIdentityList;
    }

    /**
     * Gets the tSP service.
     * 
     * @return the tSP service
     */
    public TSPServiceType getTSPService() {
        return this.tspService;
    }

    /**
     * Gets the name.
     * 
     * @param locale the locale
     * @return the name
     */
    public String getName(Locale locale) {
        TSPServiceInformationType tspServiceInformation = this.tspService.getServiceInformation();
        InternationalNamesType i18nServiceName = tspServiceInformation.getServiceName();
        return TrustServiceListUtils.getValue(i18nServiceName, locale);
    }

    /**
     * Gets the name.
     * 
     * @return the name
     */
    public String getName() {
        return getName(Locale.getDefault());
    }

    /**
     * Gets the type.
     * 
     * @return the type
     */
    public String getType() {
        TSPServiceInformationType tspServiceInformation = this.tspService.getServiceInformation();
        return tspServiceInformation.getServiceTypeIdentifier();
    }

    /**
     * Gets the status.
     * 
     * @return the status
     */
    public String getStatus() {
        TSPServiceInformationType tspServiceInformation = this.tspService.getServiceInformation();
        return tspServiceInformation.getServiceStatus();
    }

    /**
     * Gets the status starting time.
     * 
     * @return the status starting time
     */
    public Date getStatusStartingTime() {
        TSPServiceInformationType tspServiceInformation = this.tspService.getServiceInformation();
        XMLGregorianCalendar statusStartingTimeXmlCalendar = tspServiceInformation.getStatusStartingTime();
        return statusStartingTimeXmlCalendar.toGregorianCalendar().getTime();
    }

    /**
     * Gets the service digital identity.
     * 
     * @return the service digital identity
     */
    public X509Certificate getServiceDigitalIdentity() {
        TSPServiceInformationType tspServiceInformation = this.tspService.getServiceInformation();
        DigitalIdentityListType digitalIdentityList = tspServiceInformation.getServiceDigitalIdentity();
        try {
            final CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
            for (final DigitalIdentityType digitalIdentity : digitalIdentityList.getDigitalId()) {
                byte[] x509CertificateData = digitalIdentity.getX509Certificate();
                if (x509CertificateData != null) {
                    try {
                        return (X509Certificate) certificateFactory
                            .generateCertificate(new ByteArrayInputStream(x509CertificateData));
                    } catch (CertificateException e) {
                        throw new TSLException("X509 error: " + e.getMessage(), e);
                    }
                }
            }
            throw new TSLException("No X509Certificate identity specified");
        } catch (CertificateException e) {
            throw new TSLException("X509 error: " + e.getMessage(), e);
        }
    }

    /**
     * Gets the service digital identity data.
     * 
     * @return the service digital identity data
     */
    public byte[] getServiceDigitalIdentityData() {
        TSPServiceInformationType tspServiceInformation = this.tspService.getServiceInformation();
        DigitalIdentityListType digitalIdentityList = tspServiceInformation.getServiceDigitalIdentity();
        // try {
        // final CertificateFactory certificateFactory =
        // CertificateFactory.getInstance("X.509");
        for (final DigitalIdentityType digitalIdentity : digitalIdentityList.getDigitalId()) {
            byte[] x509CertificateData = digitalIdentity.getX509Certificate();
            if (x509CertificateData != null) {
                // try {
                // X509Certificate certificate = (X509Certificate)
                // certificateFactory.generateCertificate(new
                // ByteArrayInputStream(x509CertificateData));
                return x509CertificateData;
                // } catch (CertificateException e) {
                // throw new TSLException("X509 error: " + e.getMessage(),
                // e);
                // }
            }
        }
        throw new TSLException("No X509Certificate identity specified");
        // } catch (CertificateException e) {
        // throw new TSLException("X509 error: " + e.getMessage(), e);
        // }
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return getName();
    }
}
