package de.p23r.server.security.tsl.core;

import java.security.Key;
import java.security.cert.X509Certificate;
import java.util.List;

import javax.xml.crypto.AlgorithmMethod;
import javax.xml.crypto.KeySelector;
import javax.xml.crypto.KeySelectorException;
import javax.xml.crypto.KeySelectorResult;
import javax.xml.crypto.XMLCryptoContext;
import javax.xml.crypto.XMLStructure;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.X509Data;

/**
 * The Class KeyInfoKeySelector.
 */
public class KeyInfoKeySelector extends KeySelector implements KeySelectorResult {

    private X509Certificate certificate;

    /*
     * (non-Javadoc)
     * @see javax.xml.crypto.KeySelector#select(javax.xml.crypto.dsig.keyinfo.KeyInfo ,
     * javax.xml.crypto.KeySelector.Purpose, javax.xml.crypto.AlgorithmMethod,
     * javax.xml.crypto.XMLCryptoContext)
     */
    @SuppressWarnings("unchecked")
	@Override
    public KeySelectorResult select(KeyInfo keyInfo, Purpose purpose, AlgorithmMethod method,
            XMLCryptoContext context) throws KeySelectorException {
        List<XMLStructure> keyInfoContent = (List<XMLStructure>) keyInfo.getContent();
        for (XMLStructure keyInfoStructure : keyInfoContent) {
            if (keyInfoStructure instanceof X509Data) {
                X509Data x509Data = (X509Data) keyInfoStructure;
                List<Object> x509DataList = (List<Object>) x509Data.getContent();
                for (Object x509DataObject : x509DataList) {
                    if (x509DataObject instanceof X509Certificate) {
                        this.certificate = (X509Certificate) x509DataObject;
                        // stop after first match
                        return this;
                    }
                }
            }
        }
        throw new KeySelectorException("No key found!");
    }

    /*
     * (non-Javadoc)
     * @see javax.xml.crypto.KeySelectorResult#getKey()
     */
    @Override
    public Key getKey() {
        return this.certificate.getPublicKey();
    }

    /**
     * Gets the certificate.
     * 
     * @return the certificate
     */
    public X509Certificate getCertificate() {
        return this.certificate;
    }
}
