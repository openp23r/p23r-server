package de.p23r.server.security.tsl.builder;

import java.util.List;

/**
 * An implementation of <code>TrustServiceProviderSource</code> provides data for the
 * <code>TrustServiceProvider</code> element of an ETSI-TSL xml file.
 * 
 * @author bkrauf
 * 
 */
public interface TrustServiceProviderSource {

    /**
     * Gets the p23 r tsl profile.
     * 
     * @return the P23R-TS-Profile, this trust service profile conforms to
     */
    P23RTslProfile getP23RTslProfile();

    /**
     * Gets the postal address street.
     * 
     * @return the street part of the providers address
     */
    String getPostalAddressStreet();

    /**
     * Gets the postal address locality.
     * 
     * @return the locality part of the providers address
     */
    String getPostalAddressLocality();

    /**
     * Gets the postal address state or province.
     * 
     * @return the state-or-province part of the providers address
     */
    String getPostalAddressStateOrProvince();

    /**
     * Gets the postal address postal code.
     * 
     * @return the postal-code part of the providers address
     */
    String getPostalAddressPostalCode();

    /**
     * Gets the postal address country.
     * 
     * @return the country part of the providers address
     */
    String getPostalAddressCountry();

    /**
     * Gets the electronic address.
     * 
     * @return the electronic address of the provider
     */
    String getElectronicAddress();

    /**
     * Gets the information uri.
     * 
     * @return the URI to a provider information resource (e.g. web page)
     */
    String getInformationUri();

    /**
     * Gets the trust services.
     * 
     * @return a list of trusted services published by the provider
     */
    List<P23RTrustService> getTrustServices();
}
