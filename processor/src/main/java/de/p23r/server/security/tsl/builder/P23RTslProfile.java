package de.p23r.server.security.tsl.builder;

/**
 * An enumeration of TSL profiles specified in D 2.1.3 (Sicherheitsinfrastruktur Technische
 * Spezifikation).
 * 
 * @author bkrauf
 * 
 */
public enum P23RTslProfile {

    /**
     * The P23 r_ callback.
     */
    P23R_CALLBACK("P23R-Callback"),

    /**
     * The P23 r_ external.
     */
    P23R_EXTERNAL("P23R-External"),

    /**
     * The GO v_ external.
     */
    GOV_EXTERNAL("GOV-External"),

    /**
     * The CTRLCT r_ external.
     */
    CTRLCTR_EXTERNAL("CTRLCTR-External");

    private String id;

    private P23RTslProfile(String id) {
        this.id = id;
    }

    /**
     * Gets the id.
     * 
     * @return the unique identifier of the tsl profile
     */
    String getId() {
        return id;
    }
}
