package de.p23r.server.security.tsl.builder;

import java.security.cert.X509Certificate;
import java.util.Date;

/**
 * Implements TSL TSPService-fragment as specified in D 2.1.3 Sicherheitsinfrastruktur Technische
 * Spezifikation V 1.0 subsection 5.5
 * 
 * @author bkrauf
 * 
 */
public final class EntryPoint extends P23RTrustService {

    /**
     * The Enum ServiceTypeIdentifier.
     */
    public enum ServiceTypeIdentifier {

        /**
         * The EMAIL.
         */
        EMAIL("http://www.p23r.de/Svc/Svctype/EMAIL"),

        /**
         * The OSCI.
         */
        OSCI("http://www.p23r.de/Svc/Svctype/OSCI"),

        /**
         * The FAX.
         */
        FAX("http://www.p23r.de/Svc/Svctype/FAX"),

        /**
         * The SOAP.
         */
        SOAP("http://www.p23r.de/Svc/Svctype/SOAP"),

        /**
         * The REST.
         */
        REST("http://www.p23r.de/Svc/Svctype/REST"),

        /**
         * The FTP.
         */
        FTP("http://www.p23r.de/Svc/Svctype/FTP");

        private final String uri;

        private ServiceTypeIdentifier(String uri) {
            this.uri = uri;
        }
    }

    /**
     * Instantiates a new entry point.
     * 
     * @param certificate the certificate
     * @param serviceType the service type
     */
    public EntryPoint(X509Certificate certificate, ServiceTypeIdentifier serviceType) {
        super(certificate, serviceType.uri);
    }

    /**
     * Instantiates a new entry point.
     * 
     * @param certificate the certificate
     * @param serviceName the service name
     * @param statusStartingTime the status starting time
     * @param serviceType the service type
     */
    public EntryPoint(X509Certificate certificate, String serviceName, Date statusStartingTime,
            ServiceTypeIdentifier serviceType) {
        super(certificate, serviceName, statusStartingTime, serviceType.uri);
    }

}
