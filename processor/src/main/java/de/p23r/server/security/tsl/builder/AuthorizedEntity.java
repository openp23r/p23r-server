package de.p23r.server.security.tsl.builder;

import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.List;

/**
 * Implements TSL TSPService-fragment as specified in D 2.1.3 Sicherheitsinfrastruktur Technische
 * Spezifikation V 1.0 subsection 5.6
 * 
 * @author bkrauf
 * 
 */
final class AuthorizedEntity extends P23RTrustService {
    private static final String SERVICE_TYPE = "http://www.p23r.de/Svc/Svctype/AuthorizedEntity";

    /**
     * Instantiates a new authorized entity.
     * 
     * @param certificate the certificate
     */
    AuthorizedEntity(X509Certificate certificate) {
        super(certificate, SERVICE_TYPE);
    }

    /**
     * Instantiates a new authorized entity.
     * 
     * @param certificate the certificate
     * @param serviceName the service name
     * @param statusStartingTime the status starting time
     */
    AuthorizedEntity(X509Certificate certificate, String serviceName, Date statusStartingTime) {
        super(certificate, serviceName, statusStartingTime, SERVICE_TYPE);
    }

    /*
     * (non-Javadoc)
     * @see de.p23r.common.tsl.builder.P23RTrustService#addServiceSupplyPoints(java.util.List)
     */
    @Override
    /**
     * Adding service supply points to an authorized entity status information is not allowed.
     */
    public void addServiceSupplyPoints(List<String> uris) {
        throw new UnsupportedOperationException(
                "An Authorized Entity Status Information within a trust service list must not have any service supply point.");
    }
}
