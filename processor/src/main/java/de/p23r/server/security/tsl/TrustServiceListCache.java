package de.p23r.server.security.tsl;

import de.p23r.server.security.tsl.core.TrustServiceList;

/**
 * A TrustServiceListCache stores TrustServiceList objects which can be retrieved by a unique name;.
 * 
 * @author bkrauf
 */
public interface TrustServiceListCache {
    /**
     * Returns true, if this cache can provide a TrustServiceList with the given name.
     * 
     * @param tslName the name of TrustServiceList
     * @return true, if this cache can provide a TrustServiceList, false if not
     */
    boolean has(String tslName);

    /**
     * Returns the a TrustServiceList by name.
     * 
     * @param tslName the tsl name
     * @return Returns the a TrustServiceList that has assigned the provided name. If no such
     *         TrustServiceList exists in the cache, this method returns <code>null</code>.
     * @throws TrustServiceListException the trust service list exception
     */
    TrustServiceList get(String tslName) throws TrustServiceListException;

    /**
     * Adds a TrustServiceList to this cache which can be later retrieved by the provided name.
     * 
     * @param tslName The name which used to get the TrustServiceList from this cache. The name must
     *        not be null or empty.
     * @param tsl The TrustServiceList to add to this cache. The TrustServiceList must not be null.
     * @throws TrustServiceListException the trust service list exception
     */
    void put(String tslName, TrustServiceList tsl) throws TrustServiceListException;
}
