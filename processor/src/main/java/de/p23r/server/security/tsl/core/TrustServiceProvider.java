package de.p23r.server.security.tsl.core;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import org.etsi.uri._02231.v2_.AddressType;
import org.etsi.uri._02231.v2_.ElectronicAddressType;
import org.etsi.uri._02231.v2_.InternationalNamesType;
import org.etsi.uri._02231.v2_.NonEmptyMultiLangURIListType;
import org.etsi.uri._02231.v2_.NonEmptyMultiLangURIType;
import org.etsi.uri._02231.v2_.ObjectFactory;
import org.etsi.uri._02231.v2_.PostalAddressListType;
import org.etsi.uri._02231.v2_.PostalAddressType;
import org.etsi.uri._02231.v2_.TSPInformationType;
import org.etsi.uri._02231.v2_.TSPServiceType;
import org.etsi.uri._02231.v2_.TSPServicesListType;
import org.etsi.uri._02231.v2_.TSPType;

/**
 * The Class TrustServiceProvider.
 */
public class TrustServiceProvider {

    private final TSPType tsp;

    private List<TrustService> trustServices;

    private final ObjectFactory objectFactory;

    /**
     * Instantiates a new trust service provider.
     * 
     * @param tsp the tsp
     */
    TrustServiceProvider(TSPType tsp) {
        this.tsp = tsp;
        this.objectFactory = new ObjectFactory();
    }

    /**
     * Instantiates a new trust service provider.
     * 
     * @param name the name
     * @param tradeName the trade name
     */
    public TrustServiceProvider(String name, String tradeName) {
        this.objectFactory = new ObjectFactory();
        this.tsp = this.objectFactory.createTSPType();
        TSPInformationType tspInformation = this.objectFactory.createTSPInformationType();
        this.tsp.setTSPInformation(tspInformation);

        InternationalNamesType tspNames = this.objectFactory.createInternationalNamesType();
        TrustServiceListUtils.setValue(name, Locale.ENGLISH, tspNames);
        tspInformation.setTSPName(tspNames);

        InternationalNamesType tspTradeNames = this.objectFactory.createInternationalNamesType();
        TrustServiceListUtils.setValue(tradeName, Locale.ENGLISH, tspTradeNames);
        tspInformation.setTSPTradeName(tspTradeNames);
    }

    /**
     * Gets the tSP.
     * 
     * @return the tSP
     */
    TSPType getTSP() {
        return this.tsp;
    }

    /**
     * Adds the postal address.
     * 
     * @param locale the locale
     * @param streetAddress the street address
     * @param locality the locality
     * @param stateOrProvince the state or province
     * @param postalCode the postal code
     * @param countryName the country name
     */
    public void addPostalAddress(Locale locale, String streetAddress, String locality,
            String stateOrProvince, String postalCode, String countryName) {
        TSPInformationType tspInformation = getTSPInformation();
        AddressType address = tspInformation.getTSPAddress();
        if (null == address) {
            address = this.objectFactory.createAddressType();
            tspInformation.setTSPAddress(address);
        }
        PostalAddressListType postalAddresses = address.getPostalAddresses();
        if (null == postalAddresses) {
            postalAddresses = this.objectFactory.createPostalAddressListType();
            address.setPostalAddresses(postalAddresses);
        }
        List<PostalAddressType> postalAddressList = postalAddresses.getPostalAddress();
        PostalAddressType postalAddress = this.objectFactory.createPostalAddressType();
        postalAddressList.add(postalAddress);

        postalAddress.setLang(locale.getLanguage());
        postalAddress.setStreetAddress(streetAddress);
        postalAddress.setLocality(locality);
        postalAddress.setStateOrProvince(stateOrProvince);
        postalAddress.setPostalCode(postalCode);
        postalAddress.setCountryName(countryName);
    }

    /**
     * Gets the postal address.
     * 
     * @return the postal address
     */
    public PostalAddressType getPostalAddress() {
        return getPostalAddress(Locale.ENGLISH);
    }

    /**
     * Gets the postal address.
     * 
     * @param locale the locale
     * @return the postal address
     */
    public PostalAddressType getPostalAddress(Locale locale) {
        TSPInformationType tspInformation = this.tsp.getTSPInformation();
        if (null == tspInformation) {
            return null;
        }
        AddressType address = tspInformation.getTSPAddress();
        if (null == address) {
            return null;
        }
        PostalAddressListType postalAddresses = address.getPostalAddresses();
        if (null == postalAddresses) {
            return null;
        }
        List<PostalAddressType> postalAddressList = postalAddresses.getPostalAddress();
        for (PostalAddressType postalAddress : postalAddressList) {
            String lang = postalAddress.getLang();
            if (0 != locale.getLanguage().compareToIgnoreCase(lang)) {
                continue;
            }
            return postalAddress;
        }
        return null;
    }

    /**
     * Adds the electronic address.
     * 
     * @param electronicAddressUris the electronic address uris
     */
    public void addElectronicAddress(String... electronicAddressUris) {
        TSPInformationType tspInformation = getTSPInformation();
        AddressType address = tspInformation.getTSPAddress();
        if (null == address) {
            address = this.objectFactory.createAddressType();
            tspInformation.setTSPAddress(address);
        }
        ElectronicAddressType electronicAddress = address.getElectronicAddress();
        if (null == electronicAddress) {
            electronicAddress = this.objectFactory.createElectronicAddressType();
            address.setElectronicAddress(electronicAddress);
        }
        List<String> uris = electronicAddress.getURI();
        for (String electronicAddressUri : electronicAddressUris) {
            uris.add(electronicAddressUri);
        }
    }

    /**
     * Gets the electronic address.
     * 
     * @return the electronic address
     */
    public List<String> getElectronicAddress() {
        List<String> resultElectronicAddress = new LinkedList<String>();
        TSPInformationType tspInformation = this.tsp.getTSPInformation();
        if (null == tspInformation) {
            return resultElectronicAddress;
        }
        AddressType address = tspInformation.getTSPAddress();
        if (null == address) {
            return resultElectronicAddress;
        }
        ElectronicAddressType electronicAddress = address.getElectronicAddress();
        if (null == electronicAddress) {
            return resultElectronicAddress;
        }
        List<String> uris = electronicAddress.getURI();
        for (String uri : uris) {
            resultElectronicAddress.add(uri);
        }
        return resultElectronicAddress;
    }

    private TSPInformationType getTSPInformation() {
        TSPInformationType tspInformation = this.tsp.getTSPInformation();
        if (null == tspInformation) {
            tspInformation = this.objectFactory.createTSPInformationType();
            this.tsp.setTSPInformation(tspInformation);
        }
        return tspInformation;
    }

    /**
     * Adds the information uri.
     * 
     * @param locale the locale
     * @param informationUri the information uri
     */
    public void addInformationUri(Locale locale, String informationUri) {
        TSPInformationType tspInformation = getTSPInformation();
        NonEmptyMultiLangURIListType tspInformationURI = tspInformation.getTSPInformationURI();
        if (null == tspInformationURI) {
            tspInformationURI = this.objectFactory.createNonEmptyMultiLangURIListType();
            tspInformation.setTSPInformationURI(tspInformationURI);
        }
        List<NonEmptyMultiLangURIType> uris = tspInformationURI.getURI();
        NonEmptyMultiLangURIType uri = this.objectFactory.createNonEmptyMultiLangURIType();
        uri.setLang(locale.getLanguage());
        uri.setValue(informationUri);
        uris.add(uri);
    }

    /**
     * Gets the information uris.
     * 
     * @return the information uris
     */
    public List<String> getInformationUris() {
        return getInformationUris(Locale.ENGLISH);
    }

    /**
     * Gets the information uris.
     * 
     * @param locale the locale
     * @return the information uris
     */
    public List<String> getInformationUris(Locale locale) {
        List<String> results = new LinkedList<String>();
        TSPInformationType tspInformation = this.tsp.getTSPInformation();
        if (null == tspInformation) {
            return results;
        }
        NonEmptyMultiLangURIListType tspInformationURI = tspInformation.getTSPInformationURI();
        if (null == tspInformationURI) {
            return results;
        }
        List<NonEmptyMultiLangURIType> uris = tspInformationURI.getURI();
        for (NonEmptyMultiLangURIType uri : uris) {
            String lang = uri.getLang();
            if (0 != locale.getLanguage().compareToIgnoreCase(lang)) {
                continue;
            }
            results.add(uri.getValue());
        }
        return results;
    }

    /**
     * Gets the name.
     * 
     * @param locale the locale
     * @return the name
     */
    public String getName(Locale locale) {
        TSPInformationType tspInformation = this.tsp.getTSPInformation();
        InternationalNamesType i18nTspName = tspInformation.getTSPName();
        return TrustServiceListUtils.getValue(i18nTspName, locale);
    }

    /**
     * Gets the trade name.
     * 
     * @param locale the locale
     * @return the trade name
     */
    public String getTradeName(Locale locale) {
        TSPInformationType tspInformation = this.tsp.getTSPInformation();
        InternationalNamesType i18nTspTradeName = tspInformation.getTSPTradeName();
        return TrustServiceListUtils.getValue(i18nTspTradeName, locale);
    }

    /**
     * Gets the trade name.
     * 
     * @return the trade name
     */
    public String getTradeName() {
        return getTradeName(Locale.ENGLISH);
    }

    /**
     * Gets the name.
     * 
     * @return the name
     */
    public String getName() {
        Locale locale = Locale.getDefault();
        return getName(locale);
    }

    /**
     * Find trust service.
     * 
     * @param name the name
     * @return the trust service
     */
    public TrustService findTrustService(String name) {
        for (TrustService service : getTrustServices()) {
            if (service.getName().equals(name)) {
                return service;
            }
        }
        return null;
    }

    /**
     * Gets the trust services.
     * 
     * @return the trust services
     */
    public List<TrustService> getTrustServices() {
        if (null != this.trustServices) {
            return this.trustServices;
        }
        this.trustServices = new LinkedList<TrustService>();
        TSPServicesListType tspServices = this.tsp.getTSPServices();
        if (null == tspServices) {
            return this.trustServices;
        }
        List<TSPServiceType> tspServiceList = tspServices.getTSPService();
        for (TSPServiceType tspService : tspServiceList) {
            TrustService trustService = new TrustService(tspService);
            this.trustServices.add(trustService);
        }
        return this.trustServices;
    }

    /**
     * Adds the trust service.
     * 
     * @param trustService the trust service
     */
    public void addTrustService(TrustService trustService) {
        TSPServicesListType tspServicesList = this.tsp.getTSPServices();
        if (null == tspServicesList) {
            tspServicesList = this.objectFactory.createTSPServicesListType();
            this.tsp.setTSPServices(tspServicesList);
        }
        List<TSPServiceType> tspServices = tspServicesList.getTSPService();
        tspServices.add(trustService.getTSPService());
        // reset java model cache
        this.trustServices = null;
    }
}
