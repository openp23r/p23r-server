package de.p23r.server.security.tsl.builder;

import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.List;

import org.etsi.uri._02231.v2_.ServiceSupplyPointsType;
import org.etsi.uri._02231.v2_.TSPServiceInformationType;

import de.p23r.server.security.tsl.core.TrustService;

/**
 * Implements the common TSPService-element of TSLs as specified in D 2.1.3 Sicherheitsinfrastruktur
 * Technische Spezifikation V 1.0 (see subsections 5.3, 5.4, 5.5, and 5.6)
 * 
 * @author bkrauf
 * 
 */
public abstract class P23RTrustService extends TrustService {

    /**
     * The Enum ServiceCurrentStatus.
     */
    protected enum ServiceCurrentStatus {

        /**
         * The I n_ accordance.
         */
        IN_ACCORDANCE("http://uri.etsi.org/TrstSvc/Svcstatus/inaccord"),

        /**
         * The EXPIRED.
         */
        EXPIRED("http://uri.etsi.org/TrstSvc/Svcstatus/expired"),

        /**
         * The SUSPENDED.
         */
        SUSPENDED("http://uri.etsi.org/TrstSvc/Svcstatus/suspended"),

        /**
         * The REVOKED.
         */
        REVOKED("http://uri.etsi.org/TrstSvc/Svcstatus/revoked"),

        /**
         * The NO t_ i n_ accordance.
         */
        NOT_IN_ACCORDANCE("http://uri.etsi.org/TrstSvc/Svcstatus/notinaccord");

        private final String uri;

        private ServiceCurrentStatus(String uri) {
            this.uri = uri;
        }

        /**
         * Gets the uri.
         * 
         * @return the uri
         */
        protected String getUri() {
            return uri;
        }
    }

    /**
     * Creates a trust service. The service name and the validity period starting time are not
     * derived from the certificate, but from the corresponding parameters.
     * 
     * @param certificate the certificate
     * @param serviceName the service name
     * @param statusStartingTime the status starting time
     * @param serviceTypeIdentifier the service type identifier
     */
    public P23RTrustService(X509Certificate certificate, String serviceName, Date statusStartingTime,
            String serviceTypeIdentifier) {
        super(certificate, serviceName, statusStartingTime, serviceTypeIdentifier);
    }

    /**
     * Creates a trust service. The service name and the validity period starting time are derived
     * from the certificate.
     * 
     * @param certificate the certificate
     * @param serviceTypeIdentifier the service type identifier
     */
    public P23RTrustService(X509Certificate certificate, String serviceTypeIdentifier) {
        super(certificate, serviceTypeIdentifier);
    }

    /*
     * (non-Javadoc)
     * @see de.p23r.common.tsl.core.TrustService#getServiceStatus()
     */
    @Override
    protected String getServiceStatus() {
        return P23RTrustService.ServiceCurrentStatus.IN_ACCORDANCE.getUri();
    }

    /**
     * Adds the service supply points.
     * 
     * @param uris the uris
     */
    public void addServiceSupplyPoints(List<String> uris) {
        if (uris == null) {
            return;
        }

        final TSPServiceInformationType serviceInformation = super.tspService.getServiceInformation();
        if (serviceInformation.getServiceSupplyPoints() == null) {
            serviceInformation.setServiceSupplyPoints(super.objectFactory.createServiceSupplyPointsType());
        }

        final ServiceSupplyPointsType serviceSupplyPoints = serviceInformation.getServiceSupplyPoints();
        final List<String> serviceSupplyPointsList = serviceSupplyPoints.getServiceSupplyPoint();
        serviceSupplyPointsList.addAll(uris);
    }

}
