package de.p23r.server.security;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import de.p23r.server.security.tsl.TrustServiceListCache;
import de.p23r.server.security.tsl.TrustServiceListException;
import de.p23r.server.security.tsl.core.TrustServiceList;
import de.p23r.server.security.tsl.core.TrustServiceListFactory;

/**
 * A <code>TrustServiceListCache</code> which loads TSLs as a Java-resource by name. This cache is
 * read-only and intended for writing unit tests.
 * 
 * @author bkrauf
 * 
 */
class ResourceCache implements TrustServiceListCache {

    /*
     * (non-Javadoc)
     * @see de.p23r.common.tsl.TrustServiceListCache#has(java.lang.String)
     */
    @Override
    public boolean has(final String tslName) {
        final URL resource = Thread.currentThread().getContextClassLoader().getResource(tslName);
        return resource != null;
    }

    /*
     * (non-Javadoc)
     * @see de.p23r.common.tsl.TrustServiceListCache#get(java.lang.String)
     */
    @Override
    public TrustServiceList get(final String tslName) throws TrustServiceListException {
        final InputStream resourceAsStream = Thread.currentThread().getContextClassLoader()
                .getResourceAsStream(tslName);
        try {
            return TrustServiceListFactory.newInstance(resourceAsStream);
        } catch (final IOException e) {
            throw new TrustServiceListException("Failed to load TSL with resource name " + tslName + ".", e);
        }
    }

    /*
     * (non-Javadoc)
     * @see de.p23r.common.tsl.TrustServiceListCache#put(java.lang.String,
     * de.p23r.common.tsl.core.TrustServiceList)
     */
    @Override
    public void put(final String tslName, final TrustServiceList tsl) throws TrustServiceListException {
        // silently do nothing
    }

}
