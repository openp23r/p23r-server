/**
 * Provides security implementations for the p23r processor.
 */
package de.p23r.server.security;