/**
 * Provides classes to represent and build tsl data structures.
 */
package de.p23r.server.security.tsl.builder;