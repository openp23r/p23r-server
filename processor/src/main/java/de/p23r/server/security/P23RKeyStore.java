package de.p23r.server.security;

import java.io.IOException;
import java.io.InputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Enumeration;

import javax.crypto.BadPaddingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.p23r.server.security.tsl.core.TSLException;

/**
 * Loads the keystore, which is used for signing and verifying from resource.
 * 
 * @author bkrauf
 * 
 */
public class P23RKeyStore {
    private String keyStoreRessourceName;
    private final KeyStore keyStore;
    private final String password;

    private final Logger log = LoggerFactory.getLogger(getClass());

    /**
     * The Enum KeyStoreType.
     */
    public enum KeyStoreType {
        /**
         * Java key stores (*.jks*)
         */
        JKS("JKS"),

        /**
         * PKCS#12 containers (*.pfx)
         */
        PKCS12("pkcs12");

        private String id;

        private KeyStoreType(String id) {
            this.id = id;
        }
    }

    /**
     * Creates a new key store.
     *
     * @param keyStoreResource the key store resource
     * @param keyStoreType the key store type
     * @param passwordClearText the password clear text
     * @throws P23RKeyStoreException the key store exception
     */
    public P23RKeyStore(final InputStream keyStoreResource, final KeyStoreType keyStoreType,
            final String passwordClearText) throws P23RKeyStoreException {
        try {
            password = passwordClearText;
            keyStore = KeyStore.getInstance(keyStoreType.id);
            keyStore.load(keyStoreResource, password.toCharArray());
        } catch (final IOException e) {
            log.debug("create keystore", e);
            if (e.getCause() instanceof UnrecoverableKeyException
                    || e.getCause() instanceof BadPaddingException) {
                throw new P23RKeyStoreException("Failed to open key store. The password may be wrong.", e);
            } else {
                throw new P23RKeyStoreException(e.getMessage(), e);
            }
        } catch (KeyStoreException | NoSuchAlgorithmException | CertificateException e) {
            log.debug("create keystore", e);
            throw new P23RKeyStoreException(e.getMessage(), e);
        } finally {
            if (keyStoreResource != null) {
                try {
                    keyStoreResource.close();
                } catch (IOException e) {
                    log.error("Failed to close the key store resource.", e);
                }
            }
        }
    }

    /**
     * Creates a new keystore. Therefor loads a keystore from resource.
     * 
     * @param keyStoreRessourceName the key store ressource name
     * @param keyStoreType the key store type
     * @param passwordClearText the password clear text
     * @throws P23RKeyStoreException the key store exception
     */
    public P23RKeyStore(final String keyStoreRessourceName, final KeyStoreType keyStoreType,
            final String passwordClearText) throws P23RKeyStoreException {
        this(Thread.currentThread().getContextClassLoader().getResourceAsStream(keyStoreRessourceName),
            keyStoreType, passwordClearText);
    }

    /**
     * Gets the private key.
     * 
     * @return the unique private key in this keystore
     * @throws P23RKeyStoreException the key store exception
     */
    public PrivateKey getPrivateKey() throws P23RKeyStoreException {
        try {
            Enumeration<String> aliases = keyStore.aliases();
            while (aliases.hasMoreElements()) {
                final String uniqueAlias = aliases.nextElement();
                final Key key = keyStore.getKey(uniqueAlias, password.toCharArray());
                if (key instanceof PrivateKey) {
                    return (PrivateKey) key;
                }
            }
        } catch (KeyStoreException | UnrecoverableKeyException | NoSuchAlgorithmException e) {
            log.debug("get private key", e);
            throw new P23RKeyStoreException(e.getMessage(), e);
        }

        throw new P23RKeyStoreException("Could not load private key from key store: Key store "
                + keyStoreRessourceName + " is empty.");
    }

    /**
     * Gets the certificate.
     * 
     * @return the unique certificate in this keystore
     * @throws KeyStoreException the key store exception
     */
    public X509Certificate getCertificate() throws KeyStoreException {
        Enumeration<String> aliases;
        aliases = keyStore.aliases();
        while (aliases.hasMoreElements()) {
            final String uniqueAlias = aliases.nextElement();
            final Certificate certificate = keyStore.getCertificate(uniqueAlias);
            if (certificate instanceof X509Certificate) {
                return (X509Certificate) certificate;
            }
        }

        throw new KeyStoreException("Could not load certificate from key store: Key store "
                + keyStoreRessourceName + " is empty.");
    }

    /**
     * Loads a certificate file (*.cer) as resource. No keystore is needed.
     * 
     * @param resourceName the resource name
     * @return the x509 certificate
     */
    public static X509Certificate loadCertificateFromResource(final String resourceName) {
        final InputStream certificateInputStream = Thread.currentThread().getContextClassLoader()
            .getResourceAsStream(resourceName);
        if (null == certificateInputStream) {
            throw new IllegalArgumentException("could not load certificate resource: " + resourceName);
        }
        try {
            CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
            X509Certificate certificate = (X509Certificate) certificateFactory
                .generateCertificate(certificateInputStream);
            return certificate;
        } catch (final CertificateException e) {
            throw new TSLException("certificate factory error: " + e.getMessage(), e);
        }
    }

}
