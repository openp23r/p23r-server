package de.p23r.server.security.tsl;

/**
 * The Class TrustServiceListException.
 */
public class TrustServiceListException extends Exception {
    private static final long serialVersionUID = -6565474236810204285L;

    /**
     * Instantiates a new trust service list exception.
     */
    public TrustServiceListException() {
        super();
    }

    /**
     * Instantiates a new trust service list exception.
     * 
     * @param message the message
     * @param cause the cause
     */
    public TrustServiceListException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Instantiates a new trust service list exception.
     * 
     * @param message the message
     */
    public TrustServiceListException(String message) {
        super(message);
    }

    /**
     * Instantiates a new trust service list exception.
     * 
     * @param cause the cause
     */
    public TrustServiceListException(Throwable cause) {
        super(cause);
    }

}
