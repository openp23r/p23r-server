package de.p23r.server.security.tsl.core;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.XMLStructure;
import javax.xml.crypto.dom.DOMStructure;
import javax.xml.crypto.dsig.CanonicalizationMethod;
import javax.xml.crypto.dsig.DigestMethod;
import javax.xml.crypto.dsig.Reference;
import javax.xml.crypto.dsig.SignatureMethod;
import javax.xml.crypto.dsig.SignedInfo;
import javax.xml.crypto.dsig.Transform;
import javax.xml.crypto.dsig.XMLObject;
import javax.xml.crypto.dsig.XMLSignContext;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.dom.DOMValidateContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.dsig.keyinfo.KeyValue;
import javax.xml.crypto.dsig.keyinfo.X509Data;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.codec.digest.DigestUtils;
import org.etsi.uri._01903.v1_3.CertIDListType;
import org.etsi.uri._01903.v1_3.CertIDType;
import org.etsi.uri._01903.v1_3.DigestAlgAndValueType;
import org.etsi.uri._01903.v1_3.QualifyingPropertiesType;
import org.etsi.uri._01903.v1_3.SignedPropertiesType;
import org.etsi.uri._01903.v1_3.SignedSignaturePropertiesType;
import org.etsi.uri._02231.v2_.AddressType;
import org.etsi.uri._02231.v2_.ElectronicAddressType;
import org.etsi.uri._02231.v2_.InternationalNamesType;
import org.etsi.uri._02231.v2_.MultiLangStringType;
import org.etsi.uri._02231.v2_.NextUpdateType;
import org.etsi.uri._02231.v2_.NonEmptyMultiLangURIListType;
import org.etsi.uri._02231.v2_.NonEmptyMultiLangURIType;
import org.etsi.uri._02231.v2_.NonEmptyURIListType;
import org.etsi.uri._02231.v2_.ObjectFactory;
import org.etsi.uri._02231.v2_.PolicyOrLegalnoticeType;
import org.etsi.uri._02231.v2_.PostalAddressListType;
import org.etsi.uri._02231.v2_.PostalAddressType;
import org.etsi.uri._02231.v2_.TSLSchemeInformationType;
import org.etsi.uri._02231.v2_.TSPType;
import org.etsi.uri._02231.v2_.TrustServiceProviderListType;
import org.etsi.uri._02231.v2_.TrustStatusListType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3._2000._09.xmldsig_.DigestMethodType;
import org.w3._2000._09.xmldsig_.X509IssuerSerialType;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import de.p23r.server.security.tsl.TrustServiceListException;

/**
 * Trust Service List.
 * 
 * @author fcorneli
 * 
 */
public class TrustServiceList {

	private static final Logger LOG = LoggerFactory.getLogger(TrustServiceList.class);

	/**
	 * The Constant TSL_TAG.
	 */
	public static final String TSL_TAG = "http://uri.etsi.org/02231/TSLTag";

	/**
	 * The Constant TSL_TYPE.
	 */
	public static final String TSL_TYPE = "http://uri.p23r.de/TrstSvc/TSLType/generic";

	private static final String XADES_TYPE = "http://uri.etsi.org/01903#SignedProperties";

	private TrustStatusListType trustStatusList;

	/**
	 * The tsl document.
	 */
	private Document tslDocument;

	private List<TrustServiceProvider> trustServiceProviders;

	private final ObjectFactory objectFactory;

	private final DatatypeFactory datatypeFactory;

	private final org.etsi.uri._01903.v1_3.ObjectFactory xadesObjectFactory;

	private final org.w3._2000._09.xmldsig_.ObjectFactory xmldsigObjectFactory;

	/**
	 * Instantiates a new trust service list.
	 */
	protected TrustServiceList() {
		super();
		this.objectFactory = new ObjectFactory();
		this.xadesObjectFactory = new org.etsi.uri._01903.v1_3.ObjectFactory();
		this.xmldsigObjectFactory = new org.w3._2000._09.xmldsig_.ObjectFactory();
		try {
			this.datatypeFactory = DatatypeFactory.newInstance();
		} catch (DatatypeConfigurationException e) {
			throw new TSLException("datatype config error: " + e.getMessage(), e);
		}
	}

	/**
	 * Instantiates a new trust service list.
	 * 
	 * @param trustStatusList
	 *            the trust status list
	 * @param tslDocument
	 *            the tsl document
	 */
	protected TrustServiceList(TrustStatusListType trustStatusList, Document tslDocument) {
		this.trustStatusList = trustStatusList;
		this.tslDocument = tslDocument;
		this.objectFactory = new ObjectFactory();
		this.xadesObjectFactory = new org.etsi.uri._01903.v1_3.ObjectFactory();
		this.xmldsigObjectFactory = new org.w3._2000._09.xmldsig_.ObjectFactory();
		try {
			this.datatypeFactory = DatatypeFactory.newInstance();
		} catch (DatatypeConfigurationException e) {
			throw new TSLException("datatype config error: " + e.getMessage(), e);
		}
	}

	/**
	 * Gets the scheme name.
	 * 
	 * @return the scheme name
	 */
	public String getSchemeName() {
		Locale locale = Locale.getDefault();
		return getSchemeName(locale);
	}

	/**
	 * Sets the scheme name according to the default locale.
	 * 
	 * @param schemeName
	 *            the new scheme name
	 */
	public void setSchemeName(String schemeName) {
		Locale locale = Locale.getDefault();
		setSchemeName(schemeName, locale);
	}

	/**
	 * Sets the scheme operator name.
	 * 
	 * @param schemeOperatorName
	 *            the new scheme operator name
	 */
	public void setSchemeOperatorName(String schemeOperatorName) {
		Locale locale = Locale.getDefault();
		setSchemeOperatorName(schemeOperatorName, locale);
	}

	private void clearDocumentCacheAndSetChanged() {
		/*
		 * The XML signature should be regenerated anyway, so clear the TSL DOM
		 * object.
		 */
		this.tslDocument = null;
	}

	/**
	 * Sets the scheme name.
	 * 
	 * @param schemeName
	 *            the scheme name
	 * @param locale
	 *            the locale
	 */
	public void setSchemeName(String schemeName, Locale locale) {
		TSLSchemeInformationType tslSchemeInformation = TrustServiceListHelper.getSchemeInformation(trustStatusList, objectFactory);
		InternationalNamesType i18nSchemeName = tslSchemeInformation.getSchemeName();
		if (null == i18nSchemeName) {
			i18nSchemeName = this.objectFactory.createInternationalNamesType();
			tslSchemeInformation.setSchemeName(i18nSchemeName);
		}
		TrustServiceListUtils.setValue(schemeName, locale, i18nSchemeName);
		/*
		 * Also notify the listeners that we've changed content.
		 */
		clearDocumentCacheAndSetChanged();
	}

	/**
	 * Sets the scheme operator name.
	 * 
	 * @param schemeOperatorName
	 *            the scheme operator name
	 * @param locale
	 *            the locale
	 */
	public void setSchemeOperatorName(String schemeOperatorName, Locale locale) {
		TSLSchemeInformationType tslSchemeInformation = TrustServiceListHelper.getSchemeInformation(trustStatusList, objectFactory);
		InternationalNamesType i18nSchemeOperatorName = tslSchemeInformation.getSchemeOperatorName();
		if (null == i18nSchemeOperatorName) {
			i18nSchemeOperatorName = this.objectFactory.createInternationalNamesType();
			tslSchemeInformation.setSchemeOperatorName(i18nSchemeOperatorName);
		}
		TrustServiceListUtils.setValue(schemeOperatorName, locale, i18nSchemeOperatorName);
		clearDocumentCacheAndSetChanged();
	}

	/**
	 * Sets the scheme operator postal address.
	 * 
	 * @param postalAddress
	 *            the postal address
	 * @param locale
	 *            the locale
	 */
	public void setSchemeOperatorPostalAddress(PostalAddressType postalAddress, Locale locale) {
		AddressType schemeOperatorAddress = TrustServiceListHelper.getSchemeOperatorAddress(trustStatusList, objectFactory);
		PostalAddressListType postalAddresses = schemeOperatorAddress.getPostalAddresses();
		if (null == postalAddresses) {
			postalAddresses = this.objectFactory.createPostalAddressListType();
			schemeOperatorAddress.setPostalAddresses(postalAddresses);
		}
		/*
		 * First try to locate an existing address for the given locale.
		 */
		PostalAddressType existingPostalAddress = null;
		for (PostalAddressType currentPostalAddress : postalAddresses.getPostalAddress()) {
			if (currentPostalAddress.getLang().equalsIgnoreCase(locale.getLanguage())) {
				existingPostalAddress = currentPostalAddress;
				break;
			}
		}
		if (null != existingPostalAddress) {
			/*
			 * Update the existing postal address.
			 */
			existingPostalAddress.setStreetAddress(postalAddress.getStreetAddress());
			existingPostalAddress.setPostalCode(postalAddress.getPostalCode());
			existingPostalAddress.setLocality(postalAddress.getLocality());
			existingPostalAddress.setStateOrProvince(postalAddress.getStateOrProvince());
			existingPostalAddress.setCountryName(postalAddress.getCountryName());
		} else {
			LOG.debug("add postal address: " + locale.getLanguage());
			/*
			 * Add the new postal address. We really have to create a copy into
			 * a new JAXB object. This allows a caller to reuse a postal address
			 * JAXB data structure without running into trouble with the JAXB
			 * marshaller.
			 */
			PostalAddressType newPostalAddress = this.objectFactory.createPostalAddressType();
			newPostalAddress.setLang(locale.getLanguage());
			newPostalAddress.setStreetAddress(postalAddress.getStreetAddress());
			newPostalAddress.setPostalCode(postalAddress.getPostalCode());
			newPostalAddress.setLocality(postalAddress.getLocality());
			newPostalAddress.setStateOrProvince(postalAddress.getStateOrProvince());
			newPostalAddress.setCountryName(postalAddress.getCountryName());
			postalAddresses.getPostalAddress().add(newPostalAddress);
		}
	}

	/**
	 * Gets the scheme operator electronic addresses.
	 * 
	 * @return the scheme operator electronic addresses
	 */
	public List<String> getSchemeOperatorElectronicAddresses() {
		if (trustStatusList == null) {
			return Collections.emptyList();
		}
		TSLSchemeInformationType tslSchemeInformation = trustStatusList.getSchemeInformation();
		if (tslSchemeInformation == null) {
			return Collections.emptyList();
		}
		AddressType address = tslSchemeInformation.getSchemeOperatorAddress();
		if (address == null) {
			return Collections.emptyList();
		}
		ElectronicAddressType electronicAddress = address.getElectronicAddress();
		if (electronicAddress == null) {
			return Collections.emptyList();
		}
		return electronicAddress.getURI();
	}

	/**
	 * Sets the scheme operator electronic addresses.
	 * 
	 * @param addresses
	 *            the new scheme operator electronic addresses
	 */
	public void setSchemeOperatorElectronicAddresses(List<String> addresses) {
		AddressType schemeOperatorAddress = TrustServiceListHelper.getSchemeOperatorAddress(trustStatusList, objectFactory);
		ElectronicAddressType electronicAddress = schemeOperatorAddress.getElectronicAddress();
		if (null == electronicAddress) {
			electronicAddress = this.objectFactory.createElectronicAddressType();
			schemeOperatorAddress.setElectronicAddress(electronicAddress);
		}
		List<String> electronicAddresses = electronicAddress.getURI();
		electronicAddresses.clear();
		for (String address : addresses) {
			electronicAddresses.add(address);
		}
	}

	/**
	 * Gets the scheme name.
	 * 
	 * @param locale
	 *            the locale
	 * @return the scheme name
	 */
	public String getSchemeName(Locale locale) {
		if (null == this.trustStatusList) {
			return null;
		}
		TSLSchemeInformationType tslSchemeInformation = this.trustStatusList.getSchemeInformation();
		InternationalNamesType i18nSchemeName = tslSchemeInformation.getSchemeName();
		return TrustServiceListUtils.getValue(i18nSchemeName, locale);
	}

	/**
	 * Gets the scheme operator name.
	 * 
	 * @return the scheme operator name
	 */
	public String getSchemeOperatorName() {
		Locale locale = Locale.getDefault();
		return getSchemeOperatorName(locale);
	}

	/**
	 * Gets the scheme operator name.
	 * 
	 * @param locale
	 *            the locale
	 * @return the scheme operator name
	 */
	public String getSchemeOperatorName(Locale locale) {
		if (null == this.trustStatusList) {
			return null;
		}
		TSLSchemeInformationType tslSchemeInformation = this.trustStatusList.getSchemeInformation();
		InternationalNamesType i18nSchemeOperatorName = tslSchemeInformation.getSchemeOperatorName();
		return TrustServiceListUtils.getValue(i18nSchemeOperatorName, locale);
	}

	/**
	 * Adds the scheme information uri.
	 * 
	 * @param uri
	 *            the uri
	 * @param locale
	 *            the locale
	 */
	public void addSchemeInformationUri(String uri, Locale locale) {
		TSLSchemeInformationType schemeInformation = TrustServiceListHelper.getSchemeInformation(trustStatusList, objectFactory);
		NonEmptyMultiLangURIListType schemeInformationUriList = schemeInformation.getSchemeInformationURI();
		if (null == schemeInformationUriList) {
			schemeInformationUriList = this.objectFactory.createNonEmptyMultiLangURIListType();
			schemeInformation.setSchemeInformationURI(schemeInformationUriList);
		}
		NonEmptyMultiLangURIType i18nUri = this.objectFactory.createNonEmptyMultiLangURIType();
		i18nUri.setLang(locale.getLanguage());
		i18nUri.setValue(uri);
		schemeInformationUriList.getURI().add(i18nUri);
	}

	/**
	 * Gets the scheme information uris.
	 * 
	 * @return the scheme information uris
	 */
	public List<String> getSchemeInformationUris() {
		if (this.trustStatusList == null) {
			return Collections.emptyList();
		}
		TSLSchemeInformationType schemeInformation = this.trustStatusList.getSchemeInformation();
		if (schemeInformation == null) {
			return Collections.emptyList();
		}
		NonEmptyMultiLangURIListType schemeInformationUriList = schemeInformation.getSchemeInformationURI();
		if (schemeInformationUriList == null) {
			return Collections.emptyList();
		}
		List<NonEmptyMultiLangURIType> uris = schemeInformationUriList.getURI();
		List<String> results = new LinkedList<String>();
		for (NonEmptyMultiLangURIType uri : uris) {
			results.add(uri.getValue());
		}
		return results;
	}

	/**
	 * Sets the status determination approach.
	 * 
	 * @param statusDeterminationApproach
	 *            the new status determination approach
	 */
	public void setStatusDeterminationApproach(String statusDeterminationApproach) {
		TSLSchemeInformationType schemeInformation = TrustServiceListHelper.getSchemeInformation(trustStatusList, objectFactory);
		schemeInformation.setStatusDeterminationApproach(statusDeterminationApproach);
	}

	/**
	 * Gets the status determination approach.
	 * 
	 * @return the status determination approach
	 */
	public String getStatusDeterminationApproach() {
		if (null == this.trustStatusList) {
			return null;
		}
		TSLSchemeInformationType schemeInformation = this.trustStatusList.getSchemeInformation();
		if (null == schemeInformation) {
			return null;
		}
		return schemeInformation.getStatusDeterminationApproach();
	}

	/**
	 * Gets the trust service providers.
	 * 
	 * @return the trust service providers
	 */
	public List<TrustServiceProvider> getTrustServiceProviders() {
		if (null != this.trustServiceProviders) {
			// only load once
			return this.trustServiceProviders;
		}
		this.trustServiceProviders = new LinkedList<TrustServiceProvider>();
		if (null == this.trustStatusList) {
			return this.trustServiceProviders;
		}
		TrustServiceProviderListType trustServiceProviderList = this.trustStatusList.getTrustServiceProviderList();
		if (null == trustServiceProviderList) {
			return this.trustServiceProviders;
		}
		List<TSPType> tsps = trustServiceProviderList.getTrustServiceProvider();
		for (TSPType tsp : tsps) {
			TrustServiceProvider trustServiceProvider = new TrustServiceProvider(tsp);
			this.trustServiceProviders.add(trustServiceProvider);
		}
		return this.trustServiceProviders;
	}

	/**
	 * Gets the type.
	 * 
	 * @return the type
	 */
	public String getType() {
		if (null == this.tslDocument) {
			try {
				tslDocument = TrustServiceListHelper.marshall(trustStatusList, objectFactory);
			} catch (Exception e) {
				throw new TSLException("marshall error: " + e.getMessage(), e);
			}
		}
		TSLSchemeInformationType tslSchemeInformation = this.trustStatusList.getSchemeInformation();
		return tslSchemeInformation.getTSLType();
	}

	/**
	 * Gets the sequence number.
	 * 
	 * @return the sequence number
	 */
	public BigInteger getSequenceNumber() {
		if (null == this.trustStatusList) {
			return null;
		}
		TSLSchemeInformationType tslSchemeInformation = this.trustStatusList.getSchemeInformation();
		return tslSchemeInformation.getTSLSequenceNumber();
	}

	/**
	 * Gets the issue date.
	 * 
	 * @return the issue date
	 */
	public Date getIssueDate() {
		if (null == this.trustStatusList) {
			return null;
		}
		TSLSchemeInformationType tslSchemeInformation = this.trustStatusList.getSchemeInformation();
		XMLGregorianCalendar xmlGregorianCalendar = tslSchemeInformation.getListIssueDateTime();
		return xmlGregorianCalendar.toGregorianCalendar().getTime();
	}

	/**
	 * Checks for signature.
	 * 
	 * @return true, if successful
	 */
	public boolean hasSignature() {
		if (null == this.tslDocument) {
			/*
			 * Even if the JAXB TSL still has a signature, it's probably already
			 * invalid.
			 */
			return false;
		}
		Node signatureNode = TrustServiceListHelper.getSignatureNode(tslDocument);
		if (null == signatureNode) {
			return false;
		}
		return true;
	}

	/**
	 * Verify signature.
	 * 
	 * @return the x509 certificate
	 */
	public X509Certificate verifySignature() {
		if (tslDocument == null) {
			LOG.debug("first save the document");
		} else {
			Node signatureNode = TrustServiceListHelper.getSignatureNode(tslDocument);
			if (signatureNode == null) {
				LOG.debug("no ds:Signature element present");
			} else {
				KeyInfoKeySelector keyInfoKeySelector = new KeyInfoKeySelector();
				DOMValidateContext valContext = new DOMValidateContext(keyInfoKeySelector, signatureNode);
				XMLSignatureFactory xmlSignatureFactory = XMLSignatureFactory.getInstance("DOM");
				try {
					XMLSignature signature = xmlSignatureFactory.unmarshalXMLSignature(valContext);
					boolean coreValidity = signature.validate(valContext);
					// TODO: check what has been signed
					if (coreValidity) {
						LOG.debug("signature valid");
						return keyInfoKeySelector.getCertificate();
					}
				} catch (MarshalException e) {
					throw new TSLException("XML signature parse error: " + e.getMessage(), e);
				} catch (XMLSignatureException e) {
					throw new TSLException("XML signature error: " + e.getMessage(), e);
				}
			}
		}
		LOG.debug("signature invalid");
		return null;
	}

	/**
	 * Sign.
	 * 
	 * @param privateKey
	 *            the private key
	 * @param certificate
	 *            the certificate
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void sign(PrivateKey privateKey, X509Certificate certificate) throws IOException {
		LOG.debug("sign with: " + certificate.getSubjectX500Principal());
		if (null == this.tslDocument) {
			/*
			 * Marshall to DOM.
			 */
			try {
				tslDocument = TrustServiceListHelper.marshall(trustStatusList, objectFactory);
			} catch (Exception e) {
				throw new IOException("marshaller error: " + e.getMessage(), e);
			}
		}

		/*
		 * Remove existing XML signature from DOM.
		 */
		Node signatureNode = TrustServiceListHelper.getSignatureNode(tslDocument);
		if (null != signatureNode) {
			signatureNode.getParentNode().removeChild(signatureNode);
		}

		String tslId = this.trustStatusList.getId();

		/*
		 * Create new XML signature.
		 */
		try {
			xmlSign(privateKey, certificate, tslId);
		} catch (Exception e) {
			throw new IOException("XML sign error: " + e.getMessage(), e);
		}
	}

	private void xmlSign(PrivateKey privateKey, X509Certificate certificate, String tslId) throws TrustServiceListException, NoSuchAlgorithmException,
			InvalidAlgorithmParameterException, MarshalException, XMLSignatureException {
		XMLSignatureFactory signatureFactory = XMLSignatureFactory.getInstance("DOM");
		LOG.debug("xml signature factory: " + signatureFactory.getClass().getName());
		LOG.debug("loader: " + signatureFactory.getClass().getClassLoader());
		XMLSignContext signContext = new DOMSignContext(privateKey, this.tslDocument.getDocumentElement());
		signContext.putNamespacePrefix(XMLSignature.XMLNS, "ds");

		DigestMethod digestMethod = signatureFactory.newDigestMethod(DigestMethod.SHA256, null);
		List<Reference> references = new LinkedList<Reference>();
		List<Transform> transforms = new LinkedList<Transform>();
		transforms.add(signatureFactory.newTransform(Transform.ENVELOPED, (TransformParameterSpec) null));
		Transform exclusiveTransform = signatureFactory.newTransform(CanonicalizationMethod.EXCLUSIVE, (TransformParameterSpec) null);
		transforms.add(exclusiveTransform);

		Reference reference = signatureFactory.newReference("#" + tslId, digestMethod, transforms, null, null);
		references.add(reference);

		String signatureId = "xmldsig-" + UUID.randomUUID().toString();
		List<XMLObject> objects = new LinkedList<XMLObject>();
		addXadesBes(signatureFactory, this.tslDocument, signatureId, certificate, references, objects);

		SignatureMethod signatureMethod;
		if (TrustServiceListHelper.isJava6u18OrAbove()) {
			signatureMethod = signatureFactory.newSignatureMethod("http://www.w3.org/2001/04/xmldsig-more#rsa-sha256", null);
		} else {
			signatureMethod = signatureFactory.newSignatureMethod(SignatureMethod.RSA_SHA1, null);
		}
		CanonicalizationMethod canonicalizationMethod = signatureFactory.newCanonicalizationMethod(CanonicalizationMethod.EXCLUSIVE,
				(C14NMethodParameterSpec) null);
		SignedInfo signedInfo = signatureFactory.newSignedInfo(canonicalizationMethod, signatureMethod, references);

		List<Object> keyInfoContent = new LinkedList<Object>();

		KeyInfoFactory keyInfoFactory = KeyInfoFactory.getInstance();
		List<Object> x509DataObjects = new LinkedList<Object>();
		x509DataObjects.add(certificate);
		x509DataObjects.add(keyInfoFactory.newX509IssuerSerial(certificate.getIssuerX500Principal().toString(), certificate.getSerialNumber()));
		X509Data x509Data = keyInfoFactory.newX509Data(x509DataObjects);
		keyInfoContent.add(x509Data);

		KeyValue keyValue;
		try {
			keyValue = keyInfoFactory.newKeyValue(certificate.getPublicKey());
		} catch (KeyException e) {
			throw new TSLException("key exception: " + e.getMessage(), e);
		}
		keyInfoContent.add(keyValue);

		KeyInfo keyInfo = keyInfoFactory.newKeyInfo(keyInfoContent);

		String signatureValueId = signatureId + "-signature-value";
		XMLSignature xmlSignature = signatureFactory.newXMLSignature(signedInfo, keyInfo, objects, signatureId, signatureValueId);
		xmlSignature.sign(signContext);
	}

	/**
	 * Adds the xades bes.
	 *
	 * @param signatureFactory            the signature factory
	 * @param document            the document
	 * @param signatureId            the signature id
	 * @param signingCertificate            the signing certificate
	 * @param references            the references
	 * @param objects            the objects
	 * @throws TrustServiceListException the trust service list exception
	 */
	public void addXadesBes(XMLSignatureFactory signatureFactory, Document document, String signatureId, X509Certificate signingCertificate,
			List<Reference> references, List<XMLObject> objects) throws TrustServiceListException {
		LOG.debug("preSign");

		// QualifyingProperties
		QualifyingPropertiesType qualifyingProperties = this.xadesObjectFactory.createQualifyingPropertiesType();
		qualifyingProperties.setTarget("#" + signatureId);

		// SignedProperties
		SignedPropertiesType signedProperties = this.xadesObjectFactory.createSignedPropertiesType();
		String signedPropertiesId = signatureId + "-xades";
		signedProperties.setId(signedPropertiesId);
		qualifyingProperties.setSignedProperties(signedProperties);

		// SignedSignatureProperties
		SignedSignaturePropertiesType signedSignatureProperties = this.xadesObjectFactory.createSignedSignaturePropertiesType();
		signedProperties.setSignedSignatureProperties(signedSignatureProperties);

		// SigningTime
		GregorianCalendar signingTime = new GregorianCalendar();
		signingTime.setTimeZone(TimeZone.getTimeZone("Z"));
		signedSignatureProperties.setSigningTime(this.datatypeFactory.newXMLGregorianCalendar(signingTime));

		// SigningCertificate
		CertIDListType signingCertificates = this.xadesObjectFactory.createCertIDListType();
		CertIDType signingCertificateId = this.xadesObjectFactory.createCertIDType();

		X509IssuerSerialType issuerSerial = this.xmldsigObjectFactory.createX509IssuerSerialType();
		issuerSerial.setX509IssuerName(signingCertificate.getIssuerX500Principal().toString());
		issuerSerial.setX509SerialNumber(signingCertificate.getSerialNumber());
		signingCertificateId.setIssuerSerial(issuerSerial);

		DigestAlgAndValueType certDigest = this.xadesObjectFactory.createDigestAlgAndValueType();
		DigestMethodType jaxbDigestMethod = this.xmldsigObjectFactory.createDigestMethodType();
		jaxbDigestMethod.setAlgorithm(DigestMethod.SHA256);
		certDigest.setDigestMethod(jaxbDigestMethod);
		try {
			MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
			byte[] digestValue;
			try {
				digestValue = messageDigest.digest(signingCertificate.getEncoded());
			} catch (CertificateEncodingException e) {
				throw new TSLException("certificate encoding error: " + e.getMessage(), e);
			}
			certDigest.setDigestValue(digestValue);
			signingCertificateId.setCertDigest(certDigest);

			signingCertificates.getCert().add(signingCertificateId);
			signedSignatureProperties.setSigningCertificate(signingCertificates);

			// marshall XAdES QualifyingProperties
			Node qualifyingPropertiesNode = TrustServiceListHelper.marshallQualifyingProperties(document, qualifyingProperties, xadesObjectFactory);

			// add XAdES ds:Object
			List<XMLStructure> xadesObjectContent = new LinkedList<XMLStructure>();
			xadesObjectContent.add(new DOMStructure(qualifyingPropertiesNode));
			XMLObject xadesObject = signatureFactory.newXMLObject(xadesObjectContent, null, null, null);
			objects.add(xadesObject);

			// add XAdES ds:Reference
			DigestMethod digestMethod = signatureFactory.newDigestMethod(DigestMethod.SHA256, null);
			List<Transform> transforms = new LinkedList<Transform>();
			Transform exclusiveTransform = signatureFactory.newTransform(CanonicalizationMethod.EXCLUSIVE, (TransformParameterSpec) null);
			transforms.add(exclusiveTransform);
			Reference reference = signatureFactory.newReference("#" + signedPropertiesId, digestMethod, transforms, XADES_TYPE, null);
			references.add(reference);
		} catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException e) {
			LOG.debug("add xades bes", e);
			throw new TrustServiceListException(e.getMessage(), e);
		}
	}

	/**
	 * Save as.
	 * 
	 * @param tslFile
	 *            the tsl file
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void saveAs(File tslFile) throws IOException {
		if (null == tslFile) {
			throw new IllegalStateException("no TSL file set");
		}
		LOG.debug("save to: " + tslFile.getAbsolutePath());
		if (null == this.tslDocument) {
			try {
				tslDocument = TrustServiceListHelper.marshall(trustStatusList, objectFactory);
			} catch (Exception e) {
				throw new IOException("marshall error: " + e.getMessage(), e);
			}
			/*
			 * Only remove existing XML signature from new (or changed) DOM
			 * documents.
			 */
			Node signatureNode = TrustServiceListHelper.getSignatureNode(tslDocument);
			if (null != signatureNode) {
				signatureNode.getParentNode().removeChild(signatureNode);
			}
		}
		try {
			TrustServiceListHelper.toFile(tslFile, tslDocument);
		} catch (Exception e) {
			throw new IOException("DOM transformation error: " + e.getMessage(), e);
		}
	}

	/**
	 * Gets the tsl document.
	 *
	 * @return the tsl document
	 */
	public Document getTslDocument() {
		return tslDocument;
	}

	/**
	 * Sets the tsl document.
	 *
	 * @param tslDocument the new tsl document
	 */
	public void setTslDocument(Document tslDocument) {
		this.tslDocument = tslDocument;
	}

	/**
	 * Gets the scheme operator postal address.
	 * 
	 * @param locale
	 *            the locale
	 * @return the scheme operator postal address
	 */
	public PostalAddressType getSchemeOperatorPostalAddress(Locale locale) {
		List<PostalAddressType> postalAddresses = TrustServiceListHelper.getPostalAddresses(trustStatusList);
		for (PostalAddressType postalAddress : postalAddresses) {
			if (postalAddress.getLang().toLowerCase().equals(locale.getLanguage())) {
				return postalAddress;
			}
		}
		return null;
	}

	/**
	 * Adds the scheme type.
	 * 
	 * @param schemeType
	 *            the scheme type
	 */
	public void addSchemeType(String schemeType) {
		TSLSchemeInformationType schemeInformation = TrustServiceListHelper.getSchemeInformation(trustStatusList, objectFactory);
		NonEmptyURIListType schemeTypeList = schemeInformation.getSchemeTypeCommunityRules();
		if (null == schemeTypeList) {
			schemeTypeList = this.objectFactory.createNonEmptyURIListType();
			schemeInformation.setSchemeTypeCommunityRules(schemeTypeList);
		}
		schemeTypeList.getURI().add(schemeType);
	}

	/**
	 * Gets the scheme types.
	 * 
	 * @return the scheme types
	 */
	public List<String> getSchemeTypes() {
		if (this.trustStatusList == null) {
			return Collections.emptyList();
		}
		TSLSchemeInformationType schemeInformation = this.trustStatusList.getSchemeInformation();
		if (schemeInformation == null) {
			return Collections.emptyList();
		}
		NonEmptyURIListType schemeTypeList = schemeInformation.getSchemeTypeCommunityRules();
		if (schemeTypeList == null) {
			return Collections.emptyList();
		}
		return schemeTypeList.getURI();
	}

	/**
	 * Sets the scheme territory.
	 * 
	 * @param schemeTerritory
	 *            the new scheme territory
	 */
	public void setSchemeTerritory(String schemeTerritory) {
		TSLSchemeInformationType schemeInformation = TrustServiceListHelper.getSchemeInformation(trustStatusList, objectFactory);
		schemeInformation.setSchemeTerritory(schemeTerritory);
	}

	/**
	 * Gets the scheme territory.
	 * 
	 * @return the scheme territory
	 */
	public String getSchemeTerritory() {
		if (null == this.trustStatusList) {
			return null;
		}
		TSLSchemeInformationType schemeInformation = this.trustStatusList.getSchemeInformation();
		if (null == schemeInformation) {
			return null;
		}
		return schemeInformation.getSchemeTerritory();
	}

	/**
	 * Adds the legal notice.
	 * 
	 * @param legalNotice
	 *            the legal notice
	 * @param locale
	 *            the locale
	 */
	public void addLegalNotice(String legalNotice, Locale locale) {
		TSLSchemeInformationType schemeInformation = TrustServiceListHelper.getSchemeInformation(trustStatusList, objectFactory);
		PolicyOrLegalnoticeType policyOrLegalnotice = schemeInformation.getPolicyOrLegalNotice();
		if (null == policyOrLegalnotice) {
			policyOrLegalnotice = this.objectFactory.createPolicyOrLegalnoticeType();
			schemeInformation.setPolicyOrLegalNotice(policyOrLegalnotice);
		}
		List<MultiLangStringType> tslLegalNotices = policyOrLegalnotice.getTSLLegalNotice();

		MultiLangStringType tslLegalNotice = this.objectFactory.createMultiLangStringType();
		tslLegalNotice.setLang(locale.getLanguage());
		tslLegalNotice.setValue(legalNotice);

		tslLegalNotices.add(tslLegalNotice);
	}

	/**
	 * Gets the legal notice.
	 * 
	 * @return the legal notice
	 */
	public String getLegalNotice() {
		return getLegalNotice(Locale.ENGLISH);
	}

	/**
	 * Gets the legal notice.
	 * 
	 * @param locale
	 *            the locale
	 * @return the legal notice
	 */
	public String getLegalNotice(Locale locale) {
		if (null == this.trustStatusList) {
			return null;
		}
		TSLSchemeInformationType schemeInformation = this.trustStatusList.getSchemeInformation();
		if (null == schemeInformation) {
			return null;
		}
		PolicyOrLegalnoticeType policyOrLegalnotice = schemeInformation.getPolicyOrLegalNotice();
		if (null == policyOrLegalnotice) {
			return null;
		}
		List<MultiLangStringType> tslLegalNotices = policyOrLegalnotice.getTSLLegalNotice();
		for (MultiLangStringType tslLegalNotice : tslLegalNotices) {
			String lang = tslLegalNotice.getLang();
			if (locale.getLanguage().equals(lang)) {
				return tslLegalNotice.getValue();
			}
		}
		return null;
	}

	/**
	 * Sets the historical information period.
	 * 
	 * @param historicalInformationPeriod
	 *            the new historical information period
	 */
	public void setHistoricalInformationPeriod(int historicalInformationPeriod) {
		TSLSchemeInformationType schemeInformation = TrustServiceListHelper.getSchemeInformation(trustStatusList, objectFactory);
		schemeInformation.setHistoricalInformationPeriod(BigInteger.valueOf(historicalInformationPeriod));
	}

	/**
	 * Gets the historical information period.
	 * 
	 * @return the historical information period
	 */
	public Integer getHistoricalInformationPeriod() {
		if (null == this.trustStatusList) {
			return null;
		}
		TSLSchemeInformationType schemeInformation = this.trustStatusList.getSchemeInformation();
		if (null == schemeInformation) {
			return null;
		}
		BigInteger historicalInformationPeriod = schemeInformation.getHistoricalInformationPeriod();
		if (null == historicalInformationPeriod) {
			return null;
		}
		return historicalInformationPeriod.intValue();
	}

	/**
	 * Sets the list issue date time.
	 * 
	 * @param listIssueDateTime
	 *            the new list issue date time
	 */
	public void setListIssueDate(Date listIssueDateTime) {
		TSLSchemeInformationType schemeInformation = TrustServiceListHelper.getSchemeInformation(trustStatusList, objectFactory);
		GregorianCalendar listIssueCalendar = new GregorianCalendar();
		listIssueCalendar.setTime(listIssueDateTime);
		listIssueCalendar.setTimeZone(TimeZone.getTimeZone("Z"));
		schemeInformation.setListIssueDateTime(datatypeFactory.newXMLGregorianCalendar(listIssueCalendar));
	}

	/**
	 * Gets the list issue date time.
	 * 
	 * @return the list issue date time
	 */
	public Date getListIssueDate() {
		if (null == this.trustStatusList) {
			return null;
		}
		TSLSchemeInformationType schemeInformation = this.trustStatusList.getSchemeInformation();
		if (null == schemeInformation) {
			return null;
		}
		XMLGregorianCalendar listIssueDateTime = schemeInformation.getListIssueDateTime();
		if (null == listIssueDateTime) {
			return null;
		}
		return listIssueDateTime.toGregorianCalendar().getTime();
	}

	/**
	 * Sets the next update.
	 *
	 * @param nextUpdateDate the new next update
	 */
	public void setNextUpdate(Date nextUpdateDate) {
		TSLSchemeInformationType schemeInformation = TrustServiceListHelper.getSchemeInformation(trustStatusList, objectFactory);
		GregorianCalendar nextUpdateCalendar = new GregorianCalendar();
		nextUpdateCalendar.setTime(nextUpdateDate);
		nextUpdateCalendar.setTimeZone(TimeZone.getTimeZone("Z"));

		NextUpdateType nextUpdate = schemeInformation.getNextUpdate();
		if (null == nextUpdate) {
			nextUpdate = this.objectFactory.createNextUpdateType();
			schemeInformation.setNextUpdate(nextUpdate);
		}
		nextUpdate.setDateTime(this.datatypeFactory.newXMLGregorianCalendar(nextUpdateCalendar));
	}

	/**
	 * Sets the tSL sequence number.
	 * 
	 * @param sequenceNumber
	 *            the new tSL sequence number
	 */
	public void setTSLSequenceNumber(BigInteger sequenceNumber) {
		TSLSchemeInformationType schemeInformation = TrustServiceListHelper.getSchemeInformation(trustStatusList, objectFactory);
		schemeInformation.setTSLSequenceNumber(sequenceNumber);
	}

	/**
	 * Gets the next update.
	 * 
	 * @return the next update
	 */
	public Date getNextUpdate() {
		if (null == this.trustStatusList) {
			return null;
		}
		TSLSchemeInformationType schemeInformation = this.trustStatusList.getSchemeInformation();
		if (null == schemeInformation) {
			return null;
		}

		NextUpdateType nextUpdate = schemeInformation.getNextUpdate();
		if (null == nextUpdate) {
			return null;
		}
		XMLGregorianCalendar nextUpdateXmlCalendar = nextUpdate.getDateTime();
		return nextUpdateXmlCalendar == null ? null : nextUpdateXmlCalendar.toGregorianCalendar().getTime();
	}

	/**
	 * Adds the trust service provider.
	 * 
	 * @param trustServiceProvider
	 *            the trust service provider
	 */
	public void addTrustServiceProvider(TrustServiceProvider trustServiceProvider) {
		TrustStatusListType tslType = TrustServiceListHelper.getTrustStatusList(trustStatusList, objectFactory);
		TrustServiceProviderListType trustServiceProviderList = tslType.getTrustServiceProviderList();
		if (null == trustServiceProviderList) {
			trustServiceProviderList = this.objectFactory.createTrustServiceProviderListType();
			tslType.setTrustServiceProviderList(trustServiceProviderList);
		}
		List<TSPType> tspList = trustServiceProviderList.getTrustServiceProvider();
		tspList.add(trustServiceProvider.getTSP());
		// reset Java model cache
		this.trustServiceProviders = null;
	}

	/**
	 * Adds the distribution point.
	 * 
	 * @param distributionPointUri
	 *            the distribution point uri
	 */
	public void addDistributionPoint(String distributionPointUri) {
		TSLSchemeInformationType schemeInformation = TrustServiceListHelper.getSchemeInformation(trustStatusList, objectFactory);
		ElectronicAddressType distributionPoints = schemeInformation.getDistributionPoints();
		if (null == distributionPoints) {
			distributionPoints = this.objectFactory.createElectronicAddressType();
			schemeInformation.setDistributionPoints(distributionPoints);
		}
		distributionPoints.getURI().add(distributionPointUri);
	}

	/**
	 * Gets the distribution points.
	 * 
	 * @return the distribution points
	 */
	public List<String> getDistributionPoints() {
		final TSLSchemeInformationType schemeInformation = TrustServiceListHelper.getSchemeInformation(trustStatusList, objectFactory);
		final ElectronicAddressType distributionPoints = schemeInformation.getDistributionPoints();
		return distributionPoints.getURI();
	}

	/**
	 * Gets the sha1 fingerprint.
	 * 
	 * @return the sha1 fingerprint
	 */
	public String getSha1Fingerprint() {
		return DigestUtils.shaHex(toByteArray());
	}

	/**
	 * To byte array.
	 *
	 * @return the byte[]
	 * @throws TransformerFactoryConfigurationError the transformer factory configuration error
	 */
	public byte[] toByteArray() throws TransformerFactoryConfigurationError {
		Source source = new DOMSource(this.tslDocument);
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		Result result = new StreamResult(byteArrayOutputStream);
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer;
		try {
			transformer = transformerFactory.newTransformer();
		} catch (TransformerConfigurationException e) {
			throw new TSLException(e);
		}
		// transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION,
		// "yes");
		try {
			transformer.transform(source, result);
		} catch (TransformerException e) {
			throw new TSLException(e);
		}
		return byteArrayOutputStream.toByteArray();
	}

	/**
	 * Gets the sha256 fingerprint.
	 * 
	 * @return the sha256 fingerprint
	 */
	public String getSha256Fingerprint() {
		return DigestUtils.sha256Hex(toByteArray());
	}

	/**
	 * Checks if is obsolete.
	 * 
	 * @return true, if is obsolete
	 */
	public boolean isObsolete() {
		final Date nextUpdate = getNextUpdate();
		if (nextUpdate == null) {
			return false;
		}
		return nextUpdate.before(new Date()) || nextUpdate.equals(new Date());
	}
}
