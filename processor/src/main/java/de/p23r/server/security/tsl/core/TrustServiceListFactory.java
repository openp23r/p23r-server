package de.p23r.server.security.tsl.core;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.security.cert.X509Certificate;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.etsi.uri._02231.v2_.ObjectFactory;
import org.etsi.uri._02231.v2_.TrustStatusListType;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * Factory for Trust Service Lists.
 * 
 * @author fcorneli
 * 
 */
public class TrustServiceListFactory {

    private TrustServiceListFactory() {
        super();
    }

    /**
     * Creates a new trust service list from the given file.
     * 
     * @param tslUri the tsl uri
     * @return the trust service list
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static TrustServiceList newInstance(URI tslUri) throws IOException {
        if (null == tslUri) {
            throw new IllegalArgumentException();
        }
        Document tslDocument;
        try {
            tslDocument = parseDocument(tslUri);
        } catch (Exception e) {
            throw new IOException("DOM parse error: " + e.getMessage(), e);
        }
        return newInstance(tslDocument);
    }

    /**
     * Creates a new trust service list from the given inputStream.
     * 
     * @param inputStream the input stream
     * @return the trust service list
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static TrustServiceList newInstance(InputStream inputStream) throws IOException {
        if (null == inputStream) {
            throw new IllegalArgumentException();
        }
        Document tslDocument;
        try {
            tslDocument = parseDocument(inputStream);
        } catch (Exception e) {
            throw new IOException("DOM parse error: " + e.getMessage(), e);
        }
        return newInstance(tslDocument);
    }

    /**
     * Creates a trust service list from a given DOM document.
     * 
     * @param tslDocument the DOM TSL document.
     * @return the trust service list
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static TrustServiceList newInstance(Document tslDocument) throws IOException {
        if (null == tslDocument) {
            throw new IllegalArgumentException();
        }
        TrustStatusListType trustServiceStatusList;
        try {
            trustServiceStatusList = parseTslDocument(tslDocument);
        } catch (JAXBException e) {
            throw new IOException("TSL parse error: " + e.getMessage(), e);
        }
        return new TrustServiceList(trustServiceStatusList, tslDocument);
    }

    private static Document parseDocument(URI uri) throws ParserConfigurationException, SAXException,
            IOException {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setNamespaceAware(true);
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        return documentBuilder.parse(uri.toASCIIString());
    }

    private static Document parseDocument(InputStream inputStream) throws ParserConfigurationException,
            SAXException, IOException {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setNamespaceAware(true);
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        return documentBuilder.parse(inputStream);
    }

    private static TrustStatusListType parseTslDocument(Document tslDocument) throws JAXBException {
        Unmarshaller unmarshaller = getUnmarshaller();
        JAXBElement<TrustStatusListType> jaxbElement = unmarshaller.unmarshal(tslDocument,
            TrustStatusListType.class);
        return jaxbElement.getValue();
    }

    private static Unmarshaller getUnmarshaller() throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(ObjectFactory.class);
        return jaxbContext.createUnmarshaller();
    }

    /**
     * Creates a new empty trust service list.
     * 
     * @return the trust service list
     */
    public static TrustServiceList newInstance() {
        return new TrustServiceList();
    }

    /**
     * Creates a new TrustServiceList object.
     * 
     * @param name the name
     * @param tradeName the trade name
     * @return the trust service provider
     */
    public static TrustServiceProvider createTrustServiceProvider(String name, String tradeName) {
        return new TrustServiceProvider(name, tradeName);
    }

    /**
     * Creates a new TrustServiceList object.
     * 
     * @param certificate the certificate
     * @return the trust service
     */
    public static TrustService createTrustService(X509Certificate certificate) {
        return new TrustService(certificate);
    }

}
