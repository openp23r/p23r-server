package de.p23r.server.security.tsl.builder;

import java.util.List;

import org.etsi.uri._02231.v2_.PostalAddressType;

/**
 * An implementation of <code>SchemeInformationSource</code> provides data for the
 * <code>SchemeInformation</code> element of an ETSI-TSL xml file.
 * 
 * @author bkrauf
 * 
 */
public interface SchemeInformationSource {

    /**
     * Gets the scheme operator name.
     * 
     * @return the scheme operator name
     */
    String getSchemeOperatorName();

    /**
     * Gets the scheme operator postal address.
     * 
     * @return the scheme operator's postal address
     */
    PostalAddressType getSchemeOperatorPostalAddress();

    /**
     * Gets the scheme operator electronic addresses.
     * 
     * @return the scheme operator electronic addresses
     */
    List<String> getSchemeOperatorElectronicAddresses();

    /**
     * Gets the scheme name.
     * 
     * @return the scheme name
     */
    String getSchemeName();

    /**
     * Gets the scheme information uris.
     * 
     * @return a list of URIs to scheme information resources
     */
    List<String> getSchemeInformationUris();

    /**
     * Gets the status determination approach.
     * 
     * @return the status determination approach
     */
    String getStatusDeterminationApproach();

    /**
     * Gets the scheme type.
     * 
     * @return the scheme type
     */
    String getSchemeType();

    /**
     * Gets the validity period in month.
     * 
     * @return the scheme's validity period in month
     */
    String getValidityPeriodInMonth();

    /**
     * Gets the distribution points.
     * 
     * @return a list of the scheme's distribution point URIs
     */
    List<String> getDistributionPoints();

}
