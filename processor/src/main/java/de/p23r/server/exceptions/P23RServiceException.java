package de.p23r.server.exceptions;

/**
 * The Class P23RServiceException.
 *
 * @author sim
 */
public class P23RServiceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1555132920332036489L;

	private String componentName;
	
	/**
	 * Instantiates a new p23r service exception.
	 *
	 * @param componentName the component name
	 * @param message the message
	 */
	public P23RServiceException(String componentName, String message) {
		super(message);
		this.componentName = componentName;
	}

	/**
	 * Gets the component name.
	 *
	 * @return the component name
	 */
	public String getComponentName() {
		return componentName;
	}

}
