package de.p23r.server.exceptions;

/**
 * The Class P23RRuleNotFoundException.
 */
public class P23RRuleNotFoundException extends P23RServiceException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2735045309947518559L;

	/**
	 * Instantiates a new p23 r rule not found exception.
	 *
	 * @param componentName the component name
	 * @param message the message
	 */
	public P23RRuleNotFoundException(String componentName, String message) {
		super(componentName, message);
	}

}
