package de.p23r.server.exceptions;

/**
 * The Class P23RRuleNotActivatedException.
 */
public class P23RRuleNotActivatedException extends P23RServiceException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6547587325171352485L;

	/**
	 * Instantiates a new p23 r rule not activated exception.
	 *
	 * @param componentName the component name
	 * @param message the message
	 */
	public P23RRuleNotActivatedException(String componentName, String message) {
		super(componentName, message);
	}

}
