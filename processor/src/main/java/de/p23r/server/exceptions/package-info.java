/**
 * Provides exception classes for the P23R processor.
 */
package de.p23r.server.exceptions;