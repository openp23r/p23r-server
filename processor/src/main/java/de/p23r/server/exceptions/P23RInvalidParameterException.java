package de.p23r.server.exceptions;

/**
 * The Class P23RInvalidParameterException.
 */
public class P23RInvalidParameterException extends P23RServiceException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4823329537751614076L;

	/**
	 * Instantiates a new p23 r invalid parameter exception.
	 *
	 * @param componentName the component name
	 * @param message the message
	 */
	public P23RInvalidParameterException(String componentName, String message) {
		super(componentName, message);
	}

}
