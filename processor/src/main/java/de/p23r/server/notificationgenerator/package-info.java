/**
 * Provides classes representing the notification generator service. It contains mainly an interface class describing the service interface,
 * a bean class that implements the service and some wrapper classes representing the interface parameters.
 */
package de.p23r.server.notificationgenerator;