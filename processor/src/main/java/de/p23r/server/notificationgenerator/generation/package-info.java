/**
 * Containing all classes representing the notification generation steps.
 */
package de.p23r.server.notificationgenerator.generation;