package de.p23r.server.notificationgenerator.generation;

import java.util.HashMap;
import java.util.Map;

import de.p23r.server.modelandrulemanagement.GenerationStep;
import de.p23r.server.modelandrulemanagement.ModelAndRuleManagementService;
import de.p23r.server.modelandrulemanagement.NextGenerationStepParameter;

/**
 * The Class RuleProxy.
 * 
 * @author sim
 */
public class RuleProxy {

    /** The id. */
    private String id;

    /** The ruleset manager service. */
    private ModelAndRuleManagementService modelAndRuleManagement;

    // mini simple cache
    // should be replaces with ehcache e.g.
    private static Map<String, RuleProxy> cache = new HashMap<>();
    
    private Map<String, GenerationStep> generationSteps = new HashMap<>();
    
    /**
     * The selection schema.
     */
    private String selectionSchema;

    /**
     * Class constructor for a specific ruleset identified by its id.
     * 
     * @param ruleId the rule id
     */
    private RuleProxy(String ruleId, ModelAndRuleManagementService modelAndRuleManagement) {
        this.id = ruleId;
        this.modelAndRuleManagement = modelAndRuleManagement;
    }

    /**
     * Creates the.
     *
     * @param ruleId the rule id
     * @param modelAndRuleManagement the model and rule management
     * @return the rule proxy
     */
    public static RuleProxy create(String ruleId, ModelAndRuleManagementService modelAndRuleManagement) {
    	if (cache.containsKey(ruleId)) {
    		return cache.get(ruleId);
    	} else {
    		RuleProxy newProxy = new RuleProxy(ruleId, modelAndRuleManagement);
    		cache.put(ruleId, newProxy);
            return newProxy;
    	}
    }

    /**
     * Gets the id of this rule.
     * 
     * @return the rule id
     */
    public String getId() {
        return id;
    }

    /**
     * Gets the selection schema.
     * 
     * @return the selection schema
     */
    public String getSelectionSchema() {
        return selectionSchema;
    }

    /**
     * Gets the next generation step.
     *
     * @param namespace the namespace
     * @return the next generation step
     */
    public GenerationStep getNextGenerationStep(String namespace) {
    	if (generationSteps.containsKey(namespace)) {
    		return generationSteps.get(namespace);
    	} else {
            NextGenerationStepParameter parameter = new NextGenerationStepParameter();
            parameter.setRuleId(id);
            parameter.setNamespace(namespace);
            GenerationStep step = modelAndRuleManagement.getNextGenerationStep(parameter);
            generationSteps.put(namespace, step);
            return step;
    	}
    }

    /**
     * Gets the first generation step.
     *
     * @return the first generation step
     */
    public GenerationStep getFirstGenerationStep() {
    	if (generationSteps.containsKey("first")) {
    		return generationSteps.get("first");
    	} else {
    		GenerationStep first = modelAndRuleManagement.getFirstGenerationStep(id);
    		generationSteps.put("first", first);
    		return first;
    	}
    }

    /**
     * Gets the core transformation.
     * 
     * @return the core transformation
     */
    public String getCoreTransformation() {
    	return modelAndRuleManagement.getCoreTransformation(id);
    }

}
