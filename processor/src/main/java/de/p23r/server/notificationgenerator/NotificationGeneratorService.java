package de.p23r.server.notificationgenerator;

import de.p23r.server.common.GenerationContext;
import de.p23r.server.exceptions.P23RServiceException;

/**
 * The Interface NotificationGeneratorService.
 *
 * @author sim
 */
public interface NotificationGeneratorService {

	/**
	 * Generate notification.
	 *
	 * @param context the context
	 * @throws P23RServiceException the p23 r service exception
	 */
	void generateNotification(GenerationContext context) throws P23RServiceException;
	
}
