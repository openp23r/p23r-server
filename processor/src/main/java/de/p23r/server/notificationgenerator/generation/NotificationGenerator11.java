package de.p23r.server.notificationgenerator.generation;

import static de.p23r.server.common.Messages.NOTIFICATIONGENERATION;
import static de.p23r.server.common.Messages.message;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.inject.Inject;

import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.slf4j.Logger;

import de.p23r.common.modelandrulemanagement.NamespaceHelper;
import de.p23r.server.common.GenerationContext;
import de.p23r.server.common.NotificationProfileHelper;
import de.p23r.server.common.P23RError;
import de.p23r.server.common.TBRS;
import de.p23r.server.exceptions.P23RServiceException;
import de.p23r.server.modelandrulemanagement.GenerationStep;

/**
 * The Class NotificationTransformator.
 * 
 * @author sim
 */
public class NotificationGenerator11 extends NotificationGenerator {

	@Inject
	private Logger log;
	
	/**
	 * Transform.
	 *
	 * @param context the context
	 * @return the transformation result
	 * @throws P23RServiceException the p23 r service exception
	 */
	public GenerationContext generate(GenerationContext context) throws P23RServiceException {

		RuleProxy rule = RuleProxy.create(context.getProfile().getNotificationRuleId(), modelAndRuleManagement);

//		GenerationContext result = new GenerationContext();
//		result.setProfile(new NotificationProfileHelper(context.getProfile().getDocument().clone()));
		
		GenerationStep step = rule.getFirstGenerationStep();
		if (step.getName() == null) {
			log.error("no first generation step for rule {} found", rule.getId());
			protocol.logError(P23RError.GENERATION_FAILURE, context.getProfile(), "Kann ersten Generierungsschritt nicht finden.");
			throw new P23RServiceException(message(NOTIFICATIONGENERATION), "Kann ersten Generierungsschritt nicht finden.");
		} else {
			while (step.getName() != null) {
				context = generateStep(step, context);

				if (context != null && context.getNotification() != null) {
					step = rule.getNextGenerationStep(new NamespaceHelper().getNamespaceFromXmlContent(context.getNotification()));					
					// take over the actual profile
//					profile = result.getProfile();
				} else {
					throw new P23RServiceException(message(NOTIFICATIONGENERATION), "In einem Generierungsschritt ist ein Fehler aufgetreten.");
				}
			}			
		}

		return context;
	}

	protected GenerationContext generateStep(GenerationStep step, GenerationContext context) throws P23RServiceException {

		// keep old profile in case of problems
		NotificationProfileHelper currentProfile = new NotificationProfileHelper(context.getProfile().getDocument().clone());

		Document notification = null;
		if (context.getNotification() != null) {
	        SAXBuilder builder = new SAXBuilder();
	        try {
	            notification = builder.build(new ByteArrayInputStream(context.getNotification().getBytes(TBRS.CHARSET)));
	        } catch (JDOMException e) {
	            log.error("failed to set notification.", e);
	        } catch (IOException e) {
	            log.error("failed to set notification.", e);
	        }			
		}

        String nextNotification = null;
		String nextProfile = null;
		
		switch (step.getScriptType()) {
		case TRANSFORMATION:
			if (step.getProfileScript() != null) {
				nextProfile = transform(context.getProfile().getDocument(), step.getProfileScript(), context);
			} else {
				protocol.logWarning(currentProfile, "Kein Profilskript in Generierungsschritt " + step.getName() + " vorhanden!");
			}
			if (!isProfileValid(nextProfile, currentProfile)) {
				log.error("invalid transformation result of notification profile in step {}: {}", step.getName(), nextProfile);
				return null;
			} else {
				nextNotification = transform(notification, step.getNotificationScript(), context);				
			}
			break;
		case DATS:
		case SELECTION:
			if (step.getProfileScript() != null) {
				context.getProperties().put(GenerationContext.SELECTION_SCRIPT, step.getProfileScript());
				nextProfile = datapool.selectData(context);
			} else {
				protocol.logWarning(currentProfile, "Kein Profilskript in Generierungsschritt " + step.getName() + " vorhanden!");
			}
			if (!isProfileValid(nextProfile, currentProfile)) {
				log.error("invalid selection result of notification profile in step {}: {}", step.getName(), nextProfile);
				return null;
			} else {
				context.getProperties().put(GenerationContext.SELECTION_SCRIPT, step.getNotificationScript());
				nextNotification = datapool.selectData(context);
			}
			break;
		case UNKNOWN:
		default:
			log.error("invalid script format in generation step {}", step.getName());
			return null;
		}

		if (nextNotification == null || !isNotificationValid(nextNotification, currentProfile)) {
			log.error("invalid transformation/selection result of notification in step {}: {}", step.getName(), nextNotification);
		}
		
		return createResult(context, nextNotification, nextProfile, step, currentProfile);
	}

}
