package de.p23r.server.notificationgenerator;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.switchyard.component.bean.Reference;
import org.switchyard.component.bean.Service;

import de.p23r.server.common.GenerationContext;
import de.p23r.server.common.NotificationProfileHelper;
import de.p23r.server.common.annotations.P23RUnit;
import de.p23r.server.datapool.DataPoolService;
import de.p23r.server.exceptions.P23RServiceException;
import de.p23r.server.notificationdispatcher.NotificationDispatcherService;
import de.p23r.server.notificationgenerator.generation.NotificationGenerator11;
import de.p23r.server.protocolpool.Protocol;
import static de.p23r.server.common.Messages.*;

/**
 * The Class NotificationGeneratorServiceBean.
 *
 * @author sim
 */
@Service(NotificationGeneratorService.class)
public class NotificationGeneratorServiceBean implements NotificationGeneratorService {

	@Inject @P23RUnit
	private Protocol protocol;

	@Inject
	private NotificationGenerator11 transformator;
	
	@Inject
	@Reference
	private NotificationDispatcherService dispatcher;

	@Inject
	@Reference
	private DataPoolService datapool;

	@Inject @P23RUnit
	private Event<String> initialized;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
		initialized.fire(message(NOTIFICATIONGENERATION));
	}

	/* (non-Javadoc)
	 * @see de.p23r.server.notificationgenerator.NotificationGeneratorService#generateNotification(de.p23r.server.notificationgenerator.NotificationParameter)
	 */
	@Override
	public void generateNotification(GenerationContext context) throws P23RServiceException {

		NotificationProfileHelper profileHelper = context.getProfile();

		protocol.logInfo(profileHelper, message(NOTIFICATIONGENERATION_GENERATE_NOTIFICATION));

		GenerationContext transformation = transformator.generate(context);			

		protocol.logInfo(transformation.getProfile(), message(NOTIFICATIONGENERATION_NOTIFICATION_GENERATION_FINISHED));

		dispatcher.dispatchNotification(transformation);
	}

}
