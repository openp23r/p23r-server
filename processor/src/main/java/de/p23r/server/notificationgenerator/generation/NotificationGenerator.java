package de.p23r.server.notificationgenerator.generation;

import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.xml.transform.stream.StreamSource;

import org.jdom2.Document;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.jdom2.transform.JDOMSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.switchyard.component.bean.Reference;

import de.p23r.common.existdb.P23RXmlDbConfig;
import de.p23r.common.modelandrulemanagement.NamespaceHelper;
import de.p23r.server.common.GenerationContext;
import de.p23r.server.common.NotificationProfileHelper;
import de.p23r.server.common.P23RError;
import de.p23r.server.common.TBRS;
import de.p23r.server.common.annotations.P23RUnit;
import de.p23r.server.datapool.DataPoolService;
import de.p23r.server.exceptions.P23RServiceException;
import de.p23r.server.modelandrulemanagement.GenerationStep;
import de.p23r.server.modelandrulemanagement.ModelAndRuleManagementService;
import de.p23r.server.modelandrulemanagement.NextGenerationStepParameter;
import de.p23r.server.modelandrulemanagement.NotificationRuleInformation;
import de.p23r.server.protocolpool.Protocol;
import de.p23r.server.xml.P23RProcessor;

/**
 * The Class NotificationGenerator.
 * 
 * @author sim
 */

public abstract class NotificationGenerator {

	protected enum ScriptType {
		TRANSFORMATION, SELECTION, DATS, UNKNOWN
	}

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Inject @P23RUnit
	protected Protocol protocol;

	@Inject
	private P23RProcessor p23rProcessor;

	@Inject
	private P23RXmlDbConfig p23rXmlDbConfig;

	@Inject
	@Reference
	protected DataPoolService datapool;

	@Inject
	@Reference
	protected ModelAndRuleManagementService modelAndRuleManagement;

	/**
	 * Generate.
	 *
	 * @param context the context
	 * @return the transformation result
	 * @throws P23RServiceException the p23 r service exception
	 */
	public abstract GenerationContext generate(GenerationContext context) throws P23RServiceException;

	/**
	 * Returns the updated result or null, if the current result is already the
	 * final result.
	 *
	 * @param step            the step
	 * @param result            the current result
	 * @param context            the context
	 * @return updated result or null
	 * @throws P23RServiceException the p23r service exception
	 */
	protected abstract GenerationContext generateStep(GenerationStep step, GenerationContext result) throws P23RServiceException;

	/**
	 * Creates a new result for the given notification and profile.
	 * 
	 * @param result
	 *            the old result
	 * @param nextNotification
	 *            the notification
	 * @param nextProfile
	 *            the profile
	 * @param step
	 *            the step
	 * @param pr
	 *            the document
	 * @return new result
	 */
	protected GenerationContext createResult(GenerationContext context, String nextNotification, String nextProfile, GenerationStep step, NotificationProfileHelper currentProfile) {
		if (isTraceEnabled()) {
			String inputNotification = context.getNotification();
			protocol.trace(currentProfile, step.getName(), step.getTitle(), 0, nextProfile, step.getProfileScript(), inputNotification, nextNotification,
					step.getNotificationScript());
		}

		context.setNotification(nextNotification);
		context.setProfile(new NotificationProfileHelper(nextProfile));

		return context;			
	}

	/**
	 * Validates the profile
	 * 
	 * @param nextProfile
	 *            the profile
	 * @return valid or not
	 */
	protected boolean isProfileValid(String profile, NotificationProfileHelper currentProfile) {
		String messageSchema = null;
		try {
			NotificationRuleInformation info = modelAndRuleManagement.getRuleInformationByMessageType(currentProfile.getMessageNS());
			messageSchema = info.getMessageSchema();
		} catch (P23RServiceException e) {
			log.error("getting rule info", e);
			protocol.logError(P23RError.VALIDATION_FAILURE, currentProfile, e.getLocalizedMessage());
		}

		return profile != null && messageSchema != null && p23rProcessor.validate(profile, messageSchema, currentProfile);
	}

	/**
	 * Validates the notification.
	 *
	 * @param notification the notification
	 * @param step            the step
	 * @param profile the profile
	 * @return valid or not
	 */
	protected boolean isNotificationValid(String notification, NotificationProfileHelper currentProfile) {
		if (notification == null) {
			return false;
		}

		String namespace = new NamespaceHelper().getNamespaceFromXmlContent(notification);
		String schema = getSchema(namespace, currentProfile.getNotificationRuleId());
		
		return schema != null && p23rProcessor.validate(notification, schema, currentProfile);
	}

	protected String select(GenerationContext document, String script, NotificationProfileHelper profile) throws P23RServiceException {
		GenerationContext context = new GenerationContext();
		context.setProfile(profile);
		context.getProperties().put(GenerationContext.SELECTION_SCRIPT, script);

		return datapool.selectData(context);
	}

	/**
	 * Transform.
	 * 
	 * @param generation
	 *            the generation
	 * @param script
	 *            the script
	 * @return the string
	 * @throws ActionProcessingFaultException
	 *             the action processing fault exception
	 */
	protected String transform(Document generation, String script, GenerationContext context) throws P23RServiceException {
		log.debug("notification generation: {}", generation != null ? new XMLOutputter(Format.getPrettyFormat()).outputString(generation) : "not set");
		Map<String, String> docs = new HashMap<String, String>();
		if (context != null) {
			docs.put(TBRS.NAMESPACE_NOTIFICATIONPROFILE, context.getProfile().toString());			
		}
		return p23rProcessor.transform(new StreamSource(new ByteArrayInputStream(script.getBytes(TBRS.CHARSET))), generation != null ? new JDOMSource(generation) : null, docs, context);			
	}

	protected String getSchema(String namespace, String ruleId) {
		NextGenerationStepParameter parameter = new NextGenerationStepParameter();
		parameter.setNamespace(namespace);
		parameter.setRuleId(ruleId);
		return modelAndRuleManagement.getSchemaFromNamespace(parameter);
	}
	
	private boolean isTraceEnabled() {
		return Boolean.parseBoolean(p23rXmlDbConfig.get(P23RXmlDbConfig.XQUERY_GENERAL_TRACEENABLED, "false"));
	}

}
