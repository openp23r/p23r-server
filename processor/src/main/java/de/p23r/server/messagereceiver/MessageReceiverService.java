package de.p23r.server.messagereceiver;

import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault_Exception;
import de.p23r.leitstelle.ns.p23r.imessagedeliverlocal1_0.DeliverMessage;
import de.p23r.leitstelle.ns.p23r.imessagedeliverlocal1_0.DeliverMessageResponse;
import de.p23r.server.common.GenerationContext;
import de.p23r.server.exceptions.P23RServiceException;

/**
 * The Interface MessageReceiverService. It defines the service interface of the MessageReceiver component.
 *
 * @author sim
 */
public interface MessageReceiverService {

	String deliverJsonMessage(JsonMessage message) throws P23RServiceException;
	
	/**
	 * Deliver message. This method triggers a notification generation process. It is usually bound to a soap gateway and called from external.
	 *
	 * @param message the message. An XML structure with a schema namespace depending on the rule to trigger.
	 * @return the deliver message response containing the messageId. If the messageId is predefined in the request it will be the same, otherwise
	 *         the processor will assign a new unique one. The format of the id is a UUID version 4.
	 * @throws P23RAppFault_Exception the p23rappfault exception
	 */
	DeliverMessageResponse deliverMessage(DeliverMessage message) throws P23RAppFault_Exception;
	
	/**
	 * Deliver test message. It is not bound to any gateway and is only meant for internal usage, e.g by the TestManager.
	 *
	 * @param context the context
	 * @return the messageId as string. If the messageId is predefined in the request it will be the same, otherwise
	 *         the processor will assign a new unique one. The format of the id is a UUID version 4.
	 * @throws P23RServiceException the p23r service exception
	 */
	String deliverTestMessage(GenerationContext context) throws P23RServiceException;
	
}
