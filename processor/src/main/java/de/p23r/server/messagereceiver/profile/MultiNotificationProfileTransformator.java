package de.p23r.server.messagereceiver.profile;

import java.util.List;

import de.p23r.server.common.GenerationContext;
import de.p23r.server.common.NotificationProfileHelper;
import de.p23r.server.exceptions.P23RServiceException;

/**
 * The Class NotificationProfileTransformator.
 * 
 * @author mma
 * @author sim
 */
public abstract class MultiNotificationProfileTransformator {

	/**
	 * Transform.
	 *
	 * @param transformation the transformation
	 * @param selectedData the selected data
	 * @param messageSchema the message schema
	 * @param context the context
	 * @return the list
	 * @throws P23RServiceException the p23 r service exception
	 */
	abstract List<NotificationProfileHelper> transform(String transformation, String selectedData, String messageSchema, GenerationContext context) throws P23RServiceException;

}
