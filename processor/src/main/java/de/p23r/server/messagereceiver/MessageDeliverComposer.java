package de.p23r.server.messagereceiver;

import org.jboss.resteasy.client.core.BaseClientResponse;
import org.switchyard.Exchange;
import org.switchyard.ExchangeState;
import org.switchyard.HandlerException;
import org.switchyard.Message;
import org.switchyard.component.resteasy.composer.RESTEasyBindingData;
import org.switchyard.component.resteasy.composer.RESTEasyMessageComposer;

import de.p23r.server.exceptions.P23RServiceException;

public class MessageDeliverComposer extends RESTEasyMessageComposer {

    @Override
    public Message compose(RESTEasyBindingData source, Exchange exchange) throws Exception {
        final Message message = super.compose(source, exchange);
        if (message.getContent() instanceof BaseClientResponse) {
            BaseClientResponse<?> clientResponse = (BaseClientResponse<?>) message.getContent();
            if (clientResponse.getResponseStatus() == BaseClientResponse.Status.NOT_FOUND) {
                throw new P23RServiceException("MessageReceiver", "Tenant or context not found");
            }
        } else if (source.getOperationName().equals("deliverJsonMessage") && (source.getParameters().length == 3)) {
            // Wrap the parameters
            JsonMessage deliverMessage = (JsonMessage) source.getParameters()[0];
            deliverMessage.setTenant((String) source.getParameters()[1]);
            deliverMessage.setContext((String) source.getParameters()[2]);
            message.setContent(deliverMessage);
        }
        return message;
    }
    
    @Override
    public RESTEasyBindingData decompose(Exchange exchange, RESTEasyBindingData target) throws Exception {
        Object content = exchange.getMessage().getContent();
        if (exchange.getState().equals(ExchangeState.FAULT)) {
            if (content instanceof HandlerException) {
                HandlerException he = (HandlerException) content;
                if (he.getCause() instanceof P23RServiceException) {
                    throw (Exception) he.getCause();
                }
            }
        }

        target = super.decompose(exchange, target);

        if (target.getOperationName().equals("deliverJsonMessage") && (content != null) && (content instanceof JsonMessage)) {
            // Unwrap the parameters
            target.setParameters(new Object[] { content, ((JsonMessage) content).getTenant(), ((JsonMessage) content).getContext()});
        }
        return target;
    }
    
}
