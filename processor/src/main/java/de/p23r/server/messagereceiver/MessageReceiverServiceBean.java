package de.p23r.server.messagereceiver;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.switchyard.component.bean.Reference;
import org.switchyard.component.bean.Service;

import de.p23r.common.existdb.P23RXmlDbConfig;
import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault_Exception;
import de.p23r.leitstelle.ns.p23r.imessagedeliverlocal1_0.DeliverMessage;
import de.p23r.leitstelle.ns.p23r.imessagedeliverlocal1_0.DeliverMessageResponse;
import de.p23r.server.common.GenerationContext;
import de.p23r.server.common.NotificationProfileHelper;
import de.p23r.server.common.P23RError;
import de.p23r.server.common.annotations.P23RUnit;
import de.p23r.server.exceptions.P23RInvalidParameterException;
import de.p23r.server.exceptions.P23RRuleNotActivatedException;
import de.p23r.server.exceptions.P23RRuleNotFoundException;
import de.p23r.server.exceptions.P23RServiceException;
import de.p23r.server.messagereceiver.profile.NotificationProfileConstructor;
import de.p23r.server.messagereceiver.verifier.MessageVerifier;
import de.p23r.server.modelandrulemanagement.ModelAndRuleManagementService;
import de.p23r.server.modelandrulemanagement.NotificationRuleInformation;
import de.p23r.server.notificationgenerator.NotificationGeneratorService;
import de.p23r.server.protocolpool.Protocol;
import static de.p23r.server.common.Messages.*;
import static de.p23r.server.common.P23RFaultConstants.*;

/**
 * The Class MessageReceiverServiceBean. The implementation class for the MessageReceiver component. The main task is to receive a trigger message
 * and to start the notification generation process. It correlates the incoming message to the corresponding rule, instantiates one or more profiles for
 * each notification to generate (depending on the multi notification profile generation scripts), and sends them to the NotificationGeneration component.
 *
 * @author sim
 */
@Service(MessageReceiverService.class)
public class MessageReceiverServiceBean implements MessageReceiverService {

	@Inject
	private Logger log;

	@Inject @P23RUnit
	private Protocol protocol;

	@Inject
	private NotificationProfileConstructor notificationProfileConstructor;

	@Inject
	@Reference
	private ModelAndRuleManagementService marm;

	@Inject
	@Reference
	private NotificationGeneratorService notificationGenerator;

	@Inject
	private P23RXmlDbConfig p23rXmlDbConfig;

	@Inject @P23RUnit
	private Event<String> initialized;

	@Resource
	private ManagedExecutorService mes;
	
	
	/**
	 * Initializes the message receiver service. At the end it fires an initialization complete event.
	 * @see Monitor.unitInitialized
	 */
	@PostConstruct
	public void init() {
		initialized.fire(message(MESSAGERECEIPT));
	}

	/* (non-Javadoc)
	 * @see de.p23r.server.messagereceiver.MessageReceiverService#deliverMessage(de.p23r.leitstelle.ns.p23r.imessagedeliverlocal1_0.DeliverMessage)
	 */
	@Override
	public DeliverMessageResponse deliverMessage(DeliverMessage request) throws P23RAppFault_Exception {
		String tenant = request.getTenant();

		if (tenant == null) {
			throwP23RAppFault(PARAMETER_MISSING_CODE, PARAMETER_MISSING_TEXT, "tenant");
		}

		Object msg = request.getMessage();
		if (msg == null) {
			throwP23RAppFault(PARAMETER_MISSING_CODE, PARAMETER_MISSING_TEXT, "message");
		}

		MessageVerifier messageVerifier = new MessageVerifier(msg);
		if (!messageVerifier.isValidDOM()) {
			throwP23RAppFault(PARAMETER_INVALID_CODE, PARAMETER_INVALID_TEXT, "message");
		}

		GenerationContext context = new GenerationContext();
		context.getProperties().put(GenerationContext.TENANT, tenant);
		context.getProperties().put(GenerationContext.SOURCEINTERFACE, "public");

		try {
			deliverMessage(context, messageVerifier);			
		} catch (P23RRuleNotFoundException e) {
			throwP23RAppFault(RULE_UNKNOWN_CODE, RULE_UNKNOWN_TEXT, e.getMessage());
		} catch (P23RRuleNotActivatedException e) {
			throwP23RAppFault(RULE_NOTACTIVATED_CODE, RULE_NOTACTIVATED_TEXT, e.getMessage());
		} catch (P23RServiceException e) {
			throwP23RAppFault(INTERNAL_SYSTEM_ERROR, e.getMessage());			
		}

		log.info("returning with message id {}!", messageVerifier.getMessageId());

		DeliverMessageResponse response = new DeliverMessageResponse();
		response.setMessageId(messageVerifier.getMessageId());
		return response;
	}

	/* (non-Javadoc)
	 * @see de.p23r.server.messagereceiver.MessageReceiverService#deliverTestMessage(de.p23r.leitstelle.ns.p23r.imessagedeliverlocal1_0.DeliverMessage)
	 */
	@Override
	public String deliverTestMessage(GenerationContext context) throws P23RServiceException {

		String msg = context.getProperties().get(GenerationContext.MESSAGE);
		
		MessageVerifier messageVerifier = new MessageVerifier(msg);
		if (!messageVerifier.isValidDOM()) {
			throw new P23RInvalidParameterException("MessageReceiver", "message is not a DOM tree");
		}
		
		String transactionId = context.getProperties().get("transactionid");
		messageVerifier.overwriteTransactionId(transactionId);
		
		return deliverMessage(context, messageVerifier);
	}

	
	@Override
	public String deliverJsonMessage(JsonMessage message) throws P23RServiceException {
		
		GenerationContext context = new GenerationContext();
		context.getProperties().put(GenerationContext.TENANT, message.getTenant());
		context.getProperties().put(GenerationContext.SOURCEINTERFACE, message.getContext());

		MessageVerifier verifier = new MessageVerifier(message.getMessage());
		
		return deliverMessage(context, verifier);
	}
	
	private String deliverMessage(GenerationContext context, MessageVerifier message) throws P23RServiceException {

		String originator = context.getProperties().get(GenerationContext.ORIGINATORID);
		if (originator == null) {
			log.warn("originatorId is missing");
		}

		String sourceInterface = context.getProperties().get(GenerationContext.SOURCEINTERFACE);
		if (sourceInterface == null) {
			log.warn("sourceInterface is missing");
		}

		String sourceUri = context.getProperties().get(GenerationContext.SOURCEURI);
		if (sourceUri == null) {
			log.warn("sourceUri is missing");
		}

		String namespace = message.getNamespace();
		String transactionId = message.getTransactionId();
		String messageId = message.getMessageId();

		protocol.logInfo(transactionId, messageId, message(MESSAGERECEIPT_MESSAGE_RECEIVED, new String[] { sourceInterface }));

		NotificationRuleInformation notificationRuleInformation = marm.getRuleInformationByMessageType(namespace);
		// only start if rule is activated and we are not in a test case
		if (!notificationRuleInformation.isActivated() && !"test".equals(sourceInterface)) {
			throw new P23RRuleNotActivatedException("MessageReceiver", namespace);				
		}
		
		if (!message.isSchemaValid(notificationRuleInformation.getMessageSchema())) {
			log.debug("message invalid!");
			protocol.logError(P23RError.VALIDATION_FAILURE, transactionId, messageId,
					message(MESSAGERECEIPT_INVALID_MESSAGE_STRUCTURE, new String[] { message.getValidationFailure() }));
			throw new P23RInvalidParameterException("MessageReceiver", messageId);
		}

		String tenant = context.getProperties().get(GenerationContext.TENANT);
		if (tenant == null) {
			tenant = "default";
		}
		String clientConfig = p23rXmlDbConfig.getClientConfig(tenant);
		context.setClientConfig(clientConfig);
		
		final List<NotificationProfileHelper> notificationProfileList = notificationProfileConstructor.create(context, message, notificationRuleInformation.getMessageSchema(), notificationRuleInformation);

		protocol.logInfo(transactionId, messageId,
				message(MESSAGERECEIPT_GENERATION_PROFILES, new String[] { String.valueOf(notificationProfileList.size()) }));

		if (sourceInterface != null && sourceInterface.equals("test")) {
			for (NotificationProfileHelper notificationProfile : notificationProfileList) {
				Instant start = Instant.now();
				context.setProfile(notificationProfile);
				try {
					notificationGenerator.generateNotification(context);
				} catch (P23RServiceException e) {
					log.error("generate notification", e);
				}
				log.info("generation {} took {}", notificationProfile.getNotificationId(), Duration.between(start, Instant.now()));
			}			
		} else {
			mes.submit(new Runnable() {
				@Override
				public void run() {
					CompletionService<String> completionService = new ExecutorCompletionService<>(mes);
					for (NotificationProfileHelper notificationProfile : notificationProfileList) {
						completionService.submit(new Callable<String>() {

							@Override
							public String call() throws Exception {
								Instant start = Instant.now();
								context.setProfile(notificationProfile);
								try {
									notificationGenerator.generateNotification(context);
								} catch (P23RServiceException e) {
									log.error("generate notification", e);
								}
								log.info("generation {} took {}", notificationProfile.getNotificationId(), Duration.between(start, Instant.now()));
								return notificationProfile.getNotificationId();
							}
							
						});
					}
			        try {
			            for (int i = 0; i < notificationProfileList.size(); i++) {
			                log.info("Generation [{}] has finished", completionService.take().get());
			            }
			        } catch (InterruptedException | ExecutionException e) {
			            log.error("Error finishing generation tasks", e);
			        }
				}
			});			
		}
		log.info("returning message id {}", messageId);
		return messageId;
	}

}
