package de.p23r.server.messagereceiver.profile;

import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.switchyard.component.bean.Reference;

import de.p23r.common.modelandrulemanagement.SchemaHelperFactory;
import de.p23r.common.modelandrulemanagement.TransformerHelper;
import de.p23r.leitstelle.ns.p23r.nrl.commonnrl1_1.Parameter;
import de.p23r.leitstelle.ns.p23r.nrl.notificationprofile1_1.Communication;
import de.p23r.leitstelle.ns.p23r.nrl.notificationprofile1_1.General;
import de.p23r.leitstelle.ns.p23r.nrl.notificationprofile1_1.GenerationLog;
import de.p23r.leitstelle.ns.p23r.nrl.notificationprofile1_1.NotificationProfile;
import de.p23r.leitstelle.ns.p23r.nrl.notificationprofile1_1.Signatures;
import de.p23r.leitstelle.ns.p23r.nrl.notificationprofile1_1.Signatures.SignedContent;
import de.p23r.leitstelle.ns.p23r.nrl.notificationprofile1_1.Signatures.SignedCore;
import de.p23r.leitstelle.ns.p23r.nrl.rulegroupmanifest1_1.RuleGroupManifest;
import de.p23r.leitstelle.ns.p23r.nrl.rulemanifest1_1.RuleManifest;
import de.p23r.server.common.GenerationContext;
import de.p23r.server.common.NotificationProfileHelper;
import de.p23r.server.datapool.DataPoolService;
import de.p23r.server.exceptions.P23RServiceException;
import de.p23r.server.messagereceiver.verifier.MessageVerifier;
import de.p23r.server.modelandrulemanagement.NotificationRuleInformation;

/**
 * The Class NotificationProfileConstructor.
 * 
 * @author sim
 */
public class NotificationProfileConstructor {

	/** The Constant log. */
	private final Logger log = LoggerFactory.getLogger(getClass());

	@Inject
	private MultiNotificationProfileTransformator11 transformator;
	
	@Inject
	@Reference
	private DataPoolService dataPool;

	/**
	 * create.
	 *
	 * @param context the context
	 * @param message the message
	 * @param messageSchema the message schema
	 * @param notificationRuleInformation            the message
	 * @return list of notification profiles
	 * @throws P23RServiceException the p23 r service exception
	 */
	public List<NotificationProfileHelper> create(GenerationContext context, MessageVerifier message, String messageSchema, NotificationRuleInformation notificationRuleInformation) throws P23RServiceException {
		log.debug("creating list of notification profiles...");

		NotificationProfile notificationProfile = new NotificationProfile();

		notificationProfile.setTenant(context.getProperties().get(GenerationContext.TENANT));

		General general = new General();
		notificationProfile.setGeneral(general);

		general.setNotificationId(UUID.randomUUID().toString());

		general.setMessage(message.getElement());
		general.setMessageId(message.getMessageId());
		general.setMessageNS(message.getNamespace());
		general.setTransactionId(message.getTransactionId());
		general.setMessageSubject(message.getSubject() == null ? "" : message.getSubject());

		try {
			general.setReceiveTime(DatatypeFactory.newInstance().newXMLGregorianCalendar(new GregorianCalendar()));
			general.setSubmitLatestAt(DatatypeFactory.newInstance().newXMLGregorianCalendar(new GregorianCalendar()));
		} catch (DatatypeConfigurationException e) {
			log.warn("could not set the receiving time", e);
		}

		general.setOriginatorId(context.getProperties().get(GenerationContext.ORIGINATORID));
		general.setSourceInterface(context.getProperties().get(GenerationContext.SOURCEINTERFACE));
		general.setSourceURI(context.getProperties().get(GenerationContext.SOURCEURI));

		RuleManifest ruleManifest = SchemaHelperFactory.getNotificationRuleManifestSchemaHelper().create(notificationRuleInformation.getManifest());
		general.setNotificationRuleId(ruleManifest.getId());
		general.setRuleManifest(TransformerHelper.elementFromString(notificationRuleInformation.getManifest()));
		RuleGroupManifest ruleGroupManifest = SchemaHelperFactory.getNotificationRuleGroupManifestSchemaHelper().create(
				notificationRuleInformation.getGroupManifest());
		general.setNotificationRuleGroupId(ruleGroupManifest.getId());
		general.setRuleGroupManifest(TransformerHelper.elementFromString(notificationRuleInformation.getGroupManifest()));

		general.setNotificationRulePackageControlCentre("");

		notificationProfile.setCommunication(new Communication());
		Parameter param = new Parameter();
		param.setName("dummy");

		notificationProfile.getCommunication().getCriteria().add(param);
		notificationProfile.setGenerationLog(new GenerationLog());
		notificationProfile.setSignatures(new Signatures());
		notificationProfile.getSignatures().setSignedContent(new SignedContent());
		notificationProfile.getSignatures().setSignedCore(new SignedCore());

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			org.w3c.dom.Document doc = factory.newDocumentBuilder().newDocument();
			notificationProfile.setExtension(doc.createElement("extension"));
			notificationProfile.setP23RExtension(doc.createElement("p23rExtension"));
		} catch (ParserConfigurationException e) {
			log.warn("creating empty required elements failed", e);
		}

		NotificationProfileHelper profileHelper = new NotificationProfileHelper(notificationProfile);
		if (log.isTraceEnabled()) {
			log.trace("<PROFILE> initial: {}", profileHelper.prettyPrint());
		}

		List<NotificationProfileHelper> profileList = null;

		if ((notificationRuleInformation.getMultiProfileSelection() != null) && (notificationRuleInformation.getMultiProfileTransformation() != null)) {

			context.getProperties().put(GenerationContext.SELECTION_SCRIPT, notificationRuleInformation.getMultiProfileSelection());
			context.setProfile(profileHelper);

			String selectedData = dataPool.selectData(context);
			profileList = transformator.transform(notificationRuleInformation.getMultiProfileTransformation(), selectedData, messageSchema, context);

			for (NotificationProfileHelper p : profileList) {
				p.setNotificationId(UUID.randomUUID().toString());
			}

		} else {
			profileList = Collections.singletonList(profileHelper);
		}

		return profileList;
	}

}
