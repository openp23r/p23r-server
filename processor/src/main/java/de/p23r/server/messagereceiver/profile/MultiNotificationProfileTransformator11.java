package de.p23r.server.messagereceiver.profile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.jdom2.Content;
import org.jdom2.Content.CType;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.slf4j.Logger;

import de.p23r.common.modelandrulemanagement.NamespaceHelper;
import de.p23r.server.common.GenerationContext;
import de.p23r.server.common.NotificationProfileHelper;
import de.p23r.server.common.TBRS;
import de.p23r.server.exceptions.P23RServiceException;
import de.p23r.server.xml.P23RProcessor;

/**
 * The Class NotificationProfileTransformator.
 * 
 * @author mma
 * @author sim
 */
@Named
public class MultiNotificationProfileTransformator11 extends MultiNotificationProfileTransformator {

	@Inject
	private Logger log;
	
	@Inject
	private P23RProcessor p23rProcessor;

	/* (non-Javadoc)
	 * @see de.p23r.server.messagereceiver.profile.MultiNotificationProfileTransformator#transform(java.lang.String, java.lang.String, java.lang.String, org.jdom2.Document, java.lang.String)
	 */
	public List<NotificationProfileHelper> transform(String transformation, String selectedData, String messageSchema, GenerationContext context)
			throws P23RServiceException {

		String namespace = new NamespaceHelper().getNamespaceFromXmlContent(selectedData);
		
		List<NotificationProfileHelper> profiles = new ArrayList<>();
		
		String profileList = p23rProcessor.transform(transformation, context.getProfile().toString(), Collections.singletonMap(namespace, selectedData), context);
	
		SAXBuilder builder = new SAXBuilder();
		try {
			Document doc = builder.build(new ByteArrayInputStream(profileList.getBytes(TBRS.CHARSET)));
			for (Content content : doc.getContent()) {
				if (content.getCType() != CType.Element || !p23rProcessor.validate((Element)content, messageSchema, context.getProfile())) {
					throw new P23RServiceException("MessageReceiver", "Invalid multi notification profile transformation");
				} else {
					profiles.add(new NotificationProfileHelper(new Document((Element)content.clone())));
				}
			}
		} catch (JDOMException e) {
			log.error("parsing generated profile list", e);
		} catch (IOException e) {
			log.error("reading generated profile list", e);
		}

		return profiles;
	}
	
}
