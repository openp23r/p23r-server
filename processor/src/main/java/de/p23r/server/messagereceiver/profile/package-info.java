/**
 * Provides helper classes of the message receiver for constructing and transforming all necessary profiles.
 */
package de.p23r.server.messagereceiver.profile;