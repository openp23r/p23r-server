/**
 * Provides classes of the message receiver for reading and validating received massages.
 */
package de.p23r.server.messagereceiver.verifier;