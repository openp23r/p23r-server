package de.p23r.server.messagereceiver;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import de.p23r.server.exceptions.P23RServiceException;

@Path("/")
public interface MessageReceiverResource {

	@POST
    @Path("/{tenant}/{context}")
    @Consumes("application/json")
	@Produces("text/plain")
	String deliverJsonMessage(JsonMessage message, @PathParam("tenant") String tenant, @PathParam("context") String context) throws P23RServiceException;

}
