package de.p23r.server.messagereceiver.verifier;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.UUID;

import javax.xml.XMLConstants;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.DOMBuilder;
import org.jdom2.output.DOMOutputter;
import org.jdom2.output.Format;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.switchyard.common.type.Classes;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import de.p23r.common.modelandrulemanagement.TransformerHelper;
import de.p23r.server.common.TBRS;

/**
 * The Class MessageVerifier. It checks and parses the incoming message xml
 * structure. Usually it comes as DOM Tree, but will do also if it is a string.
 * 
 * @author sim
 */
public class MessageVerifier implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6933649990678019028L;

	private static final transient Logger LOG = LoggerFactory.getLogger(MessageVerifier.class);

	private String id;

	private String namespace;

	private String transactionId;

	private String subject;

	private transient org.w3c.dom.Element original;

	private org.jdom2.Element msg;

	private static final String COMMON_1_0_SCHEMA = "xsd/Common1-0.xsd";

	private static final String COMMON_NRL_1_1_SCHEMA = "xsd/CommonNrl1-1.xsd";

	private static final String XMLDSIG_CORE_SCHEMA = "xsd/xmldsig-core-schema.xsd";

	private String validationFailure = "";

	/**
	 * Instantiates a new message verifier.
	 *
	 * @param message the message
	 */
	public MessageVerifier(Object message) {
		if (message instanceof org.w3c.dom.Element) {
			original = (org.w3c.dom.Element) message;
		} else if (message instanceof org.w3c.dom.Document) {
			original = ((org.w3c.dom.Document) message).getDocumentElement();
		} else if (message instanceof String) {
			original = TransformerHelper.elementFromString(message.toString());
		} else {
			original = null;
			return;
		}
		DOMBuilder builder = new DOMBuilder();
		msg = builder.build(original);
		
		initMessageVerifier();
	}

	/**
	 * Initializes the members of the Message Verifier for the given message id
	 * 
	 * @param messageId
	 *            the message id
	 */
	private void initMessageVerifier() {
		Namespace ns = msg.getNamespace();
		
		Element content = msg.getChild("content", ns);
		Element common = content.getChild("common", ns);
		String messageId = common.getAttributeValue("messageId");
		if (messageId == null || messageId.trim().isEmpty()) {
			id = UUID.randomUUID().toString();
			common.setAttribute("messageId", id);
		}
		
		namespace = msg.getNamespaceURI();
		
		// set message namespace
//		if (namespace != null && !namespace.isEmpty()) {
//			msg.setNamespace(Namespace.getNamespace(namespace));
//		}

		// set subject
//		Element subjectElement = msg.getChild("subject", Namespace.getNamespace(namespace));
//		if (subjectElement != null) {
//			subject = subjectElement.getTextTrim();
//		}
		subject = "";
		
		// set transaction id
		transactionId = common.getAttributeValue("transactionId", ns);
		// set random transaction id if not already set
		if (transactionId == null || transactionId.trim().isEmpty()) {
			transactionId = UUID.randomUUID().toString();
			common.setAttribute("transactionId", transactionId);
		}
	}

	/**
	 * Checks if is valid dom.
	 *
	 * @return true, if is valid dom
	 */
	public boolean isValidDOM() {
		return original != null;
	}

	/**
	 * Gets the element.
	 *
	 * @return the element
	 */
	public org.w3c.dom.Element getElement() {
		if (!isValidDOM()) {
			return null;
		}

		DOMOutputter outputter = new DOMOutputter();
		try {
			return outputter.output(msg);
		} catch (JDOMException e) {
			LOG.error("convert jdom to dom", e);
		}
		return original;
	}

	/**
	 * Checks if is schema valid.
	 *
	 * @param messageSchema the message schema
	 * @return true, if is schema valid
	 */
	public boolean isSchemaValid(String messageSchema) {
		if (!isValidDOM()) {
			LOG.error("message is not a valid DOM tree");
			return false;
		}

		StreamSource[] schemaSources = new StreamSource[4];
		try {
			schemaSources[0] = new StreamSource(Classes.getResourceAsStream(COMMON_1_0_SCHEMA));
			schemaSources[1] = new StreamSource(Classes.getResourceAsStream(COMMON_NRL_1_1_SCHEMA));
			schemaSources[2] = new StreamSource(Classes.getResourceAsStream(XMLDSIG_CORE_SCHEMA));
			schemaSources[3] = new StreamSource(new ByteArrayInputStream(messageSchema.getBytes(TBRS.CHARSET)));
		} catch (IOException e) {
			LOG.error("failed to load common schema.", e);
		}

		SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		
		Schema schema;
		try {
			schema = schemaFactory.newSchema(schemaSources);
		} catch (SAXException e) {
			LOG.error("parsing message schema", e);
			validationFailure = e.getLocalizedMessage();
			return false;
		}

		try {
			Validator validator = schema.newValidator();
			validator.setErrorHandler(createErrorHandler());
			validator.validate(new DOMSource(getElement()));
			return true;
		} catch (SAXException e) {
			LOG.warn("parsing/validating message", e);
		} catch (IOException e) {
			LOG.warn("reading/streaming message", e);
		} catch (Exception e) {
			LOG.warn("validating message", e);
		}
		return false;
	}

	/**
	 * Creates an anonymous error handler
	 * 
	 * @return error handler
	 */
	private ErrorHandler createErrorHandler() {
		return new ErrorHandler() {

			@Override
			public void warning(SAXParseException exception) throws SAXException {
				validationFailure = exception.getLocalizedMessage();
				throw exception;
			}

			@Override
			public void fatalError(SAXParseException exception) throws SAXException {
				validationFailure = exception.getLocalizedMessage();
				throw exception;
			}

			@Override
			public void error(SAXParseException exception) throws SAXException {
				validationFailure = exception.getLocalizedMessage();
				throw exception;
			}
		};
	}

	/**
	 * Gets the message id.
	 *
	 * @return the message id
	 */
	public String getMessageId() {
		return id;
	}

	/**
	 * Gets the namespace.
	 *
	 * @return the namespace
	 */
	public String getNamespace() {
		return namespace;
	}

	/**
	 * Gets the transaction id.
	 *
	 * @return the transaction id
	 */
	public String getTransactionId() {
		return transactionId;
	}

	/**
	 * Gets the subject.
	 *
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * Gets the validation failure.
	 *
	 * @return the validation failure
	 */
	public String getValidationFailure() {
		return validationFailure;
	}

	/**
	 * Overwrite transaction id.
	 *
	 * @param transactionId the transaction id
	 */
	public void overwriteTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

}
