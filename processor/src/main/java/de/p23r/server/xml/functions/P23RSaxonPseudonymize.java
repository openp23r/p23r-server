package de.p23r.server.xml.functions;


import net.sf.saxon.expr.XPathContext;
import net.sf.saxon.lib.ExtensionFunctionCall;
import net.sf.saxon.lib.ExtensionFunctionDefinition;
import net.sf.saxon.om.Sequence;
import net.sf.saxon.om.StructuredQName;
import net.sf.saxon.trans.XPathException;
import net.sf.saxon.value.SequenceType;
import net.sf.saxon.value.StringValue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.fraunhofer.fokus.adep.foundation.Bloom;
import de.fraunhofer.fokus.adep.foundation.BloomHelper;
import de.fraunhofer.fokus.adep.foundation.Constants;
import de.fraunhofer.fokus.adep.foundation.ParameterBuilder;
import de.fraunhofer.fokus.adep.foundation.ParameterBuilder.BloomParameter;

/**
 * P23R Extension for the Saxon 9.6.x
 * 
 * @author ayb
 * 
 */
public class P23RSaxonPseudonymize extends ExtensionFunctionDefinition {

	/**
	 * The logger.
	 */
	private static final transient Logger LOG = LoggerFactory.getLogger(P23RSaxonPseudonymize.class);
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see net.sf.saxon.lib.ExtensionFunctionDefinition#getArgumentTypes()
	 */
	public SequenceType[] getArgumentTypes() {
		return new SequenceType[] { SequenceType.SINGLE_STRING };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.sf.saxon.lib.ExtensionFunctionDefinition#getFunctionQName()
	 */
	public StructuredQName getFunctionQName() {
		return new StructuredQName("p23r", "http://leitstelle.p23r.de/NS/p23r/processor/functions", "pseudonymize");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.sf.saxon.lib.ExtensionFunctionDefinition#getResultType(net.sf.saxon
	 * .value.SequenceType[])
	 */
	public SequenceType getResultType(SequenceType[] arguments) {
		 return SequenceType.SINGLE_STRING;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.sf.saxon.lib.ExtensionFunctionDefinition#getMinimumNumberOfArguments
	 * ()
	 */
	public int getMinimumNumberOfArguments() {
		return 1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.sf.saxon.lib.ExtensionFunctionDefinition#getMaximumNumberOfArguments
	 * ()
	 */
	public int getMaximumNumberOfArguments() {
		return 1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.sf.saxon.lib.ExtensionFunctionDefinition#makeCallExpression()
	 */
	public ExtensionFunctionCall makeCallExpression() {
		return new ExtensionFunctionCall() {

			@Override
			public Sequence call(XPathContext arg0, Sequence[] arg1) throws XPathException {
				String value = arg1[0].iterate().next().getStringValue();
				LOG.debug("pseudomynize value: " + value);
				String pseudonymized = pseudonymize(value);
				LOG.debug("pseudomynized value: " + value);

				return new StringValue(pseudonymized);
			}
		};
	}

	private String pseudonymize(String value) {

		BloomParameter bloomFilterParameter = ParameterBuilder.newBloomParameter();
		bloomFilterParameter.setFcn0(Constants.FCN0);
		bloomFilterParameter.setFcn1(Constants.FCN1);
		bloomFilterParameter.setSecret0(Constants.HMAC_SECRET);
		bloomFilterParameter.setSecret1(Constants.HMAC_SECRET);
		bloomFilterParameter.setnGramSize(Constants.N_GRAM_SIZE);
		bloomFilterParameter.setBloomVectorLength(Constants.BIT_VECTOR_LENGTH);
		bloomFilterParameter.setNbOfHashFcn(Constants.NB_OF_HASHFCN);

		Bloom bloom = new Bloom(bloomFilterParameter);

		return BloomHelper.bloomizeAttributeBase64(bloom, BloomHelper.transform(value));
	}

}
