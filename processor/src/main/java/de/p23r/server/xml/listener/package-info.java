/**
 * Provides listeners and handlers for the xslt and xquery processing engine.
 */
package de.p23r.server.xml.listener;