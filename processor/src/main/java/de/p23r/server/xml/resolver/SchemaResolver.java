package de.p23r.server.xml.resolver;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSInput;
import org.w3c.dom.ls.LSResourceResolver;

import de.p23r.common.modelandrulemanagement.NamespaceHelper;
import de.p23r.server.common.TBRS;
import de.p23r.server.modelandrulemanagement.ModelAndRuleManagementService;

/**
 * The Class SchemaResolver.
 */
public class SchemaResolver implements LSResourceResolver {

	private Logger log = LoggerFactory.getLogger(getClass());

	private ModelAndRuleManagementService modelAndRuleManagement;

	private static Map<String, String> schemaLocations = new HashMap<String, String>();

	private Map<String, String> schemas = new HashMap<String, String>();
	
	static {
		schemaLocations.put("http://www.w3.org/2000/09/xmldsig#", "xsd/xmldsig-core-schema.xsd");
		schemaLocations.put("http://leitstelle.p23r.de/NS/p23r/nrl/Common1-0", "xsd/Common1-0.xsd");
		schemaLocations.put("http://leitstelle.p23r.de/NS/p23r/nrl/CommonNrl1-1", "xsd/CommonNrl1-1.xsd");
		schemaLocations.put("http://leitstelle.p23r.de/NS/p23r/nrl/Configuration1-0", "xsd/Configuration1-0.xsd");
		schemaLocations.put("http://leitstelle.p23r.de/NS/p23r/nrl/ModelManifest1-1", "xsd/ModelManifest1-1.xsd");
		schemaLocations.put("http://leitstelle.p23r.de/NS/p23r/nrl/ModelPackageManifest1-1", "xsd/ModelPackageManifest1-1.xsd");
		schemaLocations.put("http://leitstelle.p23r.de/NS/p23r/nrl/MultiNotificationProfileOutput1-1", "xsd/MultiNotificationProfileOutput1-1.xsd");
		schemaLocations.put("http://leitstelle.p23r.de/NS/p23r/nrl/NotificationProfile1-1", "xsd/NotificationProfile1-1.xsd");
		schemaLocations.put("http://leitstelle.p23r.de/NS/p23r/nrl/PackageList1-1", "xsd/PackageList1-1.xsd");
		schemaLocations.put("http://leitstelle.p23r.de/NS/p23r/nrl/PackageListCatalogue1-0", "xsd/PackageListCatalogue1-0.xsd");
		schemaLocations.put("http://leitstelle.p23r.de/NS/p23r/nrl/RecommendationResult1-1", "xsd/RecommendationResult1-1.xsd");
		schemaLocations.put("http://leitstelle.p23r.de/NS/p23r/nrl/RuleGroupManifest1-1", "xsd/RuleGroupManifest1-1.xsd");
		schemaLocations.put("http://leitstelle.p23r.de/NS/p23r/nrl/RulePackageManifest1-1", "xsd/RulePackageManifest1-1.xsd");
		schemaLocations.put("http://leitstelle.p23r.de/NS/p23r/nrl/RuleManifest1-1", "xsd/RuleManifest1-1.xsd");
		schemaLocations.put("http://leitstelle.p23r.de/NS/p23r/nrl/TestPackageManifest1-0", "xsd/TestPackageManifest1-0.xsd");
		schemaLocations.put("http://leitstelle.p23r.de/NS/p23r/nrl/TestSetManifest1-1", "xsd/TestSetManifest1-1.xsd");
		schemaLocations.put("http://leitstelle.p23r.de/NS/p23r/nrl/TestCaseManifest1-0", "xsd/TestCaseManifest1-0.xsd");
	}

	/**
	 * Instantiates a new schema resolver.
	 *
	 * @param modelAndRuleManagement the model and rule management
	 */
	public SchemaResolver(ModelAndRuleManagementService modelAndRuleManagement) {
		this.modelAndRuleManagement = modelAndRuleManagement;
	}

	// resolving LSResourceResolver
	/* (non-Javadoc)
	 * @see org.w3c.dom.ls.LSResourceResolver#resolveResource(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public LSInput resolveResource(String type, String namespaceURI, String publicId, String systemId, String baseURI) {
		if (log.isTraceEnabled()) {
			log.trace("resolving resource of type {}", type);
			log.trace("namespaceURI: {}", namespaceURI);
			log.trace("publicId: {}", publicId);
			log.trace("systemId: {}", systemId);
			log.trace("baseURI: {}", baseURI);
		}

		LSInput input = null;
		try {
			DOMImplementation impl = DocumentBuilderFactory.newInstance().newDocumentBuilder().getDOMImplementation();
			input = ((DOMImplementationLS) impl).createLSInput();
		} catch (ParserConfigurationException e) {
			log.error("creating LSInput return value", e);
			return null;
		}

		String reference = null;
		if (systemId != null && (systemId.startsWith("rule://") || systemId.startsWith("model://"))) {
			reference = systemId;
		}
		if (reference == null) {
			reference = namespaceURI;
		}

		if (reference != null) {
			InputStream resource = resolveInput(reference);
			if (resource == null) {
				log.warn("unable to resolve resource {}", reference);
			} else {
				input.setByteStream(resource);
				return input;
			}
		}
		return null;
	}

	private InputStream resolveInput(String reference) {
		log.debug("trying to resolve resource with reference {}", reference);

		InputStream resource = null;
		if (schemas.containsKey(reference)) {
			resource = new ByteArrayInputStream(schemas.get(reference).getBytes(TBRS.CHARSET));			
		} else if (schemaLocations.containsKey(reference)) {
			resource = Thread.currentThread().getContextClassLoader().getResourceAsStream(schemaLocations.get(reference));
		} else if (modelAndRuleManagement != null && (reference.startsWith("model://") || reference.startsWith("rule://"))) {
			resource = new ByteArrayInputStream(modelAndRuleManagement.resolveResource(reference).getBytes(TBRS.CHARSET));
		}
		return resource;
	}

	/**
	 * Adds the schema.
	 *
	 * @param schema the schema
	 */
	public void addSchema(String schema) {
		schemas.put(new NamespaceHelper().getNamespaceFromXsdContent(schema), schema);
	}
	
}
