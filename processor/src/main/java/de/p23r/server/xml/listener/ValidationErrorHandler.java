package de.p23r.server.xml.listener;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import de.p23r.server.common.NotificationProfileHelper;
import de.p23r.server.common.P23RError;
import de.p23r.server.protocolpool.Protocol;

/**
 * The Class ValidationErrorHandler.
 */
public class ValidationErrorHandler implements ErrorHandler {

	private NotificationProfileHelper profile;
	
	private Protocol protocol;
	
	/**
	 * Instantiates a new validation error handler.
	 *
	 * @param profile the profile
	 * @param protocol the protocol
	 */
	public ValidationErrorHandler(NotificationProfileHelper profile, Protocol protocol) {
		this.profile = profile;
		this.protocol = protocol;
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.ErrorHandler#warning(org.xml.sax.SAXParseException)
	 */
	@Override
	public void warning(SAXParseException exception) throws SAXException {
		String msg = exception.getLocalizedMessage() + "(line: " + exception.getLineNumber() + ", column: " + exception.getColumnNumber() + ")";
		
		if (profile == null) {
			protocol.logWarning(msg);			
		} else {
			protocol.logWarning(profile, msg);						
		}

		throw exception;
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.ErrorHandler#error(org.xml.sax.SAXParseException)
	 */
	@Override
	public void error(SAXParseException exception) throws SAXException {
		String msg = exception.getLocalizedMessage() + "(line: " + exception.getLineNumber() + ", column: " + exception.getColumnNumber() + ")";
		
		if (profile == null) {
			protocol.logError(P23RError.VALIDATION_FAILURE, msg);
		} else {
			protocol.logError(P23RError.VALIDATION_FAILURE, profile, msg);
		}
		throw exception;
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.ErrorHandler#fatalError(org.xml.sax.SAXParseException)
	 */
	@Override
	public void fatalError(SAXParseException exception) throws SAXException {
		String msg = exception.getLocalizedMessage() + "(line: " + exception.getLineNumber() + ", column: " + exception.getColumnNumber() + ")";

		if (profile == null) {
			protocol.logError(P23RError.VALIDATION_FAILURE, msg);
		} else {
			protocol.logError(P23RError.VALIDATION_FAILURE, profile, msg);
		}
		
		throw exception;
	}

}
