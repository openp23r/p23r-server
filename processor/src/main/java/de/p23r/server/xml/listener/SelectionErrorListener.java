package de.p23r.server.xml.listener;

import javax.xml.transform.ErrorListener;
import javax.xml.transform.TransformerException;

import de.p23r.server.common.NotificationProfileHelper;
import de.p23r.server.common.P23RError;
import de.p23r.server.protocolpool.Protocol;

/**
 * The listener interface for receiving selectionError events.
 * The class that is interested in processing a selectionError
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addSelectionErrorListener<code> method. When
 * the selectionError event occurs, that object's appropriate
 * method is invoked.
 *
 * @see SelectionErrorEvent
 */
public class SelectionErrorListener implements ErrorListener {

	private NotificationProfileHelper profile;
	
	private Protocol protocol;
	
	/**
	 * Instantiates a new selection error listener.
	 *
	 * @param profile the profile
	 * @param protocol the protocol
	 */
	public SelectionErrorListener(NotificationProfileHelper profile, Protocol protocol) {
		this.profile = profile;
		this.protocol = protocol;
	}
	
	/* (non-Javadoc)
	 * @see javax.xml.transform.ErrorListener#error(javax.xml.transform.TransformerException)
	 */
	@Override
	public void error(TransformerException exception) throws TransformerException {
		if (profile == null) {
			protocol.logError(P23RError.SELECTION_FAILURE, exception.getLocalizedMessage() + "\n" + exception.getLocationAsString());
		} else {
			protocol.logError(P23RError.SELECTION_FAILURE, profile, exception.getLocalizedMessage() + "\n" + exception.getLocationAsString());
		}
	}

	/* (non-Javadoc)
	 * @see javax.xml.transform.ErrorListener#fatalError(javax.xml.transform.TransformerException)
	 */
	@Override
	public void fatalError(TransformerException exception) throws TransformerException {
		if (profile == null) {
			protocol.logError(P23RError.SELECTION_FAILURE, exception.getLocalizedMessage() + "\n" + exception.getLocationAsString());
		} else {
			protocol.logError(P23RError.SELECTION_FAILURE, profile, exception.getLocalizedMessage() + "\n" + exception.getLocationAsString());			
		}
	}

	/* (non-Javadoc)
	 * @see javax.xml.transform.ErrorListener#warning(javax.xml.transform.TransformerException)
	 */
	@Override
	public void warning(TransformerException exception) throws TransformerException {
		if (profile == null) {
			protocol.logWarning(exception.getLocalizedMessage() + "\n" + exception.getLocationAsString());
		} else {
			protocol.logWarning(profile, exception.getLocalizedMessage() + "\n" + exception.getLocationAsString());			
		}
	}

}
