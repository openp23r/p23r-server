/**
 * Provides p23r extension functions for the xslt and xquery scripts
 */
package de.p23r.server.xml.functions;