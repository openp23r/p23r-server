package de.p23r.server.xml;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.transform.JDOMSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.switchyard.component.bean.Reference;
import org.xml.sax.SAXException;

import de.p23r.server.common.GenerationContext;
import de.p23r.server.common.NotificationProfileHelper;
import de.p23r.server.common.TBRS;
import de.p23r.server.common.annotations.P23RUnit;
import de.p23r.server.exceptions.P23RServiceException;
import de.p23r.server.modelandrulemanagement.ModelAndRuleManagementService;
import de.p23r.server.protocolpool.Protocol;
import de.p23r.server.xml.functions.P23RSaxonProtocolWrite;
import de.p23r.server.xml.functions.P23RSaxonPseudonymize;
import de.p23r.server.xml.functions.P23RSaxonSelectData;
import de.p23r.server.xml.listener.SelectionErrorListener;
import de.p23r.server.xml.listener.TransformationErrorListener;
import de.p23r.server.xml.listener.ValidationErrorHandler;
import de.p23r.server.xml.resolver.DocumentResolver;
import de.p23r.server.xml.resolver.SchemaResolver;
import net.sf.saxon.s9api.ItemTypeFactory;
import net.sf.saxon.s9api.Processor;
import net.sf.saxon.s9api.QName;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.XQueryCompiler;
import net.sf.saxon.s9api.XQueryEvaluator;
import net.sf.saxon.s9api.XdmNode;
import net.sf.saxon.s9api.XdmSequenceIterator;
import net.sf.saxon.s9api.XdmValue;
import net.sf.saxon.s9api.XsltCompiler;
import net.sf.saxon.s9api.XsltTransformer;

/**
 * The Class P23RProcessor.
 *
 * @author sim
 */
@ApplicationScoped
public class P23RProcessor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1976671733482680578L;

	private final transient Logger log = LoggerFactory.getLogger(getClass());

	@Inject
	private P23RSaxonSelectData selectDataFunction;

	@Inject
	private P23RSaxonProtocolWrite protocolWriteFunction;

	@Inject
	private P23RSaxonPseudonymize pseudonymizeFunction;

	@Inject @P23RUnit
	private Protocol protocol;

	@Inject
	@Reference
	private transient ModelAndRuleManagementService modelAndRuleManagement;

	private transient Processor processor = new Processor(false);

	private transient SchemaFactory schemaFactory;

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
		processor.registerExtensionFunction(selectDataFunction);
		processor.registerExtensionFunction(protocolWriteFunction);
		processor.registerExtensionFunction(pseudonymizeFunction);

		schemaFactory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
	}

	/**
	 * Query.
	 *
	 * @param selection the selection
	 * @param docs the docs
	 * @param context the context
	 * @return the string
	 * @throws P23RServiceException the p23 r service exception
	 */
	public String query(String selection, Map<String, String> docs, GenerationContext context) throws P23RServiceException {
		if (selection == null || selection.isEmpty()) {
			throw new P23RServiceException("Selector", "no selection script");			
		}
		
		try {
			XQueryCompiler xqueryCompiler = processor.newXQueryCompiler();
			xqueryCompiler.setErrorListener(new SelectionErrorListener(context.getProfile(), protocol));

			XQueryEvaluator eval = xqueryCompiler.compile(selection).load();

			if (context != null) {
				eval.setExternalVariable(new net.sf.saxon.s9api.QName(GenerationContext.class.getName()), new ItemTypeFactory(processor).getExternalObject(context));				
			}
			
			DocumentResolver resolver = new DocumentResolver(modelAndRuleManagement);
//			DocumentResolver resolver = new DocumentResolver();
			if (docs != null) {
				for (Entry<String, String> doc : docs.entrySet()) {
					resolver.addDocument(doc.getKey(), doc.getValue());
				}
			}
			eval.setURIResolver(resolver);

			XdmValue val = eval.evaluate();
			XdmSequenceIterator it = val.iterator();

			StringBuilder result = new StringBuilder("");
			while (it.hasNext()) {
				XdmNode node = (XdmNode) it.next();
				result.append(node.toString());
			}
			return result.toString();
		} catch (SaxonApiException e) {
			log.error("selection failed", e);
			throw new P23RServiceException("Selector", e.getMessage());
		}
	}

	/**
	 * Transform.
	 *
	 * @param transformation the transformation
	 * @param data the data
	 * @param docs the docs
	 * @param context the context
	 * @return the string
	 * @throws P23RServiceException the p23 r service exception
	 */
	public String transform(String transformation, String data, Map<String, String> docs, GenerationContext context) throws P23RServiceException {
		return transform(new StreamSource(new ByteArrayInputStream(transformation.getBytes(TBRS.CHARSET))), new StreamSource(
				new ByteArrayInputStream(data.getBytes(TBRS.CHARSET))), docs, context);
	}

	/**
	 * Transform.
	 *
	 * @param transformation the transformation
	 * @param data the data
	 * @param docs the docs
	 * @param context the context
	 * @return the string
	 * @throws P23RServiceException the p23 r service exception
	 */
	public String transform(String transformation, Document data, Map<String, String> docs, GenerationContext context) throws P23RServiceException {
		return transform(new StreamSource(new ByteArrayInputStream(transformation.getBytes(TBRS.CHARSET))), new JDOMSource(data), docs, context);
	}

	/**
	 * Transform.
	 *
	 * @param transformation the transformation
	 * @param data the data
	 * @param context the context
	 * @return the document
	 * @throws P23RServiceException the p23 r service exception
	 */
	public Document transform(String transformation, Document data, GenerationContext context) throws P23RServiceException {
		String result = transform(new StreamSource(new ByteArrayInputStream(transformation.getBytes(TBRS.CHARSET))), new JDOMSource(data), null, context);
		SAXBuilder builder = new SAXBuilder();
		try {
			return builder.build(new ByteArrayInputStream(result.getBytes(TBRS.CHARSET)));
		} catch (JDOMException e) {
			log.error("transform result to jdom", e);
			throw new P23RServiceException("MessageReceiver", e.getMessage());
		} catch (IOException e) {
			log.error("transform result to jdom", e);
			throw new P23RServiceException("MessageReceiver", e.getMessage());
		}
	}

	/**
	 * Transform.
	 *
	 * @param transformation the transformation
	 * @param data the data
	 * @param docs the docs
	 * @param context the context
	 * @return the string
	 * @throws P23RServiceException the p23 r service exception
	 */
	public String transform(Source transformation, Source data, Map<String, String> docs, GenerationContext context) throws P23RServiceException {
		if (transformation == null) {
			throw new P23RServiceException("Transformer", "no transformation source");
		}
		
		try {
			XsltCompiler xsltCompiler = processor.newXsltCompiler();
			DocumentResolver resolver = new DocumentResolver(modelAndRuleManagement);
//			DocumentResolver resolver = new DocumentResolver();
			if (docs != null) {
				for (Entry<String, String> entry : docs.entrySet()) {
					resolver.addDocument(entry.getKey(), entry.getValue());									
				}
			}
			xsltCompiler.setURIResolver(resolver);
			
			xsltCompiler.setErrorListener(new TransformationErrorListener(context.getProfile(), protocol));
			XsltTransformer transformer = xsltCompiler.compile(transformation).load();				

			transformer.setErrorListener(new TransformationErrorListener(context.getProfile(), protocol));
			transformer.setURIResolver(resolver);
			if (data != null) {
				transformer.setSource(data);				
			} else {
				transformer.setSource(new JDOMSource(new Document()));				
				transformer.setInitialTemplate(new QName("default"));
			}

			if (context != null) {
				transformer.setParameter(new net.sf.saxon.s9api.QName(GenerationContext.class.getName()), new ItemTypeFactory(processor).getExternalObject(context));				
			}

			ByteArrayOutputStream output = new ByteArrayOutputStream();

			transformer.setDestination(processor.newSerializer(output));
			transformer.transform();
			return output.toString(TBRS.CHARSET_NAME);
		} catch (SaxonApiException e) {
			log.error("transformation failed", e);
			throw new P23RServiceException("Transformer", e.getMessage());
		} catch (UnsupportedEncodingException e) {
			log.error(TBRS.CHARSET_NAME + " encoding is not supported", e);
			throw new P23RServiceException("Transformer", e.getMessage());
		}
	}

	/**
	 * Validate.
	 *
	 * @param xml the xml
	 * @param profile the profile
	 * @return true, if successful
	 */
	public boolean validate(String xml, NotificationProfileHelper profile) {
		return validate(xml, null, profile);
	}
	
	/**
	 * Validate.
	 *
	 * @param xml the xml
	 * @param schema the schema
	 * @param profile the profile
	 * @return true, if successful
	 */
	public boolean validate(String xml, String schema, NotificationProfileHelper profile) {
		return validate(new StreamSource(new ByteArrayInputStream(xml.getBytes(TBRS.CHARSET))), schema, profile);
	}
	
	/**
	 * Validate.
	 *
	 * @param xml the xml
	 * @param schema the schema
	 * @param profile the profile
	 * @return true, if successful
	 */
	public boolean validate(Element xml, String schema, NotificationProfileHelper profile) {
		return validate(new JDOMSource(xml), schema, profile);
	}
	
	/**
	 * Validate.
	 *
	 * @param xml the xml
	 * @param schema the schema
	 * @param profile the profile
	 * @return true, if successful
	 */
	public boolean validate(Source xml, String schema, NotificationProfileHelper profile) {
		try {
			Validator validator = schemaFactory.newSchema().newValidator();
			SchemaResolver resolver = new SchemaResolver(modelAndRuleManagement);
			if (schema != null && !schema.isEmpty()) {
				resolver.addSchema(schema);					
			}
			validator.setResourceResolver(resolver);
			validator.setErrorHandler(new ValidationErrorHandler(profile, protocol));
			validator.validate(xml);
			return true;
		} catch (SAXException e) {
			log.warn("validating xml", e);
		} catch (IOException e) {
			log.warn("validating xml", e);
		}
		return false;				
	}
	
}
