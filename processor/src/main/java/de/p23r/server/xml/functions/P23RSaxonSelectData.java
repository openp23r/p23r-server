package de.p23r.server.xml.functions;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.Collections;

import javax.inject.Inject;
import javax.xml.transform.stream.StreamSource;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import javax.xml.ws.soap.SOAPBinding;

import net.sf.saxon.expr.XPathContext;
import net.sf.saxon.lib.ExtensionFunctionCall;
import net.sf.saxon.lib.ExtensionFunctionDefinition;
import net.sf.saxon.om.AxisInfo;
import net.sf.saxon.om.DocumentInfo;
import net.sf.saxon.om.Item;
import net.sf.saxon.om.NodeInfo;
import net.sf.saxon.om.Sequence;
import net.sf.saxon.om.SequenceTool;
import net.sf.saxon.om.StructuredQName;
import net.sf.saxon.pattern.NodeKindTest;
import net.sf.saxon.query.QueryResult;
import net.sf.saxon.trans.XPathException;
import net.sf.saxon.value.EmptySequence;
import net.sf.saxon.value.ObjectValue;
import net.sf.saxon.value.SequenceType;

import org.apache.commons.io.IOUtils;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.apache.http.impl.client.DefaultHttpClient;
import org.jboss.resteasy.client.ClientExecutor;
import org.jboss.resteasy.client.ProxyFactory;
import org.jboss.resteasy.client.core.executors.ApacheHttpClient4Executor;
import org.jboss.resteasy.plugins.providers.RegisterBuiltin;
import org.jboss.resteasy.spi.ResteasyProviderFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.switchyard.component.bean.Reference;
import org.w3c.dom.Document;
import org.xmldb.api.base.ResourceIterator;
import org.xmldb.api.base.ResourceSet;
import org.xmldb.api.base.XMLDBException;

import de.p23r.common.existdb.P23RXmlDbConfig;
import de.p23r.common.existdb.P23RXmlDbResource;
import de.p23r.common.modelandrulemanagement.TransformerHelper;
import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault_Exception;
import de.p23r.leitstelle.ns.p23r.isourcedataquery1_0.ISourceDataQuery;
import de.p23r.leitstelle.ns.p23r.isourcedataquery1_0.ISourceDataQueryService;
import de.p23r.server.common.GenerationContext;
import de.p23r.server.common.TBRS;
import de.p23r.server.datapool.SourceDataQueryRequest;
import de.p23r.server.datapool.SourceDataQueryResource;
import de.p23r.server.datapool.SourceDataQueryResponse;
import de.p23r.server.notificationtransporter.NotificationTransferResource;
import de.p23r.server.testmanager.TestManagerService;

/**
 * P23R Extension for the Saxon 9.6.x
 * 
 * @author ayb
 * 
 */
public class P23RSaxonSelectData extends ExtensionFunctionDefinition {

	/**
	 * The logger.
	 */
	private static final transient Logger LOG = LoggerFactory.getLogger(P23RSaxonSelectData.class);

	/**
	 * The Constant XQUERY_NAMESPACE_URL.
	 */
	public static final MessageFormat XQUERY_NAMESPACE_URL = new MessageFormat(
			"/configuration/clients/client[@tenant=\"{0}\"]/connector[@namespace=\"{1}\"]/endpoint/address/text()");
	
	/**
	 * The Constant XQUERY_NAMESPACE_TYPE.
	 */
	public static final MessageFormat XQUERY_NAMESPACE_TYPE = new MessageFormat(
			"/configuration/clients/client[@tenant=\"{0}\"]/connector[@namespace=\"{1}\"]/endpoint/@type/string()");
	
	/**
	 * The Constant XQUERY_NAMESPACE_SERVICE.
	 */
	public static final MessageFormat XQUERY_NAMESPACE_SERVICE = new MessageFormat(
			"/configuration/clients/client[@tenant=\"{0}\"]/connector[@namespace=\"{1}\"]/endpoint/service/text()");

	/**
	 * The Constant XQUERY_NAMESPACE_REQUESTTIMEOUT.
	 */
	public static final MessageFormat XQUERY_NAMESPACE_REQUESTTIMEOUT = new MessageFormat(
			"/configuration/clients/client[@tenant=\"{0}\"]/connector[@namespace=\"{1}\"]/endpoint/@requestTimeout/string()");

	@Inject
	private P23RXmlDbConfig p23rXmlDbConfig;

	@Inject
	@Reference
	private TestManagerService testManager;

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.sf.saxon.lib.ExtensionFunctionDefinition#getArgumentTypes()
	 */
	public SequenceType[] getArgumentTypes() {
		return new SequenceType[] { SequenceType.SINGLE_STRING, SequenceType.SINGLE_STRING, SequenceType.SINGLE_NODE };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.sf.saxon.lib.ExtensionFunctionDefinition#getFunctionQName()
	 */
	public StructuredQName getFunctionQName() {
		return new StructuredQName("p23r", "http://leitstelle.p23r.de/NS/p23r/processor/functions", "selectSourceData");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.sf.saxon.lib.ExtensionFunctionDefinition#getResultType(net.sf.saxon
	 * .value.SequenceType[])
	 */
	public SequenceType getResultType(SequenceType[] arguments) {
		return SequenceType.NODE_SEQUENCE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.sf.saxon.lib.ExtensionFunctionDefinition#getMinimumNumberOfArguments
	 * ()
	 */
	public int getMinimumNumberOfArguments() {
		return 2;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.sf.saxon.lib.ExtensionFunctionDefinition#getMaximumNumberOfArguments
	 * ()
	 */
	public int getMaximumNumberOfArguments() {
		return 3;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.sf.saxon.lib.ExtensionFunctionDefinition#makeCallExpression()
	 */
	public ExtensionFunctionCall makeCallExpression() {
		return new ExtensionFunctionCall() {

			@Override
            @SuppressWarnings("unchecked")
			public Sequence call(XPathContext arg0, Sequence[] arg1) throws XPathException {
				
				ObjectValue<GenerationContext> objValue = (ObjectValue<GenerationContext>) arg0.getController().getParameter(StructuredQName.fromClarkName(GenerationContext.class.getName()));
				GenerationContext context = objValue != null ? objValue.getObject() : null;
				
				String namespace = arg1[0].iterate().next().getStringValue();
				String selection = arg1[1].iterate().next().getStringValue();
				String xmlfragment = "";
				if (arg1.length == 3) {
					Item item = arg1[2].iterate().next();
					if (item != null) {
						xmlfragment = QueryResult.serialize((NodeInfo) item);
					}
				}
				LOG.debug("namespace: " + namespace);
				LOG.debug("selection: " + selection);
				LOG.debug("xmlfragment: " + xmlfragment);

				InputStream data = selectDataForNamespace(context, namespace, selection, xmlfragment);

				Sequence result = null;
				if (data == null) {
					LOG.warn("Extension Function will return an empty result!");
					result = EmptySequence.getInstance();
				} else {
					try {
						String tmp = "<fragment>" + IOUtils.toString(data, TBRS.CHARSET) + "</fragment>";
						DocumentInfo doc = arg0.getConfiguration().buildDocument(new StreamSource(new ByteArrayInputStream(tmp.getBytes(TBRS.CHARSET))));
						NodeInfo top = doc.iterateAxis(AxisInfo.CHILD, NodeKindTest.ELEMENT).next(); //skip <fragment> ...
						result = SequenceTool.toLazySequence(top.iterateAxis(AxisInfo.CHILD, NodeKindTest.ELEMENT));
					} catch (IOException e) {
						LOG.error("reading data from input stream", e);
						result = EmptySequence.getInstance();						
					}
				}

				return result;
			}
		};
	}

	/**
	 * Selects the data for the given namespace
	 * 
	 * @param tenant
	 *            the tenant
	 * @param namespace
	 *            the namespace
	 * @param selection
	 *            the selection
	 * @param xmlfragment
	 *            the xml fragment
	 * @param testpackage
	 *            the test package
	 * @param testset
	 *            the test set
	 * @param testcase
	 *            the test case
	 * @return data input stream
	 */
	private InputStream selectDataForNamespace(GenerationContext context, String namespace, String selection, String xmlfragment) {
		if (context.getTestpackage() != null) {

			context.getProperties().put(GenerationContext.SELECTION_SCRIPT, selection);
			context.getProperties().put(GenerationContext.SELECTION_NAMESPACE, namespace);
			context.getProperties().put(GenerationContext.SELECTION_PARAMETER, xmlfragment);
			
			String data = testManager.getPreselectedTestData(context);
			return new ByteArrayInputStream(data.getBytes(TBRS.CHARSET));
			
		} else {
			String path = p23rXmlDbConfig.get(XQUERY_NAMESPACE_URL.format(new Object[] { context.getClientConfig(), namespace }), "");
			LOG.debug("path: {}", path);
			if (path != null && !path.isEmpty()) {
				if (namespace.equals(TBRS.NAMESPACE_CONFIGURATION)) {
					return selectDataForConfigurationNamespace(xmlfragment, selection);
				} else {
					String type = p23rXmlDbConfig.get(XQUERY_NAMESPACE_TYPE.format(new Object[] { context.getClientConfig(), namespace }), "");
					if ("soap".equals(type)) {
						return selectDataForAnyNamespaceFromSOAP(path, context, namespace, selection, xmlfragment);						
					} else if ("rest".equals(type)) {
						return selectDataForAnyNamespaceFromREST(path, context, namespace, selection, xmlfragment);						
					}
				}
			}
		}
		return null;
	}

	/**
	 * Selects the data for the configuration namespace
	 * 
	 * @param xmlfragment
	 *            the xml fragment
	 * @param selection
	 *            the selection
	 * @return data input stream
	 */
	private InputStream selectDataForConfigurationNamespace(String xmlfragment, String selection) {
		P23RXmlDbResource configurations = p23rXmlDbConfig.getResource("/server/configurations.xml", false);
		Document elem = (Document) TransformerHelper.cleanNamespaces(xmlfragment);
		ResourceSet result = configurations.query(selection, null, Collections.singletonMap("p23rparameters", elem.getDocumentElement()));

		StringBuilder data = new StringBuilder("");
		try {
			if (result.getSize() > 0) {
				ResourceIterator it = result.getIterator();
				while (it.hasMoreResources()) {
					data.append((String) it.nextResource().getContent());
				}
			}
		} catch (XMLDBException e) {
			LOG.error("reading result failed", e);
		}

		LOG.debug("preselection result is {}", data.toString());
		return new ByteArrayInputStream(data.toString().getBytes(TBRS.CHARSET));
	}

	/**
	 * Selects the data for any namespace except configuration
	 * 
	 * @param path
	 *            the path
	 * @param tenant
	 *            the tenant
	 * @param namespace
	 *            the namespace
	 * @param selection
	 *            the selection
	 * @param xmlfragment
	 *            the xml fragment
	 * @return data input stream
	 */
	private InputStream selectDataForAnyNamespaceFromSOAP(String path, GenerationContext context, String namespace, String selection, String xmlfragment) {
		try {
			URL url = new URL(path + "?wsdl");
			String serviceName = p23rXmlDbConfig.get(XQUERY_NAMESPACE_SERVICE.format(new Object[] { context.getClientConfig(), namespace }), "");
			String requestTimeout = p23rXmlDbConfig.get(XQUERY_NAMESPACE_REQUESTTIMEOUT.format(new Object[] { context.getClientConfig(), namespace }), "");
			
			javax.xml.namespace.QName name = new javax.xml.namespace.QName(ISourceDataQueryService.SERVICE.getNamespaceURI(), serviceName);

			Service service = ISourceDataQueryService.create(url, name);
			ISourceDataQuery port = service.getPort(ISourceDataQuery.class);
			
			BindingProvider binding = (BindingProvider) port;
			((SOAPBinding) binding.getBinding()).setMTOMEnabled(true);
			if (!requestTimeout.isEmpty()) {
				Client client = ClientProxy.getClient(port);
				HTTPConduit conduit = (HTTPConduit) client.getConduit();
				HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();
				httpClientPolicy.setReceiveTimeout(Long.parseLong(requestTimeout));
				conduit.setClient(httpClientPolicy);
			}
			// TODO validUntil and timestamp for caching
			return port.getPreselectedData(selection, xmlfragment, namespace).getData().getInputStream();
		} catch (MalformedURLException e) {
			LOG.error(e.toString(), e);
		} catch (P23RAppFault_Exception e) {
			LOG.error(e.toString(), e);
		} catch (IOException e) {
			LOG.error("Failed to get preselected data input stream.", e);
		}
		return null;
	}

	private InputStream selectDataForAnyNamespaceFromREST(String path, GenerationContext context, String namespace, String selection, String xmlfragment) {
        DefaultHttpClient httpClient = new DefaultHttpClient();
        ClientExecutor clientExecutor = new ApacheHttpClient4Executor(httpClient);
        RegisterBuiltin.register(ResteasyProviderFactory.getInstance());
        SourceDataQueryResource source = ProxyFactory.create(SourceDataQueryResource.class, path, clientExecutor);

        SourceDataQueryRequest request = new SourceDataQueryRequest();
        request.setNamespace(namespace);
        request.setParameters(xmlfragment);
        request.setSelection(selection);
        request.setParametersFormat("application/xml");
        SourceDataQueryResponse response = source.selectData(request, "default");
        return IOUtils.toInputStream(response.getData());
	}
	
}
