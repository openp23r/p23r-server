package de.p23r.server.xml.resolver;

import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.Map;

import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamSource;

import net.sf.saxon.trans.NonDelegatingURIResolver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.p23r.server.common.TBRS;
import de.p23r.server.modelandrulemanagement.ModelAndRuleManagementService;

/**
 * The Class DocumentResolver.
 */
public class DocumentResolver implements NonDelegatingURIResolver {

	private Logger log = LoggerFactory.getLogger(getClass());

	private ModelAndRuleManagementService modelAndRuleManagement;

	private Map<String, String> documents = new HashMap<String, String>();

	/**
	 * Instantiates a new document resolver.
	 *
	 * @param modelAndRuleManagement the model and rule management
	 */
	public DocumentResolver(ModelAndRuleManagementService modelAndRuleManagement) {
		this.modelAndRuleManagement = modelAndRuleManagement;
	}

	/**
	 * Sets the resource mapping.
	 *
	 * @param namespace the namespace
	 * @param document the document
	 */
	public void addDocument(String namespace, String document) {
		documents.put(namespace, document);
	}

	/* (non-Javadoc)
	 * @see javax.xml.transform.URIResolver#resolve(java.lang.String, java.lang.String)
	 */
	@Override
	public Source resolve(String href, String base) throws TransformerException {
		log.trace("href: {}", href);
		log.trace("base: {}", base);

		Source src = null;

		if (href != null) {
			log.debug("trying to resolve resource with reference {}", href);
			src = resolveInput(href);
			if (src == null) {
				log.warn("unable to resolve resource {}", href);
			}
		}

		return src;
	}

	private Source resolveInput(String reference) {
		Source resource = null;
		if (documents.containsKey(reference)) {
			resource = new StreamSource(new ByteArrayInputStream(documents.get(reference).getBytes(TBRS.CHARSET)));
		} else if (modelAndRuleManagement != null && (reference.startsWith("model://") || reference.startsWith("rule://"))) {
			String content = modelAndRuleManagement.resolveResource(reference);
			if (content != null) {
				resource = new StreamSource(new ByteArrayInputStream(content.getBytes(TBRS.CHARSET)));				
			}
		}
		return resource;
	}

}
