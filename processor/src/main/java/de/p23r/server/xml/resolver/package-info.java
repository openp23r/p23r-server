/**
 * Provides resolver for the p23r processor engine.
 */
package de.p23r.server.xml.resolver;