package de.p23r.server.xml.functions;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import net.sf.saxon.expr.XPathContext;
import net.sf.saxon.lib.ExtensionFunctionCall;
import net.sf.saxon.lib.ExtensionFunctionDefinition;
import net.sf.saxon.om.Sequence;
import net.sf.saxon.om.StructuredQName;
import net.sf.saxon.trans.XPathException;
import net.sf.saxon.value.EmptySequence;
import net.sf.saxon.value.ObjectValue;
import net.sf.saxon.value.SequenceType;

import org.slf4j.Logger;

import de.p23r.leitstelle.ns.p23r.commonprotocol1_0.Level;
import de.p23r.server.common.GenerationContext;
import de.p23r.server.common.NotificationProfileHelper;
import de.p23r.server.common.P23RError;
import de.p23r.server.common.annotations.P23RUnit;
import de.p23r.server.protocolpool.Protocol;

/**
 * P23R Extension for the Saxon 9.6.x
 * 
 * @author ayb
 * 
 */
@Named
public class P23RSaxonProtocolWrite extends ExtensionFunctionDefinition {

    /**
     * The logger.
     */
    @Inject
    private transient Logger log;

    @Inject @P23RUnit("Regel")
    private Protocol protocol;

    /**
     * Inits the.
     */
    @PostConstruct
    public void init() {

    }

    /*
     * (non-Javadoc)
     * @see net.sf.saxon.lib.ExtensionFunctionDefinition#getArgumentTypes()
     */
    public SequenceType[] getArgumentTypes() {
        return new SequenceType[] { SequenceType.SINGLE_STRING, SequenceType.SINGLE_STRING, SequenceType.OPTIONAL_STRING};
    }

    /*
     * (non-Javadoc)
     * @see net.sf.saxon.lib.ExtensionFunctionDefinition#getFunctionQName()
     */
    public StructuredQName getFunctionQName() {
        return new StructuredQName("p23r", "http://leitstelle.p23r.de/NS/p23r/processor/functions", "protocolWrite");
    }

    /*
     * (non-Javadoc)
     * @see
     * net.sf.saxon.lib.ExtensionFunctionDefinition#getResultType(net.sf.saxon.value.SequenceType[])
     */
    public SequenceType getResultType(SequenceType[] arguments) {
        return SequenceType.EMPTY_SEQUENCE;
    }

    /*
     * (non-Javadoc)
     * @see net.sf.saxon.lib.ExtensionFunctionDefinition#getMinimumNumberOfArguments()
     */
    public int getMinimumNumberOfArguments() {
        return 2;
    }

    /*
     * (non-Javadoc)
     * @see net.sf.saxon.lib.ExtensionFunctionDefinition#getMaximumNumberOfArguments()
     */
    public int getMaximumNumberOfArguments() {
        return 3;
    }

    /*
     * (non-Javadoc)
     * @see net.sf.saxon.lib.ExtensionFunctionDefinition#makeCallExpression()
     */
    public ExtensionFunctionCall makeCallExpression() {
        return new ExtensionFunctionCall() {

			@Override
            @SuppressWarnings("unchecked")
            public Sequence call(XPathContext arg0, Sequence[] arg1) throws XPathException {
                if (arg1.length >= 2) {

    				ObjectValue<GenerationContext> objValue = (ObjectValue<GenerationContext>)arg0.getController().getParameter(StructuredQName.fromClarkName(GenerationContext.class.getName()));
    				GenerationContext context = objValue != null ? objValue.getObject() : null;
    				
                    String type = arg1[0].iterate().next().getStringValue();
                    log.debug("type: {}", type);
                    String message = arg1[1].iterate().next().getStringValue();
                    log.debug("message: {}", message);
                    String errorCode = "";
                    if (arg1.length >= 3) {
                        errorCode = arg1[2].iterate().next().getStringValue();
                        log.debug("errorCode: {}", errorCode);
                    }

                    if (context != null) {
                        protocolWrite(context.getProfile(), type, message, errorCode);
                    } else {
                        protocolWrite(type, message, errorCode);
                    }

                    return EmptySequence.getInstance();
                } else {
                    throw new XPathException(
                        "wrong count of input parameters. profile, type and message are needed!");
                }
            }
        };
    }

    private void protocolWrite(String type, String message, String ecode) {
        try {
            Level enumType = Level.fromValue(type);
            P23RError errorCode = null;
            // its better to set the error code here instead in each relevant case block
            // because the methods complexity grows for each "if" by one
            if (ecode != null && !ecode .isEmpty()) {
                errorCode = P23RError.valueOf(ecode);
            }

            switch (enumType) {
            case ERROR:
                protocol.logError(errorCode, message);
                break;
            case INFO:
                protocol.logInfo(message);
                break;
            case WARNING:
                protocol.logWarning(message);
                break;
            case TEST_SUCCESS:
                protocol.testSuccess(message);
                break;
            case TEST_FAIL:
                protocol.testFail(message);
                break;
            case SECURITY:
                protocol.logError(errorCode, message);
                break;
            case FATAL:
                protocol.logError(errorCode, message);
                break;
            default:
                break;
            }
        } catch (IllegalArgumentException e) {
            log.warn("unknown protocol type '" + type + "', allowed is 'info', 'error', 'warning', "
                    + "'test.success', 'test.fail', 'security' or 'fatal'", e);
        }

    }

    private void protocolWrite(NotificationProfileHelper profile, String type, String message, String ecode) {

        try {
            Level enumType = Level.fromValue(type);
            P23RError errorCode = null;
            // its better to set the error code here instead in each relevant case block
            // because the methods complexity grows for each "if" by one
            if (ecode != null && !ecode.isEmpty()) {
                errorCode = P23RError.valueOf(ecode);
            }

            switch (enumType) {
            case ERROR:
                protocol.logError(errorCode, profile, message);
                break;
            case INFO:
                protocol.logInfo(profile, message);
                break;
            case WARNING:
                protocol.logWarning(profile, message);
                break;
            case TEST_SUCCESS:
                protocol.testSuccess(profile, message);
                break;
            case TEST_FAIL:
                protocol.testFail(profile, message);
                break;
            case SECURITY:
                protocol.logError(errorCode, message);
                break;
            case FATAL:
                protocol.logError(errorCode, message);
                break;
            default:
                break;
            }
        } catch (IllegalArgumentException e) {
            log.warn("unknown protocol type '" + type + "', allowed is 'info', 'error', 'warning', "
                    + "'test.success', 'test.fail', 'security' or 'fatal'", e);
        }

    }
}
