/**
 * Provides the main xml manipulation engine (xslt and xquery) class for the p23r processor.
 */
package de.p23r.server.xml;