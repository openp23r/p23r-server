package de.p23r.server.xml.listener;

import javax.xml.transform.ErrorListener;
import javax.xml.transform.TransformerException;

import de.p23r.server.common.NotificationProfileHelper;
import de.p23r.server.common.P23RError;
import de.p23r.server.protocolpool.Protocol;

/**
 * The listener interface for receiving transformationError events.
 * The class that is interested in processing a transformationError
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addTransformationErrorListener<code> method. When
 * the transformationError event occurs, that object's appropriate
 * method is invoked.
 *
 * @see TransformationErrorEvent
 */
public class TransformationErrorListener implements ErrorListener {

	private NotificationProfileHelper profile;
	
	private String transactionId;
	
	private Protocol protocol;
	
	/**
	 * Instantiates a new transformation error listener.
	 *
	 * @param profile the profile
	 * @param protocol the protocol
	 */
	public TransformationErrorListener(NotificationProfileHelper profile, Protocol protocol) {
		this.profile = profile;
		this.protocol = protocol;
	}
	
	/**
	 * Instantiates a new transformation error listener.
	 *
	 * @param transactionId the transaction id
	 * @param protocol the protocol
	 */
	public TransformationErrorListener(String transactionId, Protocol protocol) {
		this.transactionId = transactionId;
		this.protocol = protocol;
	}
	
	/* (non-Javadoc)
	 * @see javax.xml.transform.ErrorListener#error(javax.xml.transform.TransformerException)
	 */
	@Override
	public void error(TransformerException exception) throws TransformerException {
		if (profile == null) {
			protocol.logError(P23RError.TRANSFORMATION_FAILURE, transactionId, null, exception.getLocalizedMessage() + "\n" + exception.getLocationAsString());
		} else {
			protocol.logError(P23RError.TRANSFORMATION_FAILURE, profile, exception.getLocalizedMessage() + "\n" + exception.getLocationAsString());
		}
	}

	/* (non-Javadoc)
	 * @see javax.xml.transform.ErrorListener#fatalError(javax.xml.transform.TransformerException)
	 */
	@Override
	public void fatalError(TransformerException exception) throws TransformerException {
		if (profile == null) {
			protocol.logError(P23RError.TRANSFORMATION_FAILURE, transactionId, null, exception.getLocalizedMessage() + "\n" + exception.getLocationAsString());
		} else {
			protocol.logError(P23RError.TRANSFORMATION_FAILURE, profile, exception.getLocalizedMessage() + "\n" + exception.getLocationAsString());			
		}
	}

	/* (non-Javadoc)
	 * @see javax.xml.transform.ErrorListener#warning(javax.xml.transform.TransformerException)
	 */
	@Override
	public void warning(TransformerException exception) throws TransformerException {
		if (profile == null) {
			protocol.logWarning(transactionId, null, exception.getLocalizedMessage() + "\n" + exception.getLocationAsString());
		} else {
			protocol.logWarning(profile, exception.getLocalizedMessage() + "\n" + exception.getLocationAsString());			
		}
	}

}
