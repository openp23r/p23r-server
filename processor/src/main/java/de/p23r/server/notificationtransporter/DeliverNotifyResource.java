package de.p23r.server.notificationtransporter;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

@Path("/")
public interface DeliverNotifyResource {

	@POST
    @Path("/{tenant}/{transmissionId}")
    @Consumes("application/json")
	void deliverNotify(JsonSentProtocol protocol, @PathParam("tenant") String tenant, @PathParam("transmissionId") String transmissionId);

}	
