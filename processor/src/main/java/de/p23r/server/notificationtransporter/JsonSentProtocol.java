package de.p23r.server.notificationtransporter;

import java.util.Date;
import java.util.List;

public class JsonSentProtocol {
	
	private List<String> informationText;
	
	private boolean transmissionSuccessful;
	
	private Date transmissionTime;
	
	private String resultContentFormat;
	
	private String resultContent;

	public List<String> getInformationText() {
		return informationText;
	}

	public void setInformationText(List<String> informationText) {
		this.informationText = informationText;
	}

	public boolean isTransmissionSuccessful() {
		return transmissionSuccessful;
	}

	public void setTransmissionSuccessful(boolean transmissionSuccessful) {
		this.transmissionSuccessful = transmissionSuccessful;
	}

	public Date getTransmissionTime() {
		return transmissionTime;
	}

	public void setTransmissionTime(Date transmissionTime) {
		this.transmissionTime = transmissionTime;
	}

	public String getResultContent() {
		return resultContent;
	}

	public void setResultContent(String resultContent) {
		this.resultContent = resultContent;
	}

	public String getResultContentFormat() {
		return resultContentFormat;
	}

	public void setResultContentFormat(String resultContentFormat) {
		this.resultContentFormat = resultContentFormat;
	}

}
