package de.p23r.server.notificationtransporter.registry;

import static de.p23r.server.common.Messages.NOTIFICATIONTRANSPORT_CALLING_CONNECTOR;
import static de.p23r.server.common.Messages.NOTIFICATIONTRANSPORT_CONNECTING_CONNECTOR_FAILED;
import static de.p23r.server.common.Messages.NOTIFICATIONTRANSPORT_NOTIFICATION_SENDING_FAILED;
import static de.p23r.server.common.Messages.NOTIFICATIONTRANSPORT_NOTIFICATION_SENT_SUCCESSFULLY;
import static de.p23r.server.common.Messages.message;

import java.io.ByteArrayOutputStream;
import java.util.UUID;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;

import org.apache.http.impl.client.DefaultHttpClient;
import org.jboss.resteasy.client.ClientExecutor;
import org.jboss.resteasy.client.ProxyFactory;
import org.jboss.resteasy.client.core.executors.ApacheHttpClient4Executor;
import org.jboss.resteasy.plugins.providers.RegisterBuiltin;
import org.jboss.resteasy.spi.ResteasyProviderFactory;
import org.jdom2.Element;
import org.jdom2.output.XMLOutputter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.p23r.common.modelandrulemanagement.SchemaHelperFactory;
import de.p23r.leitstelle.ns.p23r.inotificationtransfer1_2.types.Channel;
import de.p23r.leitstelle.ns.p23r.inotificationtransfer1_2.types.Payload;
import de.p23r.leitstelle.ns.p23r.nrl.rulemanifest1_1.RuleManifest;
import de.p23r.server.common.NotificationProfileHelper;
import de.p23r.server.common.P23RError;
import de.p23r.server.notificationtransporter.JsonSentProtocol;
import de.p23r.server.notificationtransporter.JsonTransmission;
import de.p23r.server.notificationtransporter.NotificationTransferResource;
import de.p23r.server.protocolpool.Protocol;

/**
 * The Class CommunicationAdapter.
 * 
 * @author sim
 */
public class CommunicationConnectorREST extends CommunicationConnector {

	private final Logger log = LoggerFactory.getLogger(getClass());
	
    /**
     * Instantiates a new communication adapter.
     * 
     * @param connectorId the connector id
     * @param channel the channel
     * @param service the service
     * @param address the address
     */
    public CommunicationConnectorREST(String connectorId, Element channel, String type, String service, String address) {
    	super(connectorId, channel, type, service, address);
    }

	public boolean sendNotification(Channel c, Payload payload, NotificationProfileHelper profileHelper, Protocol protocol) {
		log.info("trying to send to connector at {}/{}/", address, profileHelper.getTenant());
		NotificationTransferResource notificationTransfer = initService();
		if (notificationTransfer == null) {
			// connecting failed
			protocol.logError(P23RError.GENERAL, profileHelper, message(NOTIFICATIONTRANSPORT_CONNECTING_CONNECTOR_FAILED));
			return false;
		}
		JsonTransmission transmission = new JsonTransmission();
		transmission.setChannel(c);
		transmission.setPayload(convert(payload));
		transmission.setPayloadFormat("application/xml");
		transmission.setTransactionId(profileHelper.getTransactionId());
		transmission.setMessageId(profileHelper.getMessageId());
		transmission.setNotificationId(profileHelper.getNotificationId());
		transmission.setRuleId(profileHelper.getNotificationRuleId());

		RuleManifest manifest = SchemaHelperFactory.getNotificationRuleManifestSchemaHelper().create(new XMLOutputter().outputString(profileHelper.getRuleManifest()));
		transmission.setRuleName(manifest.getName());

		String transmissionId = UUID.randomUUID().toString();

		JsonSentProtocol prot = notificationTransfer.sendJsonNotification(transmission, profileHelper.getTenant(), transmissionId);
		log.debug("notification sent to communication connector {}", connectorId);
		protocol.logInfo(profileHelper, message(NOTIFICATIONTRANSPORT_CALLING_CONNECTOR, new String[] { connectorId }));
		
		// check protocol
		if (prot.isTransmissionSuccessful()) {
			log.debug("notification successfully sent by the communication connector {} at {}", connectorId, prot.getTransmissionTime().toString());
			protocol.logInfo(
					profileHelper,
					message(NOTIFICATIONTRANSPORT_NOTIFICATION_SENT_SUCCESSFULLY, new String[] { connectorId, prot.getTransmissionTime().toString() }));
			return true;
		} else {
			// sending failed -> try another channel
			log.debug("sending notification by the communication connector {} failed, reason: {}", connectorId, prot.getInformationText());
			protocol.logError(P23RError.COMMUNICATION_FAILED, profileHelper, message(NOTIFICATIONTRANSPORT_NOTIFICATION_SENDING_FAILED, new String[] { connectorId, prot.getInformationText().get(0) }));
		}

		return false;
	}
		
	private NotificationTransferResource initService() {
        DefaultHttpClient httpClient = new DefaultHttpClient();
        
        ClientExecutor clientExecutor = new ApacheHttpClient4Executor(httpClient);

        RegisterBuiltin.register(ResteasyProviderFactory.getInstance());
        
        return ProxyFactory.create(NotificationTransferResource.class, address, clientExecutor);
	}

	private String convert(Payload payload) {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		try {
			JAXBContext ctx = JAXBContext.newInstance(Payload.class);
			Marshaller m = ctx.createMarshaller();
			m.marshal(new JAXBElement<Payload>(new QName("http://leitstelle.p23r.de/ns/p23r/inotificationtransfer1_2/types", "Payload"), Payload.class, payload), output);
		} catch (JAXBException e) {
			log.error("marshalling payload", e);
		}
		
		return output.toString();
	}
	
}
