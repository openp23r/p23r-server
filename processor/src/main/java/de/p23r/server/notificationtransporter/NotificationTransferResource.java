package de.p23r.server.notificationtransporter;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

@Path("/")
public interface NotificationTransferResource {

	@POST
    @Path("/{tenant}/{transmissionId}")
    @Consumes("application/json")
	@Produces("application/json")
	JsonSentProtocol sendJsonNotification(JsonTransmission transmission, @PathParam("tenant") String tenant, @PathParam("transmissionId") String transmissionId);

}
