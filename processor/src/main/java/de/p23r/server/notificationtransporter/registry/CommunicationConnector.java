package de.p23r.server.notificationtransporter.registry;

import org.jdom2.Element;

import de.p23r.leitstelle.ns.p23r.inotificationtransfer1_2.types.Channel;
import de.p23r.leitstelle.ns.p23r.inotificationtransfer1_2.types.Payload;
import de.p23r.server.common.NotificationProfileHelper;
import de.p23r.server.protocolpool.Protocol;

public abstract class CommunicationConnector implements Comparable<CommunicationConnector> {

	protected Element channel;
	
    /**
     * The connector id.
     */
	protected String connectorId;

    /** The service. */
	protected String service;

    /** The address. */
	protected String address;

	protected String type;

    public abstract boolean sendNotification(Channel c, Payload payload, NotificationProfileHelper profileHelper, Protocol protocol);

	public static CommunicationConnector create(String connectorId, Element channel, String type, String service, String address) {
		if ("soap".equals(type)) {
			return new CommunicationConnectorSOAP(connectorId, channel, type, service, address);
		} else if ("rest".equals(type)) {
			return new CommunicationConnectorREST(connectorId, channel, type, service, address);
		}
		return null;
	}
	
	protected CommunicationConnector(String connectorId, Element channel, String type, String service, String address) {
		this.connectorId = connectorId;
		this.channel = channel;
		this.type = type;
		this.service = service;
		this.address = address;
	}
	
    /*
     * (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(CommunicationConnector o) {
        Element ochannel = o.getChannel();
        Integer opriority = Integer.valueOf(ochannel.getAttributeValue("priority"));
        Integer priority = Integer.valueOf(channel.getAttributeValue("priority"));
        if (priority == null) {
            priority = 0;
        }
        if (opriority == null) {
            opriority = 0;
        }
        return priority.compareTo(opriority);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof CommunicationConnector) {
            Element ochannel = ((CommunicationConnector) obj).getChannel();
            Integer opriority = Integer.valueOf(ochannel.getAttributeValue("priority"));
            Integer priority = Integer.valueOf(channel.getAttributeValue("priority"));
            if (priority == null) {
                priority = 0;
            }
            if (opriority == null) {
                opriority = 0;
            }
            return priority.equals(opriority);
        }
        return false;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        Integer priority = Integer.valueOf(channel.getAttributeValue("priority"));
        return priority.hashCode();
    }

    public Element getChannel() {
    	return channel;
    }
    
    /**
     * Gets the connector id.
     * 
     * @return the connector id
     */
    public String getConnectorId() {
        return connectorId;
    }

	public String getType() {
		return type;
	}

    /**
     * Gets the service.
     * 
     * @return the service
     */
    public String getService() {
        return service;
    }

    /**
     * Gets the address.
     * 
     * @return the address
     */
    public String getAddress() {
        return address;
    }

}
