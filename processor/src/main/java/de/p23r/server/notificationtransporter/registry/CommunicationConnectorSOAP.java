package de.p23r.server.notificationtransporter.registry;

import static de.p23r.server.common.Messages.NOTIFICATIONTRANSPORT_CALLING_CONNECTOR;
import static de.p23r.server.common.Messages.NOTIFICATIONTRANSPORT_CONNECTING_CONNECTOR_FAILED;
import static de.p23r.server.common.Messages.NOTIFICATIONTRANSPORT_NOTIFICATION_SENDING_FAILED;
import static de.p23r.server.common.Messages.NOTIFICATIONTRANSPORT_NOTIFICATION_SENT_SUCCESSFULLY;
import static de.p23r.server.common.Messages.message;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.UUID;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.soap.SOAPBinding;

import org.jdom2.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault_Exception;
import de.p23r.leitstelle.ns.p23r.inotificationtransfer1_2.INotificationTransfer;
import de.p23r.leitstelle.ns.p23r.inotificationtransfer1_2.INotificationTransferService;
import de.p23r.leitstelle.ns.p23r.inotificationtransfer1_2.types.Channel;
import de.p23r.leitstelle.ns.p23r.inotificationtransfer1_2.types.Payload;
import de.p23r.leitstelle.ns.p23r.inotificationtransfer1_2.types.SendProtocol;
import de.p23r.server.common.NotificationProfileHelper;
import de.p23r.server.common.P23RError;
import de.p23r.server.protocolpool.Protocol;

/**
 * The Class CommunicationAdapter.
 * 
 * @author sim
 */
public class CommunicationConnectorSOAP extends CommunicationConnector {

	private final Logger log = LoggerFactory.getLogger(getClass());
	
    /**
     * Instantiates a new communication adapter.
     * 
     * @param connectorId the connector id
     * @param channel the channel
     * @param service the service
     * @param address the address
     */
    public CommunicationConnectorSOAP(String connectorId, Element channel, String type, String service, String address) {
    	super(connectorId, channel, type, service, address);
    }

	public boolean sendNotification(Channel c, Payload payload, NotificationProfileHelper profileHelper, Protocol protocol) {
		INotificationTransfer notificationTransfer = initService();
		if (notificationTransfer == null) {
			// connecting failed
			protocol.logError(P23RError.GENERAL, profileHelper, message(NOTIFICATIONTRANSPORT_CONNECTING_CONNECTOR_FAILED));
			return false;
		}

		String transmissionId = UUID.randomUUID().toString();

		// try to send notification
		try {
			// send notification
			SendProtocol prot = notificationTransfer.sendNotification(payload, c, profileHelper.getNotificationRuleId(), profileHelper.getRuleManifest().getAttributeValue("name"), profileHelper.getNotificationId(), transmissionId);
			log.debug("notification sent to communication connector {}", connectorId);
			protocol.logInfo(profileHelper, message(NOTIFICATIONTRANSPORT_CALLING_CONNECTOR, new String[] { connectorId }));

			// check protocol
			if (prot.isTransmissionSuccessful()) {
				log.debug("notification successfully sent by the communication connector {} at {}", connectorId, prot.getTransmissionTime().toString());
				protocol.logInfo(
						profileHelper,
						message(NOTIFICATIONTRANSPORT_NOTIFICATION_SENT_SUCCESSFULLY, new String[] { connectorId, prot.getTransmissionTime().toString() }));
				return true;
			} else {
				// sending failed -> try another channel
				log.debug("sending notification by the communication connector {} failed, reason: {}", connectorId, prot.getInformationText());
				protocol.logError(P23RError.COMMUNICATION_FAILED, profileHelper, message(NOTIFICATIONTRANSPORT_NOTIFICATION_SENDING_FAILED, new String[] { connectorId, prot.getInformationText() }));
			}
		} catch (P23RAppFault_Exception e) {
			// communication failed
			log.error(e.getMessage(), e);
			protocol.logError(P23RError.COMMUNICATION_FAILED, profileHelper, e.getMessage());
		}

		return false;
	}
	
	private INotificationTransfer initService() {

		URL wsdlUrl = null;
		try {
			wsdlUrl = new URL(address + "?wsdl");
		} catch (MalformedURLException e) {
			log.error("URL creation", e);
			return null;
		}

		QName serviceName = new QName(INotificationTransferService.SERVICE.getNamespaceURI(), service);
		try {
			javax.xml.ws.Service service = INotificationTransferService.create(wsdlUrl, serviceName);
			INotificationTransfer port = service.getPort(INotificationTransfer.class);
			BindingProvider provider = (BindingProvider) port;
			((SOAPBinding) provider.getBinding()).setMTOMEnabled(true);
			return port;
		} catch (WebServiceException e) {
			log.error("connecting communication connector", e);
		}
		return null;
	}
	
}
