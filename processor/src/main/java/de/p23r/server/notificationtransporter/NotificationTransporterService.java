package de.p23r.server.notificationtransporter;

import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault_Exception;
import de.p23r.leitstelle.ns.p23r.idelivernotify1_1.SentNotify;
import de.p23r.leitstelle.ns.p23r.idelivernotify1_1.SentNotifyResponse;
import de.p23r.server.common.GenerationContext;

/**
 * The Interface NotificationTransporterService.
 *
 * @author sim
 */
public interface NotificationTransporterService {

	/**
	 * Send notification.
	 *
	 * @param context the context
	 */
	void sendNotification(GenerationContext context);
	
	/**
	 * Sent notify.
	 *
	 * @param request the request
	 * @return the sent notify response
	 * @throws P23RAppFault_Exception the p23r app fault exception
	 */
	SentNotifyResponse sentNotify(SentNotify request) throws P23RAppFault_Exception;
	
	/**
	 * Deliver notify.
	 *
	 * @param protocol the protocol
	 */
	void deliverNotify(JsonSentProtocol protocol);
	
}
