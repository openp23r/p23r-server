/**
 * Provides classes for connector registry.
 */
package de.p23r.server.notificationtransporter.registry;