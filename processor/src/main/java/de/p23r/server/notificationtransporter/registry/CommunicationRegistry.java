package de.p23r.server.notificationtransporter.registry;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.jdom2.Element;
import org.jdom2.Namespace;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;

import de.p23r.common.existdb.P23RXmlDbConfig;
import de.p23r.server.common.TBRS;

/**
 * The Class CommunicationRegistry.
 * 
 * @author sim
 */
@ApplicationScoped
public class CommunicationRegistry implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1444369322933390785L;

	private final transient Logger log = LoggerFactory.getLogger(getClass());

	@Inject
	private P23RXmlDbConfig p23rXmlDbConfig;

	/**
	 * Gets the communication adapters.
	 * 
	 * @param receiver
	 *            the receiver
	 * @return the communication adapters
	 */
	public List<CommunicationConnector> getCommunicationConnectors(Element receiver) {
		List<CommunicationConnector> communicationConnectors = new ArrayList<CommunicationConnector>();

		List<Element> channels = receiver.getChildren("channels", Namespace.getNamespace(TBRS.NAMESPACE_NOTIFICATIONPROFILE));

		for (Element channel : channels) {
			List<String> connectors = null;
			String channelId = channel.getAttributeValue("id");
			String channelType = channel.getAttributeValue("type");
			if (channelId != null && !channelId.isEmpty()) {
				connectors = p23rXmlDbConfig.getAll("/configuration/communicationregistry/connector[@id=\"" + channelId + "\"]");
			} else if (channelType != null) {
				connectors = p23rXmlDbConfig.getAll("/configuration/communicationregistry/connector[@type=\"" + channelType + "\"]");
			} else {
				connectors = p23rXmlDbConfig.getAll("/configuration/communicationregistry/connector");
			}
			if (!connectors.isEmpty()) {
				XPathFactory xpathfactory = XPathFactory.newInstance();
				try {
					XPathExpression exprtype = xpathfactory.newXPath().compile("string(/connector/endpoint/@type)");
					XPathExpression exprservice = xpathfactory.newXPath().compile("/connector/endpoint/service/text()");
					XPathExpression expraddress = xpathfactory.newXPath().compile("/connector/endpoint/address/text()");
					XPathExpression exprid = xpathfactory.newXPath().compile("string(/connector/@id)");
					for (String connector : connectors) {
						String type = exprtype.evaluate(new InputSource(new ByteArrayInputStream(connector.getBytes(Charset.forName("UTF-8")))));
						String service = exprservice.evaluate(new InputSource(new ByteArrayInputStream(connector.getBytes(Charset.forName("UTF-8")))));
						String address = expraddress.evaluate(new InputSource(new ByteArrayInputStream(connector.getBytes(Charset.forName("UTF-8")))));
						String id = exprid.evaluate(new InputSource(new ByteArrayInputStream(connector.getBytes(Charset.forName("UTF-8")))));
						communicationConnectors.add(CommunicationConnector.create(id, channel, type, service, address));
					}
				} catch (XPathExpressionException e) {
					log.error("reading connector registry", e);
				}

			} else if ("system.intern".equals(channelType)) {
				communicationConnectors.add(CommunicationConnector.create("0", channel, "java", "p23r", "MessageReceiverInternal"));
			}

		}

		Collections.sort(communicationConnectors);
		return communicationConnectors;
	}

}
