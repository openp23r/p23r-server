package de.p23r.server.notificationtransporter;

import java.util.List;
import java.util.UUID;

import javax.activation.DataHandler;
import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.mail.util.ByteArrayDataSource;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.transform.JDOMSource;
import org.slf4j.Logger;
import org.switchyard.component.bean.Reference;
import org.switchyard.component.bean.Service;
import org.w3._2000._09.xmldsig.SignatureType;

import de.p23r.common.modelandrulemanagement.TransformerHelper;
import de.p23r.leitstelle.ns.p23r.common1_0.Notification;
import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault_Exception;
import de.p23r.leitstelle.ns.p23r.idelivernotify1_1.SentNotify;
import de.p23r.leitstelle.ns.p23r.idelivernotify1_1.SentNotifyResponse;
import de.p23r.leitstelle.ns.p23r.imessagedeliverlocal1_0.DeliverMessage;
import de.p23r.leitstelle.ns.p23r.inotificationtransfer1_2.types.Channel;
import de.p23r.leitstelle.ns.p23r.inotificationtransfer1_2.types.Payload;
import de.p23r.server.common.GenerationContext;
import de.p23r.server.common.NotificationProfileHelper;
import de.p23r.server.common.P23RError;
import de.p23r.server.common.TBRS;
import de.p23r.server.common.annotations.P23RUnit;
import de.p23r.server.messagereceiver.MessageReceiverService;
import de.p23r.server.modelandrulemanagement.ChannelInformationParameter;
import de.p23r.server.modelandrulemanagement.ModelAndRuleManagementService;
import de.p23r.server.notificationtransporter.registry.CommunicationConnector;
import de.p23r.server.notificationtransporter.registry.CommunicationRegistry;
import de.p23r.server.protocolpool.Protocol;
import static de.p23r.server.common.Messages.*;

/**
 * The Class NotificationTransporterServiceBean.
 *
 * @author sim
 */
@Service(NotificationTransporterService.class)
public class NotificationTransporterServiceBean implements NotificationTransporterService {

	private static final String COMPONENT_NAME = message(NOTIFICATIONTRANSPORT);

	@Inject
	private Logger log;

	@Inject
	@P23RUnit
	private Protocol protocol;

	@Inject
	@Reference
	private ModelAndRuleManagementService marm;

	@Inject
	@Reference
	private MessageReceiverService messageReceiver;

	@Inject
	private CommunicationRegistry communicationRegistry;

	@Inject
	@P23RUnit
	private Event<String> initialized;

	private static final Namespace NAMESPACE_PROFILE = Namespace.getNamespace(TBRS.NAMESPACE_NOTIFICATIONPROFILE);

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
		initialized.fire(COMPONENT_NAME);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.p23r.server.notificationtransporter.NotificationTransporterService#
	 * sendNotification(de.p23r.server.notificationgenerator.
	 * NotificationParameter)
	 */
	@Override
	public void sendNotification(GenerationContext context) {
		NotificationProfileHelper profileHelper = context.getProfile();
		String notification = context.getNotification();

		List<Element> receivers = profileHelper.getReceivers();
		if (receivers.isEmpty()) {
			Element receiver = new Element("receivers", NAMESPACE_PROFILE);
			Element channel = new Element("channels", NAMESPACE_PROFILE);

			channel.setAttribute("id", "0");
			channel.setAttribute("priority", "1");
			channel.setAttribute("type", "fallback");
			receiver.addContent(channel);
			receiver.setAttribute("receiverId", "fallback");
			receiver.setAttribute("receiverName", "fallback");
			receivers.add(receiver);
		}

		for (Element receiver : receivers) {

			List<CommunicationConnector> connectors = communicationRegistry.getCommunicationConnectors(receiver);
			if (connectors.isEmpty()) {
				log.debug("no connector(s) for receiver {} found", receiver.getAttributeValue("receiverName"));
				protocol.logInfo(profileHelper, message(NOTIFICATIONTRANSPORT_NO_CONNECTOR_FOUND, new String[] { receiver.getAttributeValue("receiverName") }));
			} else {
				sendToConnectors(connectors, profileHelper, receiver, notification);
			}
		}
	}

	private void sendToConnectors(List<CommunicationConnector> connectors, NotificationProfileHelper profileHelper, Element receiver, String notification) {
		for (CommunicationConnector connector : connectors) {
			Element channel = connector.getChannel();
			if ("system.intern".equals(channel.getAttributeValue("type"))) {
				// internal
				DeliverMessage deliverMessage = new DeliverMessage();
				deliverMessage.setTenant(profileHelper.getTenant());
				deliverMessage.setMessage(TransformerHelper.elementFromString(notification));

				try {
					messageReceiver.deliverMessage(deliverMessage);
					protocol.logInfo(profileHelper, message(NOTIFICATIONTRANSPORT_NOTIFICATION_SENT_INTERNALLY_SUCCESSFULL));
				} catch (P23RAppFault_Exception e) {
					protocol.logError(P23RError.COMMUNICATION_FAILED, profileHelper, message(NOTIFICATIONTRANSPORT_NOTIFICATION_SENDING_FAILED));
				}
				// P23RContext.setContextProperty(invocation.getMessage().getContext(),
				// ORIGINATORID, profileHelper.getOriginatorId());
				// P23RContext.setContextProperty(invocation.getMessage().getContext(),
				// SOURCEINTERFACE, "internal");
				// P23RContext.setContextProperty(invocation.getMessage().getContext(),
				// SOURCEURI, profileHelper.getSourceURI());

			} else { // external
				// create channel
				Channel c = createChannel(channel, receiver, profileHelper);

				// create payload
				Payload payload = createPayload(notification, profileHelper);

				if (connector.sendNotification(c, payload, profileHelper, protocol)) {
					// the receiver got the notification, so no more channels to
					// try!
					break;
				}
				// else try another connector
			}
		}
	}

	/**
	 * Creates the channel for the receiver and channel element.
	 * 
	 * @param channel
	 *            the channel element
	 * @param receiver
	 *            the receiver
	 * @param profileHelper
	 *            the profile helper
	 * @return channel
	 */
	private Channel createChannel(Element channel, Element receiver, NotificationProfileHelper profileHelper) {
		Channel c = new de.p23r.leitstelle.ns.p23r.inotificationtransfer1_2.types.ObjectFactory().createChannel();
		c.setType(channel.getAttributeValue("type"));

		// set channel informations
		ChannelInformationParameter channelInformation = new ChannelInformationParameter();
		channelInformation.setChannelName(channel.getAttributeValue("name"));
		channelInformation.setReceiverGroup(receiver.getAttributeValue("receiverGroup"));
		channelInformation.setRuleId(profileHelper.getNotificationRuleId());

		// set representation script
		String presentation = marm.getPresentationTransformation(channelInformation);
		c.setRepresentationScript(presentation);

		// set timeout if available
		if (channel.getAttributeValue("timeoutAt") != null) {
			try {
				String tmp = channel.getAttributeValue("timeoutAt");
				log.info("Extracted timeoutAt value is {}", tmp);
				XMLGregorianCalendar timeoutAt = DatatypeFactory.newInstance().newXMLGregorianCalendar(channel.getAttributeValue("timeoutAt"));
				log.info("converted timeoutAt value is {}", timeoutAt.toString());
				c.setTimeoutAt(timeoutAt);
			} catch (DatatypeConfigurationException e) {
				log.error("setting timeoutAt", e);
			}
		}

		// add parameters
		for (Element parameter : channel.getChildren("parameters", NAMESPACE_PROFILE)) {
			de.p23r.leitstelle.ns.p23r.common1_0.NameValuePair param = new de.p23r.leitstelle.ns.p23r.common1_0.NameValuePair();
			param.setName(parameter.getAttributeValue("name"));
			param.setValue(parameter.getText());
			c.getParameter().add(param);
		}

		return c;
	}

	/**
	 * Creates the payload for the given notification.
	 * 
	 * @param notification
	 *            the notification
	 * @param profileHelper
	 *            the profile helper
	 * @return payload
	 */
	private Payload createPayload(String notification, NotificationProfileHelper profileHelper) {
		Payload payload = new Payload();
		payload.setNotification(new Notification());
		DataHandler handler = new DataHandler(new ByteArrayDataSource(notification.getBytes(TBRS.CHARSET), "application/xml"));
		payload.getNotification().setContent(handler);

		if (profileHelper.getContentSignature() != null) {
			Element signature = profileHelper.getContentSignature();
			payload.setSignedContent(unmarshallType(signature, SignatureType.class));
		}
		if (profileHelper.getCoreSignature() != null) {
			Element signature = profileHelper.getCoreSignature();
			payload.setSignedCore(unmarshallType(signature, SignatureType.class));
		}
		return payload;
	}

	private <T> T unmarshallType(Element elem, Class<T> clazz) {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			JAXBElement<T> jaxbElem = unmarshaller.unmarshal(new JDOMSource(elem), clazz);
			return jaxbElem.getValue();
		} catch (JAXBException e) {
			log.warn("Could not unmarshal type", e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.p23r.server.notificationtransporter.NotificationTransporterService#
	 * sentNotify(de.p23r.leitstelle.ns.p23r.idelivernotify1_0.SentNotify)
	 */
	@Override
	public SentNotifyResponse sentNotify(SentNotify request) throws P23RAppFault_Exception {
		return new SentNotifyResponse();
	}

	@Override
	public void deliverNotify(JsonSentProtocol protocol) {

	}

}
