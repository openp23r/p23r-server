/**
 * Package for the notification dispatcher service implementation. It consists of one interface class describing the service interface and
 * a bean class providing the implementation.
 */
package de.p23r.server.notificationdispatcher;