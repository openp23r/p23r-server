package de.p23r.server.notificationdispatcher;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.activation.DataHandler;
import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.mail.util.ByteArrayDataSource;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebServiceException;

import org.apache.commons.io.IOUtils;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.DOMBuilder;
import org.jdom2.output.DOMOutputter;
import org.jdom2.transform.JDOMResult;
import org.slf4j.Logger;
import org.switchyard.component.bean.Reference;
import org.w3._2000._09.xmldsig.SignatureType;

import de.p23r.common.existdb.P23RXmlDbConfig;
import de.p23r.common.modelandrulemanagement.P23RSignature;
import de.p23r.common.modelandrulemanagement.TransformerHelper;
import de.p23r.leitstelle.ns.p23r.common1_0.Notification;
import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault_Exception;
import de.p23r.leitstelle.ns.p23r.iapprovalnotify1_0.IApprovalNotify;
import de.p23r.leitstelle.ns.p23r.iapprovalnotify1_0.IApprovalNotifyService;
import de.p23r.leitstelle.ns.p23r.inotificationapprove1_0.ApproveNotification;
import de.p23r.leitstelle.ns.p23r.inotificationapprove1_0.ApproveNotificationResponse;
import de.p23r.leitstelle.ns.p23r.inotificationapprove1_0.GetMessage;
import de.p23r.leitstelle.ns.p23r.inotificationapprove1_0.GetMessageResponse;
import de.p23r.leitstelle.ns.p23r.inotificationapprove1_0.GetNotification;
import de.p23r.leitstelle.ns.p23r.inotificationapprove1_0.GetNotificationResponse;
import de.p23r.leitstelle.ns.p23r.inotificationapprove1_0.types.Approval;
import de.p23r.server.common.GenerationContext;
import de.p23r.server.common.NotificationProfileHelper;
import de.p23r.server.common.P23RError;
import de.p23r.server.common.SimpleTransformer;
import de.p23r.server.common.TBRS;
import de.p23r.server.common.annotations.P23RUnit;
import de.p23r.server.exceptions.P23RServiceException;
import de.p23r.server.modelandrulemanagement.ModelAndRuleManagementService;
import de.p23r.server.modelandrulemanagement.persistence.PackageListsRepository;
import de.p23r.server.modelandrulemanagement.persistence.RulePackageRepository;
import de.p23r.server.notificationdispatcher.persistence.XmlDbNotificationPool;
import de.p23r.server.notificationtransporter.NotificationTransporterService;
import de.p23r.server.protocolpool.Protocol;
import de.p23r.server.security.P23RKeyStore;
import de.p23r.server.security.P23RKeyStore.KeyStoreType;
import de.p23r.server.testmanager.TestManagerService;
import static de.p23r.server.common.Messages.*;

/**
 * The Class NotificationDispatcherServiceBean.
 *
 * @author sim
 */
@org.switchyard.component.bean.Service(NotificationDispatcherService.class)
public class NotificationDispatcherServiceBean implements NotificationDispatcherService {

	private static final String COMPONENT_NAME = message(NOTIFICATIONDISPATCH);

	private static final Namespace NAMESPACE_PROFILE = Namespace.getNamespace(TBRS.NAMESPACE_NOTIFICATIONPROFILE);

	@Inject
	private Logger log;

	@Inject
	@P23RUnit
	private Protocol protocol;

	@Inject
	private XmlDbNotificationPool notificationPool;

	@Inject
	private P23RXmlDbConfig p23rConfig;

//	@Inject
//	private P23RTsl p23rTsl;
//
	@Inject
	private PackageListsRepository packagelistRepository;

	@Inject
	private RulePackageRepository rulepackageRepository;

	@Inject
	@Reference
	private NotificationTransporterService transporter;

	@Inject
	@Reference
	private ModelAndRuleManagementService modelAndRuleManagement;

	@Inject
	@Reference
	private TestManagerService testManager;

	@Inject
	@P23RUnit
	private Event<String> initialized;

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
		initialized.fire(COMPONENT_NAME);
	}

	/* (non-Javadoc)
	 * @see de.p23r.server.notificationdispatcher.NotificationDispatcherService#dispatchNotification(java.lang.String)
	 */
	@Override
	public void dispatchNotification(GenerationContext context) {

		if (context.getNotification() == null) {
			protocol.logError(P23RError.PARAMETER_MISSING, message(PARAMETER_MISSING, new String[] { "notification" }));
			return;
		}

		NotificationProfileHelper profileHelper = context.getProfile();
		if (profileHelper == null) {
			protocol.logError(P23RError.PARAMETER_MISSING, message(PARAMETER_MISSING, new String[] { "notificationProfile" }));
			return;
		}

		protocol.logInfo(profileHelper, message(NOTIFICATIONDISPATCH_DISPATCHING_NOTIFICATION));

		String notificationId = profileHelper.getNotificationId();

		notificationPool.storeNotification(context.getNotification(), context);

		protocol.logInfo(profileHelper, message(NOTIFICATIONDISPATCH_NOTIFICATION_STORED));

		if (context.isTest()) {
			testManager.notifyTestApproval(context);
		} else if (p23rConfig.get(P23RXmlDbConfig.XQUERY_GENERAL_AUTOAPPROVAL, "false").equals("true")) {
			protocol.logInfo(profileHelper, message(NOTIFICATIONDISPATCH_NOTIFICATION_AUTOAPPROVED));
			transporter.sendNotification(context);
			protocol.logInfo(profileHelper, message(NOTIFICATIONDISPATCH_NOTIFICATION_SUCCESSFULLY_AUTOAPPROVED));		
		} else {
			// request approval
			IApprovalNotify notify = initApprovalNotify(context.getClientConfig());
			if (notify != null) {
				String notificationRuleId = profileHelper.getNotificationRuleId();
				try {
					notify.notifyApproval(notificationId, notificationRuleId);
				} catch (P23RAppFault_Exception e) {
					log.error("approval notify", e);
					protocol.logError(P23RError.COMMUNICATION_FAILED, profileHelper, e.getMessage() + " | " + e.getFaultInfo().getDescription());
					return;
				}

				protocol.logInfo(profileHelper, message(NOTIFICATIONDISPATCH_NOTIFYING_CLIENT_SUCCESSFULL));
			}
		}
	}

	/* (non-Javadoc)
	 * @see de.p23r.server.notificationdispatcher.NotificationDispatcherService#getNotification(de.p23r.leitstelle.ns.p23r.inotificationapprove1_0.GetNotification)
	 */
	@Override
	public GetNotificationResponse getNotification(GetNotification request) throws P23RAppFault_Exception {
		GenerationContext context = notificationPool.getGenerationContext(request.getNotificationId());
		if (context == null) {
			throw new P23RAppFault_Exception(message(INVALID_PARAMETER_VALUE, new String[] { "notificationId", request.getNotificationId() }));
		}
		NotificationProfileHelper profileHelper = context.getProfile();
		String notification = notificationPool.getNotification(request.getNotificationId());
		Element coreSignature = signCore(notification, profileHelper);
		if (coreSignature != null) {
			profileHelper.setCoreSignature(coreSignature);
			notificationPool.updateProfile(context);
		}

		if (log.isTraceEnabled()) {
			log.trace("<CURRENT NOTIFICATION> fetching notification: {}", notification);
			log.trace("<CURRENT PROFILE> fetching notification: {}", profileHelper.prettyPrint());
		}

		protocol.logInfo(profileHelper, message(NOTIFICATIONDISPATCH_NOTIFICATION_GET_SUCCESSFULL));
		GetNotificationResponse response = new GetNotificationResponse();
		response.setNotification(new Notification());
//		StreamSource source = new StreamSource(new ByteArrayInputStream(notification.getBytes(TBRS.CHARSET)));
		DataHandler handler = new DataHandler(new ByteArrayDataSource(notification.getBytes(TBRS.CHARSET), "application/xml"));
		response.getNotification().setContent(handler);

		return response;
	}

	/* (non-Javadoc)
	 * @see de.p23r.server.notificationdispatcher.NotificationDispatcherService#getMessage(de.p23r.leitstelle.ns.p23r.inotificationapprove1_0.GetMessage)
	 */
	@Override
	public GetMessageResponse getMessage(GetMessage request) throws P23RAppFault_Exception {
		GenerationContext context = notificationPool.getGenerationContext(request.getNotificationId());
		NotificationProfileHelper profileHelper = context.getProfile();
		
		Element message = profileHelper.getMessage();
		message.setNamespace(Namespace.getNamespace(profileHelper.getMessageNS()));

		GetMessageResponse response = new GetMessageResponse();
		try {
			response.setMessage(new DOMOutputter().output(message));
			protocol.logInfo(profileHelper, message(NOTIFICATIONDISPATCH_MESSAGE_GET_SUCCESSFULL));
		} catch (JDOMException e) {
			log.error("converting message from jdom2 to w3c dom", e);
		}
		return response;
	}

	/* (non-Javadoc)
	 * @see de.p23r.server.notificationdispatcher.NotificationDispatcherService#approveNotification(de.p23r.leitstelle.ns.p23r.inotificationapprove1_0.ApproveNotification)
	 */
	@Override
	public ApproveNotificationResponse approveNotification(ApproveNotification request) throws P23RAppFault_Exception {

		String notificationId = request.getNotificationId();
		Approval approval = request.getApproval();
		String notification = null;
		try {
			InputStream stream = request.getNotification().getContent().getInputStream();
			notification = IOUtils.toString(stream, TBRS.CHARSET);
		} catch (IOException e) {
			log.error("reading approved notification input stream", e);
		}

		GenerationContext context = notificationPool.getGenerationContext(notificationId);
		NotificationProfileHelper profileHelper = context.getProfile();
		
		notificationPool.storeModifiedNotification(notification, profileHelper);

		Element notificationApproval = new Element("approvals", NAMESPACE_PROFILE);
		if (approval.getName() != null) {
			notificationApproval.setAttribute("by", approval.getName());
		}
		notificationApproval.setAttribute("clientId", "");
		notificationApproval.setAttribute("clientURI", "");
		if (approval.getNote() != null) {
			Element note = new Element("notes", NAMESPACE_PROFILE);
			note.setText(approval.getNote());
			notificationApproval.addContent(note);
		}

		profileHelper.addApproval(notificationApproval);

		SignatureType signature = approval.getSignature();
		if (signature != null) {
			if (!new P23RSignature().validate(approval.getSignature(), TransformerHelper.elementFromString(notification))) {
				log.warn("approval signature is not valid!");
				protocol.logError(P23RError.VALIDATION_FAILURE, profileHelper,
						message(NOTIFICATIONDISPATCH_NOTIFICATION_VALIDATION_FAILURE, new String[] { approval.getName() }));
			} else {
				log.info("approval signature is valid!");
				protocol.logInfo(profileHelper,
						message(NOTIFICATIONDISPATCH_NOTIFICATION_VALIDATION_SUCCESS, new String[] { approval.getName() }));
			}

			// add to profile
			try {
				JAXBContext ctx = JAXBContext.newInstance(SignatureType.class);
				Marshaller m = ctx.createMarshaller();
				m.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
				JDOMResult result = new JDOMResult();
				m.marshal(new org.w3._2000._09.xmldsig.ObjectFactory().createSignature(signature), result);
				profileHelper.setContentSignature(result.getDocument().getRootElement());
			} catch (JAXBException e) {
				log.error("marshalling signature type", e);
			}
		}

		if ("approved".equals(approval.getApproved())) {

			protocol.logInfo(profileHelper, message(NOTIFICATIONDISPATCH_NOTIFICATION_APPROVED));

			queryAddresses(profileHelper);

			context.setNotification(notification);
			
			if ("test".equals(approval.getName())) {
				testManager.sendNotification(context);
			} else {
				transporter.sendNotification(context);
			}
			protocol.logInfo(profileHelper, message(NOTIFICATIONDISPATCH_NOTIFICATION_SUCCESSFULLY_APPROVED));

		} else if ("rejected".equals(approval.getApproved())) {
			protocol.logInfo(profileHelper, message(NOTIFICATIONDISPATCH_NOTIFICATION_SUCCESSFULLY_REJECTED));
		}

		notificationPool.updateProfile(context);

		return new ApproveNotificationResponse();
	}
	
	private IApprovalNotify initApprovalNotify(String clientConfig) {

		URL wsdlUrl = null;
		try {
			wsdlUrl = new URL(p23rConfig.get(String.format(P23RXmlDbConfig.XQUERY_APPROVALNOTIFY_ADDRESS, clientConfig), "") + "?wsdl");
		} catch (MalformedURLException e) {
			log.error("parsing IApprovalNotify Address", e);
			protocol.logError(P23RError.COMMUNICATION_FAILED, e.getMessage());
			return null;
		}

		QName serviceName = new QName("http://leitstelle.p23r.de/NS/P23R/IApprovalNotify1-0", p23rConfig.get(
				String.format(P23RXmlDbConfig.XQUERY_APPROVALNOTIFY_SERVICE, clientConfig), ""));

		try {
			Service service = IApprovalNotifyService.create(wsdlUrl, serviceName);
			return service.getPort(IApprovalNotify.class);
		} catch (WebServiceException e) {
			log.error("initializing approval notify port", e);
			protocol.logError(P23RError.COMMUNICATION_FAILED, e.getMessage());
			return null;
		}
	}

	/**
	 * Query all available addresses for the given profile.
	 * 
	 * @param profile
	 *            the profile
	 * @throws P23RServiceException
	 */
	private void queryAddresses(NotificationProfileHelper profileHelper) throws P23RAppFault_Exception {

		String packageId = rulepackageRepository.getPackageIdByRuleId(profileHelper.getNotificationRuleId());
		if (packageId == null || !packagelistRepository.getRulePackageSpecifications(packageId).contains("1.1")) {
			queryAddresses10(profileHelper);
		} else {
			queryAddresses11(profileHelper);
		}
		String msg = message(NOTIFICATIONDISPATCH_QUERY_ADDRESSES_SUCCESSFULL);
		protocol.logInfo(profileHelper, msg);
	}

	private void queryAddresses10(NotificationProfileHelper profileHelper) throws P23RAppFault_Exception {

		List<Element> receivers = profileHelper.getReceivers();
		if (receivers == null || receivers.isEmpty()) {
			log.warn("no receivers found");
			return;
		}

//		Element ruleManifest = profileHelper.getRuleManifest();
//		String ruleName = ruleManifest.getAttributeValue("name");

		for (Element receiver : receivers) {
			if (receiver.getChildren("channels", NAMESPACE_PROFILE).isEmpty()) {
				// old query receiver removed
			}
		}
	}

	private void queryAddresses11(NotificationProfileHelper profileHelper) throws P23RAppFault_Exception {

	}

	/**
	 * Signs the notification with the core transformation
	 * 
	 * @param notification
	 *            the notification
	 * @param notificationProfile
	 *            the profile
	 * @return signature element
	 * @throws P23RServiceException
	 */
	private Element signCore(String notification, NotificationProfileHelper profileHelper) throws P23RAppFault_Exception {

		String notificationRuleId = profileHelper.getNotificationRuleId();
		String coreTransformation = modelAndRuleManagement.getCoreTransformation(notificationRuleId);

		if (coreTransformation != null) {
			P23RKeyStore store = null;
			org.w3c.dom.Element signature = null;

			String core = SimpleTransformer.transform(notification, coreTransformation, false);
			try {
				store = new P23RKeyStore("P23R-SAML.pfx", KeyStoreType.PKCS12, "Test1234");
				signature = new P23RSignature().signWithTransformation(TransformerHelper.elementFromString(core), store.getCertificate(),
						store.getPrivateKey(), null);
				if (signature != null) {
					return new DOMBuilder().build(signature);
				}
			} catch (Exception e) {
				// we don't need specific informations about the exception for
				// logging
				log.error("creating core signature", e);
				// } catch (KeyStoreException e) {
				// LOG.error("creating core signature", e);
				// } catch (NoSuchAlgorithmException e) {
				// LOG.error("creating core signature", e);
				// } catch (CertificateException e) {
				// LOG.error("creating core signature", e);
				// } catch (IOException e) {
				// LOG.error("creating core signature", e);
				// } catch (UnrecoverableKeyException e) {
				// LOG.error("creating core signature", e);
			}
			if (store == null || signature == null) {
				protocol.logError(P23RError.GENERAL, profileHelper, message(NOTIFICATIONDISPATCH_NOTIFICATION_CORE_SIGNING_FAILURE));
				throw new P23RAppFault_Exception("creating core signature failed");
			}
		} else {
			log.warn("no core transformation defined, no core signature applied");
		}
		return null;
	}

}
