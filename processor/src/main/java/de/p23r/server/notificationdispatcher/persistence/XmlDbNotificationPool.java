package de.p23r.server.notificationdispatcher.persistence;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.CollectionManagementService;

import de.p23r.common.existdb.P23RXmlDbConfig;
import de.p23r.common.existdb.P23RXmlDbResource;
import de.p23r.common.existdb.P23RXmlDbResourceContentAccess;
import de.p23r.server.common.GenerationContext;
import de.p23r.server.common.NotificationProfileHelper;

/**
 * The Class XmlDbNotificationPool.
 */
@ApplicationScoped
public class XmlDbNotificationPool implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8446896408005803292L;

	private static final String COLLECTION_NAME = "notificationpool";

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Inject
	private P23RXmlDbConfig p23rXmlDbConfig;

	private Collection poolCollection;

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
		try {
			poolCollection = p23rXmlDbConfig.getBaseAsCollection().getChildCollection(COLLECTION_NAME);
			if (poolCollection == null) {
				CollectionManagementService service = (CollectionManagementService) p23rXmlDbConfig.getBaseAsCollection().getService(
						"CollectionManagementService", "1.0");
				poolCollection = service.createCollection(COLLECTION_NAME);
			}
		} catch (XMLDBException e) {
			log.error("initializing pool collection", e);
		}
	}

	/**
	 * Destroy.
	 */
	@PreDestroy
	public void destroy() {
		if (poolCollection != null) {
			try {
				if (poolCollection.isOpen()) {
					poolCollection.close();
				}
			} catch (XMLDBException e) {
				log.error("closing collection failed", e);
			}
		}
	}

	/**
	 * Store notification.
	 *
	 * @param notification            the notification
	 * @param context the context
	 */
	public void storeNotification(String notification, GenerationContext context) {
		if (poolCollection == null) {
			throw new IllegalStateException("collection not initialized");
		}
		P23RXmlDbResource resource = new P23RXmlDbResource(context.getProfile().getNotificationId() + ".xml", poolCollection, false);
		resource.setContent(notification);

		updateProfile(context);
	}

	/**
	 * Store modified notification.
	 *
	 * @param modifiedNotification
	 *            the modified notification
	 * @param profileHelper
	 *            the profile helper
	 */
	public void storeModifiedNotification(String modifiedNotification, NotificationProfileHelper profileHelper) {
		if (poolCollection == null) {
			throw new IllegalStateException("collection not initialized");
		}
		P23RXmlDbResource profileResource = new P23RXmlDbResource(profileHelper.getNotificationId() + "_modified.xml", poolCollection, false);
		profileResource.setContent(modifiedNotification);
	}

	/**
	 * Gets the notification profile.
	 *
	 * @param notificationId
	 *            the notification id
	 * @return the notification profile
	 */
	public GenerationContext getGenerationContext(String notificationId) {
		if (poolCollection == null) {
			throw new IllegalStateException("collection not initialized");
		}

		P23RXmlDbResource resource = new P23RXmlDbResource(notificationId + "_context.bin", poolCollection, false);
		byte[] content = P23RXmlDbResourceContentAccess.getContentAsByteArray(resource);

		GenerationContext context = null;
		
		try (ByteArrayInputStream bis = new ByteArrayInputStream(content); ObjectInput in = new ObjectInputStream(bis)) {
			 context = (GenerationContext) in.readObject();
		} catch (ClassNotFoundException e) {
			log.error("restore generation context", e);
		} catch (IOException e) {
			log.error("restore generation context", e);
		}

		return context;
	}

	/**
	 * Gets the notification.
	 *
	 * @param notificationId
	 *            the notification id
	 * @return the notification
	 */
	public String getNotification(String notificationId) {
		if (poolCollection == null) {
			throw new IllegalStateException("collection not initialized");
		}

		P23RXmlDbResource resource = new P23RXmlDbResource(notificationId + ".xml", poolCollection, false);
		return P23RXmlDbResourceContentAccess.getContentAsString(resource);
	}

	/**
	 * Update profile.
	 *
	 * @param context the context
	 */
	public void updateProfile(GenerationContext context) {
		if (poolCollection == null) {
			throw new IllegalStateException("collection not initialized");
		}
		P23RXmlDbResource resource = new P23RXmlDbResource(context.getProfile().getNotificationId() + "_context.bin", poolCollection, false);

		try (ByteArrayOutputStream bos = new ByteArrayOutputStream(); ObjectOutput out = new ObjectOutputStream(bos)) {
			out.writeObject(context);
			resource.setBinaryContent(bos.toByteArray());
		} catch (IOException e) {
			log.error("storing generation context", e);
		}
	}

}
