package de.p23r.server.notificationdispatcher;

import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault_Exception;
import de.p23r.leitstelle.ns.p23r.inotificationapprove1_0.ApproveNotification;
import de.p23r.leitstelle.ns.p23r.inotificationapprove1_0.ApproveNotificationResponse;
import de.p23r.leitstelle.ns.p23r.inotificationapprove1_0.GetMessage;
import de.p23r.leitstelle.ns.p23r.inotificationapprove1_0.GetMessageResponse;
import de.p23r.leitstelle.ns.p23r.inotificationapprove1_0.GetNotification;
import de.p23r.leitstelle.ns.p23r.inotificationapprove1_0.GetNotificationResponse;
import de.p23r.server.common.GenerationContext;

/**
 * The Interface NotificationDispatcherService.
 *
 * @author sim
 */
public interface NotificationDispatcherService {

    /**
     * Gets the notification.
     *
     * @param request the request
     * @return the notification
     * @throws P23RAppFault_Exception the p23 r app fault_ exception
     */
    GetNotificationResponse getNotification(GetNotification request) throws P23RAppFault_Exception;

    /**
     * Gets the message.
     *
     * @param request the request
     * @return the message
     * @throws P23RAppFault_Exception the p23 r app fault_ exception
     */
    GetMessageResponse getMessage(GetMessage request) throws P23RAppFault_Exception;
    		
    /**
     * Approve notification.
     *
     * @param request the request
     * @return the approve notification response
     * @throws P23RAppFault_Exception the p23 r app fault_ exception
     */
    ApproveNotificationResponse approveNotification(ApproveNotification request) throws P23RAppFault_Exception;

	/**
	 * Dispatch notification.
	 *
	 * @param context the context
	 */
	void dispatchNotification(GenerationContext context);

}
