/**
 * Provides classes that implements the persistence level of the notification pool.
 */
package de.p23r.server.notificationdispatcher.persistence;