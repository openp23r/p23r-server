package de.p23r.server.testmanager;

import java.util.List;

/**
 * The Class TestInfo.
 */
public class TestInfo {

	private String _testpackage;
	
	private String _rulepackage;
	
	private String _username;
	
	private String _password;
	
	private List<String> _testsets;

	/**
	 * Gets the testpackage.
	 *
	 * @return the testpackage
	 */
	public String getTestpackage() {
		return _testpackage;
	}

	/**
	 * Sets the testpackage.
	 *
	 * @param testpackage the new testpackage
	 */
	public void setTestpackage(String testpackage) {
		_testpackage = testpackage;
	}

	/**
	 * Gets the rulepackage.
	 *
	 * @return the rulepackage
	 */
	public String getRulepackage() {
		return _rulepackage;
	}

	/**
	 * Sets the rulepackage.
	 *
	 * @param rulepackage the new rulepackage
	 */
	public void setRulepackage(String rulepackage) {
		_rulepackage = rulepackage;
	}

	/**
	 * Gets the username.
	 *
	 * @return the username
	 */
	public String getUsername() {
		return _username;
	}

	/**
	 * Sets the username.
	 *
	 * @param username the new username
	 */
	public void setUsername(String username) {
		_username = username;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return _password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password the new password
	 */
	public void setPassword(String password) {
		_password = password;
	}

	/**
	 * Gets the testsets.
	 *
	 * @return the testsets
	 */
	public List<String> getSelectedTestSets() {
		return _testsets;
	}

	/**
	 * Sets the testsets.
	 *
	 * @param testsets the new testsets
	 */
	public void setSelectedTestSets(List<String> testsets) {
		_testsets = testsets;
	}
	
	
}
