package de.p23r.server.testmanager;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import de.p23r.leitstelle.ns.p23r.itest1_0.types.TestProtocol;

/**
 * The Interface TestPackageResource.
 */
@Path("/")
public interface TestPackageResource {

	/**
	 * Test.
	 *
	 * @param body the body
	 * @return the test protocol
	 */
	@POST
    @Path("/")
    @Consumes("application/json")
	@Produces("application/xml")
	TestProtocol test(TestInfo body);
	
}
