/**
 * Provides classes representing the notification generator service. It contains mainly an interface class describing the service interface and
 * a bean class that implements the service.
 */
package de.p23r.server.testmanager;