package de.p23r.server.testmanager;

import de.p23r.leitstelle.ns.p23r.nrl.testpackagemanifest1_0.TestPackageManifest;
import de.p23r.server.modelandrulemanagement.ModelAndRuleDepot;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import de.p23r.common.modelandrulemanagement.archive.TestPackageArchive;

/**
 * The Class TestPackageArchiveManager.
 *
 * @author sim
 */
@ApplicationScoped
public class TestPackageArchiveManager implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9201635913988460484L;

	@Inject
	private ModelAndRuleDepot depot;
	
	private Map<String, TestPackageArchive> archives = new HashMap<String, TestPackageArchive>();

	/**
	 * Gets the.
	 *
	 * @param name the name
	 * @return the test package archive
	 */
	public TestPackageArchive get(String name) {
		return archives.get(name);
	}

	/**
	 * Put.
	 *
	 * @param name the name
	 * @param archive the archive
	 */
	public void put(String name, TestPackageArchive archive) {
		archives.put(name, archive);
	}

	public void remove(String name) {
		archives.remove(name);
	}
	
	/**
	 * Load.
	 *
	 * @param location the location
	 * @param username the username
	 * @param password the password
	 * @return the test package archive
	 */
	public TestPackageArchive load(String location, String username, String password) {
		TestPackageArchive archive = (TestPackageArchive) depot.loadPackageFromUri(location, username, password);
		if (archive != null) {
			TestPackageManifest manifest = archive.getTestPackageManifestHelper();
			archives.put(manifest.getId(), archive);
		}
		return archive;
	}
	
}
