package de.p23r.server.testmanager;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.switchyard.component.bean.Reference;

import de.p23r.server.common.GenerationContext;
import de.p23r.server.modelandrulemanagement.ModelAndRuleManagementService;

/**
 * The Class RulepackageArchiveManager.
 */
@ApplicationScoped
public class RulepackageArchiveManager {

	@Inject
	@Reference
	private ModelAndRuleManagementService modelAndRuleManagement;
	
	/**
	 * Install package.
	 *
	 * @param rulepackage the rulepackage
	 * @param username the username
	 * @param password the password
	 * @param transactionId the transaction id
	 * @return the string
	 */
	public String installPackage(String rulepackage, String username, String password, String transactionId) {
		
		GenerationContext context = new GenerationContext();
		context.getProperties().put("username", username);
		context.getProperties().put("password", password);
		context.getProperties().put("transactionId", transactionId);
		context.getProperties().put("rulepackage", rulepackage);		
		
		return modelAndRuleManagement.installRulepackage(context);
	}
	
	/**
	 * Uninstall package.
	 *
	 * @param rulePackageId the rule package id
	 */
	public void uninstallPackage(String rulePackageId) {
		modelAndRuleManagement.uninstallRulepackage(rulePackageId);
	}
	
}
