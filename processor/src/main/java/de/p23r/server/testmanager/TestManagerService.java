package de.p23r.server.testmanager;

import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault_Exception;
import de.p23r.leitstelle.ns.p23r.itest1_0.TestAll;
import de.p23r.leitstelle.ns.p23r.itest1_0.TestAllResponse;
import de.p23r.leitstelle.ns.p23r.itest1_0.TestPackages;
import de.p23r.leitstelle.ns.p23r.itest1_0.TestPackagesResponse;
import de.p23r.leitstelle.ns.p23r.itest1_0.types.TestProtocol;
import de.p23r.server.common.GenerationContext;

/**
 * The Interface TestManagerService.
 *
 * @author sim
 */
public interface TestManagerService {

	/**
	 * Test.
	 *
	 * @param test the test
	 * @return the test protocol
	 */
	TestProtocol test(TestInfo test);

	/**
	 * Test all.
	 *
	 * @param parameter the parameter
	 * @return the test all response
	 * @throws P23RAppFault_Exception the p23 r app fault_ exception
	 */
	TestAllResponse testAll(TestAll parameter) throws P23RAppFault_Exception;
	
	/**
	 * Test packages.
	 *
	 * @param parameter the parameter
	 * @return the test packages response
	 * @throws P23RAppFault_Exception the p23 r app fault_ exception
	 */
	TestPackagesResponse testPackages(TestPackages parameter) throws P23RAppFault_Exception;

	/**
	 * Gets the preselected test data.
	 *
	 * @param context the context
	 * @return the preselected test data
	 */
	String getPreselectedTestData(GenerationContext context);

	/**
	 * Send notification.
	 *
	 * @param context the context
	 */
	void sendNotification(GenerationContext context);

	/**
	 * Notify test approval.
	 *
	 * @param context the context
	 */
	void notifyTestApproval(GenerationContext context);
	
}
