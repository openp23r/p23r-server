package de.p23r.server.testmanager;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import javax.activation.DataHandler;
import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.mail.util.ByteArrayDataSource;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.namespace.QName;
import javax.xml.transform.ErrorListener;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamSource;

import net.sf.saxon.om.StructuredQName;
import net.sf.saxon.s9api.DocumentBuilder;
import net.sf.saxon.s9api.Processor;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.XQueryCompiler;
import net.sf.saxon.s9api.XQueryEvaluator;
import net.sf.saxon.s9api.XdmNode;
import net.sf.saxon.s9api.XdmSequenceIterator;
import net.sf.saxon.s9api.XdmValue;
import net.sf.saxon.trans.XPathException;
import net.sf.saxon.value.SequenceType;

import org.custommonkey.xmlunit.DetailedDiff;
import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.Difference;
import org.custommonkey.xmlunit.Transform;
import org.custommonkey.xmlunit.XMLUnit;
import org.slf4j.Logger;
import org.switchyard.component.bean.Reference;
import org.switchyard.component.bean.ReferenceInvoker;
import org.switchyard.component.bean.Service;
import org.w3._2000._09.xmldsig.SignatureType;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import de.p23r.common.existdb.P23RXmlDbConfig;
import de.p23r.common.existdb.SystemConfiguration;
import de.p23r.common.modelandrulemanagement.P23RSignature;
import de.p23r.common.modelandrulemanagement.SchemaHelperFactory;
import de.p23r.common.modelandrulemanagement.TransformerHelper;
import de.p23r.common.modelandrulemanagement.archive.TestPackageArchive;
import de.p23r.leitstelle.ns.p23r.common1_0.Notification;
import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault_Exception;
import de.p23r.leitstelle.ns.p23r.commonprotocol1_0.ProtocolEntry;
import de.p23r.leitstelle.ns.p23r.inotificationapprove1_0.ApproveNotification;
import de.p23r.leitstelle.ns.p23r.inotificationapprove1_0.types.Approval;
import de.p23r.leitstelle.ns.p23r.iprotocolquery1_1.types.FilterCriteria;
import de.p23r.leitstelle.ns.p23r.itest1_0.TestAll;
import de.p23r.leitstelle.ns.p23r.itest1_0.TestAllResponse;
import de.p23r.leitstelle.ns.p23r.itest1_0.TestPackages;
import de.p23r.leitstelle.ns.p23r.itest1_0.TestPackagesResponse;
import de.p23r.leitstelle.ns.p23r.itest1_0.types.Test;
import de.p23r.leitstelle.ns.p23r.itest1_0.types.TestProtocol;
import de.p23r.leitstelle.ns.p23r.nrl.testcasemanifest1_0.TestCaseManifest;
import de.p23r.leitstelle.ns.p23r.nrl.testpackagemanifest1_0.TestPackageManifest;
import de.p23r.leitstelle.ns.p23r.nrl.testsetmanifest1_1.TestSetManifest;
import de.p23r.server.common.GenerationContext;
import de.p23r.server.common.NotificationProfileHelper;
import de.p23r.server.common.P23RError;
import de.p23r.server.common.TBRS;
import de.p23r.server.common.annotations.P23RConfig;
import de.p23r.server.common.annotations.P23RUnit;
import de.p23r.server.common.compiler.P23RSelectionCompiler;
import de.p23r.server.exceptions.P23RServiceException;
import de.p23r.server.messagereceiver.MessageReceiverService;
import de.p23r.server.protocolpool.Protocol;
import de.p23r.server.xml.P23RProcessor;
import static de.p23r.server.common.Messages.*;

/**
 * The Class TestManagerServiceBean.
 *
 * @author sim
 */
@Service(TestManagerService.class)
public class TestManagerServiceBean implements TestManagerService {

	@Inject
	private Logger log;

	@Inject @P23RUnit
	private Protocol protocol;

	@Inject
	private RulepackageArchiveManager rulepackageArchiveManager;

	@Inject
	private TestPackageArchiveManager testpackageArchiveManager;

	@Inject @P23RConfig(SystemConfiguration.P23RID)
	private String p23rId;

	@Inject
	private P23RXmlDbConfig p23rXmlDbConfig;
	
	@Inject
	@Reference
	private MessageReceiverService messageReceiver;

	@Inject
	@Reference("NotificationDispatcherService")
	private ReferenceInvoker notificationDispatcher;

	@Inject
	private P23RProcessor p23rProcessor;

	@Inject
	private P23RSelectionCompiler selectionCompiler;
	
	@Inject @P23RUnit
	private Event<String> initialized;
	
	private JAXBContext jbctx;
	
	/**
	 * Inits the test manager.
	 */
	@PostConstruct
	public void init() {
		XMLUnit.setIgnoreComments(true);
		XMLUnit.setIgnoreWhitespace(true);
		XMLUnit.setIgnoreAttributeOrder(true);
		
		try {
			jbctx = JAXBContext.newInstance(TestProtocol.class);
		} catch (JAXBException e) {
			log.error("instantiating jaxb context for TestProtocol class", e);
		}
		
		initialized.fire(message(TESTMANAGER));
	}

	/* (non-Javadoc)
	 * @see de.p23r.server.testmanager.TestManagerService#test(org.codehaus.jackson.JsonNode)
	 */
	public TestProtocol test(TestInfo testInfo) {

//		String testpackage = body.get("testpackage").getTextValue();
//		String rulepackage = body.get("rulepackage").getTextValue();
//
//		String username = body.path("username").getTextValue();
//		String password = body.path("password").getTextValue();

		String testpackage = testInfo.getTestpackage();
		String rulepackage = testInfo.getRulepackage();

		String username = testInfo.getUsername();
		String password = testInfo.getPassword();
		
//		List<String> testsets = new ArrayList<String>();
//		JsonNode selectedTestSets = body.path("selectedTestSets");
//		if (selectedTestSets.isArray()) {
//			for (JsonNode node : selectedTestSets) {
//				testsets.add(node.getTextValue());
//			}
//		}

		List<String> testsets = testInfo.getSelectedTestSets();
		
		return testPackages(username, password, Collections.singletonList(testpackage), Collections.singletonList(rulepackage), testsets);			
	}

	/* (non-Javadoc)
	 * @see de.p23r.server.testmanager.TestManagerService#testAll(de.p23r.leitstelle.ns.p23r.itest1_0.TestAll)
	 */
	@Override
	public TestAllResponse testAll(TestAll parameter) throws P23RAppFault_Exception {
		TestProtocol testProtocol = new TestProtocol();
		testProtocol.setP23RId(p23rId);

		
		String transactionId = UUID.randomUUID().toString();
		protocol.logInfo(transactionId, null, "Starte Test (Transaktions-ID: " + transactionId + ")");

		protocol.logError(P23RError.GENERAL, transactionId, null, "Dieser Dienst ist zur Zeit nicht verfügbar!");
		
//		FilterCriteria criteria = new FilterCriteria();
//		criteria.setTransactionId(transactionId);
//		testProtocol.getProtocolEntries().addAll(protocol.query(criteria));

		TestAllResponse response = new TestAllResponse();
		response.setProtocol(testProtocol);
		
		return response;
	}

	/* (non-Javadoc)
	 * @see de.p23r.server.testmanager.TestManagerService#testPackages(de.p23r.leitstelle.ns.p23r.itest1_0.TestPackages)
	 */
	@Override
	public TestPackagesResponse testPackages(TestPackages parameter) throws P23RAppFault_Exception {
		TestPackagesResponse response = new TestPackagesResponse();
		response.setProtocol(testPackages(parameter.getUsername(), parameter.getPassword(), parameter.getTestpackages(), parameter.getRulepackages(), parameter.getTestsets()));
		return response;
	}

	private TestProtocol testPackages(String username, String password, List<String> testpackages, List<String> rulepackages, List<String> testsets) {
		TestProtocol testProtocol = new TestProtocol();
		testProtocol.setP23RId(p23rId);

		String transactionId = UUID.randomUUID().toString();
		protocol.logInfo(transactionId, null, "Starte Test (Transaktions-ID: " + transactionId + ")");

		String testpackage = testpackages.get(0);
		String rulepackage = rulepackages.get(0);
		
		TestPackageArchive archive = testpackageArchiveManager.load(testpackage, username, password);
		if (archive != null) {
			String id = null;
			if (rulepackage != null && !rulepackage.isEmpty()) {
				id = rulepackageArchiveManager.installPackage(rulepackage, username, password, transactionId);
				if (id == null) {
					protocol.logError(P23RError.GENERAL, transactionId, null, "Regelpaket konnte nicht installiert werden.");					
				} else if (id.equals("[]")) {
					protocol.logWarning(transactionId, null, "Regelpaket bereits installiert. Testlauf wird mit bereits installiertem Regelpaket durchgeführt.");
				}				
			}
			
			// start testing
			testPackage(transactionId, archive, testProtocol, testsets);

			if (id != null && !id.equals("[]")) {
				rulepackageArchiveManager.uninstallPackage(id);					
			}
		} else {
			protocol.logError(P23RError.COMMUNICATION_FAILED, transactionId, null, "Testpaket konnte nicht geladen werden");
		}

		FilterCriteria criteria = new FilterCriteria();
		criteria.setTransactionId(transactionId);
		List<ProtocolEntry> entries = protocol.query(criteria);
		for (ProtocolEntry entry : entries) {
			if (entry.getMessageId() == null || entry.getMessageId().trim().isEmpty()) {	
				testProtocol.getProtocolEntries().add(entry);
			}
		}
		
//		if (log.isTraceEnabled()) {
			// should print out serialized test protocol here
			StringWriter writer = new StringWriter();
			try {
				JAXBElement<TestProtocol> jaxbTestProtocol = new JAXBElement<TestProtocol>(new QName("http://leitstelle.p23r.de/NS/P23R/ITest1-0/types", "TestProtocol"), TestProtocol.class, testProtocol);
				Marshaller m = jbctx.createMarshaller();
				m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
				m.marshal(jaxbTestProtocol, writer);
				log.info("test protocol: {}", writer.toString());
			} catch (JAXBException e) {
				log.error("marshalling test protocol", e);
			}
//		}
		
		testpackageArchiveManager.remove(archive.getTestPackageManifestHelper().getId());
		
		return testProtocol;
	}
	
	private void testPackage(String transactionId, TestPackageArchive archive, TestProtocol testProtocol, List<String> testsets) {
		String manifestContent = archive.getPackageManifest();
		if (!p23rProcessor.validate(manifestContent, null)) {
			protocol.logError(P23RError.VALIDATION_FAILURE, transactionId, null, "Ungültiges Manifest im Testpaket");
		} else {
			TestPackageManifest manifest = SchemaHelperFactory.getTestPackageManifestSchemaHelper().create(manifestContent);

			try {
				testProtocol.setStartedAt(DatatypeFactory.newInstance().newXMLGregorianCalendar(new GregorianCalendar()));
			} catch (DatatypeConfigurationException e) {
				log.warn("setting starting time failed", e);
			}
			testProtocol.setStartedBy("unknown");

			List<String> availableTestsets = archive.getTestSetPathes();
			
			log.debug("iterate through testsets: {}", availableTestsets);
			
			if (testsets != null && !testsets.isEmpty()) {
				for (String testset : availableTestsets) {
					String id = archive.getTestSetManifestHelper(testset).getId();
					if (testsets.contains(id)) {
						log.debug("processing testset {}", testset);
						testSet(transactionId, archive, testProtocol, testset, manifest.getId());					
					}
				}				
			} else {
				for (String testset : availableTestsets) {
					testSet(transactionId, archive, testProtocol, testset, manifest.getId());					
				}
			}
		}
	}

	private void testSet(String transactionId, TestPackageArchive archive, TestProtocol testProtocol, String testset, String packageId) {
		String manifestContent = archive.getTestSetManifest(testset);
		if (!p23rProcessor.validate(manifestContent, null)) {
			protocol.logError(P23RError.VALIDATION_FAILURE, transactionId, null, "Ungültiges Manifest im Testset " + testset);
		} else {
			TestSetManifest testsetManifest = archive.getTestSetManifestHelper(testset);
			log.debug("testset name: {}", testsetManifest.getName());

			List<String> testcases = archive.getTestCasePathes(testset);
			log.debug("extracted test cases:");
			for (String testcase : testcases) {
				log.debug("path found in archive: {}", testcase);
			}
			
			for (String testcase : testcases) {
				testCase(transactionId, archive, testProtocol, testsetManifest, testcase, packageId);
			}
		}
	}

	private void testCase(String transactionId, TestPackageArchive archive, TestProtocol testProtocol, TestSetManifest testsetManifest, String testcase, String packageId) {
		String manifestContent = archive.getTestCaseManifest(testcase);
		if (!p23rProcessor.validate(manifestContent, null)) {
			protocol.logError(P23RError.VALIDATION_FAILURE, transactionId, null, "Ungültiges Manifest im Testcase " + testcase);
		} else {
			TestCaseManifest testcaseManifest = archive.getTestCaseManifestHelper(testcase);

			Test test = new Test();
			test.setTestSetId(testsetManifest.getId());
			test.setTestCaseId(testcaseManifest.getId());
			test.setContext(testsetManifest.getContext());
			test.getBehaviours().addAll(testcaseManifest.getBehaviours());

			String message = archive.getTestCaseMessage(testcase);
			if (message == null) {
				log.error("message from testcase {} is not valid", testcaseManifest.getName());
				String recordId = protocol.logError(P23RError.GENERAL, "Message.xml scheint ungültig oder nicht vorhanden");
				FilterCriteria criteria = new FilterCriteria();
				criteria.setRecordId(recordId);
				test.getProtocolEntries().addAll(protocol.query(criteria));
				finalizeTestcaseProtocol(testProtocol, test);
				return;
			}
			
			GenerationContext context = new GenerationContext();
			context.setTestpackage(packageId);
			context.setTestset(testsetManifest.getName());
			context.setTestcase(testcaseManifest.getName());

			context.getProperties().put(GenerationContext.TENANT, "test");
			context.getProperties().put(GenerationContext.MESSAGE, message);
			context.getProperties().put(GenerationContext.SOURCEINTERFACE, "test");
			context.getProperties().put(GenerationContext.ORIGINATORID, p23rId);
			
			context.getProperties().put("transactionid", transactionId);
			
			try {
				long start = System.currentTimeMillis();
				String messageId = messageReceiver.deliverTestMessage(context);
				long duration = System.currentTimeMillis() - start;

				log.info("receiving message id {}", messageId);
				log.info("test finished after {}ms", duration);

				test.setSucceeded(true);

				FilterCriteria criteria = new FilterCriteria();
				criteria.setMessageId(messageId);
			
				test.getProtocolEntries().addAll(protocol.query(criteria));

				log.info("collected {} protocol entries for message id {}", test.getProtocolEntries().size(), messageId);
				
			} catch (P23RServiceException e) {
				test.setSucceeded(false);
				
			} catch (Exception e) {
				String recordId = protocol.logError(P23RError.GENERAL, e.getMessage());
				test.setSucceeded(false);
				FilterCriteria criteria = new FilterCriteria();
				criteria.setRecordId(recordId);
				test.getProtocolEntries().addAll(protocol.query(criteria));
			}
			finalizeTestcaseProtocol(testProtocol, test);
		}
	}

	private void finalizeTestcaseProtocol(TestProtocol protocol, Test test) {
		try {
			test.setFinishedAt(DatatypeFactory.newInstance().newXMLGregorianCalendar(new GregorianCalendar()));
		} catch (DatatypeConfigurationException e) {
			log.warn("setting finished time failed", e);
		}

		protocol.getTests().add(test);
	}

	/* (non-Javadoc)
	 * @see de.p23r.server.testmanager.TestManagerService#getPreselectedTestData(de.p23r.server.testmanager.TestDataSelectionParameter)
	 */
	@Override
	public String getPreselectedTestData(GenerationContext context) {
		String testpackageId = context.getTestpackage();
		String testset = context.getTestset();
		String testcase = context.getTestcase();

		TestPackageArchive archive = testpackageArchiveManager.get(testpackageId);
		if (archive == null) {
			protocol.logError(P23RError.GENERAL, message(TESTMANAGER_TESTPACKAGE_NOT_FOUND));
			return null;
		}

		String namespace = context.getProperties().get(GenerationContext.SELECTION_NAMESPACE);
		String selection = context.getProperties().get(GenerationContext.SELECTION_SCRIPT);
		String parameter = context.getProperties().get(GenerationContext.SELECTION_PARAMETER);

		// strategy will be: if no testpackage is given (during rule package
		// install) just take the
		// first data found in the archive
		// otherwise take the correct data depending on testset and testcase

		// if configuration namespace is given use the configuration selection
		// and transformation
		// scripts to generate configuration data.
		// we have to do it with the group and rule scripts.
		// any better way?

		String data = archive.getDataForTestCase(testset, testcase).get(namespace);
		if (data == null) {
			protocol.logError(P23RError.GENERAL, message(TESTMANAGER_NO_DATA_FOR_NAMESPACE, new String[] { testset, testcase, namespace }));
			return null;
		}

		log.debug("found test data for {}: {}", namespace, data);

		String selectionMode = p23rXmlDbConfig.get(P23RXmlDbConfig.XQUERY_GENERAL_SELECTIONMODE, "auto");
		if (selectionMode.equalsIgnoreCase("strict") || (selectionMode.equalsIgnoreCase("auto") && P23RSelectionCompiler.isP23RSelection(selection))) {
			selection = selectionCompiler.compileSelection(selection, "connector");
		}

		String selectedData = executeLocalXQuery(selection, parameter, data);
		log.debug("selected test data: {}", selectedData);

		return selectedData;
	}

	/* (non-Javadoc)
	 * @see de.p23r.server.testmanager.TestManagerService#notifyTestApproval(de.p23r.server.notificationgenerator.NotificationParameter)
	 */
	@Override
	public void notifyTestApproval(GenerationContext context) {
		String testpackageId = context.getTestpackage();
		String testset = context.getTestset();
		String testcase = context.getTestcase();
		
		String notificationId = context.getProfile().getNotificationId();

		// get notification
		String notification = context.getNotification();

		// approve notification
		Approval approval = new Approval();
		approval.setApproved("approved");
		approval.setNote("automatic test approvement");
		approval.setName("test");

		TestPackageArchive archive = testpackageArchiveManager.get(testpackageId);
		String pfx = archive.getKeyStore(testset, testcase);
		try {
			if (pfx != null && !pfx.isEmpty()) {
				SignatureType signature = getSignature(pfx, notification);
				if (signature != null) {
					approval.setSignature(signature);
				}
			}

			ApproveNotification approvalResult = new ApproveNotification();
			approvalResult.setNotification(new Notification());
			DataHandler handler = new DataHandler(new ByteArrayDataSource(notification.getBytes(TBRS.CHARSET), "application/xml"));
			approvalResult.getNotification().setContent(handler);
			approvalResult.setNotificationId(notificationId);
			approvalResult.setApproval(approval);

			log.debug("automatic approval of test notification for testpackage {}", testpackageId);
			notificationDispatcher.newInvocation("approveNotification").invoke(approvalResult);
			
		} catch (KeyStoreException e) {
			log.error("No keystore provider for the type 'pkcs12' is available.", e);
		} catch (NoSuchAlgorithmException e) {
			log.error("Failed to load the keystore.", e);
		} catch (CertificateException e) {
			log.error("Got certificate problems.", e);
		} catch (IOException e) {
			log.error("Failed to lead the keystore.", e);
		} catch (Exception e) {
			log.error("Failed to invoke approveNotification.", e);
		}
	}

	/* (non-Javadoc)
	 * @see de.p23r.server.testmanager.TestManagerService#sendNotification(de.p23r.server.notificationgenerator.NotificationParameter)
	 */
	@Override
	public void sendNotification(GenerationContext context) {
		String testpackageId = context.getTestpackage();
		String testset = context.getTestset();
		String testcase = context.getTestcase();

		TestPackageArchive archive = testpackageArchiveManager.get(testpackageId);
		TestCaseManifest manifest = archive.getTestCaseManifestHelper(testset, testcase);

		Map<String, String> expectedNotifications = archive.getExpectedNotifications(testset, testcase);

		log.debug("final notification: {}", context.getNotification());
		boolean compared = false;
		for (Entry<String, String> expectedNotification : expectedNotifications.entrySet()) {
			String filter = archive.getMatchingFilter(testset, testcase, expectedNotification.getKey());
			if (expectedResult(context.getProfile(), manifest.getExpect(), context.getNotification(), expectedNotification.getValue(), filter)) {
				compared = true;
				break;
			}
		}
		if (!compared) {
			protocol.testFail(context.getProfile(), "no expected result found for:\n" + context.getNotification());
		}
	}

	private String executeLocalXQuery(String selection, String parameters, String source) {
		
		Processor processor = new Processor(false);

		XQueryCompiler compiler = processor.newXQueryCompiler();
		compiler.setErrorListener(new ErrorListener() {

			@Override
			public void warning(TransformerException exception) throws TransformerException {
				protocol.logWarning(exception.getLocalizedMessage() + "\n" + exception.getLocationAsString());
			}

			@Override
			public void fatalError(TransformerException exception) throws TransformerException {
				protocol.logError(P23RError.SELECTION_FAILURE, exception.getLocalizedMessage() + "\n" + exception.getLocationAsString());
			}

			@Override
			public void error(TransformerException exception) throws TransformerException {
				protocol.logError(P23RError.SELECTION_FAILURE, exception.getLocalizedMessage() + "\n" + exception.getLocationAsString());
			}
		});
		if (parameters != null && !parameters.isEmpty()) {
			try {
				DocumentBuilder documentBuilder = processor.newDocumentBuilder();
				XdmNode p23rparameters = documentBuilder.build(new StreamSource(new ByteArrayInputStream(parameters.getBytes(TBRS.CHARSET))));
				compiler.getUnderlyingStaticContext().declareGlobalVariable(new StructuredQName("", "", "p23rparameters"), SequenceType.NODE_SEQUENCE,
						p23rparameters.getUnderlyingValue(), false);
			} catch (SaxonApiException e) {
				log.error("Failed to build the document.", e);
			} catch (XPathException e) {
				log.error("Declaration of global XPath variable failed.", e);
			}
		}
		try {
			XQueryEvaluator eval = compiler.compile(selection).load();
			eval.setSource(new StreamSource(new ByteArrayInputStream(source.getBytes(TBRS.CHARSET))));

			XdmValue val = eval.evaluate();
			XdmSequenceIterator it = val.iterator();

			StringBuilder result = new StringBuilder("");
			while (it.hasNext()) {
				XdmNode node = (XdmNode) it.next();
				result.append(node.toString());
			}
			return result.toString();
		} catch (SaxonApiException e) {
			log.error("execution of selection script failed", e);
		}

		return null;
	}

	private boolean expectedResult(NotificationProfileHelper profileHelper, String expect, String notification, String expectedNotification, String filter) {
		try {
			Diff diff = null;
			if (filter == null || filter.isEmpty()) {
				diff = new Diff(expectedNotification, notification);
			} else {
				Transform tr = new Transform(notification, filter);
				diff = new Diff(expectedNotification, tr);
			}
			DetailedDiff detailedDiff = new DetailedDiff(diff);
			@SuppressWarnings("unchecked")
			List<Difference> list = (List<Difference>) detailedDiff.getAllDifferences();
			for (Difference d : list) {
				log.debug("diff: {}", d.getDescription());
			}

			log.debug("compare xml: {}", detailedDiff);
			if (resultIsAsExpected(expect, detailedDiff)) {
				protocol.testSuccess(profileHelper, detailedDiff.toString());
				return true;
			}
			// else test fails
			protocol.testFail(profileHelper, detailedDiff.toString());
		} catch (IOException e) {
			log.error("trying to calculate xml unit diff", e);
			protocol.logError(P23RError.GENERAL, profileHelper, "Fehler beim lesen der zu vergleichenden Ergebnisbenachrichtigungen.");
		} catch (TransformerException e) {
			log.error("trying to calculate xml unit diff", e);
			protocol.logError(P23RError.TRANSFORMATION_FAILURE, profileHelper, "Fehler beim transformieren der zu vergleichenden Ergebnisbenachrichtigungen.");
		} catch (SAXException e) {
			log.error("trying to calculate xml unit diff", e);
			protocol.logError(P23RError.VALIDATION_FAILURE, profileHelper, "Fehler beim parsen der zu vergleichenden Ergebnisbenachrichtigungen.");
		}
		return false;
	}

	/**
	 * Returns true if the result equals the expected result
	 * 
	 * @param expect
	 *            the expected result
	 * @param detailedDiff
	 *            the result
	 * @return equality success
	 */
	private boolean resultIsAsExpected(String expect, DetailedDiff detailedDiff) {
		return ("matchable".equals(expect) && detailedDiff.similar()) || ("unmatchable".equals(expect) && !detailedDiff.similar());
	}

	private SignatureType getSignature(String pfx, String notification) throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException {
		KeyStore keystore = KeyStore.getInstance("pkcs12");
		keystore.load(new ByteArrayInputStream(pfx.getBytes(Charset.forName("UTF-8"))), null);
		PrivateKey key = null;
		X509Certificate cert = null;
		Enumeration<String> aliases = keystore.aliases();
		while (aliases.hasMoreElements()) {
			String alias = aliases.nextElement();
			if (key == null) {
				key = getKey(keystore, alias);
			}
			if (cert == null) {
				cert = (X509Certificate) keystore.getCertificate(alias);
			}
		}

		if (key != null && cert != null) {
			Element elem = TransformerHelper.elementFromString(notification);
			Element signature = new P23RSignature().sign(elem, cert, key);
			if (signature != null) {
				return getSignatureType(signature);
			}
		}
		return null;
	}

	private PrivateKey getKey(KeyStore keystore, String alias) throws KeyStoreException, NoSuchAlgorithmException {
		try {
			return (PrivateKey) keystore.getKey(alias, null);
		} catch (UnrecoverableKeyException e) {
			log.error("Key can not be recovered.", e);
		}
		return null;
	}

	private SignatureType getSignatureType(Element signature) {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(SignatureType.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			JAXBElement<SignatureType> jaxbSignature = unmarshaller.unmarshal(signature, SignatureType.class);
			return jaxbSignature.getValue();
		} catch (JAXBException e) {
			log.warn("Could not marshal SignatureType", e);
		}
		return null;
	}

}
