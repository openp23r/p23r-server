package de.p23r.server.protocolpool;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

import static de.p23r.server.common.Messages.*;
import de.p23r.server.common.annotations.P23RUnit;

/**
 * A factory for creating Protocol objects.
 */
public class ProtocolFactory {

	/**
	 * Creates a new Protocol object.
	 *
	 * @param protocol the protocol
	 * @param injectionPoint the injection point
	 * @return the protocol
	 */
	@Produces @P23RUnit
	Protocol createProtocolPool(Protocol protocol, InjectionPoint injectionPoint) {
		String name = injectionPoint.getAnnotated().getAnnotation(P23RUnit.class).value();
		if (name.isEmpty()) {
			name = message(injectionPoint.getMember().getDeclaringClass().getName());
			if (name.startsWith("!") && name.endsWith("!")) {
				name = injectionPoint.getMember().getDeclaringClass().getSimpleName();
			}			
		}
		protocol.setName(name);
		return protocol;
	}
	
}
