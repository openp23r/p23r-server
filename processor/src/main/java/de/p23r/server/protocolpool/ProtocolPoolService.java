package de.p23r.server.protocolpool;

import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault_Exception;
import de.p23r.leitstelle.ns.p23r.commonprotocol1_0.ProtocolEntry;
import de.p23r.leitstelle.ns.p23r.iprotocolquery1_1.QueryProtocol;
import de.p23r.leitstelle.ns.p23r.iprotocolquery1_1.QueryProtocolResponse;

/**
 * The Interface ProtocolPoolService.
 *
 * @author sim
 */
public interface ProtocolPoolService {

	/**
	 * Query protocol.
	 *
	 * @param query the query
	 * @return the query protocol response
	 * @throws P23RAppFault_Exception the p23 r app fault_ exception
	 */
	QueryProtocolResponse queryProtocol(QueryProtocol query) throws P23RAppFault_Exception;

	/**
	 * Adds the entry.
	 *
	 * @param entry the entry
	 * @return the string
	 */
	String addEntry(ProtocolEntry entry);

}
