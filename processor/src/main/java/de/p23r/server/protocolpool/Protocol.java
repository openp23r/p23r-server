package de.p23r.server.protocolpool;

import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import org.jdom2.Element;
import org.jdom2.Namespace;
import org.slf4j.Logger;
import org.switchyard.component.bean.Reference;

import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault_Exception;
import de.p23r.leitstelle.ns.p23r.commonprotocol1_0.Level;
import de.p23r.leitstelle.ns.p23r.commonprotocol1_0.Log;
import de.p23r.leitstelle.ns.p23r.commonprotocol1_0.ProtocolEntry;
import de.p23r.leitstelle.ns.p23r.commonprotocol1_0.ProtocolEntry.Content;
import de.p23r.leitstelle.ns.p23r.commonprotocol1_0.Trace;
import de.p23r.leitstelle.ns.p23r.iprotocolquery1_1.QueryProtocol;
import de.p23r.leitstelle.ns.p23r.iprotocolquery1_1.QueryProtocolResponse;
import de.p23r.leitstelle.ns.p23r.iprotocolquery1_1.types.FilterCriteria;
import de.p23r.leitstelle.ns.p23r.nrl.commonnrl1_1.Description;
import de.p23r.server.common.NotificationProfileHelper;
import de.p23r.server.common.P23RError;
import de.p23r.server.common.TBRS;

/**
 * The Class ProtocolPoolService.
 * 
 * @author sim
 */
public class Protocol {

	/**
	 * The Constant log.
	 */
	@Inject
	private Logger log;
	
	@Inject
	@Reference
	private ProtocolPoolService protocolPoolService;

	/**
	 * The name that belongs to the component using the protocol service.
	 */
	private String name = "P23R";

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Query.
	 *
	 * @param criteria the criteria
	 * @return the list
	 */
	public List<ProtocolEntry> query(FilterCriteria criteria) {
		criteria.setOffset(0D);
		criteria.setMax(100);
		
		QueryProtocol query = new QueryProtocol();
		query.setFilterCriteria(criteria);

		QueryProtocolResponse response;
		try {
			response = protocolPoolService.queryProtocol(query);
			return response.getQueryResult().getProtocolEntries();
		} catch (P23RAppFault_Exception e) {
			log.error("query protocol entries", e);
		}
		return null;
	};
	
	// TRACE

	/**
	 * Trace.
	 *
	 * @param profile the profile
	 * @param step the step
	 * @param note the note
	 * @param errorCode the error code
	 * @param outputProfile the output profile
	 * @param profileScript the profile script
	 * @param inputNotification the input notification
	 * @param outputNotification the output notification
	 * @param transformationScript the transformation script
	 * @return the string
	 */
	public String trace(NotificationProfileHelper profile,
			String step,
			String note,
			int errorCode,
			String outputProfile,
			String profileScript,
			String inputNotification,
			String outputNotification,
			String transformationScript) {
		
		Trace trace = createTrace(step, note, errorCode);

		trace.setInputProfile(profile.prettyPrint());			
		trace.setOutputProfile(outputProfile);
		trace.setProfileScript(profileScript);
		trace.setInputNotification(inputNotification);
		trace.setOutputNotification(outputNotification);
		trace.setTransformationScript(transformationScript);
		ProtocolEntry entry = createProtocolEntry(profile);
		
		return trace(entry, trace);
	}
	
	// INFO
	
	/**
	 * Log info.
	 *
	 * @param note the note
	 * @return the string
	 */
	public String logInfo(String note) {
		return logInfo(null, null, note);
	}

	/**
	 * Log info.
	 *
	 * @param transactionId the transaction id
	 * @param messageId the message id
	 * @param note the note
	 * @return the string
	 */
	public String logInfo(String transactionId, String messageId, String note) {
		Log log = createLog(Level.INFO, note, 0);
		ProtocolEntry entry = createProtocolEntry(transactionId, null, messageId, null, null, null);
		return log(entry, log);
	}
	
	/**
	 * Log info.
	 *
	 * @param ruleName the rule name
	 * @param ruleId the rule id
	 * @param release the release
	 * @param note the note
	 * @return the string
	 */
	public String logInfo(String ruleName, String ruleId, String release, String note) {
		Log log = createLog(Level.INFO, note, 0);
		ProtocolEntry entry = createProtocolEntry(null, null, null, ruleId, ruleName, release);
		return log(entry, log);		
	}
	
	/**
	 * Log info.
	 *
	 * @param profile the profile
	 * @param note the note
	 * @return the string
	 */
	public String logInfo(NotificationProfileHelper profile, String note) {
		Log log = createLog(Level.INFO, note, 0);
		ProtocolEntry entry = createProtocolEntry(profile);
		return log(entry, log);
	}
	
	
	// ERROR
	
	/**
	 * Log error.
	 *
	 * @param error the error
	 * @param note the note
	 * @return the string
	 */
	public String logError(P23RError error, String note) {
		return logError(error, null, null, note);
	}

	/**
	 * Log error.
	 *
	 * @param error the error
	 * @param transactionId the transaction id
	 * @param messageId the message id
	 * @param note the note
	 * @return the string
	 */
	public String logError(P23RError error, String transactionId, String messageId, String note) {
		Log log = createLog(Level.ERROR, note, error.ordinal());
		ProtocolEntry entry = createProtocolEntry(transactionId, null, messageId, null, null, null);
		return log(entry, log);
	}
		
	/**
	 * Log error.
	 *
	 * @param error the error
	 * @param ruleId the rule id
	 * @param ruleName the rule name
	 * @param ruleRelease the rule release
	 * @param note the note
	 * @return the string
	 */
	public String logError(P23RError error, String ruleId, String ruleName, String ruleRelease, String note) {
		Log log = createLog(Level.ERROR, note, error.ordinal());
		ProtocolEntry entry = createProtocolEntry(null, null, null, ruleId, ruleName, ruleRelease);
		return log(entry, log);
	}
		
	/**
	 * Log error.
	 *
	 * @param error the error
	 * @param profile the profile
	 * @param note the note
	 * @return the string
	 */
	public String logError(P23RError error, NotificationProfileHelper profile, String note) {
		Log log = createLog(Level.ERROR, note, error.ordinal());
		ProtocolEntry entry = createProtocolEntry(profile);
		return log(entry, log);
	}
	

	// WARNING
	
	
	/**
	 * Log warning.
	 *
	 * @param note the note
	 * @return the string
	 */
	public String logWarning(String note) {
		Log log = createLog(Level.WARNING, note, 0);
		ProtocolEntry entry = createProtocolEntry(null, null, null, null, null, null);
		return log(entry, log);				
	}

	/**
	 * Log warning.
	 *
	 * @param transactionId the transaction id
	 * @param messageId the message id
	 * @param note the note
	 * @return the string
	 */
	public String logWarning(String transactionId, String messageId, String note) {
		Log log = createLog(Level.WARNING, note, 0);
		ProtocolEntry entry = createProtocolEntry(transactionId, null, messageId, null, null, null);
		return log(entry, log);
	}
	
	/**
	 * Log warning.
	 *
	 * @param profile the profile
	 * @param note the note
	 * @return the string
	 */
	public String logWarning(NotificationProfileHelper profile, String note) {
		Log log = createLog(Level.WARNING, note, 0);
		ProtocolEntry entry = createProtocolEntry(profile);
		return log(entry, log);				
	}
	
	
	// TEST_SUCCESS
	
	
	/**
	 * Test success.
	 *
	 * @param note the note
	 * @return the string
	 */
	public String testSuccess(String note) {
		Log log = createLog(Level.TEST_SUCCESS, note, 0);
		ProtocolEntry entry = createProtocolEntry(null, null, null, null, null, null);
		return log(entry, log);				
	}

	/**
	 * Test success.
	 *
	 * @param profile the profile
	 * @param note the note
	 * @return the string
	 */
	public String testSuccess(NotificationProfileHelper profile, String note) {
		Log log = createLog(Level.TEST_SUCCESS, note, 0);
		ProtocolEntry entry = createProtocolEntry(profile);
		return log(entry, log);				
	}

	// TEST_FAIL
	
	
	/**
	 * Test fail.
	 *
	 * @param note the note
	 * @return the string
	 */
	public String testFail(String note) {
		Log log = createLog(Level.TEST_FAIL, note, 0);
		ProtocolEntry entry = createProtocolEntry(null, null, null, null, null, null);
		return log(entry, log);				
	}

	/**
	 * Test fail.
	 *
	 * @param profile the profile
	 * @param note the note
	 * @return the string
	 */
	public String testFail(NotificationProfileHelper profile, String note) {
		Log log = createLog(Level.TEST_FAIL, note, 0);
		ProtocolEntry entry = createProtocolEntry(profile);
		return log(entry, log);				
	}
	
	private ProtocolEntry createProtocolEntry(NotificationProfileHelper profile) {

		String transactionId = profile.getTransactionId();
		String notificationId = profile.getNotificationId();
		String messageId = profile.getMessageId();
		String ruleId = profile.getNotificationRuleId();
		String ruleName = null;
		String ruleRelease = null;

		if (profile.getRuleManifest() != null) {
			Element ruleManifest = profile.getRuleManifest();
			ruleName = ruleManifest.getAttributeValue("name");
			ruleRelease = ruleManifest.getChildText("release", Namespace.getNamespace(TBRS.NAMESPACE_RULEMANIFEST));
		}

		return createProtocolEntry(transactionId, notificationId, messageId, ruleId, ruleName, ruleRelease);		
	}
	
	private ProtocolEntry createProtocolEntry(String transactionId, String notificationId, String messageId, String ruleId, String ruleName, String ruleRelease) {
		ProtocolEntry protocolEntry = new de.p23r.leitstelle.ns.p23r.commonprotocol1_0.ObjectFactory().createProtocolEntry();
		try {
			protocolEntry.setEventAt(DatatypeFactory.newInstance().newXMLGregorianCalendar(new GregorianCalendar()));
		} catch (DatatypeConfigurationException e) {
			log.warn("create current date and time", e);
		}
		
		protocolEntry.setTransactionId(transactionId);
		protocolEntry.setNotificationId(notificationId);
		protocolEntry.setMessageId(messageId);

		protocolEntry.setRelease(ruleRelease);
		protocolEntry.setRuleId(ruleId);
		protocolEntry.setRuleName(ruleName);
		
		return protocolEntry;
	}

	private Log createLog(Level level, String note, int errorCode) {
		Log log = new de.p23r.leitstelle.ns.p23r.commonprotocol1_0.ObjectFactory().createLog();
		log.setLevel(level);
		Description description = new Description();
		description.setLang(Locale.getDefault().getLanguage());
		description.setContent(note);
		log.getNotes().add(description);
		if (level == Level.ERROR) {
			log.setErrorCode(errorCode);
		}

		log.setComponentName(name);
		return log;
	}
	
	private Trace createTrace(String step, String note, int errorCode) {
		Trace trace = new de.p23r.leitstelle.ns.p23r.commonprotocol1_0.ObjectFactory().createTrace();
		trace.setStep(step);
		Description description = new Description();
		description.setLang(Locale.getDefault().getLanguage());
		description.setContent(note);
		trace.getNotes().add(description);
		return trace;
	}
	
	private String log(ProtocolEntry entry, Log log) {
		entry.setContent(new Content());
		entry.getContent().setLog(log);
		return write(entry);
	}

	private String trace(ProtocolEntry entry, Trace trace) {
		entry.setContent(new Content());
		entry.getContent().setTrace(trace);
		return write(entry);
	}

	private String write(ProtocolEntry protocolEntry) {
		return protocolPoolService.addEntry(protocolEntry);
	}

}
