package de.p23r.server.protocolpool;

import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.util.GregorianCalendar;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.switchyard.component.bean.Service;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.Resource;
import org.xmldb.api.base.ResourceIterator;
import org.xmldb.api.base.ResourceSet;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.CollectionManagementService;
import org.xmldb.api.modules.XQueryService;

import de.p23r.common.existdb.P23RXmlDbConfig;
import de.p23r.common.existdb.P23RXmlDbResource;
import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault_Exception;
import de.p23r.leitstelle.ns.p23r.commonprotocol1_0.Log;
import de.p23r.leitstelle.ns.p23r.commonprotocol1_0.ObjectFactory;
import de.p23r.leitstelle.ns.p23r.commonprotocol1_0.ProtocolEntry;
import de.p23r.leitstelle.ns.p23r.commonprotocol1_0.Trace;
import de.p23r.leitstelle.ns.p23r.iprotocolquery1_1.QueryProtocol;
import de.p23r.leitstelle.ns.p23r.iprotocolquery1_1.QueryProtocolResponse;
import de.p23r.leitstelle.ns.p23r.iprotocolquery1_1.types.FilterCriteria;
import de.p23r.leitstelle.ns.p23r.iprotocolquery1_1.types.QueryResult;
import de.p23r.server.common.TBRS;
import de.p23r.server.common.annotations.P23RUnit;
import static de.p23r.server.common.Messages.*;

/**
 * The Class ProtocolPoolServiceBean.
 *
 * @author sim
 */
@Service(ProtocolPoolService.class)
public class ProtocolPoolServiceBean implements ProtocolPoolService {

	private final Logger log = LoggerFactory.getLogger(getClass());

	private static final String COMPONENT_NAME = message(PROTOCOLPOOL);

	private static final Integer DEFAULT_MAX = 30;

	@Inject
	private P23RXmlDbConfig p23rXmlDbConfig;

	@Inject
	@P23RUnit
	private Event<String> initialized;

	private Collection protocolpool;
	private XQueryService xqueryService;

	private JAXBContext ctx;

	private final static ObjectFactory factory = new ObjectFactory();

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
		try {
			protocolpool = p23rXmlDbConfig.getBaseAsCollection().getChildCollection("protocolpool");
			if (protocolpool == null) {
				CollectionManagementService service = (CollectionManagementService) p23rXmlDbConfig.getBaseAsCollection().getService(
						"CollectionManagementService", "1.0");
				protocolpool = service.createCollection("protocolpool");
			}
			xqueryService = (XQueryService) protocolpool.getService("XQueryService", "1.0");
			xqueryService.setNamespace("", "http://leitstelle.p23r.de/NS/P23R/CommonProtocol1-0");
		} catch (XMLDBException e) {
			log.error("initializing protocolpool collection", e);
		}

		try {
			ctx = JAXBContext.newInstance(ProtocolEntry.class);
		} catch (JAXBException e) {
			log.error("instantiating JAXB context", e);
		}

		initialized.fire(COMPONENT_NAME);
	}

	/* (non-Javadoc)
	 * @see de.p23r.server.protocolpool.ProtocolPoolService#queryProtocol(de.p23r.leitstelle.ns.p23r.iprotocolquery1_1.QueryProtocol)
	 */
	@Override
	public QueryProtocolResponse queryProtocol(QueryProtocol query) throws P23RAppFault_Exception {
		FilterCriteria filterCriteria = query.getFilterCriteria();
		double offset = filterCriteria.getOffset() == null ? 0 : filterCriteria.getOffset();
		int max = filterCriteria.getMax() == null ? DEFAULT_MAX : filterCriteria.getMax();
		StringBuilder buffer = new StringBuilder("for $entry in /protocolEntry");

		if (filterCriteria.getRecordId() != null && !filterCriteria.getRecordId().isEmpty()) {
			buffer.append("[@recordId = '");
			buffer.append(filterCriteria.getRecordId());
			buffer.append("']");
		}

		if (filterCriteria.getNotificationId() != null) {
			buffer.append("[@notificationId = '");
			buffer.append(filterCriteria.getNotificationId());
			buffer.append("']");
		}

		if (filterCriteria.getMessageId() != null) {
			buffer.append("[@messageId = '");
			buffer.append(filterCriteria.getMessageId());
			buffer.append("']");
		}

		if (filterCriteria.getTransactionId() != null) {
			buffer.append("[@transactionId = '");
			buffer.append(filterCriteria.getTransactionId());
			buffer.append("']");
		}

		if (filterCriteria.getRuleId() != null) {
			buffer.append("[@ruleId = '");
			buffer.append(filterCriteria.getRuleId());
			buffer.append("']");
		}

		if (filterCriteria.getRuleName() != null) {
			buffer.append("[@ruleName = '");
			buffer.append(filterCriteria.getRuleName());
			buffer.append("']");
		}

		if (filterCriteria.getRelease() != null) {
			buffer.append("[@release = '");
			buffer.append(filterCriteria.getRelease());
			buffer.append("']");
		}

		if (filterCriteria.getEventDate() != null) {
			buffer.append("[@eventAt = '");
			buffer.append(filterCriteria.getEventDate().getStartDate().toXMLFormat());
			buffer.append("' and @eventAt <= '");
			buffer.append(filterCriteria.getEventDate().getEndDate().toXMLFormat());
			buffer.append("']");
		}

		if (filterCriteria.getRecordDate() != null) {
			buffer.append("[@recordAt >= '");
			buffer.append(filterCriteria.getRecordDate().getStartDate().toXMLFormat());
			buffer.append("' and @recordAt <= '");
			buffer.append(filterCriteria.getRecordDate().getEndDate().toXMLFormat());
			buffer.append("']");
		}

		buffer.append(" order by $entry/@");
		if (filterCriteria.getSorting() != null) {
			String[] sorting = filterCriteria.getSorting().split(" ");
			buffer.append(sorting[0]);			
			if (sorting.length == 2) {
				buffer.append(" ");
				buffer.append(sorting[1]);
			}
		} else {
			buffer.append("recordAt");			
		}
		buffer.append(" return $entry");

		StringBuilder limited = new StringBuilder("subsequence(");
		limited.append(buffer);
		limited.append(", ");
		limited.append((int) offset + 1); // because subsequence works with
											// start position, not offset
		limited.append(", ");
		limited.append(max);
		limited.append(")");

		StringBuilder counter = new StringBuilder("count(");
		counter.append(buffer);
		counter.append(")");

		QueryResult queryResult = new QueryResult();
		queryResult.setOffset(offset);
		queryResult.setCount(0D);
		queryResult.setLimit(max);

		try {
			Unmarshaller um = ctx.createUnmarshaller();
			ResourceSet result = xqueryService.query(limited.toString());
			ResourceIterator it = result.getIterator();
			while (it.hasMoreResources()) {
				Resource resource = it.nextResource();
				String content = (String) resource.getContent();
				ProtocolEntry entry = um.unmarshal(new StreamSource(new ByteArrayInputStream(content.getBytes(TBRS.CHARSET))), ProtocolEntry.class).getValue();
				queryResult.getProtocolEntries().add(entry);
			}
			ResourceSet counterSet = xqueryService.query(counter.toString());
			if (counterSet.getSize() > 0) {
				queryResult.setCount(Double.parseDouble((String) counterSet.getResource(0).getContent()));
			}
		} catch (JAXBException e) {
			log.error("unmarshal query result", e);
		} catch (XMLDBException e) {
			log.error("query protocol pool collection", e);
		}

		QueryProtocolResponse response = new QueryProtocolResponse();
		response.setQueryResult(queryResult);
		return response;
	}

	/* (non-Javadoc)
	 * @see de.p23r.server.protocolpool.ProtocolPoolService#addEntry(de.p23r.leitstelle.ns.p23r.commonprotocol1_0.ProtocolEntry)
	 */
	@Override
	public String addEntry(ProtocolEntry entry) {
		logEntry(entry);

		String id = null;
		try {
			id = protocolpool.createId();
			entry.setRecordId(id.substring(0, id.lastIndexOf(".")));
		} catch (XMLDBException e) {
			log.error("creating new protocol entry id", e);
		}

		try {
			entry.setRecordAt(DatatypeFactory.newInstance().newXMLGregorianCalendar(new GregorianCalendar()));
		} catch (DatatypeConfigurationException e) {
			log.error("setting protocol entry date and time", e);
		}

		try {
			Marshaller m = ctx.createMarshaller();
			StringWriter w = new StringWriter();
			m.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
			m.marshal(factory.createProtocolEntry(entry), w);

			P23RXmlDbResource protocolentry = new P23RXmlDbResource(id, protocolpool, false);
			protocolentry.setContent(w.toString());
			protocolentry.free();
		} catch (JAXBException e) {
			log.error("marshalling protocol entry", e);
		}

		return entry.getRecordId();
	}

	private void logEntry(ProtocolEntry entry) {
		if (entry.getContent().getLog() != null) {
			Log l = entry.getContent().getLog();
			String note = l.getNotes().isEmpty() ? "" : l.getNotes().get(0).getContent();
			switch (l.getLevel()) {
			case ERROR:
				log.error("{}: {}", l.getComponentName(), note);
				break;
			case INFO:
				log.info("{}: {}", l.getComponentName(), note);
				break;
			case WARNING:
				log.warn("{}: {}", l.getComponentName(), note);
				break;
			case FATAL:
			case SECURITY:
			case TEST_FAIL:
			case TEST_SUCCESS:
			default:
				log.info("{} ({}): {}", new Object[] { l.getComponentName(), l.getLevel().name(), note });
				break;
			}
		} else if (entry.getContent().getTrace() != null && log.isTraceEnabled()) {
			Trace t = entry.getContent().getTrace();
			String note = t.getNotes().isEmpty() ? "" : t.getNotes().get(0).getContent();
			if (!t.getNotes().isEmpty()) {
				log.trace("{}: {}", t.getStep(), note);
			}
		}
	}

}
