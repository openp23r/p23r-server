/**
 * Provides classes for the protocol pool service. It contains mainly an interface class describing the service interface,
 * a bean class that implements the service and some convenient helper classes for other components using this service.
 */
package de.p23r.server.protocolpool;