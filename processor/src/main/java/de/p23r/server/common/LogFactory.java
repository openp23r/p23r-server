package de.p23r.server.common;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A factory for creating Logger objects for injection.
 * 
 * @author sim
 */
public class LogFactory {

	/**
	 * Creates a new Logger object.
	 *
	 * @param injectionPoint the injection point
	 * @return the logger
	 */
	@Produces
	Logger createLogger(InjectionPoint injectionPoint) {
		return LoggerFactory.getLogger(injectionPoint.getMember().getDeclaringClass().getName());
	}

	
}
