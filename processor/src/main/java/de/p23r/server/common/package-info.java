/**
 * Provides common helper classes for the P23R processor
 */
package de.p23r.server.common;