package de.p23r.server.common;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class Messages serves as an internationalization approach providing texts based on the current locale.
 * 
 * @author sim
 */
public final class Messages {
	
	/**
	 * The Constant STARTUP_MESSAGE.
	 */
	public static final String STARTUP_MESSAGE = "Common.StartupMessage";
	
	/**
	 * The Constant GENERAL_ERROR.
	 */
	public static final String GENERAL_ERROR = "Common.GeneralError";
	
	/**
	 * The Constant PARAMETER_MISSING.
	 */
	public static final String PARAMETER_MISSING = "Common.ParameterMissing";
	
	/**
	 * The Constant INVALID_PARAMETER_VALUE.
	 */
	public static final String INVALID_PARAMETER_VALUE = "Common.InvalidParameterValue";
	
	/**
	 * The Constant SERVICE_NOT_AVAILABLE.
	 */
	public static final String SERVICE_NOT_AVAILABLE = "Common.ServiceNotAvailable";
	
	/**
	 * The Constant NO_RULE_FOUND.
	 */
	public static final String NO_RULE_FOUND = "Common.NoRuleFound";
	
	/**
	 * The Constant SCHEDULER.
	 */
	public static final String SCHEDULER = de.p23r.server.scheduler.SchedulerServiceBean.class.getName();
	
	/**
	 * The Constant SCHEDULER_ERROR_CANCELING_JOB.
	 */
	public static final String SCHEDULER_ERROR_CANCELING_JOB = "Scheduler.ErrorCancelingJob";
	
	/**
	 * The Constant SCHEDULER_ERROR_SCHEDULING_JOB.
	 */
	public static final String SCHEDULER_ERROR_SCHEDULING_JOB = "Scheduler.ErrorSchedulingJob";
	
	/**
	 * The Constant SCHEDULER_SCHEDULER_NOT_AVAILABLE.
	 */
	public static final String SCHEDULER_SCHEDULER_NOT_AVAILABLE = "Scheduler.SchedulerNotAvailable";
	
	/**
	 * The Constant SCHEDULER_SUCCESSFULLY_SCHEDULED_JOB.
	 */
	public static final String SCHEDULER_SUCCESSFULLY_SCHEDULED_JOB = "Scheduler.SuccessfullyScheduledJob";
	
	/**
	 * The Constant SCHEDULER_SUCCESSFULLY_TRIGGERED_JOB.
	 */
	public static final String SCHEDULER_SUCCESSFULLY_TRIGGERED_JOB = "Scheduler.SuccessfullyTriggeredJob";
	
	/**
	 * The Constant SCHEDULER_NO_TRIGGERS_FOUND.
	 */
	public static final String SCHEDULER_NO_TRIGGERS_FOUND = "Scheduler.NoTriggersFound";
	
	/**
	 * The Constant SCHEDULER_TRIGGER_NEXT.
	 */
	public static final String SCHEDULER_TRIGGER_NEXT = "Scheduler.TriggerNext";
	
	/**
	 * The Constant SCHEDULER_TRIGGER_FINAL.
	 */
	public static final String SCHEDULER_TRIGGER_FINAL = "Scheduler.TriggerFinal";

	/**
	 * The Constant DATAPOOL.
	 */
	public static final String DATAPOOL = de.p23r.server.datapool.DataPoolServiceBean.class.getName();
	
	/**
	 * The Constant DATAPOOL_DATASELECTION_RECEIVED.
	 */
	public static final String DATAPOOL_DATASELECTION_RECEIVED = "DataPool.DataSelectionReceived";
	
	/**
	 * The Constant DATAPOOL_DATASELECTION_SUCCESSFULL.
	 */
	public static final String DATAPOOL_DATASELECTION_SUCCESSFULL = "DataPool.DataSelectionSuccessfull";
	
	/**
	 * The Constant DATAPOOL_DATASELECTION_FAILED.
	 */
	public static final String DATAPOOL_DATASELECTION_FAILED = "DataPool.DataSelectionFailed";
	
	/**
	 * The Constant DATAPOOL_INVALID_CONFIGURATION_RESULT.
	 */
	public static final String DATAPOOL_INVALID_CONFIGURATION_RESULT = "DataPool.InvalidConfigurationResult";
	
	/**
	 * The Constant DATAPOOL_INVALID_GENERIC_RESULT.
	 */
	public static final String DATAPOOL_INVALID_GENERIC_RESULT = "DataPool.InvalidGenericResult";
	
	/**
	 * The Constant DATAPOOL_SELECTION_TRANSFORMATION_FAILED.
	 */
	public static final String DATAPOOL_SELECTION_TRANSFORMATION_FAILED = "DataPool.SelectionTransformationFailed";
	
	/**
	 * The Constant MESSAGERECEIPT.
	 */
	public static final String MESSAGERECEIPT = "MessageReceipt.Name";
	
	/**
	 * The Constant MESSAGERECEIPT_INVALID_MULTIPROFILE_RESULT.
	 */
	public static final String MESSAGERECEIPT_INVALID_MULTIPROFILE_RESULT = "MessageReceipt.InvalidMultiProfileResult";
	
	/**
	 * The Constant MESSAGERECEIPT_INVALID_MESSAGE_STRUCTURE.
	 */
	public static final String MESSAGERECEIPT_INVALID_MESSAGE_STRUCTURE = "MessageReceipt.InvalidMessageStructure";
	
	/**
	 * The Constant MESSAGERECEIPT_INVALID_MESSAGE_TYPE.
	 */
	public static final String MESSAGERECEIPT_INVALID_MESSAGE_TYPE = "MessageReceipt.InvalidMessageType";
	
	/**
	 * The Constant MESSAGERECEIPT_INVALID_MESSAGE_TYPE.
	 */
	public static final String MESSAGERECEIPT_RULE_NOT_ACTIVATED = "MessageReceipt.RuleNotActivated";
	
	/**
	 * The Constant MESSAGERECEIPT_GENERATION_INITIATED.
	 */
	public static final String MESSAGERECEIPT_GENERATION_INITIATED = "MessageReceipt.GenerationInitiated";
	
	/**
	 * The Constant MESSAGERECEIPT_GENERATION_PROFILES.
	 */
	public static final String MESSAGERECEIPT_GENERATION_PROFILES = "MessageReceipt.GenerationProfiles";
	
	/**
	 * The Constant MESSAGERECEIPT_MESSAGE_RECEIVED.
	 */
	public static final String MESSAGERECEIPT_MESSAGE_RECEIVED = "MessageReceipt.MessageReceived";

	/**
	 * The Constant MODELANDRULEMANAGEMENT.
	 */
	public static final String MODELANDRULEMANAGEMENT = de.p23r.server.modelandrulemanagement.ModelAndRuleManagementServiceBean.class.getName();
	
	/**
	 * The Constant MODELANDRULEMANAGEMENT_UPDATING_PACKAGELIST_FAILED.
	 */
	public static final String MODELANDRULEMANAGEMENT_UPDATING_PACKAGELIST_FAILED = "ModelAndRuleManagement.UpdatingPackageListFailed";
	
	/**
	 * The Constant MODELANDRULEMANAGEMENT_UPDATING_PACKAGELIST_SUCCESSFULL.
	 */
	public static final String MODELANDRULEMANAGEMENT_UPDATING_PACKAGELIST_SUCCESSFULL = "ModelAndRuleManagement.UpdatingPackageListSuccessfull";
	
	/**
	 * The Constant MODELANDRULEMANAGEMENT_RULE_INFORMATION_SUCCESSFULL.
	 */
	public static final String MODELANDRULEMANAGEMENT_RULE_INFORMATION_SUCCESSFULL = "ModelAndRuleManagement.RuleInformationSuccessfull";
	
	/**
	 * The Constant MODELANDRULEMANAGEMENT_RULE_UPTODATE_SUCCESSFULL.
	 */
	public static final String MODELANDRULEMANAGEMENT_RULE_UPTODATE_SUCCESSFULL = "ModelAndRuleManagement.RuleUptodateSuccessfull";
	
	/**
	 * The Constant MODELANDRULEMANAGEMENT_RULE_SELECTION_SUCCESSFULL.
	 */
	public static final String MODELANDRULEMANAGEMENT_RULE_SELECTION_SUCCESSFULL = "ModelAndRuleManagement.RuleSelectionSuccessfull";
	
	/**
	 * The Constant MODELANDRULEMANAGEMENT_RULE_TRANSFORMATION_SUCCESSFULL.
	 */
	public static final String MODELANDRULEMANAGEMENT_RULE_TRANSFORMATION_SUCCESSFULL = "ModelAndRuleManagement.RuleTransformationSuccessfull";
	
	/**
	 * The Constant MODELANDRULEMANAGEMENT_RULE_CORETRANSFORMATION_SUCCESSFULL.
	 */
	public static final String MODELANDRULEMANAGEMENT_RULE_CORETRANSFORMATION_SUCCESSFULL = "ModelAndRuleManagement.RuleCoreTransformationSuccessfull";
	
	/**
	 * The Constant MODELANDRULEMANAGEMENT_RULEPACKAGE_SELECTION_SUCCESSFULL.
	 */
	public static final String MODELANDRULEMANAGEMENT_RULEPACKAGE_SELECTION_SUCCESSFULL = "ModelAndRuleManagement.RulePackageSelectionSuccessfull";
	
	/**
	 * The Constant MODELANDRULEMANAGEMENT_RULEPACKAGE_DESELECTION_SUCCESSFULL.
	 */
	public static final String MODELANDRULEMANAGEMENT_RULEPACKAGE_DESELECTION_SUCCESSFULL = "ModelAndRuleManagement.RulePackageDeselectionSuccessfull";
	
	/**
	 * The Constant MODELANDRULEMANAGEMENT_RULEGROUP_ACTIVATION_SUCCESSFULL.
	 */
	public static final String MODELANDRULEMANAGEMENT_RULEGROUP_ACTIVATION_SUCCESSFULL = "ModelAndRuleManagement.RuleGroupActivationSuccessfull";
	
	/**
	 * The Constant MODELANDRULEMANAGEMENT_RULEGROUP_DEACTIVATION_SUCCESSFULL.
	 */
	public static final String MODELANDRULEMANAGEMENT_RULEGROUP_DEACTIVATION_SUCCESSFULL = "ModelAndRuleManagement.RuleGroupDeactivationSuccessfull";
	
	/**
	 * The Constant MODELANDRULEMANAGEMENT_RULE_DEACTIVATION_SUCCESSFULL.
	 */
	public static final String MODELANDRULEMANAGEMENT_RULE_DEACTIVATION_SUCCESSFULL = "ModelAndRuleManagement.RuleDeactivationSuccessfull";

	/**
	 * The Constant NOTIFICATIONGENERATION.
	 */
	public static final String NOTIFICATIONGENERATION = de.p23r.server.notificationgenerator.NotificationGeneratorServiceBean.class.getName();
	
	/**
	 * The Constant NOTIFICATIONGENERATION_INVALID_TRANSFORMATION_RESULT.
	 */
	public static final String NOTIFICATIONGENERATION_INVALID_TRANSFORMATION_RESULT = "NotificationGeneration.InvalidTransformationResult";
	
	/**
	 * The Constant NOTIFICATIONGENERATION_TRANSFORMATION_STEP_FINISHED.
	 */
	public static final String NOTIFICATIONGENERATION_TRANSFORMATION_STEP_FINISHED = "NotificationGeneration.TransformationStepFinished";
	
	/**
	 * The Constant NOTIFICATIONGENERATION_GENERATE_NOTIFICATION.
	 */
	public static final String NOTIFICATIONGENERATION_GENERATE_NOTIFICATION = "NotificationGeneration.GenerateNotification";
	
	/**
	 * The Constant NOTIFICATIONGENERATION_DATA_SELECTION_FAILED.
	 */
	public static final String NOTIFICATIONGENERATION_DATA_SELECTION_FAILED = "NotificationGeneration.DataSelectionFailed";
	
	/**
	 * The Constant NOTIFICATIONGENERATION_INVALID_DATA_SELECTION_RESULT.
	 */
	public static final String NOTIFICATIONGENERATION_INVALID_DATA_SELECTION_RESULT = "NotificationGeneration.InvalidDataSelectionResult";
	
	/**
	 * The Constant NOTIFICATIONGENERATION_NOTIFICATION_GENERATION_SUCCESSFULL.
	 */
	public static final String NOTIFICATIONGENERATION_NOTIFICATION_GENERATION_SUCCESSFULL = "NotificationGeneration.NotificationGenerationSuccessfull";
	
	/**
	 * The Constant NOTIFICATIONGENERATION_NOTIFICATION_GENERATION_FINISHED.
	 */
	public static final String NOTIFICATIONGENERATION_NOTIFICATION_GENERATION_FINISHED = "NotificationGeneration.NotificationGenerationFinished";

	/**
	 * The Constant NOTIFICATIONDISPATCH.
	 */
	public static final String NOTIFICATIONDISPATCH = de.p23r.server.notificationdispatcher.NotificationDispatcherServiceBean.class.getName();
	
	/**
	 * The Constant NOTIFICATIONDISPATCH_DISPATCHING_NOTIFICATION.
	 */
	public static final String NOTIFICATIONDISPATCH_DISPATCHING_NOTIFICATION = "NotificationDispatch.DispatchingNotification";
	
	/**
	 * The Constant NOTIFICATIONDISPATCH_NOTIFICATION_STORED.
	 */
	public static final String NOTIFICATIONDISPATCH_NOTIFICATION_STORED = "NotificationDispatch.NotificationStored";
	
	/**
	 * The Constant NOTIFICATIONDISPATCH_CONNECTING_CLIENT_FAILURE.
	 */
	public static final String NOTIFICATIONDISPATCH_CONNECTING_CLIENT_FAILURE = "NotificationDispatch.ConnectingClientFailure";
	
	/**
	 * The Constant NOTIFICATIONDISPATCH_NOTIFYING_CLIENT_SUCCESSFULL.
	 */
	public static final String NOTIFICATIONDISPATCH_NOTIFYING_CLIENT_SUCCESSFULL = "NotificationDispatch.NotifyingClientSuccessfull";
	
	/**
	 * The Constant NOTIFICATIONDISPATCH_NOTIFICATION_VALIDATION_FAILURE.
	 */
	public static final String NOTIFICATIONDISPATCH_NOTIFICATION_VALIDATION_FAILURE = "NotificationDispatch.NotificationValidationFailure";
	
	/**
	 * The Constant NOTIFICATIONDISPATCH_NOTIFICATION_VALIDATION_SUCCESS.
	 */
	public static final String NOTIFICATIONDISPATCH_NOTIFICATION_VALIDATION_SUCCESS = "NotificationDispatch.NotificationValidationSuccess";
	
	/**
	 * The Constant NOTIFICATIONDISPATCH_NOTIFICATION_APPROVED.
	 */
	public static final String NOTIFICATIONDISPATCH_NOTIFICATION_APPROVED = "NotificationDispatch.NotificationApproved";
	
	/**
	 * The Constant NOTIFICATIONDISPATCH_NOTIFICATION_SUCCESSFULLY_APPROVED.
	 */
	public static final String NOTIFICATIONDISPATCH_NOTIFICATION_SUCCESSFULLY_APPROVED = "NotificationDispatch.NotificationSuccessfullyApproved";
	
	/**
	 * The Constant NOTIFICATIONDISPATCH_NOTIFICATION_AUTOAPPROVED.
	 */
	public static final String NOTIFICATIONDISPATCH_NOTIFICATION_AUTOAPPROVED = "NotificationDispatch.NotificationAutoApproved";
	
	/**
	 * The Constant NOTIFICATIONDISPATCH_NOTIFICATION_SUCCESSFULLY_AUTOAPPROVED.
	 */
	public static final String NOTIFICATIONDISPATCH_NOTIFICATION_SUCCESSFULLY_AUTOAPPROVED = "NotificationDispatch.NotificationSuccessfullyAutoApproved";
	
	/**
	 * The Constant NOTIFICATIONDISPATCH_NOTIFICATION_SUCCESSFULLY_REJECTED.
	 */
	public static final String NOTIFICATIONDISPATCH_NOTIFICATION_SUCCESSFULLY_REJECTED = "NotificationDispatch.NotificationSuccessfullyRejected";
	
	/**
	 * The Constant NOTIFICATIONDISPATCH_MESSAGE_GET_SUCCESSFULL.
	 */
	public static final String NOTIFICATIONDISPATCH_MESSAGE_GET_SUCCESSFULL = "NotificationDispatch.MessageGetSuccessfull";
	
	/**
	 * The Constant NOTIFICATIONDISPATCH_NOTIFICATION_GET_SUCCESSFULL.
	 */
	public static final String NOTIFICATIONDISPATCH_NOTIFICATION_GET_SUCCESSFULL = "NotificationDispatch.NotificationGetSuccessfull";
	
	/**
	 * The Constant NOTIFICATIONDISPATCH_QUERY_ADDRESSES_SUCCESSFULL.
	 */
	public static final String NOTIFICATIONDISPATCH_QUERY_ADDRESSES_SUCCESSFULL = "NotificationDispatch.QueryAddressesSuccessfull";
	
	/**
	 * The Constant NOTIFICATIONDISPATCH_NOTIFICATION_CORE_SIGNING_FAILURE.
	 */
	public static final String NOTIFICATIONDISPATCH_NOTIFICATION_CORE_SIGNING_FAILURE = "NotificationDispatch.NotificationCoreSigningFailure";

	/**
	 * The Constant NOTIFICATIONTRANSPORT.
	 */
	public static final String NOTIFICATIONTRANSPORT = de.p23r.server.notificationtransporter.NotificationTransporterServiceBean.class.getName();
	
	/**
	 * The Constant NOTIFICATIONTRANSPORT_NOTIFICATION_SEND.
	 */
	public static final String NOTIFICATIONTRANSPORT_NOTIFICATION_SEND = "NotificationTransport.NotificationSend";
	
	/**
	 * The Constant NOTIFICATIONTRANSPORT_NO_CONNECTOR_FOUND.
	 */
	public static final String NOTIFICATIONTRANSPORT_NO_CONNECTOR_FOUND = "NotificationTransport.NoConnectorFound";
	
	/**
	 * The Constant NOTIFICATIONTRANSPORT_NOTIFICATION_SENT_INTERNALLY_SUCCESSFULL.
	 */
	public static final String NOTIFICATIONTRANSPORT_NOTIFICATION_SENT_INTERNALLY_SUCCESSFULL = "NotificationTransport.NotificationSentInternallySuccessfull";
	
	/**
	 * The Constant NOTIFICATIONTRANSPORT_CONNECTING_CONNECTOR_FAILED.
	 */
	public static final String NOTIFICATIONTRANSPORT_CONNECTING_CONNECTOR_FAILED = "NotificationTransport.ConnectingConnectorFailed";
	
	/**
	 * The Constant NOTIFICATIONTRANSPORT_CALLING_CONNECTOR.
	 */
	public static final String NOTIFICATIONTRANSPORT_CALLING_CONNECTOR = "NotificationTransport.CallingConnector";
	
	/**
	 * The Constant NOTIFICATIONTRANSPORT_NOTIFICATION_SENT_SUCCESSFULLY.
	 */
	public static final String NOTIFICATIONTRANSPORT_NOTIFICATION_SENT_SUCCESSFULLY = "NotificationTransport.NotificationSentSuccessfully";
	
	/**
	 * The Constant NOTIFICATIONTRANSPORT_NOTIFICATION_SENDING_FAILED.
	 */
	public static final String NOTIFICATIONTRANSPORT_NOTIFICATION_SENDING_FAILED = "NotificationTransport.NotificationSendingFailed";

	
	/**
	 * The Constant TESTMANAGER.
	 */
	public static final String TESTMANAGER = de.p23r.server.testmanager.TestManagerServiceBean.class.getName();
	
	/**
	 * The Constant TESTMANAGER_TESTPACKAGE_NOT_FOUND.
	 */
	public static final String TESTMANAGER_TESTPACKAGE_NOT_FOUND = "TestManager.TestPackageNotFound";
	
	/**
	 * The Constant TESTMANAGER_NO_DATA_FOR_NAMESPACE.
	 */
	public static final String TESTMANAGER_NO_DATA_FOR_NAMESPACE = "TestManager.NoDataForNamespace";
	
	/**
	 * The Constant TESTMANAGER_DATASELECTION_FAILED.
	 */
	public static final String TESTMANAGER_DATASELECTION_FAILED = "TestManager.DataSelectionFailed";
	
	/**
	 * The Constant PROTOCOLPOOL.
	 */
	public static final String PROTOCOLPOOL = de.p23r.server.protocolpool.ProtocolPoolServiceBean.class.getName();

	/**
	 * The Constant SI001E.
	 */
	public static final String SI001E = "P23RAppFault.SI001E";
	
	/**
	 * The Constant SI002E.
	 */
	public static final String SI002E = "P23RAppFault.SI002E";
	
	/**
	 * The Constant SI003E.
	 */
	public static final String SI003E = "P23RAppFault.SI003E";
	
	/**
	 * The Constant SI004E.
	 */
	public static final String SI004E = "P23RAppFault.SI004E";

	/**
	 * The Constant MR001.
	 */
	public static final String MR001 = "P23RAppFault.MR001";
	
	/**
	 * The Constant MR002.
	 */
	public static final String MR002 = "P23RAppFault.MR002";
	
	/**
	 * The Constant MR003E.
	 */
	public static final String MR003E = "P23RAppFault.MR003E";
	
	/**
	 * The Constant MR004E.
	 */
	public static final String MR004E = "P23RAppFault.MR004E";
	
	/**
	 * The Constant MR005E.
	 */
	public static final String MR005E = "P23RAppFault.MR005E";

	/**
	 * The Constant NM001.
	 */
	public static final String NM001 = "P23RAppFault.NM001";
	
	/**
	 * The Constant NM002.
	 */
	public static final String NM002 = "P23RAppFault.NM002";
	
	/**
	 * The Constant NM003.
	 */
	public static final String NM003 = "P23RAppFault.NM003";
	
	/**
	 * The Constant NM005.
	 */
	public static final String NM005 = "P23RAppFault.NM005";
	
	/**
	 * The Constant NM006.
	 */
	public static final String NM006 = "P23RAppFault.NM006";

	/**
	 * The Constant ND001.
	 */
	public static final String ND001 = "P23RAppFault.ND001";
	
	/**
	 * The Constant ND002.
	 */
	public static final String ND002 = "P23RAppFault.ND002";
	
	/**
	 * The Constant ND003.
	 */
	public static final String ND003 = "P23RAppFault.ND003";

	/**
	 * The Constant RF001.
	 */
	public static final String RF001 = "P23RAppFault.RF001";
	
	/**
	 * The Constant RF002.
	 */
	public static final String RF002 = "P23RAppFault.RF002";
	
	/**
	 * The Constant RF003.
	 */
	public static final String RF003 = "P23RAppFault.RF003";

	/**
	 * The Constant SD001.
	 */
	public static final String SD001 = "P23RAppFault.SD001";
	
	/**
	 * The Constant SD002.
	 */
	public static final String SD002 = "P23RAppFault.SD002";
	
	/**
	 * The Constant SD003.
	 */
	public static final String SD003 = "P23RAppFault.SD003";
	
	/**
	 * The Constant SD004.
	 */
	public static final String SD004 = "P23RAppFault.SD004";

	/**
	 * The Constant PP001.
	 */
	public static final String PP001 = "P23RAppFault.PP001";
	
	/**
	 * The Constant PP002.
	 */
	public static final String PP002 = "P23RAppFault.PP002";
	
	/**
	 * The Constant PP003.
	 */
	public static final String PP003 = "P23RAppFault.PP003";
	
	/**
	 * The Constant PP004.
	 */
	public static final String PP004 = "P23RAppFault.PP004";

	/**
	 * The Constant NT001.
	 */
	public static final String NT001 = "P23RAppFault.NT001";
	
	/**
	 * The Constant NT002.
	 */
	public static final String NT002 = "P23RAppFault.NT002";
	
	/**
	 * The Constant NT003.
	 */
	public static final String NT003 = "P23RAppFault.NT003";
	
	/**
	 * The Constant NT004.
	 */
	public static final String NT004 = "P23RAppFault.NT004";
	
	/**
	 * The Constant NT005.
	 */
	public static final String NT005 = "P23RAppFault.NT005";
	
	/**
	 * The Constant NT006.
	 */
	public static final String NT006 = "P23RAppFault.NT006";

	/**
	 * The Constant MRM001.
	 */
	public static final String MRM001 = "P23RAppFault.MRM001";
	
	/**
	 * The Constant MRM002.
	 */
	public static final String MRM002 = "P23RAppFault.MRM002";

	private static final String BUNDLE_NAME = "messages";
	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);

	private static final Logger LOG = LoggerFactory.getLogger(Messages.class);
	
	private Messages() {
	}

	/**
	 * Message.
	 *
	 * @param key the key
	 * @return the string
	 */
	public static String message(String key) {
		if (RESOURCE_BUNDLE.containsKey(key)) {
			try {
				return RESOURCE_BUNDLE.getString(key);
			} catch (MissingResourceException e) {
				LOG.warn("looking for message", e);
			}			
		}
		return '!' + key + '!';
	}

	/**
	 * Message.
	 *
	 * @param key the key
	 * @param substitutes the substitutes
	 * @return the string
	 */
	public static String message(String key, Object[] substitutes) {
		if (RESOURCE_BUNDLE.containsKey(key)) {
			try {
				MessageFormat formatter = new MessageFormat(RESOURCE_BUNDLE.getString(key));
				return formatter.format(substitutes);
			} catch (MissingResourceException e) {
				LOG.warn("looking for message", e);
			}			
		}
		return '!' + key + '!';
	}

}
