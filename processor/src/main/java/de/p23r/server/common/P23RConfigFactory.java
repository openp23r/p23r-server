package de.p23r.server.common;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Inject;

import de.p23r.common.existdb.SystemConfiguration;
import de.p23r.server.common.annotations.P23RConfig;

/**
 * A factory for creating P23RConfig objects.
 * 
 * @author sim
 */
public class P23RConfigFactory {

	@Inject
	private SystemConfiguration systemConfiguration;
	
	/**
	 * Creates a new P23RConfig object.
	 *
	 * @param injectionPoint the injection point
	 * @return the string
	 */
	@Produces @P23RConfig(value = "", defaultValue = "")
	String createConfigValue(InjectionPoint injectionPoint) {
		String xpath = injectionPoint.getAnnotated().getAnnotation(P23RConfig.class).value();
		String defaultValue = injectionPoint.getAnnotated().getAnnotation(P23RConfig.class).defaultValue();
		return systemConfiguration.query(xpath, defaultValue);
	}
	
}
