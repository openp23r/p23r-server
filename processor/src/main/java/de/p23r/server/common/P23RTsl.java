package de.p23r.server.common;

import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.p23r.common.existdb.P23RXmlDbConfig;
import de.p23r.server.security.P23RKeyStoreException;
import de.p23r.server.security.tsl.P23RTrustServiceList;
import de.p23r.server.security.tsl.TrustServiceListDepot;
import de.p23r.server.security.tsl.TrustServiceListException;
import de.p23r.server.security.tsl.TrustServiceListDepot.TslResource;
import de.p23r.server.security.tsl.core.TrustService;
import de.p23r.server.security.tsl.core.TrustServiceProvider;

/**
 * The Class P23RTsl.
 */
@ApplicationScoped
public class P23RTsl implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5192064965919821349L;

	private final Logger log = LoggerFactory.getLogger(getClass());
	
	private Map<String, String> addresses = new HashMap<String, String>();

	@Inject
	private P23RXmlDbConfig p23rConfig;
	
	private String discoverServiceEndpointAddress(String clientConfig, String name) {
		String key = clientConfig + "_" + name;
		if (addresses.containsKey(key)) {
			return addresses.get(key);
		}
		
		String tsladdress = p23rConfig.get(String.format(P23RXmlDbConfig.XQUERY_TSL_URL,  clientConfig), "");
		String username = p23rConfig.get(String.format(P23RXmlDbConfig.XQUERY_USERNAME,  clientConfig), "");
		String password = p23rConfig.get(String.format(P23RXmlDbConfig.XQUERY_PASSWORD,  clientConfig), "");
		
		try {
			URI tslendpoint = new URI(tsladdress);
			String supplyAddress = null;
			TrustServiceListDepot tsldepot = new TrustServiceListDepot();
			P23RTrustServiceList serviceList = tsldepot.get(new TslResource("P23R", tslendpoint, username, password));
			for (TrustServiceProvider provider : serviceList.getTrustServiceProviders()) {
				TrustService service = provider.findTrustService(name);
				if (service != null) {
					supplyAddress = service.getTSPService().getServiceInformation().getServiceSupplyPoints().getServiceSupplyPoint().get(0);
					break;
				}
			}
			if (supplyAddress != null) {
				addresses.put(key,  supplyAddress);
			}
			return supplyAddress;
		} catch (URISyntaxException e) {
			log.error("bootstrapping tsl failure", e);
		} catch (TrustServiceListException e) {
			log.error("processing tsl failure", e);
		} catch (P23RKeyStoreException e) {
			log.error("processing tsl failure", e);
		}
		return null;
	}

	/**
	 * Discover model and rule depot address.
	 *
	 * @param clientConfig the client config
	 * @return the string
	 */
	public String discoverModelAndRuleDepotAddress(String clientConfig) {
		String modelandruledepot = p23rConfig.get(String.format(P23RXmlDbConfig.XQUERY_MODELANDRULEDEPOT_TSLSERVICENAME, clientConfig), "");
		return discoverServiceEndpointAddress(clientConfig, modelandruledepot);
	}

}
