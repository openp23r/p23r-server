package de.p23r.server.common;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * The Class GenerationContext.
 *
 * @author sim
 */
public class GenerationContext implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1395106629826237821L;

	/**
	 * The Constant TENANT.
	 */
	public static final String TENANT = "tenant";

	/**
	 * The Constant SOURCEINTERFACE.
	 */
	public static final String SOURCEINTERFACE = "sourceinterface";

	/**
	 * The Constant SOURCEURI.
	 */
	public static final String SOURCEURI = "sourceuri";
	
	/**
	 * The Constant MESSAGE.
	 */
	public static final String MESSAGE = "message";

	/**
	 * The Constant ORIGINATORID.
	 */
	public static final String ORIGINATORID = "originatorid";

	/**
	 * The Constant SELECTION_NAMESPACE.
	 */
	public static final String SELECTION_NAMESPACE = "selection_namespace";
	
	/**
	 * The Constant SELECTION_SCRIPT.
	 */
	public static final String SELECTION_SCRIPT = "selection_script";
	
	/**
	 * The Constant SELECTION_PARAMETER.
	 */
	public static final String SELECTION_PARAMETER = "selection_parameter";

	private NotificationProfileHelper profile;
	
	private String notification;

	private String clientConfig;

	private String testpackage;
	
	private String testset;
	
	private String testcase;

	private Map<String, String> properties = new HashMap<String, String>();
	
	/**
	 * Gets the profile.
	 *
	 * @return the profile
	 */
	public NotificationProfileHelper getProfile() {
		return profile;
	}

	/**
	 * Sets the profile.
	 *
	 * @param profile the new profile
	 */
	public void setProfile(NotificationProfileHelper profile) {
		this.profile = profile;
	}

	/**
	 * Gets the notification.
	 *
	 * @return the notification
	 */
	public String getNotification() {
		return notification;
	}

	/**
	 * Sets the notification.
	 *
	 * @param notification the new notification
	 */
	public void setNotification(String notification) {
		this.notification = notification;
	}

	/**
	 * Gets the client config.
	 *
	 * @return the client config
	 */
	public String getClientConfig() {
		return clientConfig;
	}

	/**
	 * Sets the client config.
	 *
	 * @param clientConfig the new client config
	 */
	public void setClientConfig(String clientConfig) {
		this.clientConfig = clientConfig;
	}

	/**
	 * Gets the testpackage.
	 *
	 * @return the testpackage
	 */
	public String getTestpackage() {
		return testpackage;
	}

	/**
	 * Sets the testpackage.
	 *
	 * @param testpackage the new testpackage
	 */
	public void setTestpackage(String testpackage) {
		this.testpackage = testpackage;
	}

	/**
	 * Gets the testset.
	 *
	 * @return the testset
	 */
	public String getTestset() {
		return testset;
	}

	/**
	 * Sets the testset.
	 *
	 * @param testset the new testset
	 */
	public void setTestset(String testset) {
		this.testset = testset;
	}

	/**
	 * Gets the testcase.
	 *
	 * @return the testcase
	 */
	public String getTestcase() {
		return testcase;
	}

	/**
	 * Sets the testcase.
	 *
	 * @param testcase the new testcase
	 */
	public void setTestcase(String testcase) {
		this.testcase = testcase;
	}

	/**
	 * Checks if is test.
	 *
	 * @return true, if is test
	 */
	public boolean isTest() {
		return testpackage != null && !testpackage.isEmpty();
	}

	/**
	 * Gets the properties.
	 *
	 * @return the properties
	 */
	public Map<String, String> getProperties() {
		return properties;
	}
	
}
