/**
 * Provides annotation definition classes for the P23R processor.
 */
package de.p23r.server.common.annotations;