package de.p23r.server.common;

import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault;
import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault_Exception;

/**
 * The Class P23RFaultConstants.
 */
public final class P23RFaultConstants {

	private P23RFaultConstants() {
	}
	
	/**
	 * The Constant INTERNAL_SYSTEM_ERROR.
	 */
	public static final String INTERNAL_SYSTEM_ERROR = "INTERNAL_SYSTEM_ERROR";

	/**
	 * The Constant PARAMETER_MISSING_CODE.
	 */
	public static final String PARAMETER_MISSING_CODE = "PARAMETER_MISSING";
	
	/**
	 * The Constant PARAMETER_MISSING_TEXT.
	 */
	public static final String PARAMETER_MISSING_TEXT = "Parameter %s is missing";

	/**
	 * The Constant PARAMETER_INVALID_CODE.
	 */
	public static final String PARAMETER_INVALID_CODE = "PARAMETER_INVALID";
	
	/**
	 * The Constant PARAMETER_INVALID_TEXT.
	 */
	public static final String PARAMETER_INVALID_TEXT = "Parameter %s is invalid";

	/**
	 * The Constant RULE_UNKNOWN_CODE.
	 */
	public static final String RULE_UNKNOWN_CODE = "RULE_UNKNOWN";
	
	/**
	 * The Constant RULE_UNKNOWN_TEXT.
	 */
	public static final String RULE_UNKNOWN_TEXT = "Rule with namespace %s is not known";

	/**
	 * The Constant RULE_NOTACTIVATED_CODE.
	 */
	public static final String RULE_NOTACTIVATED_CODE = "RULE_NOTACTIVATED";
	
	/**
	 * The Constant RULE_NOTACTIVATED_TEXT.
	 */
	public static final String RULE_NOTACTIVATED_TEXT = "Rule with namespace %s is not activated";

	/**
	 * The Constant RULE_NOTACTIVE_CODE.
	 */
	public static final String RULE_NOTACTIVE_CODE = "RULE_NOTACTIVE";
	
	/**
	 * The Constant RULE_NOTACTIVE_TEXT.
	 */
	public static final String RULE_NOTACTIVE_TEXT = "Rule with namespace %s is not active";

	/**
	 * The Constant RULE_UNUSABLE_CODE.
	 */
	public static final String RULE_UNUSABLE_CODE = "RULE_UNUSABLE";
	
	/**
	 * The Constant RULE_UNUSABLE_TEXT.
	 */
	public static final String RULE_UNUSABLE_TEXT = "Rule with namespace %s is not usable";

	/**
	 * Throw p23 r app fault.
	 *
	 * @param code the code
	 * @param description the description
	 * @param parameter the parameter
	 * @throws P23RAppFault_Exception the p23 r app fault_ exception
	 */
	public static void throwP23RAppFault(String code, String description, Object... parameter) throws P23RAppFault_Exception {
		P23RAppFault fault = new P23RAppFault();
		fault.setCode(code);
		if (parameter.length > 0) {
			fault.setDescription(String.format(description, parameter));			
		} else {
			fault.setDescription(description);						
		}
		throw new P23RAppFault_Exception("Processor", fault);
	}
	
}
