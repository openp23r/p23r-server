package de.p23r.server.common.compiler;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.inject.Inject;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.p23r.common.existdb.P23RXmlDbConfig;
import de.p23r.server.common.TBRS;

/**
 * The Class P23RSelectionCompiler.
 */
public class P23RSelectionCompiler {
	private static final Logger log = LoggerFactory.getLogger(P23RSelectionCompiler.class);

	private String command;

	private String newScript;
	
	private String errors;

	@Inject
	private P23RXmlDbConfig p23rXmlDbConfig;
	
	private P23RSelectionCompiler() {
	}

	private P23RSelectionCompiler(String command) {
		this.command = command;
	}
	
	/**
	 * Checks if is p23 r selection.
	 *
	 * @param selection the selection
	 * @return true, if is p23 r selection
	 */
	public static boolean isP23RSelection(String selection) {
		return false;
	}

	/**
	 * New compiler.
	 *
	 * @param command the command
	 * @return the p23 r selection compiler
	 */
	public static P23RSelectionCompiler newCompiler(String command) {
		return new P23RSelectionCompiler(command);
	}
	
	/**
	 * Compile selection.
	 *
	 * @param script the script
	 * @param options the options
	 * @return the string
	 */
	public String compileSelection(String script, String... options) {
		if (command == null) {
			command = p23rXmlDbConfig.get(P23RXmlDbConfig.XQUERY_GENERAL_SELECTIONCOMPILER, "");			
		}
		try {
			StringBuffer finalcommand = new StringBuffer(command);
			if (options.length > 0) {
				for (String option : options) {
					finalcommand.append(" ");
					finalcommand.append(option);
				}
			}
			final Process cmd = Runtime.getRuntime().exec(finalcommand.toString());
			new Thread(new Runnable() {
				public void run() {
					try (InputStream inStream = cmd.getInputStream()) {
						newScript = IOUtils.toString(inStream, TBRS.CHARSET);
					} catch (IOException e) {
						log.error("input stream", e);
					}
				}
			}).start();

			new Thread(new Runnable() {
				public void run() {
					try (InputStream errStream = cmd.getErrorStream()) {
						errors = IOUtils.toString(errStream, TBRS.CHARSET);
						if (errors != null && !errors.isEmpty()) {
							log.error(errors);							
						}
					} catch (IOException e) {
						log.error("input stream", e);
					}
				}
			}).start();

			try (OutputStream outStream = cmd.getOutputStream()) {
				IOUtils.write(script, outStream, TBRS.CHARSET);
			} catch (IOException e) {
				log.error("output stream", e);
			}
			
			cmd.waitFor();
		} catch (IOException | InterruptedException e) {
			log.error("process cmd", e);
		}
		return newScript;
	}

	/**
	 * Compile selection.
	 *
	 * @param script the script
	 * @param options the options
	 * @return the string
	 */
	public String compileSelection(InputStream script, String... options) {
		if (command == null) {
			command = p23rXmlDbConfig.get(P23RXmlDbConfig.XQUERY_GENERAL_SELECTIONCOMPILER, "");			
		}
		try {
			StringBuffer finalcommand = new StringBuffer(command);
			if (options.length > 0) {
				for (String option : options) {
					finalcommand.append(" ");
					finalcommand.append(option);
				}
			}
			final Process cmd = Runtime.getRuntime().exec(finalcommand.toString());
			new Thread(new Runnable() {
				public void run() {
					try (InputStream inStream = cmd.getInputStream()) {
						newScript = IOUtils.toString(inStream, TBRS.CHARSET);
					} catch (IOException e) {
						log.error("input stream", e);
					}
				}
			}).start();

			new Thread(new Runnable() {
				public void run() {
					try (InputStream errStream = cmd.getErrorStream()) {
						errors = IOUtils.toString(errStream, TBRS.CHARSET);
						if (errors != null && !errors.isEmpty()) {
							log.error(errors);							
						}
					} catch (IOException e) {
						log.error("input stream", e);
					}
				}
			}).start();

			try (OutputStream outStream = cmd.getOutputStream()) {
				IOUtils.copy(script, outStream);
			} catch (IOException e) {
				log.error("output stream", e);
			}
			
			cmd.waitFor();
		} catch (IOException | InterruptedException e) {
			log.error("process cmd", e);
		}
		return newScript;		
	}
	
	/**
	 * Checks if is failure.
	 *
	 * @return true, if is failure
	 */
	public boolean isFailure() {
		return errors != null && !errors.isEmpty();
	}
	
	/**
	 * Gets the errors.
	 *
	 * @return the errors
	 */
	public String getErrors() {
		return errors;
	}
	
}
