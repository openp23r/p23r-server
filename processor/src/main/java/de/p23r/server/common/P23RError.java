package de.p23r.server.common;

/**
 * The Enum P23RError.
 * 
 * @author sim
 */
public enum P23RError {
	
	/**
	 * The COMMUNICATION_FAILED - error code for communication error.
	 */
	GENERAL("general failure"),
	
	/**
	 * The COMMUNICATION_FAILED - error code for communication error.
	 */
	COMMUNICATION_FAILED("communicaton failed"),
	
	/**
	 * The PARAMETER_MISSING - error code for missing parameter.
	 */
	PARAMETER_MISSING("parameter missing"),
	
	/**
	 * The PARAMETER_INVALID - error code for missing parameter.
	 */
	PARAMETER_INVALID("parameter invalid"),
	
	/**
	 * The GENERATION_FAILURE - error code for generation.
	 */
	GENERATION_FAILURE("generation failed"),
	
	/**
	 * The VALIDATION_FAILURE - error code for validation.
	 */
	VALIDATION_FAILURE("validation failed"),
	
	/**
	 * The TRANSFORMATION_FAILURE - error code for transformation.
	 */
	TRANSFORMATION_FAILURE("transformation failed"),
	
	/**
	 * The SELECTION_FAILURE - error code for selection.
	 */
	SELECTION_FAILURE("selection failed");
	
	private String description;
	
	private P23RError(String description) {
		this.description = description;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Enum#toString()
	 */
	public String toString() {
		return description;
	}
	
    /**
     * From description.
     *
     * @param v the v
     * @return the p23 r error
     */
    public static P23RError fromDescription(String v) {
        for (P23RError c: P23RError.values()) {
            if (c.description.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
