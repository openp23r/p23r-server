package de.p23r.server.common;

import static de.p23r.server.common.Messages.STARTUP_MESSAGE;
import static de.p23r.server.common.Messages.message;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Initialized;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.switchyard.component.bean.Reference;

import de.p23r.common.existdb.P23RXmlDbConfig;
import de.p23r.common.existdb.SystemConfiguration;
import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault_Exception;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.GetNotificationRulePackageStates;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.GetNotificationRulePackageStatesResponse;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.SetNotificationRulePackageStates;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.SetNotificationRulePackageStatesResponse;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.UpdateNotificationRulePackages;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.types.RuleGroupStateType;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.types.RulePackageStateResultType;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.types.RulePackageStateType;
import de.p23r.server.common.annotations.P23RUnit;
import de.p23r.server.modelandrulemanagement.ModelAndRuleManagementService;

/**
 * The Class Monitor receives unitInitialized events from all components and finally logs an info message when every unit (9) is initialized.
 * 
 * @author sim
 */
@ApplicationScoped
public class Monitor {

	@Inject
	private Logger log;

	@Inject
	private SystemConfiguration systemConfiguration;

	@Inject
	@Reference
	private ModelAndRuleManagementService marm;
	
	@Inject
	private P23RXmlDbConfig config;

	/**
	 * The count.
	 */
	int count = 0;
	
	/**
	 * Unit initialized.
	 *
	 * @param name the name of the sending unit
	 */
	public void unitInitialized(@Observes @P23RUnit String name) {
		++count;

		log.info(message(STARTUP_MESSAGE, new String[] { name }));
		if (count == 9) {
			log.info("");
			log.info("************************************************************");
			log.info("{} - Version {}", systemConfiguration.getProduct(), systemConfiguration.getProductRelease());
			log.info("Vendor: {}", systemConfiguration.getVendor());
			log.info("Provider: {}", systemConfiguration.getProvider());
			log.info("Id: {}", systemConfiguration.getP23RId());
			log.info("************************************************************");
			log.info("");

			String autoRuleActivate = config.get("/configuration/controlcentres/controlcentre[@id = 'default']/autoRuleActivate/text()", null);
			if (autoRuleActivate != null && !autoRuleActivate.trim().isEmpty()) {
				log.info("Auto activating packages");
				initPackages();
			}
			
		}
		
	}

	public void init(@Observes @Initialized(ApplicationScoped.class) Object init) {
		log.info("Application initlialized");
	}
	
	private void initPackages() {
		// from config get all tenants and loop over them
		UpdateNotificationRulePackages updateRequest = new UpdateNotificationRulePackages();
		updateRequest.setTenant("default");
		updateRequest.setUpdateInformation("boot update");
		try {
			marm.updateNotificationRulePackages(updateRequest);
		} catch (P23RAppFault_Exception e) {
			log.error("boot update from depot", e);
			return;
		}

		GetNotificationRulePackageStatesResponse getStatesResponse = null;
		GetNotificationRulePackageStates getStatesRequest = new GetNotificationRulePackageStates();
		getStatesRequest.setTenant("default");
		try {
			getStatesResponse = marm.getNotificationRulePackageStates(getStatesRequest);
		} catch (P23RAppFault_Exception e) {
			log.error("retreiving state information", e);
			return;
		}

		SetNotificationRulePackageStates setStatesRequest = new SetNotificationRulePackageStates();
		setStatesRequest.setTenant("default");

		RulePackageStateResultType states = getStatesResponse.getResult();
		for (RulePackageStateType packageState : states.getPackageStates()) {
			if (packageState.isIsSupported()) {
				packageState.setIsSelected(true);
				log.info("Selecting package with id {}", packageState.getId());
				setStatesRequest.getPackageStates().add(packageState);
			}
		}
		
		SetNotificationRulePackageStatesResponse setStatesResponse = new SetNotificationRulePackageStatesResponse();
		try {
			setStatesResponse = marm.setNotificationRulePackageStates(setStatesRequest);
		} catch (P23RAppFault_Exception e) {
			log.error("selecting packages", e);
			return;
		}

		setStatesRequest = new SetNotificationRulePackageStates();
		setStatesRequest.setTenant("default");

		states = setStatesResponse.getResult();
		for (RulePackageStateType packageState : states.getPackageStates()) {
			if (packageState.isIsSelected()) {
				for (RuleGroupStateType groupState : packageState.getRuleGroups()) {
					groupState.setIsActivated(true);
					log.info("Activating group with id {}", groupState.getId());
				}
				setStatesRequest.getPackageStates().add(packageState);
			}
		}		
		
		setStatesResponse = new SetNotificationRulePackageStatesResponse();
		try {
			setStatesResponse = marm.setNotificationRulePackageStates(setStatesRequest);
		} catch (P23RAppFault_Exception e) {
			log.error("activating rule groups", e);
			return;
		}
		
	}
	
}
