package de.p23r.server.common;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The class SimpleTransformer.
 */
public abstract class SimpleTransformer {

    private static final Logger LOG = LoggerFactory.getLogger(SimpleTransformer.class);

    private static final TransformerFactory FACTORY = TransformerFactory.newInstance();

    /**
     * Hide public default constructor
     */
    private SimpleTransformer() {
    }

    /**
     * Transforms the content by the transformation script.
     *
     * @param content the content
     * @param transformation the transformation script
     * @param format pretty format or not
     * @return transformed content
     */
    public static String transform(String content, String transformation, boolean format) {
        try {
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            Transformer transformer = FACTORY.newTransformer(new StreamSource(new ByteArrayInputStream(
                transformation.getBytes(Charset.forName("UTF-8")))));
            if (format) {
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            }
            transformer.transform(
                new StreamSource(new ByteArrayInputStream(content.getBytes(Charset.forName("UTF-8")))),
                new StreamResult(output));
            return output.toString("UTF-8");
        } catch (TransformerConfigurationException e) {
            LOG.error("transforming content", e);
        } catch (TransformerException e) {
            LOG.error("transforming content", e);
        } catch (UnsupportedEncodingException e) {
            LOG.error("utf-8 encoding is not supported", e);
        }
        return null;
    }

}
