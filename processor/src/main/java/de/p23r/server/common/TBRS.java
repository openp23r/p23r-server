package de.p23r.server.common;

import java.nio.charset.Charset;

/**
 * The Class TBRS defines constant values from the TBRS (Technische Benachrichtigungsregelsprache).
 */
public final class TBRS {
	
	private TBRS() {
	}

	/**
	 * The Name of the charset to use.
	 */
	public static final String CHARSET_NAME = "UTF-8";
	
	/**
	 * The Object Charset to use.
	 */
	public static final Charset CHARSET = Charset.forName(CHARSET_NAME);
	
	/**
	 * The extension for xquery selection filenames.
	 */
	public static final String SELECTION_XQUERY_SUFFIX = ".xquery";
	
	/**
	 * The extension for p23r selection filenames.
	 */
	public static final String SELECTION_DATSEL_SUFFIX = ".dats";
	
	/**
	 * The extension for the actual used selection filenames.
	 */
	public static final String SELECTION_SUFFIX = SELECTION_DATSEL_SUFFIX;
	
	/**
	 * The regular expression for the actual used selection filenames.
	 */
	public static final String REGEX_SELECTION_SUFFIX = "\\" + SELECTION_SUFFIX;

	/**
	 * The Constant TRANSFORMATION_SUFFIX.
	 */
	public static final String TRANSFORMATION_SUFFIX = ".xslt";
	
	/**
	 * The Constant REGEX_TRANSFORMATION_SUFFIX.
	 */
	public static final String REGEX_TRANSFORMATION_SUFFIX = "\\.xsl[t]?";

	/**
	 * The Constant SCHEMA_SUFFIX.
	 */
	public static final String SCHEMA_SUFFIX = ".xsd";
	
	/**
	 * The Constant REGEX_SCHEMA_SUFFIX.
	 */
	public static final String REGEX_SCHEMA_SUFFIX = "\\" + SCHEMA_SUFFIX;

	/**
	 * The Constant XML_SUFFIX.
	 */
	public static final String XML_SUFFIX = ".xml";
	
	/**
	 * The Constant REGEX_XML_SUFFIX.
	 */
	public static final String REGEX_XML_SUFFIX = "\\" + XML_SUFFIX;

	/**
	 * The Constant REGEX_REPRESENTATION.
	 */
	public static final String REGEX_REPRESENTATION = "representation[^/]+";
	
	/**
	 * The Constant MANIFEST_NAME.
	 */
	public static final String MANIFEST_NAME = "Manifest";
	
	/**
	 * The Constant MANIFEST.
	 */
	public static final String MANIFEST = MANIFEST_NAME + XML_SUFFIX;

	/**
	 * The Constant MESSAGE_NAME.
	 */
	public static final String MESSAGE_NAME = "Message";
	
	/**
	 * The Constant MESSAGE_SCHEMA.
	 */
	public static final String MESSAGE_SCHEMA = MESSAGE_NAME + SCHEMA_SUFFIX;

	/**
	 * The Constant RECOMMENDATION_NAME.
	 */
	public static final String RECOMMENDATION_NAME = "Recommendation";
	
	/**
	 * The Constant RECOMMENDATIONSELECTION.
	 */
	public static final String RECOMMENDATIONSELECTION = RECOMMENDATION_NAME + SELECTION_SUFFIX;
	
	/**
	 * The Constant RECOMMENDATIONTRANSFORMATION.
	 */
	public static final String RECOMMENDATIONTRANSFORMATION = RECOMMENDATION_NAME + TRANSFORMATION_SUFFIX;

	/**
	 * The Constant SELECTION_NAME.
	 */
	public static final String SELECTION_NAME = "Selection";
	
	/**
	 * The Constant SELECTION.
	 */
	public static final String SELECTION = SELECTION_NAME + SELECTION_SUFFIX;
	
	/**
	 * The Constant SELECTION_SCHEMA.
	 */
	public static final String SELECTION_SCHEMA = SELECTION_NAME + SCHEMA_SUFFIX;

	/**
	 * The Constant CORETRANSFORMATION_NAME.
	 */
	public static final String CORETRANSFORMATION_NAME = "CoreTransformation";
	
	/**
	 * The Constant CORETRANSFORMATION.
	 */
	public static final String CORETRANSFORMATION = CORETRANSFORMATION_NAME + TRANSFORMATION_SUFFIX;

	/**
	 * The Constant CONFIGURATION_NAME.
	 */
	public static final String CONFIGURATION_NAME = "Configuration";
	
	/**
	 * The Constant CONFIGURATIONSELECTION.
	 */
	public static final String CONFIGURATIONSELECTION = CONFIGURATION_NAME + SELECTION_SUFFIX;
	
	/**
	 * The Constant CONFIGURATIONTRANSFORMATION.
	 */
	public static final String CONFIGURATIONTRANSFORMATION = CONFIGURATION_NAME + TRANSFORMATION_SUFFIX;
		
	/**
	 * The Constant MULTINOTIFICATIONPROFILE_NAME.
	 */
	public static final String MULTINOTIFICATIONPROFILE_NAME = "MultiNotificationProfile";
	
	/**
	 * The Constant MULTINOTIFICATIONPROFILESELECTION.
	 */
	public static final String MULTINOTIFICATIONPROFILESELECTION = MULTINOTIFICATIONPROFILE_NAME + SELECTION_SUFFIX;
	
	/**
	 * The Constant MULTINOTIFICATIONPROFILETRANSFORMATION.
	 */
	public static final String MULTINOTIFICATIONPROFILETRANSFORMATION = MULTINOTIFICATIONPROFILE_NAME + TRANSFORMATION_SUFFIX;
	
	/**
	 * The Constant NAMESPACE_RULEPACKAGEMANIFEST.
	 */
	public static final String NAMESPACE_RULEPACKAGEMANIFEST = "http://leitstelle.p23r.de/NS/p23r/nrl/RulePackageManifest1-1";
	
	/**
	 * The Constant NAMESPACE_RULEGROUPMANIFEST.
	 */
	public static final String NAMESPACE_RULEGROUPMANIFEST = "http://leitstelle.p23r.de/NS/p23r/nrl/RuleGroupManifest1-1";
	
	/**
	 * The Constant NAMESPACE_RULEMANIFEST.
	 */
	public static final String NAMESPACE_RULEMANIFEST = "http://leitstelle.p23r.de/NS/p23r/nrl/RuleManifest1-1";
	
	/**
	 * The Constant NAMESPACE_MODELPACKAGEMANIFEST.
	 */
	public static final String NAMESPACE_MODELPACKAGEMANIFEST = "http://leitstelle.p23r.de/NS/p23r/nrl/ModelPackageManifest1-1";
	
	/**
	 * The Constant NAMESPACE_MODELMANIFEST.
	 */
	public static final String NAMESPACE_MODELMANIFEST = "http://leitstelle.p23r.de/NS/p23r/nrl/ModelManifest1-1";
	
	/**
	 * The Constant NAMESPACE_PACKAGELISTMANIFEST.
	 */
	public static final String NAMESPACE_PACKAGELISTMANIFEST = "http://leitstelle.p23r.de/NS/p23r/nrl/PackageList1-1";
	
	/**
	 * The Constant NAMESPACE_NOTIFICATIONPROFILE.
	 */
	public static final String NAMESPACE_NOTIFICATIONPROFILE = "http://leitstelle.p23r.de/NS/p23r/nrl/NotificationProfile1-1";

	/**
	 * The Constant NAMESPACE_CONFIGURATION.
	 */
	public static final String NAMESPACE_CONFIGURATION = "http://leitstelle.p23r.de/NS/p23r/nrl/Configuration1-0";

	/**
	 * The Constant NAMESPACE_CONFIGURATIONRESULT.
	 */
	public static final String NAMESPACE_CONFIGURATIONRESULT = "http://leitstelle.p23r.de/NS/p23r/nrl/ConfigurationResult1-0";

}
