package de.p23r.server.common;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.DOMBuilder;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.p23r.common.modelandrulemanagement.SchemaHelperFactory;
import de.p23r.leitstelle.ns.p23r.nrl.notificationprofile1_1.NotificationProfile;

/**
 * The Class NotificationProfileHelper wraps a notification profile document and provides convenient access short cuts to several values and printing methods.
 *
 * @author sim
 */
public class NotificationProfileHelper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1767674813718076911L;

	private static final Namespace NAMESPACE = Namespace.getNamespace(TBRS.NAMESPACE_NOTIFICATIONPROFILE);
	
	private static final Logger log = LoggerFactory.getLogger(NotificationProfileHelper.class);
	
	private Document profile;
	
	/**
	 * Instantiates a new notification profile helper.
	 *
	 * @param profile the profile
	 */
	public NotificationProfileHelper(org.w3c.dom.Element profile) {
		DOMBuilder builder = new DOMBuilder();
		this.profile = new Document(builder.build(profile));
		this.profile.getRootElement().setName("notificationProfile");
		this.profile.getRootElement().setNamespace(NAMESPACE);
	}
	
	/**
	 * Instantiates a new notification profile helper.
	 *
	 * @param profile the profile
	 */
	public NotificationProfileHelper(Document profile) {
		this.profile = profile;
		this.profile.getRootElement().setName("notificationProfile");
		this.profile.getRootElement().setNamespace(NAMESPACE);
	}

	/**
	 * Instantiates a new notification profile helper.
	 *
	 * @param profile the profile
	 */
	public NotificationProfileHelper(String profile) {
		SAXBuilder builder = new SAXBuilder();
		try {
			this.profile = builder.build(new ByteArrayInputStream(profile.getBytes(TBRS.CHARSET)));
		} catch (JDOMException e) {
			log.error("parsing and building profile", e);
			this.profile = null;
		} catch (IOException e) {
			log.error("reading profile from string", e);
			this.profile = null;
		}
	}
	
	/**
	 * Instantiates a new notification profile helper.
	 *
	 * @param profile the profile
	 */
	public NotificationProfileHelper(NotificationProfile profile) {
		this.profile = SchemaHelperFactory.getNotificationProfileSchemaHelper().marshallToJdom(profile);
	}
	
	/**
	 * Gets the document.
	 *
	 * @return the document
	 */
	public Document getDocument() {
		return profile;
	}
	
	/**
	 * Gets the general.
	 *
	 * @return the general
	 */
	public Element getGeneral() {
		return profile.getRootElement().getChild("general", NAMESPACE);
	}
	
	/**
	 * Gets the communication.
	 *
	 * @return the communication
	 */
	public Element getCommunication() {
		return profile.getRootElement().getChild("communication", NAMESPACE);
	}
	
	/**
	 * Gets the signatures.
	 *
	 * @return the signatures
	 */
	public Element getSignatures() {
		return profile.getRootElement().getChild("signatures", NAMESPACE);
	}
	
	/**
	 * Gets the generation log.
	 *
	 * @return the generation log
	 */
	public Element getGenerationLog() {
		return profile.getRootElement().getChild("generationLog", NAMESPACE);
	}
	
	/**
	 * Gets the tenant.
	 *
	 * @return the tenant
	 */
	public String getTenant() {
		return profile.getRootElement().getAttributeValue("tenant");
	}

	/**
	 * Gets the message id.
	 *
	 * @return the message id
	 */
	public String getMessageId() {
		return getGeneral().getAttributeValue("messageId");
	}

	/**
	 * Gets the notification id.
	 *
	 * @return the notification id
	 */
	public String getNotificationId() {
		return getGeneral().getAttributeValue("notificationId");
	}

	/**
	 * Sets the notification id.
	 *
	 * @param notificationId the new notification id
	 */
	public void setNotificationId(String notificationId) {
		getGeneral().setAttribute("notificationId", notificationId);
	}
	
	/**
	 * Gets the notification rule id.
	 *
	 * @return the notification rule id
	 */
	public String getNotificationRuleId() {
		return getGeneral().getAttributeValue("notificationRuleId");
	}

	/**
	 * Gets the notification rule name.
	 *
	 * @return the notification rule name
	 */
	public String getNotificationRuleName() {
		return getGeneral().getAttributeValue("notificationRuleName");
	}

	/**
	 * Gets the notification rule group id.
	 *
	 * @return the notification rule group id
	 */
	public String getNotificationRuleGroupId() {
		return getGeneral().getAttributeValue("notificationRuleGroupId");
	}

	/**
	 * Gets the transaction id.
	 *
	 * @return the transaction id
	 */
	public String getTransactionId() {
		return getGeneral().getAttributeValue("transactionId");
	}

	/**
	 * Gets the originator id.
	 *
	 * @return the originator id
	 */
	public String getOriginatorId() {
		return getGeneral().getAttributeValue("originatorId");
	}

	/**
	 * Gets the source uri.
	 *
	 * @return the source uri
	 */
	public String getSourceURI() {
		return getGeneral().getAttributeValue("sourceURI");
	}
	

    /**
     * Gets the message ns.
     *
     * @return the message ns
     */
    public String getMessageNS() {
        return getGeneral().getAttributeValue("messageNS");
    }

	/**
	 * Gets the rule manifest.
	 *
	 * @return the rule manifest
	 */
	public Element getRuleManifest() {
		return getGeneral().getChild("ruleManifest", NAMESPACE);
	}

	/**
	 * Adds the approval.
	 *
	 * @param approval the approval
	 */
	public void addApproval(Element approval) {
		approval.setName("approvals");
		getGenerationLog().addContent(approval.detach());
	}
	
	/**
	 * Sets the core signature.
	 *
	 * @param signature the new core signature
	 */
	public void setCoreSignature(Element signature) {
		signature.setName("signedCore");
		signature.setNamespace(NAMESPACE);
//		getSignatures().removeChild("signedCore", NAMESPACE);
		getSignatures().setContent(0, signature.detach());
	}

	/**
	 * Gets the core signature.
	 *
	 * @return the core signature
	 */
	public Element getCoreSignature() {
		Element elem = null;
		Element coreSig = getSignatures().getChild("signedCore", NAMESPACE);
		if (coreSig != null) {
			elem = coreSig.clone();
			elem.setNamespace(Namespace.getNamespace("http://www.w3.org/2000/09/xmldsig#"));
		}
		return elem;
	}

	/**
	 * Sets the content signature.
	 *
	 * @param signature the new content signature
	 */
	public void setContentSignature(Element signature) {
		signature.setName("signedContent");
		signature.setNamespace(NAMESPACE);
//		getSignatures().removeChild("signedContent", NAMESPACE);
		getSignatures().setContent(1, signature.detach());
	}

	/**
	 * Gets the content signature.
	 *
	 * @return the content signature
	 */
	public Element getContentSignature() {
		Element elem = null;
		Element contSig = getSignatures().getChild("signedContent", NAMESPACE);
		if (contSig != null) {
			elem = contSig.clone();
			elem.setNamespace(Namespace.getNamespace("http://www.w3.org/2000/09/xmldsig#"));			
		}
		return elem;
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public Element getMessage() {
		return getGeneral().getChild("message", NAMESPACE);
	}

	/**
	 * Gets the receivers.
	 *
	 * @return the receivers
	 */
	public List<Element> getReceivers() {
		return getCommunication().getChildren("receivers", NAMESPACE);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return new XMLOutputter().outputString(profile);
	}

	/**
	 * Pretty print.
	 *
	 * @return the string
	 */
	public String prettyPrint() {
		return new XMLOutputter(Format.getPrettyFormat()).outputString(profile);
	}
	
}
