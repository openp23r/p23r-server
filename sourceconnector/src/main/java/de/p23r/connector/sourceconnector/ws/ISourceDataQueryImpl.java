package de.p23r.connector.sourceconnector.ws;

import java.nio.charset.Charset;
import java.util.Collections;
import java.util.GregorianCalendar;

import javax.activation.DataHandler;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jws.WebService;
import javax.mail.util.ByteArrayDataSource;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.soap.MTOM;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xmldb.api.base.ResourceIterator;
import org.xmldb.api.base.ResourceSet;
import org.xmldb.api.base.XMLDBException;

import de.p23r.common.existdb.P23RXmlDbClientConfig;
import de.p23r.common.existdb.P23RXmlDbResource;
import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault;
import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault_Exception;
import de.p23r.leitstelle.ns.p23r.isourcedataquery1_0.ISourceDataQuery;
import de.p23r.leitstelle.ns.p23r.isourcedataquery1_0.types.DataUptodate;
import de.p23r.leitstelle.ns.p23r.isourcedataquery1_0.types.MirrorData;
import de.p23r.leitstelle.ns.p23r.isourcedataquery1_0.types.PreselectedData;

/**
 * The Class ISourceDataQueryImpl.
 */
@WebService(serviceName = "ISourceDataQuery", endpointInterface = "de.p23r.leitstelle.ns.p23r.isourcedataquery1_0.ISourceDataQuery", targetNamespace = "http://leitstelle.p23r.de/NS/P23R/ISourceDataQuery1-0")
@MTOM(enabled = true)
public class ISourceDataQueryImpl implements ISourceDataQuery {

	private static final Logger LOG = LoggerFactory.getLogger(ISourceDataQueryImpl.class);

	// TTL of the pivot (valid until) in ms
	private static final long PIVOT_TTL = 8 * (1000 * 60 * 60);

	@Inject
	private P23RXmlDbClientConfig p23rXmlDbClientConfig;

	@PostConstruct
	public void init() {
		String selectionMode = p23rXmlDbClientConfig.get("/configuration/selection/@mode/string()", null);
		if (selectionMode == null) {
			
		}
	}
	
	/* (non-Javadoc)
	 * @see de.p23r.leitstelle.ns.p23r.isourcedataquery1_0.ISourceDataQuery#isAbleToMirror(java.lang.String)
	 */
	@Override
	public boolean isAbleToMirror(String namespace) throws P23RAppFault_Exception {
		LOG.debug("-> isAbleToMirror()");
		LOG.debug("-  namespace: {}", namespace);

		return false;
	}

	/* (non-Javadoc)
	 * @see de.p23r.leitstelle.ns.p23r.isourcedataquery1_0.ISourceDataQuery#getMirrorData(java.lang.String)
	 */
	@Override
	public MirrorData getMirrorData(String namespace) throws P23RAppFault_Exception {
		throw createP23RAppFault("mirroring is not supported", null);
	}

	/* (non-Javadoc)
	 * @see de.p23r.leitstelle.ns.p23r.isourcedataquery1_0.ISourceDataQuery#getPreselectedData(java.lang.String, java.lang.Object, java.lang.String)
	 */
	@Override
	public PreselectedData getPreselectedData(String preselectionScript, Object parameters, String namespace) throws P23RAppFault_Exception {
		LOG.debug("-> getPreselectedData()");
		LOG.debug("-  preselectionScript: {}", preselectionScript);
		LOG.debug("-  parameters: {}", parameters);
		LOG.debug("-  namespace: {}", namespace);

		LOG.debug("-  documentname: {}", getNSFile(namespace));

		// parameter checking
		if (preselectionScript == null || preselectionScript.isEmpty() || namespace == null || namespace.isEmpty()) {
			throw createP23RAppFault("one of preselection script or namespace is null or empty", null);
		}

		// initialize resource
		P23RXmlDbResource pivotResource = p23rXmlDbClientConfig.getResource("/client/pivotdata/" + getNSFile(namespace), true);

		if (!pivotResource.exists()) {
			pivotResource.free();
			LOG.warn("unsupported namespace {}", namespace);
			throw createP23RAppFault("unsupported namespace " + namespace, null);
		}
		
		String selectionMode = p23rXmlDbClientConfig.get("/configuration/selection/@mode/string()", "auto");
		String selectionCompiler = p23rXmlDbClientConfig.get("/configuration/selection/compiler/external/text()", "");
		if (selectionMode.equalsIgnoreCase("strict") || (selectionMode.equalsIgnoreCase("auto") && P23RSelectionCompiler.isP23RSelection(preselectionScript))) {
			P23RSelectionCompiler compiler = P23RSelectionCompiler.newCompiler(selectionCompiler);
			preselectionScript = compiler.compileSelection(preselectionScript, "connector");
			LOG.debug("compiled script:\n{}", preselectionScript);
		}
		
		// query the data
		return createPreselectedData(getData(preselectionScript, namespace, parameters, pivotResource));
	}

	/* (non-Javadoc)
	 * @see de.p23r.leitstelle.ns.p23r.isourcedataquery1_0.ISourceDataQuery#isDataUptodate(java.lang.String, java.lang.Object, java.lang.String, javax.xml.datatype.XMLGregorianCalendar)
	 */
	@Override
	public DataUptodate isDataUptodate(String preselectionScript, Object parameters, String namespace, XMLGregorianCalendar timestamp) throws P23RAppFault_Exception {
		throw createP23RAppFault("isdatauptodate not supported", null);
	}

	private StringBuilder getData(String preselectionScript, String namespace, Object parameters, P23RXmlDbResource pivotResource) throws P23RAppFault_Exception {
		StringBuilder data = new StringBuilder("");

		if (!preselectionScript.trim().isEmpty()) {

			ResourceSet result = query(preselectionScript, namespace, parameters, pivotResource);

			if (result == null) {
				pivotResource.free();
				throw createP23RAppFault("invalid selection script", null);
			}

			try {
				if (result.getSize() > 0) {
					ResourceIterator it = result.getIterator();
					while (it.hasMoreResources()) {
						data.append((String) it.nextResource().getContent());
					}
					LOG.info("preselected pivot data: {}", data);
				}
			} catch (XMLDBException e) {
				LOG.error("reading result failed", e);
			}
		}
		pivotResource.free();
		return data;
	}

	private ResourceSet query(String preselectionScript, String namespace, Object parameters, P23RXmlDbResource pivotResource) {
		ResourceSet result = null;
		if (parameters instanceof String && !parameters.toString().isEmpty() && ((String) parameters).startsWith("<")) {
			Document elem = (Document) TransformerHelper.cleanNamespaces((String) parameters);
			result = pivotResource.query(preselectionScript.trim(), namespace, Collections.singletonMap("p23rparameters", elem.getDocumentElement()));
		} else if (parameters != null) {
			result = pivotResource.query(preselectionScript.trim(), namespace, Collections.singletonMap("p23rparameters", parameters));
		} else {
			result = pivotResource.query(preselectionScript.trim());
		}
		return result;
	}

	private PreselectedData createPreselectedData(StringBuilder data) throws P23RAppFault_Exception {
		PreselectedData preselectedData = new PreselectedData();
		DataHandler handler = new DataHandler(new ByteArrayDataSource(data.toString().getBytes(Charset.forName("UTF-8")), "application/xml"));
//		preselectedData.setData(new StreamSource(new ByteArrayInputStream(data.toString().getBytes(Charset.forName("UTF-8")))));
		preselectedData.setData(handler);

		DatatypeFactory factory = null;
		try {
			factory = DatatypeFactory.newInstance();
		} catch (DatatypeConfigurationException e) {
			LOG.error("Datatype factory instantiation", e);
			return preselectedData;
		}
		
		preselectedData.setTimestamp(factory.newXMLGregorianCalendar(new GregorianCalendar()));

		Duration ttlDuration = factory.newDuration(PIVOT_TTL);
		XMLGregorianCalendar validUntil = factory.newXMLGregorianCalendar(new GregorianCalendar());
		validUntil.add(ttlDuration);
		preselectedData.setValidUntil(validUntil);
		
		return preselectedData;
	}

	private String getNSFile(String namespace) {
		String fileName = namespace;
		fileName = fileName.replace("http://", "");
		fileName = fileName.replace("https://", "");
		fileName = fileName.replace("/", "_");
		fileName = fileName + ".xml";
		return fileName;
	}

	private P23RAppFault_Exception createP23RAppFault(String description, Exception exception) {
		P23RAppFault faultInfo = new P23RAppFault();
		if (exception != null) {
			description = description + " -> " + exception.getMessage();
		}
		faultInfo.setDescription(description);
		return new P23RAppFault_Exception(description, faultInfo);
	}

}
