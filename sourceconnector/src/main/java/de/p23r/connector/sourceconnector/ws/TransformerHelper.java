package de.p23r.connector.sourceconnector.ws;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * The Class TransformerHelper provides methods for transforming/converting xml strings from and to dom tree objects. It is a convenient wrapper for
 * the java.xml.transfrom.Transformer functionality.
 * 
 * @author sim
 * 
 */
public abstract class TransformerHelper {

	private static final Logger LOG = LoggerFactory.getLogger(TransformerHelper.class);

	/**
	 * Hide public default constructor. It is an abstract class with only static methods.
	 */
	private TransformerHelper() {
	}

	/**
	 * Transforms Document to String. Type safe wrapper for nodeToString with format = true.
	 *
	 * @param document the Document object to transform
	 * 
	 * @return the string representing the given document or empty if transformation fails
	 * 
	 * @see noteToString
	 */
	public static final String documentToString(Document document) {
		return nodeToString(document, true);
	}

	/**
	 * Transforms Element to String. Type safe wrapper for nodeToString with format = true.
	 * 
	 * @param element the element object to transform
	 * 
	 * @return the string representing the given element or empty if transformation fails
	 * 
	 * @see noteToString
	 */
	public static final String elementToString(Element element) {
		return nodeToString(element, true);
	}

	/**
	 * Transforms any Node to a string. If this is not possible the result is empty.
	 *
	 * @param node the DOM node containing the source tree.
	 * 
	 * @param format if the string should be formatted
	 * 
	 * @return the string if transformation fails it returns an empty string.
	 */
	public static final String nodeToString(Node node, boolean format) {
		ByteArrayOutputStream writer = new ByteArrayOutputStream();

		TransformerFactory factory = TransformerFactory.newInstance();
		try {
			Transformer transformer = factory.newTransformer();
			if (format) {
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			}
			transformer.transform(new DOMSource(node), new StreamResult(writer));
		} catch (TransformerConfigurationException e) {
			LOG.error("transforming element to string", e);
		} catch (TransformerException e) {
			LOG.error("transforming element to string", e);
		}
		try {
			return writer.toString("UTF-8");
		} catch (UnsupportedEncodingException e) {
			// should never happen
			LOG.error("utf-8 encoding is not supported.", e);
			return "";
		}
	}

	/**
	 * Document from string.
	 *
	 * @param xml the xml
	 * 
	 * @return the document
	 */
	public static final Document documentFromString(String xml) {
		return (Document) nodeFromSource(new StreamSource(new ByteArrayInputStream(xml.getBytes(Charset.forName("UTF-8")))));
	}

	/**
	 * Element from string. Converts an xml string to a corresponding dom element
	 * 
	 * @param xml the xml string to convert
	 *            
	 * @return the dom element
	 */
	public static final Element elementFromString(String xml) {
		Document doc = (Document) nodeFromSource(new StreamSource(new ByteArrayInputStream(xml.getBytes(Charset.forName("UTF-8")))));
		return doc.getDocumentElement();
	}

	/**
	 * Element from stream. Reads the xml from a stream and converts it to a dom element.
	 * 
	 * @param stream the input stream to read from
	 *            
	 * @return the dom element
	 */
	public static final Element elementFromStream(InputStream stream) {
		Document doc = (Document) nodeFromSource(new StreamSource(stream));
		return doc.getDocumentElement();
	}

	/**
	 * Node from source.
	 *
	 * @param source the source
	 * 
	 * @return the node
	 */
	public static final Node nodeFromSource(Source source) {
		TransformerFactory factory = TransformerFactory.newInstance();
		DOMResult result = new DOMResult();
		try {
			Transformer transformer = factory.newTransformer();
			transformer.transform(source, result);
		} catch (TransformerConfigurationException e) {
			LOG.error("transforming source to elment", e);
			return null;
		} catch (TransformerException e) {
			LOG.error("transforming source to elment", e);
			return null;
		}
		return result.getNode();
	}

	/**
	 * String from source.
	 *
	 * @param source the source
	 * 
	 * @return the string
	 */
	public static final String stringFromSource(Source source) {
		TransformerFactory factory = TransformerFactory.newInstance();
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		StreamResult result = new StreamResult(stream);
		try {
			Transformer transformer = factory.newTransformer();
			transformer.transform(source, result);
		} catch (TransformerConfigurationException e) {
			LOG.error("transforming source to elment", e);
			return null;
		} catch (TransformerException e) {
			LOG.error("transforming source to elment", e);
			return null;
		}
		try {
			return stream.toString("UTF-8");
		} catch (UnsupportedEncodingException e) {
			LOG.error("utf-8 is not supported.", e);
			return null;
		}
	}

	/**
	 * Clean namespaces of a given xml. 
	 *
	 * @param xml the xml string
	 * 
	 * @return the cleaned node
	 */
	public static final Node cleanNamespaces(String xml) {
		InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream("cleannamespaces.xslt");
		TransformerFactory factory = TransformerFactory.newInstance();
		DOMResult result = new DOMResult();
		try {
			Transformer transformer = factory.newTransformer(new StreamSource(stream));
			transformer.transform(new StreamSource(new ByteArrayInputStream(xml.getBytes(Charset.forName("UTF-8")))), result);
		} catch (TransformerConfigurationException e) {
			LOG.error("transforming source to elment", e);
			return null;
		} catch (TransformerException e) {
			LOG.error("transforming source to elment", e);
			return null;
		}
		return result.getNode();
	}
	
}
