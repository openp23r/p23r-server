# P23R Server

## Build and install the server

Prerequisites:

* JDK 1.8 or newer
* eXist-db 3.0.RC1
* WildFly 8.1.0.Final + SwitchYard 2.0.0.Final
* [P23R selection compiler](openp23r/p23r-selection-compiler)

To build and deploy from source:

 1. Clone the git repository:
    ```
    $> git clone https://gitlab.com/openp23r/p23r-server.git
    ```

 2. Package and install the administration application:
    ```
    $> cd p23r-server/p23radmin
    $> mvn assembly:single package
    $> cp target/p23radmin-x.x.x.xar EXISTDB_HOME/autodeploy
    ```
Then restart eXist-db. This guarantees that the correct user with all permissions is created.
 3. Build and install the processor:
    
    ! Note: The default configuration assumes that the eXist-db runs with port 8888 (as proposed in the administration guide). If this is not the case, you have to edit the file ``xmldb/src/main/resources/existdb.properties`` to change the corresponding values.
    ```
    $> cd ..
    $> mvn package wildfly:deploy
    ```
It is necessary that all servers (eXist-db, WildFly + SwitchYard) are running. Otherwise the deployment will fail.

If you would like to build and install the connectors as well, you have to use the profile 'connectors' when executing the maven command:
```
$> mvn package wildfly:deploy -Pconnectors
```

## Legal issues

This project is part of the openP23R initiative. All related projects are listed at https://gitlab.com/openp23r/openp23r .

The legal conditions see LICENSE file.

The project is maintained by P23R-Team (a) Fraunhofer FOKUS

By submitting a "pull request" or otherwise contributing to this repository, you agree to license your contribution under the license mentioned above.