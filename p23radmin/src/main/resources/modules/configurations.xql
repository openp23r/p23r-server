xquery version "3.0";

module namespace conf = "http://p23r.de/p23radmin/configurations";

import module namespace templates = "http://exist-db.org/xquery/templates";

declare namespace c = "http://leitstelle.p23r.de/NS/p23r/nrl/Configuration1-0";

(: -------------------------------------------------------------------------- :)
(: -------------------------------------------------------------------------- :)
(: -------------------------------------------------------------------------- :)
(: -------------------------------------------------------------------------- :)

declare function conf:init($node as node(), $model as map (*)) {
    let $system := doc("../server/configurations.xml")//c:system
    return
        map { "system" := $system }    
};

declare function conf:systemid($node as node(), $model as map (*)) {
    let $system := $model("system")
    return
        <input class="form-control" id="system_id" type="text" value="{data($system/@id)}" name="system_id"/>
};

declare function conf:action($node as node(), $model as map (*), $system_id as xs:string?) {
    let $system := $model("system")
    return
        if ($system_id) then
            update value $system/@id with $system_id
        else ()
};

declare
%templates:wrap
function conf:static($node as node(), $model as map (*), $attribute as xs:string) {
    let $system := $model("system")
    return data($system/@*[name() = $attribute])
};
