xquery version "3.0";


module namespace app = "http://p23r.de/p23radmin/app";


import module namespace templates = "http://exist-db.org/xquery/templates";

(: -------------------------------------------------------------------------- :)
(: -------------------------------------------------------------------------- :)
(: -------------------------------------------------------------------------- :)
(: -------------------------------------------------------------------------- :)

(: Menus :)

declare function app:navigation($node as node(), $model as map (*))
{
    <ul class="nav navbar-nav">
        <li class="{ if (contains(request:get-uri(), 'index.html')) then 'active' else () }">
            <a href="index.html">
                <span class="glyphicon glyphicon-home"/>
            </a>
        </li>
        <li class="{ if (contains(request:get-uri(), 'configurations.html')) then 'active' else () }">
            <a href="configurations.html">Konfigurationen</a>
        </li>
        <li class="{ if (contains(request:get-uri(), 'controlcentres.html')) then 'active' else () }">
            <a href="controlcentres.html">Leitstellen</a>
        </li>
        <li class="{ if (contains(request:get-uri(), 'clients.html')) then 'active' else () }">
            <a href="clients.html">Mandanten</a>
        </li>
        <li class="{ if (contains(request:get-uri(), 'communicationregistry.html')) then 'active' else () }">
            <a href="communicationregistry.html">Konnektoren</a>
        </li>
        <li class="{ if (contains(request:get-uri(), 'protocolpool.html')) then 'active' else () }">
            <a href="protocolpool.html">Protokoll</a>
        </li>
    </ul>
};

