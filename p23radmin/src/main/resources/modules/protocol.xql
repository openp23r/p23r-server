xquery version "3.0";

module namespace protocol = "http://p23r.de/p23radmin/protocol";

import module namespace templates = "http://exist-db.org/xquery/templates";

declare namespace pe = "http://leitstelle.p23r.de/NS/P23R/CommonProtocol1-0";

(: -------------------------------------------------------------------------- :)
(: -------------------------------------------------------------------------- :)
(: -------------------------------------------------------------------------- :)
(: -------------------------------------------------------------------------- :)

declare variable $protocol:max := 10;
declare variable $protocol:count := count(collection("/db/apps/p23radmin/protocolpool")/pe:protocolEntry);
declare variable $protocol:pages := ($protocol:count + $protocol:max - 1) idiv $protocol:max;
declare variable $protocol:map := map {
    "error" := "Fehler",
    "warning" := "Warnung",
    "info" := "Info",
    "test.fail" := "Testfehler",
    "test.success" := "Testerfolg"
};

declare
%templates:wrap
%templates:default("currentPage", 1)
function protocol:currentPage($node as node(), $model as map (*), $currentPage as xs:integer) {
    let $offset := 1 + (($currentPage - 1) * $protocol:max)
    let $protocolEntries := subsequence(for $e in collection("/db/apps/p23radmin/protocolpool")/pe:protocolEntry order by $e/@recordAt descending return $e, $offset, $protocol:max)
    return
        map { "protocolEntries" := $protocolEntries }    
};

declare
%templates:wrap
function protocol:column1($node as node(), $model as map (*)) {
    let $entry := $model("protocolEntry")
    let $icon :=
        switch ($entry/pe:content/pe:log/pe:level)
        case "test.fail" return "glyphicon-thumbs-down"
        case "test.success" return "glyphicon-thumbs-up"
        case "error" return "glyphicon-fire"
        case "warning" return "glyphicon-warning-sign"
        case "info" return "glyphicon-info-sign"
        default return ()
    return
        if ($entry/pe:content/pe:log) then
            <span title="{$entry/@recordId}" class="glyphicon {$icon}" />
        else if ($entry/pe:content/pe:trace) then
            <span title="{$entry/@recordId}" class="glyphicon glyphicon-random" />
        else
            <span title="{$entry/@recordId}" class="glyphicon glyphicon-th-large" />
};

declare
%templates:wrap
function protocol:column2($node as node(), $model as map (*)) {
    let $entry := $model("protocolEntry")
    let $color :=
        switch ($entry/pe:content/pe:log/pe:level)
        case "test.fail" return "text-danger"
        case "test.success" return "text-success"
        case "info" return "text-info"
        case "error" return "text-danger"
        case "warning" return "text-warning"
        default return ()
    return
        if ($entry/pe:content/pe:log) then
            <span class="{$color}"><b>{$protocol:map(data($entry/pe:content/pe:log/pe:level))}</b></span>
        else if ($entry/pe:content/pe:trace) then
            <span class="text-primary"><b>{data($entry/pe:content/pe:trace/@step)}</b></span>
        else
            ()
};

declare
%templates:wrap
function protocol:column3($node as node(), $model as map (*)) {
    let $entry := $model("protocolEntry")
    return
        if ($entry/pe:content/pe:log) then
            data($entry/pe:content/pe:log/pe:notes[1])
        else if ($entry/pe:content/pe:trace) then
            data($entry/pe:content/pe:trace/pe:notes[1])
        else
            data($entry/pe:content/pe:generic)
};

declare
%templates:wrap
function protocol:column4($node as node(), $model as map (*)) {
    let $entry := $model("protocolEntry")
    return
        if ($entry/pe:content/pe:trace) then
            <a href="tracedetails.html?recordid={$entry/@recordId}">Details</a>
        else
            ()
};

declare
%templates:wrap
function protocol:column5($node as node(), $model as map (*))
{
    datetime:format-dateTime($model("protocolEntry")/@recordAt, "dd.MM.yyyy HH:mm:ss")
};

declare
%templates:default("currentPage", 1)
function protocol:pagerPrevious($node as node(), $model as map (*), $currentPage as xs:integer)
{
    if ($currentPage = 1) then
        <li class="disabled"><a>Vorherige Seite</a></li>
    else
        <li><a href="?currentPage={$currentPage - 1}">Vorherige Seite</a></li>
};

declare
%templates:default("currentPage", 1)
function protocol:pagerNext($node as node(), $model as map (*), $currentPage as xs:integer)
{
    if ($currentPage = $protocol:pages) then
        <li class="disabled"><a>Nächste Seite</a></li>
    else
        <li><a href="?currentPage={$currentPage + 1}">Nächste Seite</a></li>
};

declare
%templates:wrap
function protocol:countEntries($node as node(), $model as map (*))
{
    if ($protocol:count = 0) then "Keine Einträge vorhanden"
    else if ($protocol:count = 1) then 
        concat($protocol:count, " Eintrag")
    else
        concat($protocol:count, " Einträge")
};

declare
%templates:wrap
%templates:default("currentPage", 1)
function protocol:pageIndicator($node as node(), $model as map (*), $currentPage as xs:integer) as xs:string
{
    concat("Seite ", $currentPage, " von ", $protocol:pages)
};

declare
function protocol:initdetails($node as node(), $model as map (*), $recordid as xs:string?)
{
    let $entry := collection("/db/apps/p23radmin/protocolpool")/pe:protocolEntry[@recordId = $recordid]
    return
        map {"entry" := $entry}
};

declare
%templates:wrap
function protocol:step($node as node(), $model as map (*)) 
{
    data($model("entry")/pe:content/pe:trace/@step)
};

declare
%templates:wrap
function protocol:note($node as node(), $model as map (*)) 
{
    data($model("entry")/pe:content/pe:trace/pe:notes[1])
};

declare
%templates:wrap
function protocol:notificationscript($node as node(), $model as map (*))
{
    data($model("entry")/pe:content/pe:trace/pe:transformationScript)
};

declare
%templates:wrap
function protocol:inputnotification($node as node(), $model as map (*))
{
    data($model("entry")/pe:content/pe:trace/pe:inputNotification)
};

declare
%templates:wrap
function protocol:outputnotification($node as node(), $model as map (*))
{
    data($model("entry")/pe:content/pe:trace/pe:outputNotification)
};

declare
%templates:wrap
function protocol:profilescript($node as node(), $model as map (*))
{
    data($model("entry")/pe:content/pe:trace/pe:profileScript)
};

declare
%templates:wrap
function protocol:inputprofile($node as node(), $model as map (*))
{
    data($model("entry")/pe:content/pe:trace/pe:inputProfile)
};

declare
%templates:wrap
function protocol:outputprofile($node as node(), $model as map (*))
{
    data($model("entry")/pe:content/pe:trace/pe:outputProfile)
};
