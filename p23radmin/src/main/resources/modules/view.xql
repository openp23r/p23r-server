(:~
 : This is the main XQuery which will (by default) be called by controller.xql
 : to process any URI ending with ".html". It receives the HTML from
 : the controller and passes it to the templating system.
 :)
xquery version "3.0";

import module namespace templates="http://exist-db.org/xquery/templates" ;

(: 
 : The following modules provide functions which will be called by the 
 : templating.
 :)
import module namespace config="http://p23r.de/p23radmin/config" at "config.xqm";
import module namespace app="http://p23r.de/p23radmin/app" at "app.xql";
import module namespace general="http://p23r.de/p23radmin/general" at "general.xql";
import module namespace conf="http://p23r.de/p23radmin/configurations" at "configurations.xql";
import module namespace cc="http://p23r.de/p23radmin/controlcentres" at "controlcentres.xql";
import module namespace connectors="http://p23r.de/p23radmin/connectors" at "connectors.xql";
import module namespace clients="http://p23r.de/p23radmin/clients" at "clients.xql";
import module namespace protocol="http://p23r.de/p23radmin/protocol" at "protocol.xql";

declare option exist:serialize "method=html5 media-type=text/html enforce-xhtml=yes";

let $config := map {
    $templates:CONFIG_APP_ROOT := $config:app-root,
    $templates:CONFIG_STOP_ON_ERROR := true()
}
(:
 : We have to provide a lookup function to templates:apply to help it
 : find functions in the imported application modules. The templates
 : module cannot see the application modules, but the inline function
 : below does see them.
 :)
let $lookup := function($functionName as xs:string, $arity as xs:int) {
    try {
        function-lookup(xs:QName($functionName), $arity)
    } catch * {
        ()
    }
}
(:
 : The HTML is passed in the request from the controller.
 : Run it through the templating system and return the result.
 :)
let $content := request:get-data()
return
    templates:apply($content, $lookup, (), $config)