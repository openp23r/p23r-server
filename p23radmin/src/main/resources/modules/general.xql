xquery version "3.0";

module namespace general = "http://p23r.de/p23radmin/general";

import module namespace templates = "http://exist-db.org/xquery/templates";

(: -------------------------------------------------------------------------- :)
(: -------------------------------------------------------------------------- :)
(: -------------------------------------------------------------------------- :)
(: -------------------------------------------------------------------------- :)


declare variable $general:settings := doc("../runtime/serverconfig.xml")/configuration/general;

declare function general:trace($node as node(), $model as map (*)) {
    let $current := data($general:settings/traceEnabled)
    return
        if ($current = "true") then
            <input class="form-control" id="trace" name="trace" type="checkbox" data-on-text="Ja" data-off-text="Nein" checked="on" />
        else
            <input class="form-control" id="trace" name="trace" type="checkbox" data-on-text="Ja" data-off-text="Nein" />
};

declare function general:autoupdate($node as node(), $model as map (*)) {
    let $current := data($general:settings/autoupdateDisabled)
    return
        if ($current = "true") then
            <input class="form-control" id="autoupdate" name="autoupdate" type="checkbox" data-on-text="Ja" data-off-text="Nein" />
        else
            <input class="form-control" id="autoupdate" name="autoupdate" type="checkbox" data-on-text="Ja" data-off-text="Nein" checked="on" />
};

declare function general:verification($node as node(), $model as map (*)) {
    let $current := data($general:settings/verificationDisabled)
    return
        if ($current = "true") then
            <input class="form-control" id="verification" name="verification" type="checkbox" data-on-text="Ja" data-off-text="Nein" />
        else
            <input class="form-control" id="verification" name="verification" type="checkbox" data-on-text="Ja" data-off-text="Nein" checked="on" />
};

declare function general:save($node as node(), $model as map (*), $save as xs:string?, $trace as xs:string?, $autoupdate as xs:string?, $verification as xs:string?)
{
    if ($save) then
        let $traceValue := if ($trace = "on") then "true" else "false"
        let $autoupdateValue := if ($autoupdate = "on") then "false" else "true"
        let $verificationValue := if ($verification = "on") then "false" else "true"
        return (
            update value $general:settings/traceEnabled with $traceValue,
            update value $general:settings/autoupdateDisabled with $autoupdateValue,
            update value $general:settings/verificationDisabled with $verificationValue
        )
    else
        ()    
};