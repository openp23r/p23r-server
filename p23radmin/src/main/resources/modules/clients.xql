xquery version "3.0";

module namespace clients = "http://p23r.de/p23radmin/clients";

import module namespace templates = "http://exist-db.org/xquery/templates";

declare function clients:init($node as node(), $model as map (*)) {
    let $clients := doc("../runtime/serverconfig.xml")//client
    return 
        map { "clients" := $clients }    
};

declare function clients:sidebar($node as node(), $model as map (*))
{
    let $clients := $model("clients")
    return
        <ul class="nav nav-pills nav-stacked" id="sidebar_clients">
        {
            for $c in $clients
                let $tenant := data($c//@tenant)
                return    
                    <li class="{if (index-of($clients, $c) = 1) then "active" else ()}">
                        <a href="#clients_{$tenant}" data-toggle="tab">{$tenant}</a>
                    </li>
        }
        </ul>
};

declare function clients:action($node as node(), $model as map (*), $saveClient as xs:string?, $removeClient as xs:string?)
{
    let $c := if ($saveClient) then doc("../runtime/serverconfig.xml")//client[@tenant = $saveClient] else ()
    return 
        if ($c) then (
            <div class="container">
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&#215;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    Änderungen wurden gespeichert!
                </div>
            </div>
        ) else if ($removeClient) then
            update delete doc("../runtime/serverconfig.xml")//client[@tenant = $removeClient]
    else ()
};

declare function clients:new($node as node(), $model as map (*), $newclient as xs:string?) {
    if ($newclient) then 
        update insert <client tenant="{$newclient}"/> into doc("../runtime/serverconfig.xml")/configuration/clients
    else ()
};

declare function clients:content($node as node(), $model as map (*)) 
{
    let $clients := $model("clients")
    for $c in $clients
        let $tenant := data($c//@tenant)
        return
        <div class="{if (index-of($clients, $c) = 1) then "tab-pane active" else "tab-pane"}" id="clients_{$tenant}">
            <form class="form form-horizontal" role="form">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="text-primary">{$tenant}</span></h3>
                    </div>
                    <ul class="list-group">
                        {local:connectors($c)}
                    </ul>
                    <table class="table">
                        <tr>
                            <td>
                                <input id="{$tenant}_new_namespace" type="text" class="form-control" placeholder="Namespace" name="new_namespace" required="true"/>
                            </td>
                            <td>
                                <input type="url" class="form-control" placeholder="Addresse" name="new_address" required="true"/>
                            </td>
                            <td>
                                <input type="text" class="form-control" placeholder="Service" name="new_service"/>
                            </td>
                            <td>
                                <button type="submit" class="btn btn-success" name="newConnector" value="{$tenant}">
                                    <span class="glyphicon glyphicon-plus"/>
                                </button>
                            </td>
                        </tr>
                    </table>
                </div>
                {
                    if ($tenant = "default") then ()
                    else
                        <button type="submit" class="btn btn-danger" name="removeClient" value="{$tenant}">
                            <span class="glyphicon glyphicon-remove"/> Entfernen
                        </button>
                }
            </form>
        </div>
};

declare function local:connectors($client as node()) 
{
    let $tenant     := data($client//@tenant)
    let $connectors := $client//connector
    for $c in $connectors
    let $index      := index-of($connectors//@namespace, $c//@namespace)
    let $namespace  := $c//@namespace
    let $address    := $c//endpoint//address
    let $service    := $c//endpoint//service
    order by $index ascending
    return 
        <li class="list-group-item">
            {data($namespace)}
            <div class="pull-right">
                <a class="text-right" data-toggle="collapse" href="#collapse_{$index}"><span class="glyphicon glyphicon-pencil"/></a>
            </div>
            <div id="collapse_{$index}" class="panel-collapse collapse">
                <hr/>
                <form class="form form-horizontal" role="form">
                    <div class="form-group">
                        <label for="address" class="col-sm-2 control-label">Adresse</label>
                        <div class="col-sm-8">
                            <input type="url" class="form-control" id="address" name="address" value="{$address}" required="true" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="service" class="col-sm-2 control-label">Service</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="service" name="service" value="{$service}" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-4">
                            <button type="submit" class="btn btn-primary" name="saveConnector" value="{$tenant}">
                                <span class="glyphicon glyphicon-save"/> Speichern
                            </button>
                            <span style="margin-left:5px;"/>
                            <button type="submit" class="btn btn-danger" name="removeConnector" value="{$index}" title="Entfernen des Konnektors">
                                <span class="glyphicon glyphicon-remove"/> Entfernen
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </li>
};

declare
function clients:switchTab($node as node(), $model as map (*), $newclient as xs:string?, $saveClient as xs:string?)
{
    let $switch := if ($newclient) then $newclient else if ($saveClient) then $saveClient else ()
    return
        if ($switch) then
            <script type="text/javascript">
                switchTab("clients", "{ $switch }");
            </script>
        else ()
};

declare function clients:updateConnector($connector as item(), $namespace as xs:string, $address as xs:string, $service as xs:string) {
    update value $connector//@namespace with $namespace,
    update value $connector//endpoint//address with $address,
    update value $connector//endpoint//service with $service
};
