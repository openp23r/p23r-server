xquery version "3.0";

module namespace cc = "http://p23r.de/p23radmin/controlcentres";

import module namespace templates = "http://exist-db.org/xquery/templates";
import module namespace httpclient = "http://exist-db.org/xquery/httpclient";

declare namespace hc = "http://exist-db.org/xquery/httpclient";

(: -------------------------------------------------------------------------- :)
(: -------------------------------------------------------------------------- :)
(: -------------------------------------------------------------------------- :)
(: -------------------------------------------------------------------------- :)

declare function cc:init($node as node(), $model as map (*)) {
    session:create(),
    let $controlcentres := doc("../runtime/serverconfig.xml")//controlcentre
    return
        map { "controlcentres" := $controlcentres }    
};

declare function cc:sidebar($node as node(), $model as map (*))
{
    let $controlcentre := $model("controlcentre")
    let $id := data($controlcentre//@id)
    return    
        <a href="#controlcentres_{$id}" data-toggle="tab">{$id}</a>
};

declare
function cc:content($node as node(), $model as map (*), $url as xs:string?, $tested as xs:string?, $teststatus as xs:integer?, $testresult as xs:string?)
{
    let $controlcentres := $model("controlcentres")
    for $c in $controlcentres
        let $id := data($c//@id)
        let $username := data($c//auth//username)
        let $password := data($c//auth//password)
        let $appendix := "/p23r.tsl"
        let $url := if ($url and $tested and ($tested = $id)) then $url else substring-before(data($c//tsl//url), $appendix)
        return (
            <div class="tab-pane fade{if (index-of($controlcentres, $c) = 1) then " in active" else ()}" id="controlcentres_{$id}">
                <form class="form form-horizontal">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><span class="text-primary">{$id}</span></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="username" class="col-sm-2 control-label">Benutzername</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="username" name="username" value="{$username}" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="password" class="col-sm-2 control-label">Passwort</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="password" name="password" value="{$password}" />
                                </div>
                            </div>
                            <div class="form-group{if ($tested = $id) then if ($teststatus = 200) then ' has-success' else ' has-error' else ()}">
                                <label for="url" class="col-sm-2 control-label">URL</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <input type="url" class="form-control" id="{concat($id,'_url')}" value="{$url}" name="url" required="true" />
                                        <span class="input-group-addon">{$appendix}</span>
                                        <span class="input-group-btn">
                                            <button class="btn {if ($tested = $id) then if ($teststatus = 200) then 'btn-success' else 'btn-danger' else 'btn-info'}" type="submit" name="test" value="{$id}">
                                                <span class="glyphicon glyphicon-check"/>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-primary" type="submit" name="saveControlcentre" value="{$id}">
                        <span class="glyphicon glyphicon-save" /> Speichern
                    </button>
                    <span style="margin-left:5px;" />
                    { if ($id = "default") then () else
                        <button class="btn btn-danger" type="submit" name="removeControlcentre" value="{$id}"><span class="glyphicon glyphicon-remove" /> Entfernen</button>
                    }
                </form>
                {
                    if ($tested = $id) then (
                        <p/>,
                        if ($teststatus != 200) then 
                            <div class="alert alert-danger alert-dismissible" role="alert">{$testresult}</div>
                        else
                            <pre><code class="xml">{$testresult}</code></pre>,
                        session:remove-attribute("tested"),
                        session:remove-attribute("teststatus"),
                        session:remove-attribute("testresult")
                    ) 
                    else ()
                }
            </div>,
            session:remove-attribute("tested")
        )
};

declare function cc:test($node as node(), $model as map (*), $test as xs:string?, $url as xs:string?, $username as xs:string?, $password as xs:string?) {
    if ($test) then (
        session:set-attribute("tested", $test),
        let $credentials := util:base64-encode(concat($username, ':', $password))
        let $final := concat($url, '/p23r.tsl')
        let $response := httpclient:get($final, false(), <headers><header name="Authorization" value="Basic {$credentials}" /></headers>)
        let $status := $response//@statusCode
        return (
            session:set-attribute("teststatus", $status),
            if ($status = 200) then
                session:set-attribute("testresult", util:serialize($response//hc:body/*:TrustServiceStatusList, ()))
            else
                session:set-attribute("testresult", concat("Addresse nicht erreichbar: ", $response//@statusCode))
        )
    )
    else ()
};

declare function cc:action($node as node(), $model as map (*), $saveControlcentre as xs:string?, $removeControlcentre as xs:string?, $username as xs:string?, $password as xs:string?, $url as xs:string?) {
    let $c := if ($saveControlcentre) then doc("../runtime/serverconfig.xml")//controlcentre[@id = $saveControlcentre] else ()
    return 
        if ($c) then (
        update value $c/auth/username   with $username,
        update value $c/auth/password   with $password,
        update value $c/tsl/url         with concat($url, '/p23r.tsl'),
        <div class="container">
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&#215;</span>
                    <span class="sr-only">Close</span>
                </button>
                Änderungen wurden gespeichert!
            </div>
        </div>
    ) else if ($removeControlcentre) then
        update delete doc("../runtime/serverconfig.xml")//controlcentre[@id = $removeControlcentre]
    else ()
};

declare function cc:new($node as node(), $model as map (*), $newcc as xs:string?) {
    if ($newcc) then 
        if (exists(doc("../runtime/serverconfig.xml")//controlcentre[@id = $newcc])) then
            <div class="container">
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&#215;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    Mandant bereits vorhanden!
                </div>
            </div>
        else
            let $cc := 
                <controlcentre id="{$newcc}">
                    <auth>
                        <username />
                        <password />
                    </auth>
                    <tsl>
                        <url />
                    </tsl>
                    <modelandruledepot tlsservice="ModelAndRuleDepot"/>
                </controlcentre>
            return
                update insert $cc into doc("../runtime/serverconfig.xml")/configuration/controlcentres
    else ()
};

declare
function cc:switchTab($node as node(), $model as map (*), $newcc as xs:string?, $saveControlcentre as xs:string?)
{
    let $switch := if ($newcc) then $newcc else if ($saveControlcentre) then $saveControlcentre else "default"
    return
        if ($switch) then
            <script type="text/javascript">
                switchTab("controlcentres", "{ $switch }");
            </script>
        else ()
};
