xquery version "3.0";

module namespace connectors = "http://p23r.de/p23radmin/connectors";

import module namespace templates = "http://exist-db.org/xquery/templates";

(: -------------------------------------------------------------------------- :)
(: -------------------------------------------------------------------------- :)
(: -------------------------------------------------------------------------- :)
(: -------------------------------------------------------------------------- :)

declare function connectors:init($node as node(), $model as map (*)) {
    let $connectors := doc("../runtime/serverconfig.xml")//communicationregistry/connector
    return
        map { "connectors" := $connectors }    
};

declare function connectors:sidebar($node as node(), $model as map (*))
{
    let $connectors := $model("connectors")
    return
        <ul class="nav nav-pills nav-stacked" id="sidebar_connectors">
        {
            for $c in $connectors
                let $type := data($c/@type)
                let $id := data($c/@id)
                return
                    <li class="{if (index-of($connectors/@type, $c/@type) = 1) then "active" else ()}">
                        <a href="#connectors_{$id}" title="{$type}" data-toggle="tab">{if (string-length($type) > 30) then concat(substring($type, 1, 30), "...") else $type}</a>
                    </li>
        }
        </ul>
};

declare
function connectors:content($node as node(), $model as map (*))
{
    let $connectors := $model("connectors")
    for $c in $connectors
        let $id := data($c/@id)
        let $type := data($c/@type)
        let $address := data($c/endpoint/address)
        let $service := data($c/endpoint/service)
        return
            <div class="tab-pane fade{if (index-of($connectors/@type, $c/@type) = 1) then " in active" else ()}" id="connectors_{$id}">
                <form class="form form-horizontal">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><span class="text-primary">{$type}</span></h3>
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label for="address" class="col-sm-2 control-label">Adresse</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="address" name="address" value="{$address}" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="service" class="col-sm-2 control-label">Service</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="service" name="service" value="{$service}" />
                                    </div>
                                </div>
                        </div>
                    </div>
                    <button class="btn btn-primary" type="submit" name="saveConnector" value="{$type}">
                        <span class="glyphicon glyphicon-save" /> Speichern
                    </button>
                    <span style="margin-left:5px;" />
                    <button class="btn btn-danger" type="submit" name="removeConnector" value="{$type}">
                        <span class="glyphicon glyphicon-remove" /> Entfernen
                    </button>
                </form>
            </div>
};

declare function connectors:action($node as node(), $model as map (*), $saveConnector as xs:string?, $removeConnector as xs:string?, $address as xs:string?, $service as xs:string?) {
    let $c := if ($saveConnector) then doc("../runtime/serverconfig.xml")//communicationregistry/connector[@type = $saveConnector] else ()
    return 
        if ($c) then (
        update value $c/endpoint/address with $address,
        update value $c/endpoint/service with $service,
        <div class="container">
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&#215;</span>
                    <span class="sr-only">Close</span>
                </button>
                Änderungen wurden gespeichert!
            </div>
        </div>
    ) else if ($removeConnector) then
        update delete doc("../runtime/serverconfig.xml")//communicationregistry/connector[@type = $removeConnector]
    else ()
};

declare function connectors:new($node as node(), $model as map (*), $newconnector as xs:string?) {
    if ($newconnector) then 
        if (exists(doc("../runtime/serverconfig.xml")//communicationregitry/connector[@type = $newconnector])) then
            <div class="container">
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&#215;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    Konnektor bereits vorhanden!
                </div>
            </div>
        else
            let $connector := 
                <connector type="{$newconnector}" id="{max(doc("../runtime/serverconfig.xml")//communicationregistry/connector/@id) + 1}">
                    <endpoint>
                        <service>INotificationTransfer</service>
                        <address>http://localhost:8080/communicationconnector/INotificationTransfer</address>
                    </endpoint>
                </connector>
            return
                update insert $connector into doc("../runtime/serverconfig.xml")/configuration/communicationregistry
    else ()
};

declare
function connectors:switchTab($node as node(), $model as map (*), $newconnector as xs:string?, $saveConnector as xs:string?)
{
    let $type := if ($newconnector) then $newconnector else if ($saveConnector) then $saveConnector else ()
    let $switch := if ($type) then data(doc("../runtime/serverconfig.xml")/configuration/communicationregistry/connector[@type = $type]/@id) else ()
    return 
        if ($switch) then
            <script type="text/javascript">
                switchTab("connectors", "{ $switch }");
            </script>
        else ()
};
