/* Javascript file */


$('.dropdown-toggle').dropdown();

var currentPage = $('#page').val();
var totalPages = $('#totalPages').val();
paginator(totalPages, currentPage);

// constructs the paginator with the given total pages and current page options
function paginator(total, curPage) {
    var options = {
        alignment: "center",
        currentPage: curPage,
        numberOfPages: 10,
        totalPages: total,
        pageUrl: function(type, page, current) {
            switch(type){
            case "first":
                return serialize(1);
            case "last":
                return serialize(total);
            default:
                return serialize(page);
            }
        }
    };
    $('#paginator').bootstrapPaginator(options);
}

// constructs an url with the given page as additional parameter. it preserves the other parameters too.
function serialize(page) {
    var vars = getParams();
    if (!vars.page) {
        vars.push("page");
    }
    vars.page = page;
    var url = "?page="+page;
    for (var i=0; i < vars.length; i++) {
        if (vars[i] !== "page") {
            url = url + "&" + vars[i] + "=" + vars[vars[i]];
        }
    }
    return url;
}

// returns all url get parameters
function getParams() {
    var hashes = window.location.href.split('?')[1];
    if (!hashes) {
        return [];
    }
    var params = hashes.split('&');
    var vars = [];
    $.each(params, function(key, element) {
        var param = element.split("=");
        vars.push(param[0]);
        vars[param[0]] = param[1];
    });
    return vars;
}

