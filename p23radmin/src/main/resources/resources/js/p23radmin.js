/* Javascript file */

// checks for duplicated ids on user inputs and gives feedback to him
function checkID(inputId, ids, placeholder, alertMsg) {
    var id = document.getElementById(inputId).value;
    if ($.inArray(id,ids) >= 0) {
        // in array -> dont add / submit it
        alert(placeholder + alertMsg);
        return false;
    } else {
        // not in array -> add / submit it
        return true;
    }
}

// checks input field with the given id for empty input
function checkEmptyInput(inputId, alertMsg) {
    if ( !$('#'+inputId).val() || $.trim($('#'+inputId).val()) == "" ) {
        alert(alertMsg);
        return false;
    } else {
        return true;
    }
}

// checks situation for adding a new client connector
function checkAddClientConnector(inputId, ids, placeholder, alertMsgId, alertMsgNs) {
    return checkEmptyInput(inputId, alertMsgNs) && checkID(inputId, ids, placeholder, alertMsgId);
}

// activates the tab of the category with the given id  (new version for nav menu not nav tabs)
function switchTab(category, id) {
    $('#sidebar_'+category+' a[href="#'+category+'_'+id+'"]').tab('show')
}

// checks namespaces
function checkNamespaces(tenant, n, alertMsg) {
    var namespaces = [];
    for (i=1; i <= n; i++) {
        var ns = $("#"+tenant+"_namespace_"+i).val();
        if ( ($.inArray(ns, namespaces)) >= 0 ) {
            alert(alertMsg);
            return false;
        }
        namespaces.push(ns);
    }
    return true;
}

// toggles an element
function toggle(id) {
    $(id).toggle();
}





