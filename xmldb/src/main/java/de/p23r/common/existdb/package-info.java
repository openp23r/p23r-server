/**
 * Provides access classes for the eXist-db persistence level.
 */
package de.p23r.common.existdb;