package de.p23r.common.existdb;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.Resource;
import org.xmldb.api.base.ResourceIterator;
import org.xmldb.api.base.ResourceSet;
import org.xmldb.api.base.XMLDBException;

import de.p23r.common.existdb.P23RXmlDbResource;

/**
 * The Class P23RXmlDbClientConfig.
 */
@ApplicationScoped
public class P23RXmlDbClientConfig {
	
	private static final String EXISTDB_URI_PROPERTY = "base.uri";
	private static final String EXISTDB_COLLECTION_PROPERTY = "base.collection";
	private static final String EXISTDB_ADMIN_USERNAME = "authentication.admin.username";
	private static final String EXISTDB_ADMIN_PASSWORD = "authentication.admin.password";
	
	private static final String CONFIG_RESOURCE_PATH = "/runtime/clientconfig.xml";

	/**
	 * The Constant XQUERY_TENANT.
	 */
	public static final String XQUERY_TENANT = "/configuration/tenant/text()";
	
	/**
	 * The Constant XQUERY_IRULEACTIVATE_URL.
	 */
	public static final String XQUERY_IRULEACTIVATE_URL = "/configuration/p23r/endpoints/iruleactivate/text()";
	
	/**
	 * The Constant XQUERY_INOTIFICATIONAPPROVE_URL.
	 */
	public static final String XQUERY_INOTIFICATIONAPPROVE_URL = "/configuration/p23r/endpoints/inotificationapprove/text()";
	
	/**
	 * The Constant XQUERY_IMESSAGEDELIVERLOCAL_URL.
	 */
	public static final String XQUERY_IMESSAGEDELIVERLOCAL_URL = "/configuration/p23r/endpoints/imessagedeliverlocal/text()";
	
	/**
	 * The Constant XQUERY_IPROTOCOLQUERY_URL.
	 */
	public static final String XQUERY_IPROTOCOLQUERY_URL = "/configuration/p23r/endpoints/iprotocolquery/text()";
	
    /**
     * The Constant XQUERY_IRULEACTIVATE_SERVICE.
     */
    public static final String XQUERY_IRULEACTIVATE_SERVICE = "/configuration/p23r/endpoints/iruleactivate/@service/string()";
    
    /**
     * The Constant XQUERY_INOTIFICATIONAPPROVE_SERVICE.
     */
    public static final String XQUERY_INOTIFICATIONAPPROVE_SERVICE = "/configuration/p23r/endpoints/inotificationapprove/@service/string()";
    
    /**
     * The Constant XQUERY_IMESSAGEDELIVERLOCAL_SERVICE.
     */
    public static final String XQUERY_IMESSAGEDELIVERLOCAL_SERVICE = "/configuration/p23r/endpoints/imessagedeliverlocal/@service/string()";
    
    /**
     * The Constant XQUERY_IPROTOCOLQUERY_SERVICE.
     */
    public static final String XQUERY_IPROTOCOLQUERY_SERVICE = "/configuration/p23r/endpoints/iprotocolquery/@service/string()";

    /**
     * The Constant XQUERY_DEFAULT_USERNAME.
     */
    public static final String XQUERY_USERNAME = "/configuration/controlcentres/controlcentre[@id='%s']/auth/username/text()";
	
	/**
	 * The Constant XQUERY_DEFAULT_PASSWORD.
	 */
	public static final String XQUERY_PASSWORD = "/configuration/controlcentres/controlcentre[@id='%s']/auth/password/text()";
	
	private final Logger log = LoggerFactory.getLogger(getClass());

	private String baseuri;
	private String basecollection;
	private String adminusername;
	private String adminpassword;
	
	private P23RXmlDbResource resource;
	
	@PostConstruct
	private void init() {
		baseuri = "xmldb:exist://localhost:8888/exist/xmlrpc";
		basecollection = "/db/apps/p23radmin";
		adminusername = "";
		adminpassword = "";
						
		InputStream stream = null;
		
		String filename = System.getenv("P23R_EXISTDB_PROPERTIES");
		if (filename != null) {
			try {
				stream = new FileInputStream(new File(filename));
			} catch (FileNotFoundException e) {
				log.debug("exist-db properties file {} not found", filename);
			}						
		}

		if (stream == null) {
			String userHome = System.getProperty("user.home");
			try {
				stream = new FileInputStream(new File(userHome + File.separator + ".p23r" + File.separator + "existdb.properties"));
			} catch (FileNotFoundException e) {
				log.debug("exist-db properties file in user home not found");
			}			
		}

		if (stream == null) {
			stream = Thread.currentThread().getContextClassLoader().getResourceAsStream("existdb.properties");
		}

		Properties config = new Properties();
		// initialize configuration data
		try {
			config.load(stream);
		} catch (IOException e) {
			log.warn("could not load initial properties, trying some default values...", e);
		}
		
		if (stream != null) {
			try {
				stream.close();
			} catch (IOException e) {
				log.error("closing configuration file input stream", e);
			}
		}
		
		baseuri = config.getProperty(EXISTDB_URI_PROPERTY, "xmldb:exist://localhost:8888/exist/xmlrpc");
		basecollection = config.getProperty(EXISTDB_COLLECTION_PROPERTY, "/db/apps/p23radmin");
		adminusername = config.getProperty(EXISTDB_ADMIN_USERNAME, "");
		adminpassword = config.getProperty(EXISTDB_ADMIN_PASSWORD, "");

		resource = new P23RXmlDbResource(baseuri + basecollection + CONFIG_RESOURCE_PATH, true, adminusername, adminpassword);
	}

	/**
	 * Destroy.
	 */
	@PreDestroy
	public void destroy() {
		if (resource != null) {
			resource.free();
		}
	}

	public boolean available() {
		return resource != null && resource.exists();
	}
	
	/**
	 * Gets the base as string.
	 *
	 * @return the base as string
	 */
	public String getBaseAsString() {
		return baseuri + basecollection;
	}
	
	/**
	 * Gets the base as collection.
	 *
	 * @return the base as collection
	 */
	public Collection getBaseAsCollection() {
		try {
			return DatabaseManager.getCollection(baseuri + basecollection, adminusername, adminpassword);
		} catch (XMLDBException e) {
			log.error("getting base collection", e);
			return null;
		}
	}
	
	/**
	 * Gets the resource.
	 *
	 * @param path the path
	 * @param localResource the local resource
	 * @return the resource
	 */
	public P23RXmlDbResource getResource(String path, boolean localResource) {
		return new P23RXmlDbResource(baseuri + basecollection + path, localResource, adminusername, adminpassword);
	}

	/**
	 * Gets the single result of an xpath expression.
	 *
	 * @param xpath the xpath
	 * @param defaultValue the default value
	 * @return the string
	 */
	public String get(String xpath, String defaultValue) {
		try {
			ResourceSet set = resource.query(xpath);
			if (set != null && set.getSize() > 0) {
				return set.getResource(0).getContent().toString().trim();
			}
		} catch (XMLDBException e) {
			log.warn("reading configuration value", e);
		}
		return defaultValue;
	}

	/**
	 * Gets the results of an xquery script.
	 *
	 * @param xquery the xquery script
	 * @return all results
	 */
	public List<String> getAll(String xquery) {
		List<String> result = new ArrayList<String>();
		try {
			ResourceSet set = resource.query(xquery);
			if (set != null) {
				ResourceIterator it = set.getIterator();
				while (it.hasMoreResources()) {
					Resource res = it.nextResource();
					result.add(res.getContent().toString().trim());
				}
			}
		} catch (XMLDBException e) {
			log.warn("reading configuration value", e);
		}
		return result;
	}
	
}
