package de.p23r.common.existdb;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.IOUtils;
import org.exist.xmldb.EXistResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;
import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.Database;
import org.xmldb.api.base.Resource;
import org.xmldb.api.base.ResourceSet;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.BinaryResource;
import org.xmldb.api.modules.CollectionManagementService;
import org.xmldb.api.modules.XMLResource;
import org.xmldb.api.modules.XQueryService;

/**
 * The Class P23RXmlDbResource.
 */
public class P23RXmlDbResource {

	private static final Logger log = LoggerFactory.getLogger(P23RXmlDbResource.class);
	
	private static final String DRIVER_CLASS = "org.exist.xmldb.DatabaseImpl";

	private Collection collection;
	private String id;
	
	static {
		try {
			Database database = (Database)Class.forName(DRIVER_CLASS).newInstance();
			database.setProperty("create-database", "true");
			DatabaseManager.registerDatabase(database);
		} catch (InstantiationException e) {
			log.error("p23r xmldb resource database initialization failed", e);
		} catch (IllegalAccessException e) {
			log.error("p23r xmldb resource database initialization failed", e);
		} catch (ClassNotFoundException e) {
			log.error("p23r xmldb resource database initialization failed", e);
		} catch (XMLDBException e) {
			log.error("p23r xmldb resource parent collection initialization failed", e);
		}		
	}

	/**
	 * Instantiates a new p23r xml db resource.
	 *
	 * @param path the path
	 * @param parent the parent
	 */
	public P23RXmlDbResource(String path, Collection parent) {
		collection = parent;
		
		if (!path.endsWith("/")) {
			id = path.substring(path.lastIndexOf("/") + 1);
			path = path.substring(0, path.lastIndexOf("/") + 1);
		}
		
		String[] segments = path.split("/");
		try {
			for (String segment : segments) {
				Collection child = collection.getChildCollection(segment);
				if (child == null) {
					CollectionManagementService service = (CollectionManagementService)collection.getService("CollectionManagementService", "1.0");
					collection = service.createCollection(segment);
				} else {
					collection = child;
				}					
			}			
		} catch (XMLDBException e) {
			log.error("p23r xmldb resource parent collection initialization failed", e);
		}
	}
	
	/**
	 * Instantiates a new p23 r xml db resource.
	 *
	 * @param uri the uri
	 * @param localResource the local resource
	 */
	public P23RXmlDbResource(String uri, boolean localResource) {
		this(uri, localResource, "p23radmin", "#p23radmin!");
	}
	
	/**
	 * Instantiates a new p23r xml db resource.
	 *
	 * @param uri the uri
	 * @param localResource the local resource
	 * @param username the username
	 * @param password the password
	 */
	public P23RXmlDbResource(String uri, boolean localResource, String username, String password) {

		// TODO proof of ending '/' or no '/' at all
		id = uri.substring(uri.lastIndexOf("/") + 1);
		String path = uri.substring(0, uri.lastIndexOf("/") + 1);

		try {
			collection = DatabaseManager.getCollection(path, username, password);
			if (collection == null) {
				int index = path.indexOf("db/");
				String base = path.substring(0, index + 3);
				path = path.substring(index + 3);
				
				collection = DatabaseManager.getCollection(base, username, password);

				String [] segments = path.split("/");
				for (String segment : segments) {
					Collection child = collection.getChildCollection(segment);
					if (child == null) {
						CollectionManagementService service = (CollectionManagementService)collection.getService("CollectionManagementService", "1.0");
						collection = service.createCollection(segment);
					} else {
						collection = child;
					}					
				}
			}
			init(localResource);
		} catch (XMLDBException e) {
			log.error("p23r xmldb resource parent collection initialization failed", e);
		}
	}
	
	/**
	 * Instantiates a new p23r xml db resource.
	 *
	 * @param id the id
	 * @param collection the collection
	 * @param localResource the local resource
	 */
	public P23RXmlDbResource(String id, Collection collection, boolean localResource) {
		this.id = id;
		this.collection = collection;
		init(localResource);
	}

	private void init(boolean localResource) {
		Resource resource = null;
		try {
			resource = collection.getResource(id);
			if (resource == null) {
				if (localResource) {
					InputStream input = Thread.currentThread().getContextClassLoader().getResourceAsStream(id);
					if (input != null) {
						if (id.endsWith(".xslt") || id.endsWith(".xml") || id.endsWith(".xsd")) {
				        	resource = collection.createResource(id, "XMLResource");
				    		TransformerFactory factory = TransformerFactory.newInstance();
				    		DOMResult result = new DOMResult();
				    		try {
				    			Transformer transformer = factory.newTransformer();
				    	        transformer.transform(new StreamSource(input), result);
				    	        ((XMLResource)resource).setContentAsDOM(result.getNode());
				    		} catch (TransformerConfigurationException e) {
				    			log.error("transforming source to elment", e);
				    		} catch (TransformerException e) {
				    			log.error("transforming source to elment", e);
				    		}							
						} else {
				        	resource = collection.createResource(id, "BinaryResource");
				        	try {
								resource.setContent(IOUtils.toByteArray(input));
							} catch (IOException e) {
								log.error("reading binary content", e);
							}
						}
						collection.storeResource(resource);						
					} else {
						log.error("no local resource for initialization available");
					}
				}
			}
		} catch (XMLDBException e) {
			log.error("initialization of p23r xmldb resource failed", e);
		} finally {
			if (resource != null) {
				try {
					((EXistResource)resource).freeResources();
				} catch (XMLDBException e) {
					log.error("freeing resource", e);
				}
			}
		}		
	}
	
	/**
	 * Checks if the resource exists in the eXist db.
	 *
	 * @return true, if successful
	 */
	public boolean exists() {
		if (collection == null) {
			throw new IllegalStateException();
		}
		Resource resource = null;
		try {
			resource = collection.getResource(id);
			return resource != null;
		} catch (XMLDBException e) {
			log.error("p23r xmldb check resource exist", e);
		} finally {
			if (resource != null) {
				try {
					((EXistResource)resource).freeResources();
				} catch (XMLDBException e) {
					log.error("p23r xmldb freeing resource", e);
				}
			}
		}
		return false;
	}
	
	/**
	 * Gets the eXist db id of the resource.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Gets the parent collection.
	 *
	 * @return the parent collection
	 */
	public Collection getParentCollection() {
		return collection;
	}
	
	/**
	 * Gets the content as byte array.
	 *
	 * @return the content as byte array
	 */
	protected byte[] getContentAsByteArray() {
		if (collection == null) {
			throw new IllegalStateException();
		}
		Resource resource = null;
		try {
			resource = collection.getResource(id);
			if (resource.getResourceType().equals("BinaryResource")) {
				return (byte[])resource.getContent();
			}
		} catch (XMLDBException e) {
			log.error("p23r xmldb resource " + id + " getting content as byte array failed", e);
		} finally {
			if (resource != null) {
				try {
					((EXistResource)resource).freeResources();
				} catch (XMLDBException e) {
					log.error("freeing resource", e);
				}
			}
		}
		return null;		
	}
	
	/**
	 * Gets the content as string.
	 *
	 * @return the content as string
	 */
	protected String getContentAsString() {
		if (collection == null) {
			throw new IllegalStateException();
		}
		Resource resource = null;
		try {
			resource = collection.getResource(id);
			if (resource.getResourceType().equals("XMLResource")) {
				return ((XMLResource)resource).getContent().toString();
			} else {
				return new String((byte[])resource.getContent(), Charset.forName("UTF-8"));
			}
		} catch (XMLDBException e) {
			log.error("p23r xmldb resource " + id + " getting content as string failed", e);
		} finally {
			if (resource != null) {
				try {
					((EXistResource)resource).freeResources();
				} catch (XMLDBException e) {
					log.error("freeing resource", e);
				}
			}
		}
		return null;		
	}
	
	/**
	 * Gets the content as node.
	 *
	 * @return the content as node
	 */
	protected Node getContentAsNode() {
		if (collection == null) {
			throw new IllegalStateException();
		}
		XMLResource resource = null;
		try {
			resource = (XMLResource)collection.getResource(id);
			return resource.getContentAsDOM();
		} catch (XMLDBException e) {
			log.error("p23r xmldb resource " + id + " getting content as node failed", e);
		} finally {
			if (resource != null) {
				try {
					((EXistResource)resource).freeResources();
				} catch (XMLDBException e) {
					log.error("freeing resource", e);
				}
			}
		}
		return null;		
	}
	
	/**
	 * Query the reosurce.
	 *
	 * @param xquery the xquery
	 * @return the resource set
	 */
	public ResourceSet query(String xquery) {
		return query(xquery, null, null);
	}
	
	/**
	 * Query the resource.
	 *
	 * @param xquery the xquery
	 * @param namespace the namespace
	 * @param variables the variables
	 * @return the resource set
	 */
	public ResourceSet query(String xquery, String namespace, Map<String, ?> variables) {
		if (collection == null) {
			throw new IllegalStateException();
		}
		try {
			XQueryService service = (XQueryService)collection.getService("XQueryService", "1.0");

			if (variables != null) {
				for (Entry<String, ?> entry : variables.entrySet()) {
					service.declareVariable(entry.getKey(), entry.getValue());					
				}
			}

			if (namespace != null) {
				service.setNamespace("", namespace);				
			}
			
			return service.queryResource(id,  xquery);
		} catch (XMLDBException e) {
			log.error("p23r xmldb resource " + id + " applying a query statement failed", e);
		}
		return null;
	}
	
	/**
	 * Sets the content.
	 *
	 * @param content the content
	 * @return true, if successful
	 */
	public boolean setContent(String content) {
		if (collection == null) {
			throw new IllegalStateException();
		}

		XMLResource resource = null;
		try {
			resource = (XMLResource)collection.getResource(id);
			if (resource == null) {
				resource = (XMLResource)collection.createResource(id, "XMLResource");
			}
			if (!content.isEmpty()) {
				resource.setContent(content);								
			} else {
				log.warn("empty resource: {}", resource.getId());
			}
			
			collection.storeResource(resource);
		} catch (XMLDBException e) {
			log.error("p23r xmldb resource " + id + " setting content (string) failed", e);
			return false;
		} finally {
			if (resource != null) {
				try {
					((EXistResource)resource).freeResources();
				} catch (XMLDBException e) {
					log.error("freeing resource", e);
				}
			}
		}
		
		return true;
	}

	/**
	 * Sets the content.
	 *
	 * @param content the content
	 * @return true, if successful
	 */
	public boolean setContent(Node content) {
		if (collection == null) {
			throw new IllegalStateException();
		}

		XMLResource resource = null;
		try {
			resource = (XMLResource)collection.getResource(id);
			if (resource == null) {
				resource = (XMLResource)collection.createResource(id, "XMLResource");
			}
			resource.setContentAsDOM(content);
			collection.storeResource(resource);
		} catch (XMLDBException e) {
			log.error("p23r xmldb resource " + id + " setting content (node) failed", e);
			return false;
		} finally {
			if (resource != null) {
				try {
					((EXistResource)resource).freeResources();
				} catch (XMLDBException e) {
					log.error("freeing resource", e);
				}
			}
		}
		
		return true;
	}

	/**
	 * Sets the content.
	 *
	 * @param content the content
	 * @return true, if successful
	 */
	public boolean setContent(File content) {
		if (collection == null) {
			throw new IllegalStateException();
		}

		XMLResource resource = null;
		try {
			resource = (XMLResource)collection.getResource(id);
			if (resource == null) {
				resource = (XMLResource)collection.createResource(id, "XMLResource");
			}
			resource.setContent(content);
			collection.storeResource(resource);
		} catch (XMLDBException e) {
			log.error("p23r xmldb resource " + id + " setting content (file) failed", e);
			return false;
		} finally {
			if (resource != null) {
				try {
					((EXistResource)resource).freeResources();
				} catch (XMLDBException e) {
					log.error("freeing resource", e);
				}
			}
		}
		
		return true;
	}

	/**
	 * Sets the binary content.
	 *
	 * @param content the content
	 * @return true, if successful
	 */
	public boolean setBinaryContent(byte[] content) {
		if (collection == null) {
			throw new IllegalStateException();
		}

		BinaryResource resource = null;
		try {
			resource = (BinaryResource)collection.getResource(id);
			if (resource == null) {
				resource = (BinaryResource)collection.createResource(id, "BinaryResource");
			}
			resource.setContent(content);
			collection.storeResource(resource);
		} catch (XMLDBException e) {
			log.error("p23r xmldb resource " + id + " setting content (binary) failed", e);
			return false;
		} finally {
			if (resource != null) {
				try {
					((EXistResource)resource).freeResources();
				} catch (XMLDBException e) {
					log.error("freeing resource", e);
				}
			}
		}
		
		return true;
	}

	/**
	 * Removes the resource from the eXist db.
	 */
	public void remove() {
		if (collection == null) {
			throw new IllegalStateException();
		}

		try {
			XMLResource resource = (XMLResource)collection.getResource(id);
			if (resource != null) {
				collection.removeResource(resource);				
			}
		} catch (XMLDBException e) {
			log.error("p23r xmldb removing resource " + id + " failed", e);
		}			
		free();
	}
	
	/**
	 * Closes the collection and frees all resources.
	 */
	public void free() {
		if (collection != null) {
			try {
				if (collection.isOpen()) {
					collection.close();					
				}
			} catch (XMLDBException e) {
				log.error("closing collection failed", e);
			}
		}
	}

}
