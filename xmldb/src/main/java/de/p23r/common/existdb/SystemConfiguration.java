package de.p23r.common.existdb;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmldb.api.base.ResourceSet;
import org.xmldb.api.base.XMLDBException;

/**
 * The Class SystemConfiguration.
 */
@ApplicationScoped
public class SystemConfiguration {

	private static final String NAMESPACE_CONFIGURATION = "http://leitstelle.p23r.de/NS/p23r/nrl/Configuration1-0";

	/**
	 * The Constant P23RID.
	 */
	public static final String P23RID = "string(/configuration/system/@id)";
	
	private final Logger log = LoggerFactory.getLogger(getClass());
	
	private P23RXmlDbResource configurations;

	@Inject
	private P23RXmlDbConfig p23rXmlDbConfig;
	
	/**
	 * Initiates this application scoped bean.
	 */
	@PostConstruct
	public void init() {
		configurations = p23rXmlDbConfig.getResource("/server/configurations.xml", true);
	}
	
	/**
	 * Gets the p23r id.
	 *
	 * @return the p23r id
	 */
	public String getP23RId() {
		return query("string(/configuration/system/@id)", null);
	}
	
	/**
	 * Sets the p23 r id.
	 *
	 * @param id the new p23 r id
	 */
	public void setP23RId(String id) {
		query("update value /configuration/system/@id with '" + id + "'", "");
	}
	
	/**
	 * Gets the product.
	 *
	 * @return the product
	 */
	public String getProduct() {
		return query("string(/configuration/system/@product)", null);
	}
	
	/**
	 * Gets the product release.
	 *
	 * @return the product release
	 */
	public String getProductRelease() {
		return query("string(/configuration/system/@productRelease)", null);
	}
	
	/**
	 * Gets the vendor.
	 *
	 * @return the vendor
	 */
	public String getVendor() {
		return query("string(/configuration/system/@vendor)", null);
	}
	
	/**
	 * Gets the preferred language.
	 *
	 * @return the preferred language
	 */
	public String getPreferredLanguage() {
		return query("string(/configuration/system/@language)", null);
	}
	
	/**
	 * Gets the provider.
	 *
	 * @return the provider
	 */
	public String getProvider() {
		return query("string(/configuration/system/@provider)", null);
	}
	
	/**
	 * Update.
	 *
	 * @param newconfig the newconfig
	 */
	public void update(String newconfig) {
		query("update replace /configuration/system with " + newconfig, "");
	}

	/**
	 * Query.
	 *
	 * @param xquery the xquery
	 * @param defaultValue the default value
	 * @return the string
	 */
	public String query(String xquery, String defaultValue) {
		try {
			ResourceSet set = configurations.query(xquery, NAMESPACE_CONFIGURATION, null);
			if (set != null && set.getSize() > 0) {
				return set.getResource(0).getContent().toString().trim();
			}
		} catch (XMLDBException e) {
			log.warn("query p23r system configuration", e);
		}
		return defaultValue;		
	}

}
