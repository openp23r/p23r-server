package de.p23r.common.existdb;

import org.w3c.dom.Node;

/**
 * The Class P23RXmlDbResourceContentAccess.
 */
public class P23RXmlDbResourceContentAccess {

	/**
	 * Gets the content as string.
	 *
	 * @param resource the resource
	 * @return the content as string
	 */
	public static String getContentAsString(P23RXmlDbResource resource) {
		return resource.getContentAsString();
	}

	/**
	 * Gets the content as node.
	 *
	 * @param resource the resource
	 * @return the content as node
	 */
	public static Node getContentAsNode(P23RXmlDbResource resource) {
		return resource.getContentAsNode();		
	}
	
	/**
	 * Gets the content as node.
	 *
	 * @param resource the resource
	 * @return the content as node
	 */
	public static byte[] getContentAsByteArray(P23RXmlDbResource resource) {
		return resource.getContentAsByteArray();
	}
	
}
