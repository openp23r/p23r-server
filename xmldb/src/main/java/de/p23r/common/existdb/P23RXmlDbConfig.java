package de.p23r.common.existdb;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.Resource;
import org.xmldb.api.base.ResourceIterator;
import org.xmldb.api.base.ResourceSet;
import org.xmldb.api.base.XMLDBException;

import de.p23r.common.existdb.P23RXmlDbResource;

/**
 * The Class P23RXmlDbConfig.
 */
@ApplicationScoped
public class P23RXmlDbConfig {
	
	private static final String EXISTDB_BASEURI_ENV = "P23R_EXISTDB_BASEURI";
	private static final String EXISTDB_BASECOLLECTION_ENV = "P23R_EXISTDB_BASECOLLECTION";
	private static final String EXISTDB_AUTH_USERNAME_ENV = "P23R_EXISTDB_AUTH_USERNAME";
	private static final String EXISTDB_AUTH_PASSWORD_ENV = "P23R_EXISTDB_AUTH_PASSWORD";

	private static final String EXISTDB_URI_PROPERTY = "base.uri";
	private static final String EXISTDB_COLLECTION_PROPERTY = "base.collection";
	private static final String EXISTDB_ADMIN_USERNAME = "authentication.admin.username";
	private static final String EXISTDB_ADMIN_PASSWORD = "authentication.admin.password";
	
	private static final String SERVERCONFIG_RESOURCE_PATH = "/runtime/serverconfig.xml";

	/**
	 * The Constant XQUERY_GENERAL_SELECTIONMODE.
	 */
	public static final String XQUERY_GENERAL_SELECTIONMODE = "/configuration/general/selection/@mode/string()";
	
	/**
	 * The Constant XQUERY_GENERAL_AUTOAPPROVE.
	 */
	public static final String XQUERY_GENERAL_AUTOAPPROVAL = "/configuration/general/autoApproval/text()";
	
	/**
	 * The Constant XQUERY_GENERAL_SELECTIONMODE.
	 */
	public static final String XQUERY_GENERAL_SELECTIONCOMPILER = "/configuration/general/selection/compiler/external/text()";
	
	/**
	 * The Constant XQUERY_GENERAL_TRACEENABLED.
	 */
	public static final String XQUERY_GENERAL_TRACEENABLED = "/configuration/general/traceEnabled/text()";
	
	/**
	 * The Constant XQUERY_GENERAL_AUTOUPDATECCSDISABLED.
	 */
	public static final String XQUERY_GENERAL_AUTOUPDATECCSDISABLED = "/configuration/general/autoupdateDisabled/text()";
	
	/**
	 * The Constant XQUERY_GENERAL_VERIFICATIONDISABLED.
	 */
	public static final String XQUERY_GENERAL_VERIFICATIONDISABLED = "/configuration/general/verificationDisabled/text()";

	/**
	 * The Constant XQUERY_CCS.
	 */
	public static final String XQUERY_CCS = "/configuration/controlcentres/controlcentre/@id/string()";

	/**
	 * The Constant XQUERY_USERNAME.
	 */
	public static final String XQUERY_USERNAME = "/configuration/controlcentres/controlcentre[@id=\"%s\"]/auth/username/text()";
	
	/**
	 * The Constant XQUERY_PASSWORD.
	 */
	public static final String XQUERY_PASSWORD = "/configuration/controlcentres/controlcentre[@id=\"%s\"]/auth/password/text()";

	/**
	 * The Constant XQUERY_TSL_URL.
	 */
	public static final String XQUERY_TSL_URL = "/configuration/controlcentres/controlcentre[@id=\"%s\"]/tsl/url/text()";
	
	/**
	 * The Constant XQUERY_MODELANDRULEDEPOT_TSLSERVICENAME.
	 */
	public static final String XQUERY_MODELANDRULEDEPOT_TSLSERVICENAME = "/configuration/controlcentres/controlcentre[@id=\"%s\"]/modelandruledepot/@tlsservice/string()";
	
	/**
	 * The Constant XQUERY_NOTIFICATIONRECEIVERQUERY_TSLSERVICENAME.
	 */
	public static final String XQUERY_NOTIFICATIONRECEIVERQUERY_TSLSERVICENAME = "/configuration/controlcentres/controlcentre[@id=\"%s\"]/notificationreceiverquery/@tlsservice/string()";
	
	/**
	 * The Constant XQUERY_NOTIFICATIONRECEIVERQUERY_SERVICE.
	 */
	public static final String XQUERY_NOTIFICATIONRECEIVERQUERY_SERVICE = "/configuration/controlcentres/controlcentre[@id=\"%s\"]/notificationreceiverquery/endpoint/service/text()";
	
	/**
	 * The Constant XQUERY_ADDRESSQUERY_TSLSERVICENAME.
	 */
	public static final String XQUERY_ADDRESSQUERY_TSLSERVICENAME = "/configuration/controlcentres/controlcentre[@id=\"%s\"]/addressquery/@tlsservice/string()";
	
	/**
	 * The Constant XQUERY_ADDRESSQUERY_SERVICE.
	 */
	public static final String XQUERY_ADDRESSQUERY_SERVICE = "/configuration/controlcentres/controlcentre[@id=\"%s\"]/addressquery/endpoint/service/text()";

	/**
	 * The Constant XQUERY_CLIENTCONFIG.
	 */
	public static final String XQUERY_CLIENTCONFIG = "/configuration/clients/client[@tenant=\"%s\"]/@tenant/string()";

	/**
	 * The Constant XQUERY_NAMESPACES.
	 */
	public static final String XQUERY_NAMESPACES = "/configuration/clients/client[@tenant=\"%s\"]/connector/@namespace/string()";

	/**
	 * The Constant XQUERY_APPROVALNOTIFY_ADDRESS.
	 */
	public static final String XQUERY_APPROVALNOTIFY_ADDRESS = "/configuration/clients/client[@tenant=\"%s\"]/approvalnotify/endpoint/address/text()";
	
	/**
	 * The Constant XQUERY_APPROVALNOTIFY_SERVICE.
	 */
	public static final String XQUERY_APPROVALNOTIFY_SERVICE = "/configuration/clients/client[@tenant=\"%s\"]/approvalnotify/endpoint/service/text()";

	private final Logger log = LoggerFactory.getLogger(getClass());

	private String baseuri;
	private String basecollection;
	private String adminusername;
	private String adminpassword;
	
	private P23RXmlDbResource resource;
	
	private static final MessageFormat XUPDATE_INSERT = new MessageFormat(
			"if (exists({0})) "
			+ "then update replace {0} with {1} "
			+ "else update insert {1} into {2}");

	@PostConstruct
	private void init() {
		// first try to fetch from env
		baseuri = System.getenv(EXISTDB_BASEURI_ENV);
		basecollection = System.getenv(EXISTDB_BASECOLLECTION_ENV);
		adminusername = System.getenv(EXISTDB_AUTH_USERNAME_ENV);
		adminpassword = System.getenv(EXISTDB_AUTH_PASSWORD_ENV);

		InputStream stream = null;

		String filename = System.getenv("P23R_EXISTDB_PROPERTIES");			
		if (filename != null) {
			Path filePath = Paths.get(URI.create(filename));

			if (Files.exists(filePath)) {
				try {
					stream = Files.newInputStream(filePath);
				} catch (IOException e) {
					log.error("opening existdb properties file " + filename, e);
				}
			}			
		}
		
		if (stream == null) {
			String userHome = System.getProperty("user.home");
			
//			filename = "file:/" + userHome + FileSystems.getDefault().getSeparator() + ".p23r" + FileSystems.getDefault().getSeparator() + "existdb.properties";
//			Path filePath = Paths.get(URI.create(filename));
			Path filePath = FileSystems.getDefault().getPath(userHome, ".p23r", "existdb.properties");
			if (Files.exists(filePath)) {
				try {
					stream = Files.newInputStream(filePath);
				} catch (IOException e) {
					log.error("opening existdb properties file " + filename, e);
				}
			}			
		}

		if (stream == null) {
			stream = Thread.currentThread().getContextClassLoader().getResourceAsStream("existdb.properties");
		}
		
		if (stream != null) {
			Properties config = new Properties();
			// initialize configuration data
			try {
				config.load(stream);
			} catch (IOException e) {
				log.warn("could not load initial properties, trying some default values...", e);
			}

			if (baseuri == null) {
				baseuri = config.getProperty(EXISTDB_URI_PROPERTY, "xmldb:exist://localhost:8888/exist/xmlrpc");
			}
			if (basecollection == null) {
				basecollection = config.getProperty(EXISTDB_COLLECTION_PROPERTY, "/db/apps/p23radmin");				
			}
			if (adminusername == null) {
				adminusername = config.getProperty(EXISTDB_ADMIN_USERNAME, "");				
			}
			if (adminpassword == null) {
				adminpassword = config.getProperty(EXISTDB_ADMIN_PASSWORD, "");				
			}

			try {
				stream.close();
			} catch (IOException e) {
				log.error("closing configuration file input stream", e);
			}
		} else {
			log.warn("could not determine exist db properties file...");
		}
		
		if (baseuri == null) {
			baseuri = "xmldb:exist://localhost:8888/exist/xmlrpc";
		}
		if (basecollection == null) {
			basecollection = "/db/apps/p23radmin";						
		}
		if (adminusername == null) {
			adminusername = "";						
		}
		if (adminpassword == null) {
			adminpassword = "";						
		}
		log.info("Using eXist-db configuration {}, credentials: {} : {}", baseuri + basecollection + SERVERCONFIG_RESOURCE_PATH, adminusername, adminpassword);
		resource = new P23RXmlDbResource(baseuri + basecollection + SERVERCONFIG_RESOURCE_PATH, true, adminusername, adminpassword);
	}

	/**
	 * Destroy.
	 */
	@PreDestroy
	public void destroy() {
		if (resource != null) {
			resource.free();
		}
	}

	/**
	 * Update.
	 *
	 * @param tag the tag
	 * @param value the value
	 */
	public void update(String tag, String value) {
		String[] fragments = tag.split("/");
		String parent = fragments[fragments.length];
		String xquery = XUPDATE_INSERT.format(new Object[] { tag, value, parent });
		resource.query(xquery);
	}

	/**
	 * Gets the base as string.
	 *
	 * @return the base as string
	 */
	public String getBaseAsString() {
		return baseuri + basecollection;
	}
	
	/**
	 * Gets the base as collection.
	 *
	 * @return the base as collection
	 */
	public Collection getBaseAsCollection() {
		try {
			return DatabaseManager.getCollection(baseuri + basecollection, adminusername, adminpassword);
		} catch (XMLDBException e) {
			log.error("getting base collection", e);
			return null;
		}
	}
	
	/**
	 * Gets the resource.
	 *
	 * @param path the path
	 * @param localResource the local resource
	 * @return the resource
	 */
	public P23RXmlDbResource getResource(String path, boolean localResource) {
		return new P23RXmlDbResource(baseuri + basecollection + path, localResource, adminusername, adminpassword);
	}

	/**
	 * Gets the.
	 *
	 * @param xpath the xpath
	 * @param defaultValue the default value
	 * @return the string
	 */
	public String get(String xpath, String defaultValue) {
		try {
			ResourceSet set = resource.query(xpath);
			if (set != null && set.getSize() > 0) {
				return set.getResource(0).getContent().toString().trim();
			}
		} catch (XMLDBException e) {
			log.warn("reading configuration value", e);
		}
		return defaultValue;
	}

	/**
	 * Gets the all.
	 *
	 * @param xquery the xquery
	 * @return the all
	 */
	public List<String> getAll(String xquery) {
		List<String> result = new ArrayList<String>();
		try {
			ResourceSet set = resource.query(xquery);
			if (set != null) {
				ResourceIterator it = set.getIterator();
				while (it.hasMoreResources()) {
					Resource res = it.nextResource();
					result.add(res.getContent().toString().trim());
				}
			}
		} catch (XMLDBException e) {
			log.warn("reading configuration value", e);
		}
		return result;
	}

	/**
	 * Gets the collection.
	 *
	 * @param path the path
	 * @return the collection
	 */
	public Collection getCollection(String path) {
		try {
			return DatabaseManager.getCollection(baseuri + path, adminusername, adminpassword);
		} catch (XMLDBException e) {
			log.error("geting collection", e);
		}
		return null;
	}

	/**
	 * Gets the client config.
	 *
	 * @param tenant the tenant
	 * @return the client config
	 */
	public String getClientConfig(String tenant) {
		return get(String.format(XQUERY_CLIENTCONFIG, tenant), "default");
	}
}
